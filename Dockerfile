# Maven build container
#FROM maven:3.5.2-jdk-8-alpine AS maven_build
FROM hub.vnpt.vn/tablet-checkin/3rd.ic.checkin/maven_spring:3.6.2-jdk-8_2.1.18 AS maven_build
# RUN echo 10.159.12.175 crelease.devops.vnpt.vn > /etc/hosts; cat /etc/hosts
# RUN cat /etc/hosts
#COPY settings.xml /root/.m2/
# RUN ping -c 4 crelease.devops.vnpt.vn
WORKDIR /tmp/
COPY ojdbc7-12.1.0.2.jar /tmp/
RUN mvn install:install-file -Dfile=ojdbc7-12.1.0.2.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.2 -Dpackaging=jar
#RUN mvn -X deploy:deploy-file -Durl=file:///tmp/repo/ -Dfile=/tmp/ojdbc7-12.1.0.2.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.2 -Dpackaging=jar
#RUN mvn install:install-file -Dfile=/tmp/ojdbc7-12.1.0.2.jar -DpomFile=/tmp/pom.xml
COPY pom.xml /tmp/
#RUN mvn dependency:go-offline
COPY src /tmp/src/
RUN mvn package -Dmaven.test.skip=true


#pull base image
FROM hub.vnpt.vn/tablet-checkin/3rd.ic.checkin/openjdk:8u242-jdk-slim
EXPOSE 80
ENV JAVA_OPTS="-Xms3072m -Xmx4096m -XX:PermSize=512m -XX:MaxPermSize=1024m"
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
COPY --from=maven_build /tmp/target/*.jar /app.jar
