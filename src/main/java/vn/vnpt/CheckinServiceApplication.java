package vn.vnpt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.security.Principal;
import java.util.TimeZone;

/**
 * @author trananh
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableResourceServer
@RestController
@EnableSwagger2
@EnableEurekaClient
@EnableKafka
@EnableCaching // enables Spring Caching functionality
@EnableScheduling
public class CheckinServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckinServiceApplication.class, args);
	}

	@PostConstruct
	public void init() {
		// Setting Spring Boot SetTimeZone
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
	}

	@RequestMapping("/account_user")
	public Principal user(Principal user) {
		return user;
	}

	@LoadBalanced
	@Bean
	RestTemplate loadBalanced() {
		return new RestTemplate();
	}

	@Primary
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	/**
	 * @return
	 */
	ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Account REST CRUD operations API")
				.description("Account REST CRUD operations API").termsOfServiceUrl("").version("0.0.1")
				.contact(new Contact("Anhtcn", "https://idg.vnpt.vn", "https://idg.vnpt.vn")).build();
	}

	@Bean
	public Docket configureControllerPackageAndConvertors() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("vn.vnpt.api"))
				.build().apiInfo(apiInfo());
	}

//	@Bean
//	public FilterRegistrationBean<CorsFilter> customCorsFilter() {
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		CorsConfiguration config = new CorsConfiguration();
//		config.setAllowCredentials(true);
//		config.addAllowedOrigin("*");
//		config.addAllowedHeader("*");
//		config.addAllowedMethod("*");
//		source.registerCorsConfiguration("/**", config);
//		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
//
//		// IMPORTANT #2: I didn't stress enough the importance of this line in my
//		// original answer,
//		// but it's here where we tell Spring to load this filter at the right point in
//		// the chain
//		// (with an order of precedence higher than oauth2's filters)
//		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
//		return bean;
//	}

}
