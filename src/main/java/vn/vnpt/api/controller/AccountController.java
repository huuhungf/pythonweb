package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.service.AccountService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/account", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(AccountController.class);

	private final AccountService accountService;

	@Autowired
	protected AccountController(ResponseEntites<Object> responseEntites, AccountService accountService) {
		super(responseEntites);
		this.accountService = accountService;
	}

	@PostMapping(value = "/login")
	public DeferredResult<ResponseEntity<?>> login(@Valid @RequestBody LoginDtoIn loginDtoIn) {
		logger.info(String.format("Started processing asynchronous request web login %s", loginDtoIn.getUsername()));

		return responseEntityDeferredResult(() -> {
			OAuth2AccessToken accessToken = accountService.login(loginDtoIn);
			logger.info(String.format("Handle success request web login %s", loginDtoIn.getUsername()));
			return accessToken;
		});
	}

	@GetMapping("/list-filter")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER', 'USER')")
	public DeferredResult<ResponseEntity<?>> accountListPagingFilter(WebListAccountDtoIn webListAccountDtoIn,
																	 @Valid PagingDtoIn pageable) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing asynchronous request web list account: %s, %s",
				webListAccountDtoIn, pageable));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			PagingDTO<WebAccountDtoOut> pagingDTO = accountService.webListAccount(webListAccountDtoIn, pageable);
			logger.info(String.format("Handle success request web list account %s, %s: ",
					webListAccountDtoIn, pageable));
			return pagingDTO;
		});
	}

	@PostMapping(value = "", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> addAccount(@Valid WebAccountDtoIn accountDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String adminUsername = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> roles = authentication.getAuthorities();
		String role = roles.iterator().next().getAuthority();

		logger.info(String.format("Received async request web add account: %s %s", adminUsername, accountDtoIn.getUserCode()));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			WebAccountDtoOut accountDtoOut = accountService.createAccount(accountDtoIn);

			logger.info(String.format("Handle success web add account: %s", accountDtoOut.getUsername()));
			return accountDtoOut;
		});
	}

	@PostMapping(value = "/update", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> updateAccount(@Valid WebUpdateAccountDtoIn webUpdateAccountDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String adminUsername = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		logger.info(String.format("Started processing asynchronous request web update account: %s, %s", adminUsername,
				webUpdateAccountDtoIn.getUuidAccount()));

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			WebAccountDtoOut accountDtoOut = accountService.updateAccount(webUpdateAccountDtoIn);
			logger.info(String.format("Handle success request web update account: %s, %s", adminUsername, accountDtoOut.getUuidAccount()));

			return accountDtoOut;
		});
	}

	@DeleteMapping(value = "/{uuidAccount}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> deleteAccount(@PathVariable(value = "uuidAccount") String uuidAccount) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String adminUsername = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> roles = authentication.getAuthorities();
		String role = roles.iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request web delete account: %s %s", adminUsername, uuidAccount));

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			accountService.deleteAccount(uuidAccount);
			logger.info(String.format("Handle success request web delete account: %s %s", adminUsername, uuidAccount));

			return new HashMap<>();
		});
	}

	@PutMapping(value = "/reset-password")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> adminResetPassword(@Valid @RequestBody ResetPasswordDtoIn resetPasswordDtoIn) {
		logger.info(String.format("Started processing asynchronous request web reset password: %s", resetPasswordDtoIn.getUuidAccount()));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			accountService.resetPassword(resetPasswordDtoIn);
			logger.info(String.format("Handle success request web reset password: %s", resetPasswordDtoIn.getUuidAccount()));

			return new HashMap<>();
		});
	}

	@GetMapping(value = "/profile")
	public DeferredResult<ResponseEntity<?>> getAccountProfile() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		String role = authorities.iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request web get account profile: %s, %s", username, role));

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			AccountProfile accountProfile = accountService.getAccountProfile(username, role);
			logger.info(String.format("Handle success request web get account profile: %s, %s", username, role));

			return accountProfile;
		});
	}

	@PutMapping(value = "/change-password")
	public DeferredResult<ResponseEntity<?>> changePassword(@Valid @RequestBody ChangePasswordDtoIn changePasswordDtoIn) {
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		logger.info(String.format("Started processing asynchronous request web change password: %s", username));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			accountService.changePassword(username, changePasswordDtoIn);
			logger.info(String.format("Handle success request web change password: %s", username));

			return new HashMap<>();
		});
	}

	@GetMapping(value = "/{uuidAccount}")
	@PreAuthorize(value = "hasAnyAuthority('SUPERADMIN', 'ADMIN', 'OWNER', 'USER')")
	public DeferredResult<ResponseEntity<?>> getAccount(
			@PathVariable(value = "uuidAccount") String uuidAccount
	) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String adminUsername = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		String role = authorities.iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request web get account %s, %s",
				adminUsername, uuidAccount));

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			AccountProfileDtoOut accountProfile = accountService.getAccount(adminUsername, role, uuidAccount);
			logger.info(String.format("Handle success request web get account: %s, %s", adminUsername, accountProfile.getUuidAccount()));

			return accountProfile;
		});
	}

	@PostMapping(value = "/create-forgot-password")
	public DeferredResult<ResponseEntity<?>> createForgotPassword(@Valid @RequestBody CreateForgotPasswordDtoIn createForgotPasswordDtoIn) {
		logger.info(String.format("Started processing asynchronous request web create forgot password: %s", createForgotPasswordDtoIn));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			accountService.createForgotPassword(createForgotPasswordDtoIn);
			logger.info(String.format("Handle success request web create forgot password: %s", createForgotPasswordDtoIn));

			return new HashMap<>();
		});
	}

	@PostMapping(value = "/confirm-forgot-password")
	public DeferredResult<ResponseEntity<?>> confirmForgotPassword(@Valid @RequestBody ConfirmForgotPasswordDtoIn confirmForgotPasswordDtoIn) {
		logger.info(String.format("Started processing asynchronous request web confirm forgot password: %s", confirmForgotPasswordDtoIn.getUsername()));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			accountService.confirmForgotPassword(confirmForgotPasswordDtoIn);
			logger.info(String.format("Handle success request web confirm forgot password: %s", confirmForgotPasswordDtoIn.getUsername()));

			return new HashMap<>();
		});
	}

	@PostMapping(value = "/list")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> createListAccount(@Valid @RequestBody WebCreateListAccountDtoIn listAccountDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		String role = authorities.iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request web create list account: %s, %s, %s", username,
				role, listAccountDtoIn.getAccounts().size()));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			WebCreateListAccountDtoOut listAccountDtoOut = accountService.createListAccount(listAccountDtoIn);
			logger.info(String.format("Handle request web create list account: %s, %s, %s, %s", username, role,
					listAccountDtoIn.getAccounts().size(), listAccountDtoOut.getErrors().size()));
			return listAccountDtoOut;
		});
	}

	@PostMapping(value = "/stranger")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'ADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> createAccountFromStranger(@Valid @RequestBody WebAccountFromStrangerDtoIn webAccountFromStrangerDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		String role = authorities.iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request web create account from stranger: %s, %s, %s", username,
				role, webAccountFromStrangerDtoIn.getUuidStranger()));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			WebAccountDtoOut accountDtoOut = accountService.createAccountFromStranger(webAccountFromStrangerDtoIn);
			logger.info(String.format("Handle request web create account from stranger: %s, %s, %s", username, role,
					webAccountFromStrangerDtoIn.getUuidStranger()));
			return accountDtoOut;
		});
	}

	@PutMapping(value = "/stranger")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> updateAccountFromStranger(@Valid @RequestBody WebUpdateAccountFromStrangerDtoIn updateAccountFromStrangerDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		String role = authorities.iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request web update account from stranger: %s, %s, %s", username,
				role, updateAccountFromStrangerDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			WebAccountDtoOut accountDtoOut = accountService.updateAccountFromStranger(username, role, updateAccountFromStrangerDtoIn);
			logger.info(String.format("Handle request web update account from stranger: %s, %s, %s", username, role,
					updateAccountFromStrangerDtoIn));
			return accountDtoOut;
		});
	}

	@PostMapping(value = "/register")
	public DeferredResult<ResponseEntity<?>> register(@Valid @RequestBody WebRegisterDtoIn webRegisterDtoIn) {
		logger.info(String.format("Started processing asynchronous request web register account: %s, %s", webRegisterDtoIn.getUsername(), webRegisterDtoIn.getEmail()));

		return responseEntityDeferredResult(() -> {
			WebAccountDtoOut accountDtoOut = accountService.register(webRegisterDtoIn);
			logger.info(String.format("Handle request web register account: %s, %s", webRegisterDtoIn.getUsername(), webRegisterDtoIn.getEmail()));
			return accountDtoOut;
		});
	}

	@PostMapping(value = "/confirm-register")
	public DeferredResult<ResponseEntity<?>> confirmRegister(@Valid @RequestBody WebConfirmRegisterDtoIn webConfirmRegisterDtoIn) {
		logger.info(String.format("Started processing asynchronous request web confirm register: %s", webConfirmRegisterDtoIn.getEmail()));

		return responseEntityDeferredResult(() -> {
			accountService.confirmRegister(webConfirmRegisterDtoIn);
			logger.info(String.format("Handle request web confirm register: %s", webConfirmRegisterDtoIn.getEmail()));
			return new HashMap<>();
		});
	}

	@GetMapping(value = "/exist")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> checkAccountExist(@RequestParam(value = "userCode") String userCode) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String adminUsername = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request web check account exist: %s, %s, %s",
				adminUsername, role, userCode));

		return responseEntityDeferredResult(() -> {
			WebAccountExistDtoOut webAccountExistDtoOut = accountService.checkAccountExist(adminUsername, role, userCode);
			logger.info(String.format("Handle request web check account exist: %s, %s, %s, %s",
					adminUsername, role, userCode, webAccountExistDtoOut));
			return webAccountExistDtoOut;
		});
	}

	@PostMapping(value = "/super-admin/update", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> updateSuperAdmin(@Valid WebUpdateSuperAccountDtoIn webUpdateSuperAccountDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web update super admin: %s", username));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			accountService.updateSuperAdmin(username, webUpdateSuperAccountDtoIn);
			logger.info(String.format("Handle success request web update super admin: %s", username));
			return new HashMap<>();
		});
	}

	@GetMapping(value = "/forgot-password/list")
	public DeferredResult<ResponseEntity<?>> listAccountByUsernameOrEmail(@RequestParam(value = "keyword") String keyword) {
		logger.info(String.format("Started request web list forgot password: %s", keyword));

		return responseEntityDeferredResult(() -> {
			ListForgotPasswordDtoOut listForgotPasswordDtoOut = accountService.listAccountByUsernameOrEmail(keyword);
			logger.info(String.format("Handle success request web list forgot password: %s", keyword));
			return listForgotPasswordDtoOut;
		});
	}

	@PutMapping(value = "/lock")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> lockAccount(@RequestBody @Valid LockAccountDtoIn lockAccountDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String adminUsername = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request lock account: %s, %s", adminUsername, lockAccountDtoIn));

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			accountService.lockAccount(lockAccountDtoIn);
			logger.info(String.format("Handle success request lock account: %s, %s", adminUsername, lockAccountDtoIn));

			return new HashMap<>();
		});
	}
}
