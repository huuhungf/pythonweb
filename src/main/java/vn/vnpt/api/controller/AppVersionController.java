package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.out.AppVersionDtoOut;
import vn.vnpt.api.service.AppVersionService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import java.util.List;

@RestController
@RequestMapping(value = "/app-version", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AppVersionController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(AppVersionController.class);

	private final AppVersionService appVersionService;

	@Autowired
	protected AppVersionController(ResponseEntites<Object> responseEntites,
								   AppVersionService appVersionService) {
		super(responseEntites);
		this.appVersionService = appVersionService;
	}

	@GetMapping(value = "/latest")
	public DeferredResult<ResponseEntity<?>> latest() {

		logger.info("Started processing request list latest app version");

		return responseEntityDeferredResult(() -> {
			List<AppVersionDtoOut> appVersionDtoOuts = appVersionService.listLatest();
			logger.info(String.format("Handle success request list lates app version: %s", appVersionDtoOuts));
			return appVersionDtoOuts;
		});
	}
}
