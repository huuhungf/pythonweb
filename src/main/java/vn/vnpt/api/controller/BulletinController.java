package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.BulletinDtoIn;
import vn.vnpt.api.dto.out.BulletinDtoOut;
import vn.vnpt.api.service.BulletinService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/bulletin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class BulletinController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(BulletinController.class);

	@Autowired
	private BulletinService bulletinService;

	protected BulletinController(ResponseEntites<Object> responseEntites) {
		super(responseEntites);
	}

	@PostMapping("")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> saveBulletin(@Valid @RequestBody BulletinDtoIn bulletinDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Start process request save bulletin: %s, %s", username, bulletinDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			bulletinService.save(bulletinDtoIn);
			logger.info(String.format("Handle success request save bulletin: %s, %s", username, bulletinDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping("")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> getBulletin() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Start process request get bulletin: %s", username));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			BulletinDtoOut bulletinDtoOut = bulletinService.get();
			logger.info(String.format("Handle success request get bulletin: %s", username));
			return bulletinDtoOut;
		});
	}
}
