package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.out.ChannelCategoryDtoOut;
import vn.vnpt.api.service.ChannelCategoryService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import java.util.List;

@RestController
@RequestMapping(value = "/channel-category", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ChannelCategoryController extends AbstractResponseController {
	private static final Logger logger = LogManager.getLogger(ChannelCategoryController.class);

	private final ChannelCategoryService channelCategoryService;

	@Autowired
	protected ChannelCategoryController(ResponseEntites<Object> responseEntites, ChannelCategoryService channelCategoryService) {
		super(responseEntites);
		this.channelCategoryService = channelCategoryService;
	}

	@GetMapping(value = "")
	public DeferredResult<ResponseEntity<?>> listChannelCategory() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();

		logger.info(String.format("Received async request web list channel category: %s", username));

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			List<ChannelCategoryDtoOut> categoryDtoOutList = channelCategoryService.listChannelCategory();

			logger.info(String.format("Handle success web list channel category: %s", username));
			return categoryDtoOutList;
		});
	}
}
