package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.ChatBotDtoIn;
import vn.vnpt.api.service.ChatBotService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/chatbot", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ChatBotController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(ChatBotController.class);

	private final ChatBotService chatBotService;

	@Autowired
	protected ChatBotController(ResponseEntites<Object> responseEntites, ChatBotService chatBotService) {
		super(responseEntites);
		this.chatBotService = chatBotService;
	}

	@PostMapping(value = "/receive")
	public DeferredResult<ResponseEntity<?>> receive(@Valid @RequestBody ChatBotDtoIn chatBotDtoIn) {
		logger.info(String.format("Received async request ott receive: %s", chatBotDtoIn));

		return responseEntityDeferredResult(() -> {
			chatBotService.receive(chatBotDtoIn);
			logger.info(String.format("Handle success ott receive: %s", chatBotDtoIn));
			return new HashMap<>();
		});
	}
}
