package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.WebUpdateCompanyDtoIn;
import vn.vnpt.api.dto.in.WebUpdateLogoDtoIn;
import vn.vnpt.api.dto.out.CompanyDtoOut;
import vn.vnpt.api.dto.out.CompanyExistDtoOut;
import vn.vnpt.api.service.CompanyService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/company", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class CompanyController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(CompanyController.class);

	private final CompanyService companyService;

	@Autowired
	protected CompanyController(ResponseEntites<Object> responseEntites, CompanyService companyService) {
		super(responseEntites);
		this.companyService = companyService;
	}

	@PutMapping(value = "/logo")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> updateLogo(@Valid @RequestBody WebUpdateLogoDtoIn updateLogoDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web update logo: %s, %s", username, updateLogoDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			companyService.updateLogo(username, updateLogoDtoIn);
			logger.info(String.format("Handle success request web update logo: %s, %s", username, updateLogoDtoIn));
			return new HashMap<>();
		});
	}

	@PutMapping(value = "/{uuidCompany}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> update(
			@PathVariable("uuidCompany") String uuidCompany,
			@Valid @RequestBody WebUpdateCompanyDtoIn webUpdateCompanyDtoIn
	) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web update company: %s, %s, %s", username,
				uuidCompany, webUpdateCompanyDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			companyService.update(uuidCompany, webUpdateCompanyDtoIn);
			logger.info(String.format("Handle success request web update company: %s, %s, %s", username,
					uuidCompany, webUpdateCompanyDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping(value = "/exist")
	public DeferredResult<ResponseEntity<?>> checkCompanyExist(@RequestParam(value = "companyCode") String companyCode) {
		logger.info(String.format("Started processing asynchronous request check company exist %s", companyCode));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			CompanyExistDtoOut companyExistDtoOut = companyService.checkExist(companyCode);
			logger.info(String.format("Handle success request check company exist %s, %s", companyCode,
					companyExistDtoOut));
			return companyExistDtoOut;
		});
	}

	@GetMapping(value = "/{uuidCompany}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> get(@PathVariable("uuidCompany") String uuidCompany) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web get company: %s, %s", username, uuidCompany));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			CompanyDtoOut companyDtoOut = companyService.get(uuidCompany);
			logger.info(String.format("Handle success request web get company: %s, %s", username, uuidCompany));
			return companyDtoOut;
		});
	}
}
