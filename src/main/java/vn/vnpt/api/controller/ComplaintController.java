package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.ComplaintDtoIn;
import vn.vnpt.api.dto.in.ResolveComplaintDtoIn;
import vn.vnpt.api.dto.out.ComplaintDetailsDtoOut;
import vn.vnpt.api.dto.out.ComplaintDtoOut;
import vn.vnpt.api.service.ComplaintService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/complaint")
public class ComplaintController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(ComplaintController.class);

	@Autowired
	private ComplaintService companyService;

	@Autowired
	protected ComplaintController(ResponseEntites<Object> responseEntites) {
		super(responseEntites);
	}

	@GetMapping
	public DeferredResult<ResponseEntity<?>> getListComplaint(@Valid ComplaintDtoIn complaintDtoIn,
	                                                          @Valid PagingDtoIn pagingDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing request get list complaint %s", complaintDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			PagingDTO<ComplaintDtoOut> pagingDTO = companyService.getListComplaints(complaintDtoIn, pagingDtoIn);
			logger.info(String.format("Handle success request get list complaint %s", pagingDTO));
			return pagingDTO;
		});
	}

	@GetMapping("/{uuidComplaint}")
	public DeferredResult<ResponseEntity<?>> getComplaint(@PathVariable(value = "uuidComplaint") String uuidComplaint) {
		logger.info(String.format("Started processing request get complaint %s", uuidComplaint));
		return responseEntityDeferredResult(() -> {
			ComplaintDetailsDtoOut detailsDtoOut = companyService.getComplaint(uuidComplaint);
			logger.info(String.format("Handle success request get complaint %s", detailsDtoOut));
			return detailsDtoOut;
		});
	}

	@PutMapping("/status/{uuidComplaint}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN','OWNER', 'USER')")
	public DeferredResult<ResponseEntity<?>> resolveComplaint(@RequestBody ResolveComplaintDtoIn resolverComplaint,
	                                                          @PathVariable(value = "uuidComplaint") String uuidComplaint) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing request resolve complaint %s", uuidComplaint));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			companyService.resolveComplaint(resolverComplaint, uuidComplaint);
			logger.info(String.format("Handle success request resolve complaint %s", uuidComplaint));
			return new HashMap<>();
		});
	}

	@PutMapping("/profile/{uuidComplaint}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN','OWNER')")
	public DeferredResult<ResponseEntity<?>> updateImgProfile(@PathVariable(value = "uuidComplaint") String uuidComplaint) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing request resolve complaint %s", uuidComplaint));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			companyService.updateImgProfile(uuidComplaint);
			logger.info(String.format("Handle success request resolve complaint %s", uuidComplaint));
			return new HashMap<>();
		});
	}


}
