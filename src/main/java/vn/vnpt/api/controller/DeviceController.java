package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.DeviceDtoIn;
import vn.vnpt.api.dto.in.ResetPasswordDeviceDtoIn;
import vn.vnpt.api.dto.in.UpdateDeviceDtoIn;
import vn.vnpt.api.dto.out.DeviceDtoOut;
import vn.vnpt.api.dto.out.DeviceExistDtoOut;
import vn.vnpt.api.service.DeviceService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/devices", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class DeviceController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(DeviceController.class);

	private final DeviceService deviceService;

	@Autowired
	protected DeviceController(ResponseEntites<Object> responseEntites, DeviceService deviceService) {
		super(responseEntites);
		this.deviceService = deviceService;
	}

	@GetMapping(value = "/owner")
	@PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> getDeviceOfOwner() {
		logger.info("Started processing asynchronous request get device");

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String adminUsername = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			return deviceService.getDeviceOfOwner(adminUsername);
		});
	}

	@PostMapping
	@PreAuthorize("hasAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> createDevice(@Valid @RequestBody DeviceDtoIn deviceDtoIn) {
		String superUsername = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		logger.info(String.format("Started processing asynchronous request create device %s, %s", superUsername,
				deviceDtoIn.getDeviceCode()));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			DeviceDtoOut deviceDtoOut = deviceService.createDevice(superUsername, deviceDtoIn);
			logger.info(String.format("Handle success request create device %s, %s", superUsername,
					deviceDtoIn.getDeviceCode()));
			return deviceDtoOut;
		});
	}

	@GetMapping(value = "/list-filter")
	@PreAuthorize("hasAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> listDevice(PagingDtoIn pagingDtoIn) {
		String superUsername = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		logger.info(String.format("Started processing asynchronous request list device %s, %s", superUsername, pagingDtoIn));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			PagingDTO<DeviceDtoOut> pagingDTO = deviceService.listDevice(superUsername, pagingDtoIn);
			logger.info(String.format("Handle success request list device %s, %s", superUsername, pagingDtoIn));
			return pagingDTO;
		});
	}

	@DeleteMapping(value = "/{uuidDevice}")
	@PreAuthorize("hasAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> deleteDevice(@PathVariable(value = "uuidDevice") String uuidDevice) {
		String superUsername = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		logger.info(String.format("Started processing asynchronous request delete device %s, %s", superUsername,
				uuidDevice));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			deviceService.deleteDevice(superUsername, uuidDevice);
			logger.info(String.format("Handle success request delete device %s, %s", superUsername, uuidDevice));
			return new HashMap<>();
		});
	}

	@PutMapping(value = "/reset-password")
	@PreAuthorize("hasAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> resetPassword(
			@Valid @RequestBody ResetPasswordDeviceDtoIn resetPasswordDeviceDtoIn) {
		String superUsername = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		logger.info(String.format("Started processing asynchronous request reset password device %s, %s", superUsername,
				resetPasswordDeviceDtoIn.getUuidDevice()));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			deviceService.resetPassword(superUsername, resetPasswordDeviceDtoIn);
			logger.info(String.format("Handle success request reset password device %s, %s", superUsername,
					resetPasswordDeviceDtoIn.getUuidDevice()));
			return new HashMap<>();
		});
	}

	@PutMapping(value = "/{uuidDevice}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> updateDevice(@PathVariable(value = "uuidDevice") String uuidDevice,
														  @Valid @RequestBody UpdateDeviceDtoIn updateDeviceDtoIn) {
		String superUsername = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		logger.info(String.format("Started processing asynchronous request update device %s, %s", superUsername,
				uuidDevice));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			DeviceDtoOut deviceDtoOut = deviceService.updateDevice(superUsername, uuidDevice, updateDeviceDtoIn);
			logger.info(String.format("Handle success request update device %s, %s", superUsername, uuidDevice));
			return deviceDtoOut;
		});
	}

	@GetMapping(value = "/exist")
	public DeferredResult<ResponseEntity<?>> checkDeviceExist(@RequestParam(value = "deviceCode") String deviceCode) {
		logger.info(String.format("Started processing asynchronous request check device exist %s", deviceCode));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			DeviceExistDtoOut deviceExistDtoOut = deviceService.checkDeviceExist(deviceCode);
			logger.info(String.format("Handle success request check device exist %s, %s", deviceCode,
					deviceExistDtoOut));
			return deviceExistDtoOut;
		});
	}
}
