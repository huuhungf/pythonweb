package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.DeviceLicenseDtoOut;
import vn.vnpt.api.dto.out.ListDeviceLicenseDtoOut;
import vn.vnpt.api.service.DeviceLicenseService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/device-license", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class DeviceLicenseController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(DeviceLicenseController.class);

	private final DeviceLicenseService deviceLicenseService;

	@Autowired
	protected DeviceLicenseController(ResponseEntites<Object> responseEntites, DeviceLicenseService deviceLicenseService) {
		super(responseEntites);
		this.deviceLicenseService = deviceLicenseService;
	}

	@GetMapping(value = "/list/{uuidDevice}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER', 'ADMIN')")
	public DeferredResult<ResponseEntity<?>> listDeviceLicense(
			@PathVariable(value = "uuidDevice") String uuidDevice
	) {
		logger.info(String.format("Started processing asynchronous request list device license %s", uuidDevice));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			ListDeviceLicenseDtoOut listDeviceLicenseDtoOut = deviceLicenseService.listDeviceLicense(uuidDevice);
			logger.info(String.format("Handle success request list device license %s", uuidDevice));
			return listDeviceLicenseDtoOut;
		});
	}

	@PostMapping
	@PreAuthorize("hasAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> createDeviceLicense(@Valid @RequestBody DeviceLicenseDtoIn deviceLicenseDtoIn) {
		String superUsername = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		logger.info(String.format("Started processing asynchronous request create device license %s, %s", superUsername,
				deviceLicenseDtoIn));
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			DeviceLicenseDtoOut deviceLicenseDtoOut = deviceLicenseService.createDeviceLicense(superUsername, deviceLicenseDtoIn);
			logger.info(String.format("Handle success request create device license %s, %s", superUsername, deviceLicenseDtoIn));
			return deviceLicenseDtoOut;
		});
	}

	@DeleteMapping
	@PreAuthorize("hasAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> deleteDeviceLicense(
			@Valid @RequestBody DeleteDeviceLicenseDtoIn deleteDeviceLicenseDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String superUsername = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request delete device license %s, %s, %s",
				role, superUsername, deleteDeviceLicenseDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			deviceLicenseService.deleteDeviceLicense(role, superUsername, deleteDeviceLicenseDtoIn);
			logger.info(String.format("Handle success request delete device license %s, %s, %s", role, superUsername,
					deleteDeviceLicenseDtoIn));
			return new HashMap<>();
		});
	}

	@PutMapping(value = "/active")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN','OWNER')")
	public DeferredResult<ResponseEntity<?>> activeDeviceLicense(@Valid @RequestBody ActiveDeviceLicenseDtoIn activeDeviceLicenseDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request active device license %s, %s",
				username, activeDeviceLicenseDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			deviceLicenseService.activeDeviceLicense(role, username, activeDeviceLicenseDtoIn);
			logger.info(String.format("Handle success request active device license %s, %s", username,
					activeDeviceLicenseDtoIn));
			return new HashMap<>();
		});
	}

	@PutMapping(value = "/lock")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN','OWNER')")
	public DeferredResult<ResponseEntity<?>> lockDeviceLicense(@Valid @RequestBody LockDeviceLicenseDtoIn lockDeviceLicenseDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request lock device license %s, %s",
				username, lockDeviceLicenseDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			deviceLicenseService.lockDeviceLicense(role, username, lockDeviceLicenseDtoIn);
			logger.info(String.format("Handle success request lock device license %s, %s", username,
					lockDeviceLicenseDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping(value = "")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN','OWNER','ADMIN')")
	public DeferredResult<ResponseEntity<?>> getDeviceLicense(@Valid GetDeviceLicenseDtoIn getDeviceLicenseDtoIn) {
		String superUsername = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		logger.info(String.format("Started processing asynchronous request get device license %s, %s",
				superUsername, getDeviceLicenseDtoIn));
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			DeviceLicenseDtoOut deviceLicenseDtoOut = deviceLicenseService.getDeviceLicense(getDeviceLicenseDtoIn);
			logger.info(String.format("Handle success request get device license %s, %s", superUsername,
					getDeviceLicenseDtoIn));
			return deviceLicenseDtoOut;
		});
	}

	@PutMapping(value = "")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN','OWNER')")
	public DeferredResult<ResponseEntity<?>> updateDeviceLicense(
			@Valid @RequestBody UpdateDeviceLicenseDtoIn updateDeviceLicenseDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request update device license %s, %s, %s",
				role, username, updateDeviceLicenseDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			DeviceLicenseDtoOut deviceLicenseDtoOut = deviceLicenseService.updateDeviceLicense(role, username, updateDeviceLicenseDtoIn);
			logger.info(String.format("Handle success request update device license %s, %s, %s", role, username,
					updateDeviceLicenseDtoIn));
			return deviceLicenseDtoOut;
		});
	}

	@PutMapping(value = "/collect-image")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN','OWNER')")
	public DeferredResult<ResponseEntity<?>> updateCollectImageStatus(
			@Valid @RequestBody UpdateDeviceLicenseCollectImageDtoIn updateCollectImageDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();

		logger.info(String.format("Started processing asynchronous request update collect image %s, %s, %s",
				role, username, updateCollectImageDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			deviceLicenseService.updateCollectImageStatus(role, username, updateCollectImageDtoIn);
			logger.info(String.format("Handle success request update collect image %s, %s, %s", role, username,
					updateCollectImageDtoIn));
			return new HashMap<>();
		});
	}
}
