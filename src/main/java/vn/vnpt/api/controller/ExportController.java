package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.ListOrderDtoIn;
import vn.vnpt.api.dto.out.OrderDtoOut;
import vn.vnpt.api.service.ExportService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/export", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ExportController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(ExportController.class);

	private final ExportService exportService;

	protected ExportController(ResponseEntites<Object> responseEntites, ExportService exportService) {
		super(responseEntites);
		this.exportService = exportService;
	}

	@GetMapping("/order/list-filter")
	@PreAuthorize("hasAnyAuthority('EXPORTER')")
	public DeferredResult<ResponseEntity<?>> listOrders(@Valid PagingDtoIn pagingDtoIn, @Valid ListOrderDtoIn listOrderDtoIn) {
		logger.info(String.format("Started handle request exporter list order: %s, %s", pagingDtoIn, listOrderDtoIn));

		return responseEntityDeferredResult(() -> {
			PagingDTO<OrderDtoOut> pagingDTO = exportService.listOrders(pagingDtoIn, listOrderDtoIn);

			logger.info(String.format("Handle success request exporter list order: %s, %s", pagingDtoIn, listOrderDtoIn));

			return pagingDTO;
		});
	}
}
