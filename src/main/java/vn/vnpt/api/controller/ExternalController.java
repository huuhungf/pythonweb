package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.ExternalAccountDtoOut;
import vn.vnpt.api.dto.out.ExternalNotifyDtoOut;
import vn.vnpt.api.dto.out.ExternalRegisterDtoOut;
import vn.vnpt.api.dto.out.HisCheckinExternalDtoOut;
import vn.vnpt.api.service.ExternalService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/external", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ExternalController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(ExternalController.class);

	private final ExternalService externalService;

	@Autowired
	protected ExternalController(ResponseEntites<Object> responseEntites, ExternalService externalService) {
		super(responseEntites);
		this.externalService = externalService;
	}

	@GetMapping(value = "/his-checkin/list-filter")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> list(@Valid ExternalListCheckinDtoIn listCheckinDtoIn, @Valid PagingDtoIn pagingDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web list history checkin external %s %s %s", username,
				listCheckinDtoIn, pagingDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			PagingDTO<HisCheckinExternalDtoOut> pagingDTO = externalService.list(username, listCheckinDtoIn, pagingDtoIn);
			logger.info(String.format("Handle success request web list stranger %s %s %s", username, listCheckinDtoIn, pagingDtoIn));
			return pagingDTO;
		});
	}

	@PutMapping(value = "/account/list")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> update(@RequestBody @Valid ExternalUpdateAccountList externalUpdateAccountList) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web update account external %s",
				externalUpdateAccountList));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			externalService.updateListUser(username, externalUpdateAccountList);
			logger.info(String.format("Handle success request external list update %s  %s", username, externalUpdateAccountList));
			return new HashMap<>();
		});
	}

	@DeleteMapping(value = "/account/list")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> delete(@RequestBody @Valid ExternalDeleteAccountListDtoIn externalAccountList) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request delete external account external %s",
				externalAccountList));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			externalService.deleteListUser(username, externalAccountList);
			logger.info(String.format("Handle success request external list delete %s  %s", username, externalAccountList));
			return externalAccountList;
		});
	}

	@PostMapping(value = "/account/plan")
	public DeferredResult<ResponseEntity<?>> registerOrExtendAccountPlan(@Valid @RequestBody ExternalRegisterDtoIn externalRegisterDtoIn) {
		logger.info(String.format("Started processing asynchronous request external register %s",
				externalRegisterDtoIn));

		return responseEntityDeferredResult(() -> {
			ExternalRegisterDtoOut externalRegisterDtoOut = externalService.registerOrExtendAccountPlan(externalRegisterDtoIn);
			logger.info(String.format("Handle success request external register %s", externalRegisterDtoIn));
			return externalRegisterDtoOut;
		});
	}

	@PutMapping(value = "/account/plan")
	public DeferredResult<ResponseEntity<?>> updateAttrAccountPlan(@Valid @RequestBody ExternalUpdateAccountPlanDtoIn externalUpdateAccountPlanDtoIn) {
		logger.info(String.format("Started processing asynchronous request external update account plan %s",
				externalUpdateAccountPlanDtoIn));

		return responseEntityDeferredResult(() -> {
			externalService.updateAccountPlan(externalUpdateAccountPlanDtoIn);
			logger.info(String.format("Handle success request external update account plan %s", externalUpdateAccountPlanDtoIn));
			return new HashMap<>();
		});
	}

	@DeleteMapping(value = "/account/plan")
	public DeferredResult<ResponseEntity<?>> cancelAccountPlan(@Valid @RequestBody ExternalCancelAccountPlanDtoIn externalCancelAccountPlanDtoIn) {
		logger.info(String.format("Started processing asynchronous request external cancel account plan %s",
				externalCancelAccountPlanDtoIn));

		return responseEntityDeferredResult(() -> {
			externalService.cancelAccountPlan(externalCancelAccountPlanDtoIn);
			logger.info(String.format("Handle success request external cancel account plan %s", externalCancelAccountPlanDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping(value = "/account/list")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> list(@Valid ExternalListAccountDtoIn externalListAccountDtoIn, @Valid PagingDtoIn pagingDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing request external list account %s", externalListAccountDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			PagingDTO<ExternalAccountDtoOut> pagingDTO = externalService.listAccount(username, externalListAccountDtoIn, pagingDtoIn);
			logger.info(String.format("Handle success request external list account %s  %s", username, externalListAccountDtoIn));
			return pagingDTO;
		});
	}

	@PostMapping(value = "/account/list")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> createList(@RequestBody @Valid ExternalCreateAccountList createAccountList) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web create account external %s",
				createAccountList));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			externalService.createListUser(username, createAccountList);
			logger.info(String.format("Handle success request external list create %s  %s", username, createAccountList));
			return new HashMap<>();
		});
	}

	@PostMapping(value = "/notify/account")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> notify(@Valid @RequestBody ExternalNotifyDtoIn notifyDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing request external notify: %s, %s", username, notifyDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			ExternalNotifyDtoOut notifyDtoOut = externalService.notifyAccount(username, notifyDtoIn);
			logger.info(String.format("Handle success request external notify: %s, %s, %s", username, notifyDtoIn, notifyDtoOut));
			return notifyDtoOut;
		});
	}

}
