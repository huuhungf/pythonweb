package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.GroupDtoIn;
import vn.vnpt.api.dto.in.ListGroupDeviceDtoIn;
import vn.vnpt.api.dto.in.UpdateGroupDtoIn;
import vn.vnpt.api.dto.out.GroupDtoOut;
import vn.vnpt.api.dto.out.ListGroupDeviceDtoOut;
import vn.vnpt.api.dto.out.ListGroupDtoOut;
import vn.vnpt.api.dto.out.ListGroupTreeDtoOut;
import vn.vnpt.api.service.AccountService;
import vn.vnpt.api.service.GroupService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/group", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class GroupController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(GroupController.class);

	private final AccountService accountService;

	private final GroupService groupService;

	@Autowired
	protected GroupController(ResponseEntites<Object> responseEntites,
							  AccountService accountService,
							  GroupService groupService) {
		super(responseEntites);
		this.accountService = accountService;
		this.groupService = groupService;
	}

	@PostMapping(value = "")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> create(@Valid @RequestBody GroupDtoIn groupDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request create group %s, %s", username, groupDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			GroupDtoOut groupDtoOut = groupService.create(username, groupDtoIn);
			logger.info(String.format("Handle success request create group %s, %s", username, groupDtoIn));
			return groupDtoOut;
		});
	}

	@GetMapping(value = "/company")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN','OWNER')")
	public DeferredResult<ResponseEntity<?>> listGroupTree() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		logger.info(String.format("Started processing asynchronous request list group tree %s", username));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			ListGroupTreeDtoOut listGroupTreeDtoOut = groupService.listGroupTree(username, role);
			logger.info(String.format("Handle success request list group tree %s", username));
			return listGroupTreeDtoOut;
		});
	}

	@DeleteMapping(value = "{uuidGroup}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> delete(@PathVariable(value = "uuidGroup") String uuidGroup) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request delete group %s, %s", username, uuidGroup));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			groupService.delete(username, uuidGroup);
			logger.info(String.format("Handle success request delete group %s, %s", username, uuidGroup));
			return new HashMap<>();
		});
	}

	@PutMapping(value = "{uuidGroup}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> update(@PathVariable(value = "uuidGroup") String uuidGroup,
													@Valid @RequestBody UpdateGroupDtoIn updateGroupDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request update group %s, %s, %s",
				username, uuidGroup, updateGroupDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			groupService.update(username, uuidGroup, updateGroupDtoIn);
			logger.info(String.format("Handle success request update group %s, %s, %s",
					username, uuidGroup, updateGroupDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping(value = "/device")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> listGroupDevice(@Valid ListGroupDeviceDtoIn listGroupDeviceDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request list group device %s", username));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			ListGroupDeviceDtoOut listGroupDeviceDtoOut = groupService.listGroupDevice(username, listGroupDeviceDtoIn);
			logger.info(String.format("Handle success request list group device %s", username));
			return listGroupDeviceDtoOut;
		});
	}

	@GetMapping(value = "")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER', 'USER')")
	public DeferredResult<ResponseEntity<?>> list() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		logger.info(String.format("Started processing asynchronous request list group %s", username));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			ListGroupDtoOut listGroupDtoOut = groupService.listGroup(username, role);
			logger.info(String.format("Handle success request list group %s", username));
			return listGroupDtoOut;
		});
	}
}
