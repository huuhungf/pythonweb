package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.GetHisCheckinDtoIn;
import vn.vnpt.api.dto.in.HisCheckinDtoIn;
import vn.vnpt.api.dto.in.ListHisCheckinDtoIn;
import vn.vnpt.api.dto.out.HisCheckinAccountDtoOut;
import vn.vnpt.api.dto.out.HisCheckinDetailDtoOut;
import vn.vnpt.api.dto.out.ListDtoOut;
import vn.vnpt.api.dto.out.WebHisCheckinDtoOut;
import vn.vnpt.api.service.HisCheckinService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/his-checkin", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class HisCheckinController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(HisCheckinController.class);

	private final HisCheckinService hisCheckinService;

	@Autowired
	protected HisCheckinController(ResponseEntites<Object> responseEntites, HisCheckinService hisCheckinService) {
		super(responseEntites);
		this.hisCheckinService = hisCheckinService;
	}

	@GetMapping(value = "/list-filter")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER', 'USER')")
	public DeferredResult<ResponseEntity<?>> listHisCheckin(@Valid ListHisCheckinDtoIn listHisCheckinDtoIn,
	                                                        @Valid PagingDtoIn pagingDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing asynchronous request web list checkin %s %s",
				listHisCheckinDtoIn, pagingDtoIn));

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);
			PagingDTO<WebHisCheckinDtoOut> pagingDTO = hisCheckinService.listHisCheckin(listHisCheckinDtoIn, pagingDtoIn);
			logger.info(String.format("Handle success request web list checkin %s %s",
					listHisCheckinDtoIn, pagingDtoIn));
			return pagingDTO;
		});
	}

	@GetMapping(value = "/detail")
	public DeferredResult<ResponseEntity<?>> getHisCheckinDetail(@Valid GetHisCheckinDtoIn getHisCheckinDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web list checkin %s %s", username,
				getHisCheckinDtoIn));

		return responseEntityDeferredResult(() -> {

			SecurityContextHolder.getContext().setAuthentication(authentication);

			List<HisCheckinDetailDtoOut> hisCheckinDetailDtoOuts = hisCheckinService.getHisCheckinDetail(getHisCheckinDtoIn);
			logger.info(String.format("Handle success request web list checkin %s %s", username,
					getHisCheckinDtoIn));
			return hisCheckinDetailDtoOuts;
		});
	}

	@PostMapping("")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> addHisCheckin(@RequestBody @Valid HisCheckinDtoIn hisCheckinDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web addCheckin %s %s", username,
				hisCheckinDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			hisCheckinService.addHisCheckin(hisCheckinDtoIn);
			logger.info(String.format("Handle success request web addCheckin %s %s", username,
					hisCheckinDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping("/account")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> getHisCheckinAccount(@Valid GetHisCheckinDtoIn getHisCheckinDtoIn) {
		logger.info(String.format("Started processing asynchronous request web getHisCheckinAccount %s", getHisCheckinDtoIn));
		return responseEntityDeferredResult(() -> {
			ListDtoOut<HisCheckinAccountDtoOut> rs = hisCheckinService.getHisCheckinAccount(getHisCheckinDtoIn);
			logger.info(String.format("Handle success request web getHisCheckinAccount %s", rs));
			return rs;
		});
	}

}
