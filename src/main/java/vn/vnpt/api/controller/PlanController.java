package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.service.PlanService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import java.util.HashMap;

@RestController
@RequestMapping(value = "/plan", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class PlanController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(PlanController.class);

	private final PlanService planService;

	@Autowired
	protected PlanController(ResponseEntites<Object> responseEntites, PlanService planService) {
		super(responseEntites);
		this.planService = planService;
	}

	@PostMapping(value = "/register")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> register() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web register plan: %s", username));

		return responseEntityDeferredResult(() -> {
			planService.register(username);
			logger.info(String.format("Handle success request web register plan %s", username));
			return new HashMap<>();
		});
	}
}
