package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.PortalAccountDtoOut;
import vn.vnpt.api.dto.out.PortalAccountProfileDtoOut;
import vn.vnpt.api.dto.out.PortalCreateAccountDtoOut;
import vn.vnpt.api.dto.out.PortalHisPlanDtoOut;
import vn.vnpt.api.service.PortalService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/portal", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class PortalController extends AbstractResponseController {

	public static final Logger logger = LogManager.getLogger(PortalController.class);

	private final PortalService portalService;

	@Autowired
	protected PortalController(ResponseEntites<Object> responseEntites,
							   PortalService portalService) {
		super(responseEntites);
		this.portalService = portalService;
	}

	@PostMapping("/agent/account")
	public DeferredResult<ResponseEntity<?>> createAccount(@Valid @RequestBody PortalCreateAccountDtoIn portalDtoIn) {
		logger.info(String.format("Started processing asynchronous request portal create account: %s", portalDtoIn));
		return responseEntityDeferredResult(() -> {
			PortalCreateAccountDtoOut portalAccount = portalService.createAccount(portalDtoIn);
			logger.info(String.format("Handle request portal create account: %s", portalAccount));
			return portalAccount;
		});
	}

	@GetMapping("/agent/account")
	public DeferredResult<ResponseEntity<?>> listAccount(@Valid PortalListAccountDtoIn listAccountDtoIn, @Valid PagingDtoIn pagingDtoIn) {
		logger.info(String.format("Started processing asynchronous request portal agent list account: %s, %s",
				listAccountDtoIn, pagingDtoIn));
		return responseEntityDeferredResult(() -> {
			PagingDTO<PortalAccountDtoOut> pagingDTO = portalService.listAccount(listAccountDtoIn, pagingDtoIn);
			logger.info(String.format("Handle request portal agent list account: %s", pagingDTO));
			return pagingDTO;
		});
	}

	@PostMapping(value = "/agent/plan/register")
	public DeferredResult<ResponseEntity<?>> registerPlan(@Valid @RequestBody PortalRegisterPlanDtoIn registerPlanDtoIn) {
		logger.info(String.format("Started processing asynchronous request portal agent register plan: %s", registerPlanDtoIn));
		return responseEntityDeferredResult(() -> {
			portalService.registerPlan(registerPlanDtoIn);
			logger.info(String.format("Handle request portal agent register plan: %s", registerPlanDtoIn));
			return new HashMap<>();
		});
	}

	@PostMapping(value = "/agent/plan/buy-more")
	public DeferredResult<ResponseEntity<?>> buyMorePlan(@Valid @RequestBody PortalBuyMorePlanDtoIn buyMorePlanDtoIn) {
		logger.info(String.format("Started processing asynchronous request portal agent buy-more plan: %s", buyMorePlanDtoIn));
		return responseEntityDeferredResult(() -> {
			portalService.buyMorePlan(buyMorePlanDtoIn);
			logger.info(String.format("Handle request portal agent buy-more plan: %s", buyMorePlanDtoIn));
			return new HashMap<>();
		});
	}

	@PostMapping(value = "/agent/plan/renew")
	public DeferredResult<ResponseEntity<?>> renewPlan(@Valid @RequestBody PortalPlanRenewDtoIn planRenewDtoIn) {
		logger.info(String.format("Started processing asynchronous request portal agent renew plan: %s", planRenewDtoIn));
		return responseEntityDeferredResult(() -> {
			portalService.renewPlan(planRenewDtoIn);
			logger.info(String.format("Handle request portal agent renew plan: %s", planRenewDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping(value = "/agent/account/info")
	public DeferredResult<ResponseEntity<?>> getAccountInfo(@Valid PortalAccountInfoDtoIn accountInfoDtoIn) {
		logger.info(String.format("Started processing request portal agent get account info: %s", accountInfoDtoIn));
		return responseEntityDeferredResult(() -> {
			PortalAccountProfileDtoOut accountProfileDtoOut = portalService.getAccountProfile(accountInfoDtoIn);
			logger.info(String.format("Handle request portal agent get account info: %s", accountProfileDtoOut));
			return accountProfileDtoOut;
		});
	}

	@GetMapping(value = "/agent/order/history")
	public DeferredResult<ResponseEntity<?>> getPlanHistory(@Valid PortalHisPlanDtoIn planHistoryDtoIn, @Valid PagingDtoIn pagingDtoIn) {
		logger.info(String.format("Started processing request portal agent get plan history: %s", planHistoryDtoIn));
		return responseEntityDeferredResult(() -> {
			PagingDTO<PortalHisPlanDtoOut> listPlanHistory = portalService.listPlanHistory(planHistoryDtoIn, pagingDtoIn);
			logger.info(String.format("Handle request portal agent get plan history: %s", listPlanHistory));
			return listPlanHistory;
		});
	}
}
