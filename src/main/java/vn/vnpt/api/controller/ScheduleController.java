package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.ParamScheduleAccountDtoIn;
import vn.vnpt.api.dto.in.ScheduleAccountDtoIn;
import vn.vnpt.api.dto.in.ScheduleDtoIn;
import vn.vnpt.api.dto.out.ScheduleAccountDtoOut;
import vn.vnpt.api.dto.out.ScheduleDtoOut;
import vn.vnpt.api.service.ScheduleService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/schedules")
public class ScheduleController extends AbstractResponseController {

	public final Logger logger = LogManager.getLogger(ScheduleController.class);

	@Autowired
	protected ScheduleController(ResponseEntites<Object> responseEntites) {
		super(responseEntites);
	}

	@Autowired
	private ScheduleService scheduleService;

	@PostMapping("")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> createSchedule(@RequestBody @Valid ScheduleDtoIn scheduleDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing create schedule %s %s:", userName, scheduleDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			logger.info(String.format("Processing request create schedule %s successfully!", scheduleDtoIn));
			scheduleService.createSchedule(userName, scheduleDtoIn);
			return new HashMap<>();
		});
	}

	@GetMapping("")
	public DeferredResult<ResponseEntity<?>> listSchedule() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		logger.info(String.format("Started processing get listSchedules %s, %s:", username, role));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			List<ScheduleDtoOut> listSchedules = scheduleService.listSchedules(username, role);
			logger.info(String.format("Processing request get listSchedules %s successfully!", listSchedules));
			return listSchedules;
		});
	}

	@GetMapping("/{uuidSchedule}")
	public DeferredResult<ResponseEntity<?>> getSchedule(@PathVariable("uuidSchedule") String uuidSchedule) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing get schedule %s:", userName));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			ScheduleDtoOut listSchedules = scheduleService.getSchedule(uuidSchedule);
			logger.info(String.format("Processing request get schedule %s successfully!", listSchedules));
			return listSchedules;
		});
	}

	@DeleteMapping("/{uuidSchedule}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> deleteSchedule(@PathVariable("uuidSchedule") String uuidSchedule) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing delete schedule %s:", uuidSchedule));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			scheduleService.deleteSchedule(userName, uuidSchedule);
			logger.info(String.format("Processing request get schedule %s successfully!", uuidSchedule));
			return new HashMap<>();
		});
	}

	@PutMapping("/{uuidSchedule}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> updateSchedule(@RequestBody @Valid ScheduleDtoIn scheduleDtoIn, @PathVariable("uuidSchedule") String uuidSchedule) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing update schedule %s:", uuidSchedule));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			scheduleService.updateSchedule(userName, uuidSchedule, scheduleDtoIn);
			logger.info(String.format("Processing request update schedule %s successfully!", uuidSchedule));
			return new HashMap<>();
		});
	}

	@PostMapping("/account")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> assignScheduleAccount(@RequestBody @Valid ScheduleAccountDtoIn scheduleAccountDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing assignScheduleAccount %s:", scheduleAccountDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			scheduleService.assignScheduleAccount(userName, scheduleAccountDtoIn);
			logger.info(String.format("Processing request assignScheduleAccount %s successfully!", scheduleAccountDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping("/account")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> listScheduleAccount(@Valid ParamScheduleAccountDtoIn scheduleAccountDtoIn, @Valid PagingDtoIn pageDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing listScheduleAccount  %s:", scheduleAccountDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			PagingDTO<ScheduleAccountDtoOut> rs = scheduleService.listScheduleAccount(username, scheduleAccountDtoIn, pageDtoIn);
			logger.info(String.format("Processing request listScheduleAccount %s successfully!", rs.getData()));
			return rs;
		});
	}
}
