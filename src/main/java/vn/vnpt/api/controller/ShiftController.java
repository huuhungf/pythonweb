package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.ListShiftDtoIn;
import vn.vnpt.api.dto.in.ShiftAccountDtoIn;
import vn.vnpt.api.dto.in.ShiftDtoIn;
import vn.vnpt.api.dto.in.ShiftTmpDtoIn;
import vn.vnpt.api.dto.out.ShiftDetailDtoOut;
import vn.vnpt.api.service.ShiftService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;

import javax.validation.Valid;
import java.util.HashMap;


@RestController
@RequestMapping(value = "/shifts")
public class ShiftController extends AbstractResponseController {

	public static final Logger logger = LogManager.getLogger(ShiftController.class);

	@Autowired
	protected ShiftController(ResponseEntites<Object> responseEntites) {
		super(responseEntites);
	}

	@Autowired
	private ShiftService shiftService;

	@GetMapping("")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> getListShifts(@Valid ListShiftDtoIn listShiftDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		logger.info(String.format("Started processing request get list shifts %s, %s", username, role));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			return shiftService.getListShift(username, listShiftDtoIn, role);
		});
	}

	@PostMapping("")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> createShift(@RequestBody @Valid ShiftDtoIn shiftDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing request create shift %s,%s", username, shiftDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			shiftService.createShift(username, shiftDtoIn);
			logger.info("Processing request create shift successfully!");
			return new HashMap<>();
		});
	}

	@PutMapping("/{uuidShift}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> updateShift(@RequestBody @Valid ShiftDtoIn shiftDtoIn,
														 @PathVariable("uuidShift") String uuidShift) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing request update shift %s,%s", username, shiftDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			shiftService.update(username, uuidShift, shiftDtoIn);
			logger.info(String.format("Processing request update uuidShift %s successfully!", uuidShift));
			return new HashMap<>();
		});
	}

	@DeleteMapping("/{uuidShift}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> deleteShift(@PathVariable("uuidShift") String uuidShift) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing request delete shift %s,%s", username, uuidShift));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			shiftService.delete(username, uuidShift);
			logger.info(String.format("Processing request delete uuidShift %s successfully!", uuidShift));
			return new HashMap<>();
		});
	}

	@GetMapping("/{uuidShift}")
	public DeferredResult<ResponseEntity<?>> getShift(@PathVariable("uuidShift") String uuidShift) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing request get shift %s,%s", username, uuidShift));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			ShiftDetailDtoOut dtoOut = shiftService.getShift(username, uuidShift);
			logger.info(String.format("Processing request success get shift %s ", dtoOut));
			return dtoOut;
		});
	}

	@PostMapping("/tmp/account")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> assignShiftTmp(@RequestBody @Valid ShiftTmpDtoIn shiftTmpDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing request assignShift %s", shiftTmpDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			shiftService.assignShiftTmp(shiftTmpDtoIn, username);
			logger.info(String.format("Processing request assignShift %s successfully!", shiftTmpDtoIn));
			return new HashMap<>();
		});
	}

	@GetMapping("/account")
	public DeferredResult<ResponseEntity<?>> getShiftsAccount(@Valid ShiftAccountDtoIn shiftAccount) {
		logger.info(String.format("Started processing request getShiftsAccount %s", shiftAccount));
		return responseEntityDeferredResult(() -> {
			logger.info(String.format("Processing request getShiftsAccount %s successfully!", shiftAccount));
			return shiftService.listShiftsAccount(shiftAccount);
		});
	}
}
