package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.DeleteAllStrangerDtoIn;
import vn.vnpt.api.dto.in.DeleteListStrangerDtoIn;
import vn.vnpt.api.dto.in.WebListStrangerDtoIn;
import vn.vnpt.api.dto.out.StrangerDtoOut;
import vn.vnpt.api.service.StrangerService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/stranger", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class StrangerController extends AbstractResponseController {

	private static final Logger logger = LogManager.getLogger(StrangerController.class);

	private final StrangerService strangerService;

	@Autowired
	protected StrangerController(ResponseEntites<Object> responseEntites, StrangerService strangerService) {
		super(responseEntites);
		this.strangerService = strangerService;
	}

	@GetMapping(value = "/list-filter")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'ADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> list(@Valid WebListStrangerDtoIn webListStrangerDtoIn, @Valid PagingDtoIn pagingDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		logger.info(String.format("Started processing asynchronous request web list stranger %s %s %s", username,
				webListStrangerDtoIn, pagingDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			PagingDTO<StrangerDtoOut> pagingDTO = strangerService.list(username, role, webListStrangerDtoIn, pagingDtoIn);
			logger.info(String.format("Handle success request web list stranger %s %s %s", username, webListStrangerDtoIn, pagingDtoIn));
			return pagingDTO;
		});
	}

	@DeleteMapping(value = "/{uuidStranger}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'ADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> delete(@PathVariable(value = "uuidStranger") String uuidStranger) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web delete stranger %s %s", username, uuidStranger));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			strangerService.delete(uuidStranger);
			logger.info(String.format("Handle success request web delete stranger %s %s", username, uuidStranger));
			return new HashMap<>();
		});
	}

	@DeleteMapping(value = "/list")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'ADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> deleteList(@RequestBody @Valid DeleteListStrangerDtoIn deleteListStrangerDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		logger.info(String.format("Started processing asynchronous request web delete strangers %s %s", username, deleteListStrangerDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			strangerService.deleteList(deleteListStrangerDtoIn);
			logger.info(String.format("Handle success request web delete strangers %s %s", username, deleteListStrangerDtoIn));
			return new HashMap<>();
		});
	}

	@DeleteMapping(value = "/all")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'ADMIN', 'OWNER')")
	public DeferredResult<ResponseEntity<?>> deleteAll(@RequestBody @Valid DeleteAllStrangerDtoIn deleteAllStrangerDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		logger.info(String.format("Started processing asynchronous request web delete all strangers %s, %s, %s", username,
				role, deleteAllStrangerDtoIn));

		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);

			strangerService.deleteAll(username, role, deleteAllStrangerDtoIn);
			logger.info(String.format("Handle success request web delete all strangers %s, %s, %s", username, role, deleteAllStrangerDtoIn));
			return new HashMap<>();
		});
	}
}
