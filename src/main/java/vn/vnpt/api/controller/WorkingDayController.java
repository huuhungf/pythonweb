package vn.vnpt.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.api.dto.in.WorkingDayDetailADayDtoIn;
import vn.vnpt.api.dto.in.WorkingDayDetailDtoIn;
import vn.vnpt.api.dto.in.WorkingDayReportDtoIn;
import vn.vnpt.api.dto.in.WorkingDayReportSummaryDtoIn;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.service.WorkingDayService;
import vn.vnpt.common.AbstractResponseController;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import javax.validation.Valid;

@RestController
@RequestMapping("/working-day")
public class WorkingDayController extends AbstractResponseController {

	public static final Logger logger = LogManager.getLogger(ShiftController.class);

	@Autowired
	protected WorkingDayController(ResponseEntites<Object> responseEntites) {
		super(responseEntites);
	}

	@Autowired
	private WorkingDayService workingDayService;

	@GetMapping("/account")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER', 'USER')")
	public DeferredResult<ResponseEntity<?>> workingDayReport(@Valid WorkingDayReportDtoIn workingDayReportDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing request workingDayReport account %s", workingDayReportDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			ListDtoOut<WorkingDayReportDtoOut> rs = workingDayService.workingDayReport(workingDayReportDtoIn);
			logger.info(String.format("Processing request workingDayReport account successfully: %s", rs.getData().size()));
			return rs;
		});
	}

	@GetMapping("/summary")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN', 'OWNER', 'USER')")
	public DeferredResult<ResponseEntity<?>> workingDayReportSummary(@Valid PagingDtoIn pagingDtoIn,
																	 @Valid WorkingDayReportSummaryDtoIn wDReportSummaryDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing request workingDayReportSummary %s", wDReportSummaryDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			PagingDTO<WorkingDayReportSummaryDtoOut> rs = workingDayService.workingDayReportSummary(wDReportSummaryDtoIn, pagingDtoIn);
			logger.info(String.format("Processing request workingDayReportSummary successfully: %s", rs));
			return rs;
		});
	}

	@GetMapping("/detail/{uuidAccount}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> workingDayDetail(@PathVariable String uuidAccount,
															  @Valid WorkingDayDetailDtoIn workingDayDetailDtoIn) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing request workingDayReportDetail: %s", workingDayDetailDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			WorkingDayDetailDtoOut rs = workingDayService.workingDayDetail(workingDayDetailDtoIn, uuidAccount);
			logger.info(String.format("Processing request workingDayReportDetail successfully: %s", rs.getData().size()));
			return rs;
		});

	}

	@GetMapping("/date/{uuidAccount}")
	@PreAuthorize("hasAnyAuthority('SUPERADMIN')")
	public DeferredResult<ResponseEntity<?>> workingDayDetailADay(@Valid WorkingDayDetailADayDtoIn workingDayDetailADayDtoIn,
																  @PathVariable String uuidAccount) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.info(String.format("Started processing request workingDayDetailADay: %s", workingDayDetailADayDtoIn));
		return responseEntityDeferredResult(() -> {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			WorkingDayDetailADayDtoOut rs = workingDayService.workingDetailADay(workingDayDetailADayDtoIn, uuidAccount);
			logger.info(String.format("Processing request workingDayDetailADay successfully: %s", rs));
			return rs;
		});

	}
}
