package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class AccountChannelDtoIn {
	@NotNull(message = "IDG-00000001")
	private String uuidChannelCategory;

	@NotNull(message = "IDG-00000001")
	@Min(value = 0, message = "IDG-00000004")
	private int userOTT;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 2, message = "IDG-00000004")
	private int status;
}
