package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.model.Role;

@Data
public class AccountDtoIn {
	private String fullName;
	private Device device;
	private Role role;
	private String userCode;
	private String username;
	private String password;
	private String imageUrl;
	private String imageEmbed;
	private String email;
	private String gender;
	private String audioUrl;
	private Integer imageType;
	private String phoneNumber;
}
