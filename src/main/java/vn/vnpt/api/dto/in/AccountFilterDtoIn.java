package vn.vnpt.api.dto.in;

import lombok.Data;

@Data
public class AccountFilterDtoIn {
	private String deviceCode;
}
