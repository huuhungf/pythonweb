package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class AttributeDtoIn {
	@NotNull(message = "IDG-00000001")
	private String unit;

	@NotNull(message = "IDG-00000001")
	@Min(value = 0, message = "IDG-00000004")
	private Integer quantity;
}
