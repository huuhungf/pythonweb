package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@Data
public class BulletinDtoIn {
	@NotNull(message = "IDG-00000001")
	@Min(value = 1, message = "IDG-00000004")
	@Max(value = 2, message = "IDG-00000004")
	private Integer status;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(pattern = ConstantString.DDMMYYYY, message = "IDG-00000007")
	private String startDate;

	@ValidateDate(pattern = ConstantString.DDMMYYYY, message = "IDG-00000007")
	private String endDate;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(pattern = ConstantString.HHMM, message = "IDG-00000007")
	private String startHour;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(pattern = ConstantString.HHMM, message = "IDG-00000007")
	private String endHour;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 200, message = "IDG-00000004")
	private String title;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 4000, message = "IDG-00000004")
	private String content;

	@Valid
	@NotNull(message = "IDG-00000001")
	@NotEmpty(message = "IDG-00000001")
	private List<BulletinPageDtoIn> pageInfo;
}
