package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ChatBotDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String botId;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String userId;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String text;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String channel;
}
