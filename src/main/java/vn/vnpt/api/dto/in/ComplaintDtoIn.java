package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;

import javax.validation.constraints.NotNull;

@Data
public class ComplaintDtoIn {

	@NotNull(message = "IDG-00000001")
	private String uuidGroup;

	private Integer status = 0;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007")
	private String startDate;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007")
	private String endDate;

	private String keySearch;
}
