package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.PasswordMatches;
import vn.vnpt.api.validators.UsernameExist;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@PasswordMatches(message = "IDG-00000030")
public class ConfirmForgotPasswordDtoIn {
	@NotNull(message = "IDG-00000001")
	@UsernameExist
	private String username;

	@NotNull(message = "IDG-00000001")
	@Size(min = 6, max = 50, message = "IDG-00000005")
	@Pattern(regexp = Patterns.IDG_PASSWORD, message = "IDG-00000006")
	private String password;

	@NotNull(message = "IDG-00000001")
	@Size(min = 6, max = 50, message = "IDG-00000005")
	@Pattern(regexp = Patterns.IDG_PASSWORD, message = "IDG-00000006")
	private String passwordConfirm;

	@NotNull(message = "IDG-00000001")
	private String token;

	public String getUsername() {
		return username.toUpperCase();
	}
}
