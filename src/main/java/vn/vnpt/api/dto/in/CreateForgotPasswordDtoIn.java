package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.UsernameExist;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class CreateForgotPasswordDtoIn {
	@NotNull(message = "IDG-00000001")
	@UsernameExist
	private String username;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(regexp = Patterns.IDG_EMAIL, message = ErrorCode.IDG_00002010)
	private String email;

	public String getUsername() {
		return username.toUpperCase();
	}
}
