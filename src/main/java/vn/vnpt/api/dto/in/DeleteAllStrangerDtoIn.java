package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DeleteAllStrangerDtoIn {
	@NotNull(message = "IDG-00000001")
	private String uuidDevice;

	@NotNull(message = "IDG-00000001")
	private String serialNumber;
}
