package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.UniqueDeviceCode;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class DeviceDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String name;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 32, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002007, regexp = Patterns.IDG_DEVICE_CODE)
	@UniqueDeviceCode
	private String deviceCode;

	@NotNull(message = "IDG-00000001")
	@Size(min = 6, max = 50, message = "IDG-00000005")
	@Pattern(regexp = Patterns.IDG_PASSWORD, message = "IDG-00000006")
	private String password;

	@Min(value = 1, message = "IDG-00000004")
	private Integer limitDeviceLicense;

	@NotNull(message = "IDG-00000001")
	private String uuidGroup;

	public String getDeviceCode() {
		return deviceCode.toUpperCase();
	}
}
