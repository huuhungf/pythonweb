package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Data
public class DeviceLicenseCollectImage {
	@NotNull(message = "IDG-00000001")
	private String serialNumber;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 1, message = "IDG-00000004")
	private Integer collectImage;
}
