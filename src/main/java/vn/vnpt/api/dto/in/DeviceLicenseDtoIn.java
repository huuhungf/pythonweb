package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.UuidDeviceExist;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class DeviceLicenseDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String name;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String serialNumber;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@UuidDeviceExist
	private String uuidDevice;
}
