package vn.vnpt.api.dto.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.vnpt.common.Common;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class ExternalCancelAccountPlanDtoIn {
	@JsonProperty(value = "external_customer_id")
	@NotEmpty(message = "IDG-00000001")
	private String externalCustomerId;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002010, regexp = Patterns.IDG_EMAIL)
	private String email;

	@JsonProperty(value = "created_by")
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String createdBy;

	public String getEmail() {
		return Common.lowerCase(email);
	}
}
