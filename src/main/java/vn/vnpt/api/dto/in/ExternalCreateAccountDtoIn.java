package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.common.Common;
import vn.vnpt.common.EnumGender;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;
import vn.vnpt.validate.ValidateString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class ExternalCreateAccountDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String userCode;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String fullName;

	@NotNull(message = "IDG-00000001")
	@Pattern(message = ErrorCode.IDG_00002010, regexp = Patterns.IDG_EMAIL)
	private String email;

	@ValidateString(enumClazz = EnumGender.class)
	private String gender;

	public String getUserCode() {
		return Common.upperCase(userCode);
	}

	public String getEmail() {
		return Common.lowerCase(email);
	}

	public String getGender() {
		return Common.upperCase(gender);
	}
}
