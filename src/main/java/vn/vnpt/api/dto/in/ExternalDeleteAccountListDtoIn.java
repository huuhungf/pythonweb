package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

@Data
public class ExternalDeleteAccountListDtoIn {
    private List<String> userCodes;

    @Size(min = 1, max = 255, message = "IDG-00000004")
    private String channel;
}
