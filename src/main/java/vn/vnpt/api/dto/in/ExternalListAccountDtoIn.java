package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

@Data
public class ExternalListAccountDtoIn {

	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYY)
	private String startDate;

	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYY)
	private String endDate;
}
