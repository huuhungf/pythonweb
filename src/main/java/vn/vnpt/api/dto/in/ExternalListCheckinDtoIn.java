package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

import javax.validation.constraints.NotNull;

@Data
public class ExternalListCheckinDtoIn {
	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYYHHMMSS)
	private String startDate;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYYHHMMSS)
	private String endDate;

	private String userCodes;
}
