package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class ExternalNotifyDtoIn {
	@NotEmpty(message = "IDG-00000001")
	private List<String> userCodes;

	@NotEmpty(message = "IDG-00000001")
	private List<String> channels;

	@NotEmpty(message = "IDG-00000001")
	private String content;
}
