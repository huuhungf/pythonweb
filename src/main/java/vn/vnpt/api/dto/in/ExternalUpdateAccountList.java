package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class ExternalUpdateAccountList {
    @Valid
    private List<ExternalUpdateAccountDtoIn> externalAccounts;

    @Size(min = 1, max = 255, message = "IDG-00000004")
    private String channel;
}
