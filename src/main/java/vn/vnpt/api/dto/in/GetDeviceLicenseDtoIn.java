package vn.vnpt.api.dto.in;

import lombok.Data;

@Data
public class GetDeviceLicenseDtoIn {
	private String uuidDevice;
	private String serialNumber;
}
