package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ListGroupDeviceDtoIn {
	@NotNull(message = "IDG-00000001")
	private String uuidDevice;
}
