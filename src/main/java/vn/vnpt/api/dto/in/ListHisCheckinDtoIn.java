package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

import javax.validation.constraints.NotNull;

@Data
public class ListHisCheckinDtoIn {
	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYYHHMMSS)
	private String startDate;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYYHHMMSS)
	private String endDate;

	private String uuidDevice;

	private String keySearch;

	private String type = ConstantString.LIST_HIS_CHECKIN_TYPE.ALL;

	private String uuidGroup;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String minFirstCheckin = "00:00";

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String maxFirstCheckin = "23:59";

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String minLastCheckin = "00:00";

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String maxLastCheckin = "23:59";
}
