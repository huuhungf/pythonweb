package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

import javax.validation.constraints.NotNull;

@Data
public class ListOrderDtoIn {
	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYY)
	private String startDate;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYY)
	private String endDate;

	private String keySearch;
}
