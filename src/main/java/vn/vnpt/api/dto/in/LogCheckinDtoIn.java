package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;
import vn.vnpt.api.validators.DeviceExist;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class LogCheckinDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@DeviceExist
	private String deviceCode;

	@NotNull(message = "IDG-00000001")
	private MultipartFile imageFile;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYYHHMMSS)
	private String dateCheckin;

	@NotNull(message = "IDG-00000001")
	@Range(min = 1, max = 100, message = "IDG-00000004")
	private Float faceProb;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 1, message = "IDG-00000004")
	private Integer faceMask;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 1, message = "IDG-00000004")
	private Integer faceLiveness;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 10000, message = "IDG-00000004")
	private Integer matchRate = 0;

	private String extraInfo;
}
