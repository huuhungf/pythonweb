package vn.vnpt.api.dto.in;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class MessageNotification {
	private List<String> destEmails;

	private String channelCode;

	private String emailCategory;

	private String messageProperties;

	private String attachmentContent;

	private String attachmentFileName;

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private final MessageNotification messageNotification = new MessageNotification();

		public Builder destEmails(String... emails) {
			this.messageNotification.destEmails = Arrays.asList(emails);
			return this;
		}

		public Builder channelCode(String channelCode) {
			messageNotification.channelCode = channelCode;
			return this;
		}

		public Builder emailCategory(String emailCategory) {
			messageNotification.emailCategory = emailCategory;
			return this;
		}

		public Builder messageProperties(ObjectMapper objectMapper, Map<String, String> properties) {
			String propertiesJson = null;
			try {
				propertiesJson = objectMapper.writeValueAsString(properties);
			} catch (JsonProcessingException e) {
				throw new RuntimeException("could not json message properties: " + properties.entrySet().stream()
						.map(entry -> entry.getKey() + ":" + entry.getValue())
						.collect(Collectors.joining(", ")));
			}
			messageNotification.messageProperties = propertiesJson;
			return this;
		}

		public MessageNotification build() {
			return this.messageNotification;
		}
	}
}
