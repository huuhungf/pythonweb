package vn.vnpt.api.dto.in;

import lombok.Data;

@Data
public class ParamScheduleAccountDtoIn {

	private String keySearch;

	private String uuidGroup;

}
