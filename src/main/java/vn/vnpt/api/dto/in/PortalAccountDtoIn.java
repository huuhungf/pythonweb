package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class PortalAccountDtoIn {

    @NotNull(message = "IDG-00000001")
    @Pattern(message = ErrorCode.IDG_00002010, regexp =  Patterns.IDG_EMAIL)
    private String email;

    @NotNull(message = "IDG-00000001")
    @Pattern(message = ErrorCode.IDG_00000008, regexp = Patterns.IDG_VN_PHONE_NUMBER)
    private String phoneNumber;

}
