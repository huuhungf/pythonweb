package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class PortalBuyMorePlanDtoIn {

    @NotNull(message = "IDG-00000001")
    private String uuidAccount;

    @NotNull(message = "IDG-00000001")
    @Min(value = 0, message = "IDG-00000004")
    @Max(value = 1000, message = "IDG-00000004")
    private Integer addonOtt;

    @NotNull(message = "IDG-00000001")
    @Min(value = 0, message = "IDG-00000004")
    @Max(value = 200, message = "IDG-00000004")
    private Integer addonDevice;

    @NotNull(message = "IDG-00000001")
    @Min(value = 0, message = "IDG-00000004")
    private Integer addonUser;

}
