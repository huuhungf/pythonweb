package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class PortalCreateAccountDtoIn {

	@NotNull(message = "IDG-00000001")
	private String uuidAgent;

	@NotNull(message = "IDG-00000001")
	@Valid
	private PortalAccountDtoIn account;

	@NotNull(message = "IDG-00000001")
	@Valid
	private PortalCompanyDtoIn company;

	@NotNull(message = "IDG-00000001")
	@Valid
	private PortalPlanDtoIn plan;

}
