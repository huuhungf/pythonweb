package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class PortalHisPlanDtoIn {

	@NotNull(message = "IDG-00000001")
	private String uuidAgent;

	private String uuidAccount;

	@Size(max = 255, message = "IDG-00000004")
	private String keySearch;

	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYY)
	private String startDate;

	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.DDMMYYYY)
	private String endDate;
}
