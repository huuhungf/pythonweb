package vn.vnpt.api.dto.in;

import lombok.Data;

@Data
public class PortalListAccountDtoIn {
	private String uuidAgent;

	private String keySearch;
}
