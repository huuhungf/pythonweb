package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class PortalRegisterPlanDtoIn {

	@NotNull(message = "IDG-00000001")
	private String uuidAccount;

	@NotNull(message = "IDG-00000001")
	private Integer planType;

	@NotNull(message = "IDG-00000001")
	@Min(value = 0, message = "IDG-00000004")
	@Max(value = 1000, message = "IDG-00000004")
	private Integer addonOtt = 0;

	@NotNull(message = "IDG-00000001")
	@Min(value = 0, message = "IDG-00000004")
	@Max(value = 200, message = "IDG-00000004")
	private Integer addonDevice = 0;

	@NotNull(message = "IDG-00000001")
	@Min(value = 0, message = "IDG-00000004")
	private Integer addonUser = 0;
}
