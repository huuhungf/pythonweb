package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class ResetPasswordDeviceDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String uuidDevice;

	@NotNull(message = "IDG-00000001")
	@Size(min = 6, max = 50, message = "IDG-00000005")
	@Pattern(regexp = Patterns.IDG_PASSWORD, message = "IDG-00000006")
	private String password;
}
