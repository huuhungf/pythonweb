package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Data
public class ResolveComplaintDtoIn {

	@NotNull(message = "IDG-00000001")
	@Range(min=2 ,max = 3, message = "IDG-00000004")
	private Integer status;
}
