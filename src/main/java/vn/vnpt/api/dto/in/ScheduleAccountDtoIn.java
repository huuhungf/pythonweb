package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ScheduleAccountDtoIn {

	@Size(max = 255, message = "IDG-00000004")
	private String uuidSchedule;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, message = "IDG-00000004")
	private String uuidAccounts;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private String uuidGroup;
}
