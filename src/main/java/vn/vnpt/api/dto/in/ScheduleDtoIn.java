package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
public class ScheduleDtoIn {

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String scheduleName;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 2, message = "IDG-00000004")
	private Integer cycleMode;

	private List<WeekScheduleDtoIn> weekSchedule = new ArrayList<>();

	private List<MonthScheduleDtoIn> monthSchedule = new ArrayList<>();

	private List<YearScheduleDtoIn> yearSchedule = new ArrayList<>();
}
