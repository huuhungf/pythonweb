package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;

import javax.validation.constraints.NotNull;

@Data
public class ShiftAccountDtoIn {

	@NotNull(message = "IDG-00000001")
	private String uuidAccount;

	@ValidateDate(message = "IDG-00000007")
	private String date;

}
