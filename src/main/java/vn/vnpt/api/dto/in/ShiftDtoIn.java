package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ShiftDtoIn {

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String shiftCode;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String onDuty;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String offDuty;

	@Range(min = 0, max = 1, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer dayCount = 0;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String onTimeIn;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String onTimeOut;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String cutIn;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String cutOut;

	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String onLunch;

	@ValidateDate(message = "IDG-00000007", pattern = ConstantString.HHMM)
	private String offLunch;

	@Min(value = 0, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer workingTime = 0;

	@NotNull(message = "IDG-00000001")
	@Min(value = 0, message = "IDG-00000004")
	private Float workingDay = 1.0f;

	@Min(value = 0, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer showPosition = 0;

	@Min(value = 0, message = "IDG-00000004")
	@Max(value = 1, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer isLate = 0;

	@Min(value = 0, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer lateGrace = 0;

	@Min(value = 0, message = "IDG-00000004")
	@Max(value = 1, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer isLateGrace = 0;

	@Min(value = 1, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer roundStepLate = 1;

	@Min(value = 1, message = "IDG-00000004")
	@Max(value = 2, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer roundTypeLate = 1;

	@Min(value = 0, message = "IDG-00000004")
	@Max(value = 1, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer isEarly = 0;

	@Min(value = 0, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer earlyGrace = 0;

	@Min(value = 0, message = "IDG-00000004")
	@Max(value = 1, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer isEarlyGrace = 0;

	@Min(value = 1, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer roundStepEarly = 1;

	@Min(value = 1, message = "IDG-00000004")
	@Max(value = 2, message = "IDG-00000004")
	@NotNull(message = "IDG-00000001")
	private Integer roundTypeEarly = 1;
}
