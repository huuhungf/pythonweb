package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Data
public class UpdateDeviceDtoIn {
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String name;

	@Min(value = 1, message = "IDG-00000004")
	private Integer limitDeviceLicense;
}
