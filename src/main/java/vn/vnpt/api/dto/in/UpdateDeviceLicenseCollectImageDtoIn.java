package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UpdateDeviceLicenseCollectImageDtoIn {
	@NotNull(message = "IDG-00000001")
	private String uuidDevice;

	@Valid
	@NotNull(message = "IDG-00000001")
	private List<DeviceLicenseCollectImage> deviceLicenseConfigs;
}
