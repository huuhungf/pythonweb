package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class UpdateDeviceLicenseDtoIn {
	@NotNull(message = "IDG-00000001")
	private String uuidDevice;

	@NotNull(message = "IDG-00000001")
	private String serialNumber;

	private String name;

	@NotNull(message = "IDG-00000001")
	@Range(min = 1, max = 100, message = "IDG-00000004")
	private Float faceProb;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 1, message = "IDG-00000004")
	private Integer faceMask;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 1, message = "IDG-00000004")
	private Integer faceLiveness;

	@Size(min = 6, max = 6, message = "IDG-00000004")
	@Pattern(regexp = Patterns.IDG_PIN_CODE, message = ErrorCode.IDG_00002013)
	private String pinCode;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 1, message = "IDG-00000004")
	private Integer virtualAssistant;

	@NotNull(message = "IDG-00000001")
	@Range(min = 0, max = 1, message = "IDG-00000004")
	private Integer collectImage;
}
