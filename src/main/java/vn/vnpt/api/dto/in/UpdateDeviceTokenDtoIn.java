package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UpdateDeviceTokenDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String deviceId;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String deviceToken;
}
