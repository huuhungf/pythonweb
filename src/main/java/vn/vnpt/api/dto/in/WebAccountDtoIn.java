package vn.vnpt.api.dto.in;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import vn.vnpt.api.validators.EmailOrPhoneNotEmpty;
import vn.vnpt.common.Common;
import vn.vnpt.common.EnumGender;
import vn.vnpt.common.EnumRole;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;
import vn.vnpt.validate.ValidateString;

import javax.annotation.RegEx;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@EmailOrPhoneNotEmpty
public class WebAccountDtoIn {

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String fullName;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002006, regexp = Patterns.IDG_USER_CODE)
	private String userCode;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@ValidateString(enumClazz = EnumRole.class)
	private String role = EnumRole.USER.name();

	private MultipartFile imageFile;

	@Size(max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002010, regexp = Patterns.IDG_EMAIL)
	private String email;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@ValidateString(enumClazz = EnumGender.class)
	private String gender;

	@NotNull(message = "IDG-00000001")
	private String accountChannels;

	private String uuidGroups;

	@Pattern(regexp = Patterns.IDG_VN_PHONE_NUMBER, message = "IDG-00000006")
	private String phoneNumber;

	public String getGender() {
		return Common.upperCase(gender);
	}

	public String getUserCode() {
		return Common.upperCase(userCode);
	}

	public String getEmail() {
		return Common.lowerCase(email);
	}
}
