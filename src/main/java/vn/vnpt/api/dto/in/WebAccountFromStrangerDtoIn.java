package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.EmailOrPhoneNotEmpty;
import vn.vnpt.common.EnumGender;
import vn.vnpt.common.EnumRole;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;
import vn.vnpt.validate.ValidateString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@EmailOrPhoneNotEmpty
public class WebAccountFromStrangerDtoIn {

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String fullName;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002006, regexp = Patterns.IDG_USER_CODE)
	private String userCode;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@ValidateString(enumClazz = EnumRole.class)
	private String role;

	@Size(min = 0, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002010, regexp = Patterns.IDG_EMAIL)
	private String email;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@ValidateString(enumClazz = EnumGender.class)
	private String gender;

	@NotNull(message = "IDG-00000001")
	private List<AccountChannelDtoIn> accountChannels;

	@NotNull(message = "IDG-00000001")
	private String uuidStranger;

	private List<String> uuidGroups;

	@Pattern(regexp = Patterns.IDG_VN_PHONE_NUMBER, message = "IDG-00000006")
	private String phoneNumber;

	public String getGender() {
		if (gender == null) {
			return null;
		}
		return gender.toUpperCase();
	}

	public String getUserCode() {
		if (userCode == null) {
			return null;
		}

		return userCode.toUpperCase();
	}
}
