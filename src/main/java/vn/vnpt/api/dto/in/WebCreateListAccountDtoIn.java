package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class WebCreateListAccountDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 1000, message = "IDG-00000004")
	private List<WebElementAccountDtoIn> accounts;
}
