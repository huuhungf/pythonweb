package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.EmailOrPhoneNotEmpty;
import vn.vnpt.common.Common;
import vn.vnpt.common.EnumGender;
import vn.vnpt.common.EnumRole;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;
import vn.vnpt.validate.ValidateString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@EmailOrPhoneNotEmpty
public class WebElementAccountDtoIn {

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String fullName;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002006, regexp = Patterns.IDG_USER_CODE)
	private String userCode;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@ValidateString(enumClazz = EnumRole.class)
	private String role = EnumRole.USER.name();

	@Size(min = 0, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002010, regexp = Patterns.IDG_EMAIL)
	private String email;

	@NotNull(message = "IDG-00000001")
	@Valid
	private List<AccountChannelDtoIn> accountChannels;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@ValidateString(enumClazz = EnumGender.class)
	private String gender;

	private String uuidGroups;

	@Pattern(regexp = Patterns.IDG_VN_PHONE_NUMBER, message = "IDG-00000006")
	private String phoneNumber;

	public String getUserCode() {
		return Common.upperCase(userCode);
	}

	public String getEmail() {
		return Common.lowerCase(email);
	}

	public String getGender() {
		return Common.upperCase(gender);
	}
}
