package vn.vnpt.api.dto.in;

import lombok.Data;

@Data
public class WebListAccountDtoIn {
	private String keySearch;
	private Integer imageMode = 0;
	private String uuidGroup;
	private Integer adminGroup = 0;
}
