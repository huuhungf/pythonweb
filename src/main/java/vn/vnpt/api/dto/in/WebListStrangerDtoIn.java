package vn.vnpt.api.dto.in;

import lombok.Data;

@Data
public class WebListStrangerDtoIn {
	private String uuidDevice;

	private String serialNumbers;
}
