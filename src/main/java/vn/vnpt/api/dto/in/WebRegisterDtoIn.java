package vn.vnpt.api.dto.in;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import vn.vnpt.common.Common;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class WebRegisterDtoIn {

	private MultipartFile imageFile;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002010, regexp = Patterns.IDG_EMAIL)
	private String email;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00000008, regexp = Patterns.IDG_VN_PHONE_NUMBER)
	private String phoneNumber;

	@NotNull(message = "IDG-00000001")
	@Size(min = 6, max = 50, message = "IDG-00000005")
	@Pattern(regexp = Patterns.IDG_PASSWORD, message = "IDG-00000006")
	private String password;

	public String getUsername() {
		return Common.upperCase(email);
	}

	public String getEmail() {
		return Common.lowerCase(email);
	}
}
