package vn.vnpt.api.dto.in;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import vn.vnpt.api.validators.EmailOrPhoneNotEmpty;
import vn.vnpt.common.EnumGender;
import vn.vnpt.common.EnumRole;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;
import vn.vnpt.validate.ValidateString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@EmailOrPhoneNotEmpty
public class WebUpdateAccountDtoIn {

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String uuidAccount;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String fullName;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@ValidateString(enumClazz = EnumRole.class)
	private String role;

	private MultipartFile imageFile;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00002010, regexp = Patterns.IDG_EMAIL)
	private String email;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@ValidateString(enumClazz = EnumGender.class)
	private String gender;

	private String accountChannels;

	private String uuidGroups;

	private String uuidAdminGroups;

	@Pattern(regexp = Patterns.IDG_VN_PHONE_NUMBER, message = "IDG-00000006")
	private String phoneNumber;

	public String getGender() {
		if (gender == null) {
			return null;
		}
		return gender.toUpperCase();
	}
}
