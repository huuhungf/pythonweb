package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class WebUpdateAccountFromStrangerDtoIn {

	@NotNull(message = "IDG-00000001")
	private String uuidAccount;

	@NotNull(message = "IDG-00000001")
	private String uuidStranger;
}
