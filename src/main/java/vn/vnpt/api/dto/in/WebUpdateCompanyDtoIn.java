package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.common.Common;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class WebUpdateCompanyDtoIn {
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String name;

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 10, message = "IDG-00000004")
	@Pattern(regexp = Patterns.IDG_DEVICE_CODE, message = ErrorCode.IDG_00002007)
	private String companyCode;

	public String getCompanyCode() {
		return Common.upperCase(companyCode);
	}
}
