package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.pattern.Patterns;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class WebUpdateSuperAccountDtoIn {

	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String fullName;

	private MultipartFile imageFile;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String career;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String company;

	@Size(min = 1, max = 255, message = "IDG-00000004")
	@Pattern(message = ErrorCode.IDG_00000008, regexp = Patterns.IDG_VN_PHONE_NUMBER)
	private String phoneNumber;

	@Range(min = 0, max = 1, message = "IDG-00000004")
	private Integer notification = 0;
}
