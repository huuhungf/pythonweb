package vn.vnpt.api.dto.in;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class WeekScheduleDtoIn {

	@NotNull(message = "IDG-00000001")
	@Min(value = 1, message = "IDG-00000004")
	@Max(value = 7, message = "IDG-00000004")
	private Integer dayId;

	@NotNull(message = "IDG-00000001")
	private String uuidShift;

}
