package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;

@Data
public class WorkingDayDetailDtoIn {

	@ValidateDate(message = "IDG-00000007")
	private String startDate;

	@ValidateDate(message = "IDG-00000007")
	private String endDate;
}
