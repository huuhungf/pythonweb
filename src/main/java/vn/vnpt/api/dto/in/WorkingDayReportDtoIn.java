package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class WorkingDayReportDtoIn {

	@NotNull(message = "IDG-00000001")
	private String uuidAccounts;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007")
	private String startDate;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007")
	private String endDate;
}
