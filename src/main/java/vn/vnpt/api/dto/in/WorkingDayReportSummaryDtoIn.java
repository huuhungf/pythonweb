package vn.vnpt.api.dto.in;

import lombok.Data;
import vn.vnpt.api.validators.ValidateDate;
import vn.vnpt.common.constant.ConstantString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class WorkingDayReportSummaryDtoIn {

	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String uuidGroup;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007")
	private String startDate;

	@NotNull(message = "IDG-00000001")
	@ValidateDate(message = "IDG-00000007")
	private String endDate;

	private String keySearch;

}
