package vn.vnpt.api.dto.in;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class YearScheduleDtoIn {

	@NotNull(message = "IDG-00000001")
	@Min(value = 1, message = "IDG-00000004")
	@Max(value = 12, message = "IDG-00000004")
	private Integer monthId;

	@NotNull(message = "IDG-00000001")
	@Min(value = 1, message = "IDG-00000004")
	@Max(value = 31, message = "IDG-00000004")
	private Integer dayId;

	@NotNull(message = "IDG-00000001")
	private String uuidShift;

}
