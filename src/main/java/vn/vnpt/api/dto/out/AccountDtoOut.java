package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class AccountDtoOut {

	private String uuidAccount;

	private String username;

	private String fullName;

	private String imageUrl;

	private String imageEmbed;

	private String uuidDevice;

	private String userCode;

	private String role;

	private Integer syncStatus;

	private String email;

	private String gender;

	private String audioUrl;
}
