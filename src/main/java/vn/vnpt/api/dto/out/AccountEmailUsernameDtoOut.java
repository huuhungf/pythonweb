package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class AccountEmailUsernameDtoOut {
	private String email;
	private String username;
}
