package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class AccountPlanDtoOut {
	private String uuidAccountPlan;
	private String uuidAccount;
	private String uuidPlan;
	private Integer status;
	private String activeDate;
	private String expireDate;
	private Integer remainDay;
	private String uuidPlanVersion;

	private PlanDtoOut plan;
}
