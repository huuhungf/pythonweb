package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class AccountProfileDtoOut implements AccountProfile {

	private String uuidAccount;

	private String fullName;

	private String userCode;

	private String username;

	private String gender;

	private String email;

	private String imageUrl;

	private String uuidDevice;

	private String deviceCode;

	private String deviceName;

	private String role;

	private String phoneNumber;

	private List<ChannelOttDtoOut> accountChannels;

	private AccountPlanDtoOut accountPlan;

	private List<GroupNode> groups;

	private List<GroupNode> adminGroups;
}
