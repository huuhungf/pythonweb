package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class AppVersionDtoOut {
	@Col(name = "TYPE")
	private String type;

	@Col(name = "LATEST_VERSION_CODE")
	private String latestVersionCode;
}
