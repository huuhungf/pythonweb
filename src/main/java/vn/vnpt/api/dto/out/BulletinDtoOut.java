package vn.vnpt.api.dto.out;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
public class BulletinDtoOut {
	private Integer status;
	private String startDate;
	private String endDate;
	private String startHour;
	private String endHour;
	private String title;
	private String content;
	private JsonNode pageInfo;
}
