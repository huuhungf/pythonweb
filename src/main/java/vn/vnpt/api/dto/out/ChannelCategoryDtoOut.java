package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class ChannelCategoryDtoOut {
	private String uuidChannelCategory;
	private String name;
}
