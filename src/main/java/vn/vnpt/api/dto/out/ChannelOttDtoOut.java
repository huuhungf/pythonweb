package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class ChannelOttDtoOut {
	private String uuidChannelCategory;
	private String channelName;
	private int status;
	private int userOTT;
	private int activeUser;
}
