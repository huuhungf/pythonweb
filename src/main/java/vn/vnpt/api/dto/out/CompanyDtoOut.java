package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class CompanyDtoOut {
	private String uuidCompany;
	private String name;
	private String description;
	private String ownedAccount;
	private String career;
	private Integer notification;
	private String logoName;
	private String companyCode;
}
