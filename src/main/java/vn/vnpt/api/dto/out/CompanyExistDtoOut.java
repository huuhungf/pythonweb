package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class CompanyExistDtoOut {
	private Boolean isExist;
}
