package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.model.ComplaintDetails;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;

@Data
public class ComplaintDetailsDtoOut {

	private String uuidComplaint;
	private String complaintCode;
	private String fullNameComplainer;
	private String userCodeComplainer;
	private String imageUrlComplainer;
	private String genderComplainer;
	private String emailComplainer;
	private String imageUrlComplaint;
	private String dateCheckin;
	private Integer status;
	private Integer type;
	private String userCodeWrongAccount;
	private String fullNameWrongAccount;
	private String imageUrlWrongAccount;
	private String userCodeResolver;
	private String usernameResolver;
	private String fullNameResolver;
	private String emailResolver;
	private String updatedDate;
	private String transId;

	public ComplaintDetailsDtoOut(ComplaintDetails details) {
		this.uuidComplaint = details.getUuidComplaint();
		this.complaintCode = details.getComplaintCode();
		this.transId = details.getTransId();

		this.fullNameComplainer = details.getFullNameComplainer();
		this.userCodeComplainer = details.getUserCodeComplainer();
		this.imageUrlComplainer = details.getImageUrlComplainer();
		this.genderComplainer = details.getGenderComplainer();
		this.emailComplainer = details.getEmailComplainer();

		this.imageUrlComplaint = details.getImageUrlComplaint();
		this.dateCheckin = details.getDateCheckin();
		this.status = details.getStatus();
		this.type = details.getType();
		this.updatedDate = details.getUpdatedDate();

		this.userCodeWrongAccount = details.getUserCodeWrongAccount();
		this.fullNameWrongAccount = details.getFullNameWrongAccount();
		this.imageUrlWrongAccount = details.getImageUrlWrongAccount();

		this.userCodeResolver = details.getUserCodeResolver();
		this.usernameResolver = details.getUsernameResolver();
		this.fullNameResolver = details.getFullNameResolver();
		this.emailResolver = details.getEmailResolver();

		if (details.getStatus() != ConstantString.STATUS_COMPLAINT.PENDING) {
			if (Common.isNullOrEmpty(details.getUuidResolver())) {
				this.fullNameResolver = ConstantString.RESOLVER_SYSTEM;
			} else {
				if (Common.isNullOrEmpty(details.getRoleResolver())) {
					this.fullNameResolver = ConstantString.RESOLVER_DELETED;
				} else {
					if (ConstantString.ROLE.SUPERADMIN.equals(details.getRoleResolver())
							|| ConstantString.ROLE.OWNER.equals(details.getRoleResolver())) {
						this.fullNameResolver = ConstantString.RESOLVER_ADMIN;
						this.userCodeResolver = null;
					}
				}
			}
		}

	}

}
