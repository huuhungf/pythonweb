package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.model.Complaint;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;

@Data
public class ComplaintDtoOut {

	private String uuidComplaint;
	private String complaintCode;
	private String fullNameComplainer;
	private String userCodeComplainer;
	private String imageUrlComplainer;
	private String genderComplainer;
	private String emailComplainer;
	private String dateCheckin;
	private Integer status;
	private Integer type;
	private String fullNameResolver;
	private String userCodeResolver;
	private String usernameResolver;
	private String emailResolver;
	private String transId;

	public ComplaintDtoOut() {
	}

	public ComplaintDtoOut(Complaint complaint) {
		this.fullNameComplainer = complaint.getFullNameComplainer();
		this.userCodeComplainer = complaint.getUserCodeComplainer();
		this.imageUrlComplainer = complaint.getImageUrlComplainer();
		this.genderComplainer = complaint.getGenderComplainer();
		this.emailComplainer = complaint.getEmailComplainer();

		this.fullNameResolver = complaint.getFullNameResolver();
		this.userCodeResolver = complaint.getUserCodeResolver();
		this.usernameResolver = complaint.getUsernameResolver();
		this.emailResolver = complaint.getEmailResolver();

		if (complaint.getStatus() != ConstantString.STATUS_COMPLAINT.PENDING) {
			if (Common.isNullOrEmpty(complaint.getUuidResolver())) {
				this.fullNameResolver = ConstantString.RESOLVER_SYSTEM;
			} else {
				if (Common.isNullOrEmpty(complaint.getRoleResolver())) {
					this.fullNameResolver = ConstantString.RESOLVER_DELETED;
				} else {
					if (ConstantString.ROLE.SUPERADMIN.equals(complaint.getRoleResolver())
							|| ConstantString.ROLE.OWNER.equals(complaint.getRoleResolver())) {
						this.fullNameResolver = ConstantString.RESOLVER_ADMIN;
						this.userCodeResolver = null;
					}
				}
			}
		}

		this.uuidComplaint = complaint.getUuidComplaint();
		this.complaintCode = complaint.getComplaintCode();
		this.dateCheckin = complaint.getDateCheckin();
		this.status = complaint.getStatus();
		this.type = complaint.getType();
		this.transId = complaint.getTransId();
	}
}
