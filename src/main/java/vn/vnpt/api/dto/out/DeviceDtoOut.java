package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class DeviceDtoOut {
	@Col(name = "UUID_DEVICE")
	private String uuidDevice;

	@Col(name = "NAME")
	private String name;

	@Col(name = "DEVICE_CODE")
	private String deviceCode;

	@Col(name = "TYPE")
	private String type;

	@Col(name = "STATUS")
	private Integer status;

	@Col(name = "PUBLIC_KEY")
	private String publicKey;

	@Col(name = "TOTAL_DEVICE_LICENSE")
	private Integer deviceLicenseTotal = 0;

	@Col(name = "LIMIT_DEVICE_LICENSE")
	private Integer limitDeviceLicense = 0;
}
