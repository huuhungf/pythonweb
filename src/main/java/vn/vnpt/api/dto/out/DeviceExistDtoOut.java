package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class DeviceExistDtoOut {
	private Boolean isExist;
}
