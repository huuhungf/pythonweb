package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class DeviceLicenseDtoOut {
	private String uuidDevice;
	private String name;
	private String serialNumber;
	private String licenseKey;
	private Integer status;
	private Float faceProb;
	private Integer faceMask;
	private Integer faceLiveness;
	private String lastActive;
	private Integer collectImage;
	private String pinCode;
	private Integer virtualAssistant;
	private Integer battery;
	private Float latitude;
	private Float longitude;
	private String wifi;
	private String os;
	private String appVersion;
}
