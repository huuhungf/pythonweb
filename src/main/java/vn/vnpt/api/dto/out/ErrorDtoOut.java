package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ErrorDtoOut {
    private String userCode;
    private List<FieldError> fields;
}
