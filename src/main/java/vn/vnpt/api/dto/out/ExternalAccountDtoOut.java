package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class ExternalAccountDtoOut {
	@Col(name = "UUID_ACCOUNT")
	private String uuidAccount;

	@Col(name = "USER_CODE")
	private String userCode;

	@Col(name = "GENDER")
	private String gender;

	@Col(name = "FULL_NAME")
	private String fullName;

	@Col(name = "IMAGE_URL")
	private String imageUrl;

	@Col(name = "LAST_UPDATE")
	private String lastUpdate;
}
