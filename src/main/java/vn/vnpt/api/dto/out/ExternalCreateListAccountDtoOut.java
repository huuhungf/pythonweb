package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ExternalCreateListAccountDtoOut {
    private List<ErrorDtoOut> errors = new ArrayList<>();
}
