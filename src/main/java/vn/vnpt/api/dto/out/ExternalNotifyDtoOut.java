package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ExternalNotifyDtoOut {
	private List<ExternalOttDtoOut> data;
}
