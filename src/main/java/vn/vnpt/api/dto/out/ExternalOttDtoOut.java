package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class ExternalOttDtoOut {
	private String userCode;
	private String channel;
}
