package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class ExternalRegisterDtoOut {
	private String uuidAccount;
	private String email;
	private String uuidPlan;
	private String planName;
	private Integer status;
	private String activeDate;
	private String expireDate;
}
