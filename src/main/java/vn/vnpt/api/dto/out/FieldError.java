package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class FieldError {
	private String name;
	private List<String> errorCodes;
}
