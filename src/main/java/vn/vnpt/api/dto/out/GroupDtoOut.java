package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class GroupDtoOut {
	private String uuidGroup;
	private String name;
	private String uuidDevice;
	private String uuidParentGroup;
	private String uuidCompany;
}
