package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class GroupNode {
	@Col(name = "LVL")
	private Integer lvl;

	@Col(name = "UUID_GROUP")
	private String uuidGroup;

	@Col(name = "NAME")
	private String name;

	@Col(name = "UUID_DEVICE")
	private String uuidDevice;

	@Col(name = "UUID_PARENT_GROUP")
	private String uuidParentGroup;

	@Col(name = "UUID_COMPANY")
	private String uuidCompany;

	@Col(name = "NUM_ACCOUNT")
	private Integer numAccount = 0;

	@Col(name = "DEVICE_CODE")
	private String deviceCode;
}
