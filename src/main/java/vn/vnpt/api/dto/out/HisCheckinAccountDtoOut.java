package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class HisCheckinAccountDtoOut {

	@Col(name = "DATE_CHECKIN")
	private String dateCheckin;

	@Col(name = "USERNAME_CREATOR")
	private String creatorUsername;

	@Col(name = "CREATED_DATE")
	private String createdDate;
}
