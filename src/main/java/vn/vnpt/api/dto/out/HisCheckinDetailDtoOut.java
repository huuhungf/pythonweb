package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class HisCheckinDetailDtoOut {
	@Col(name = "FULL_NAME")
	private String fullName;

	@Col(name = "USER_CODE")
	private String userCode;

	@Col(name = "USERNAME")
	private String username;

	private String role;

	@Col(name = "IMAGE_URL")
	private String imageUrl;

	@Col(name = "DEVICE_CODE")
	private String deviceCode;

	@Col(name = "SERIAL_NUMBER")
	private String serialNumber;

	@Col(name = "TABLET_NAME")
	private String tabletName;

	@Col(name = "CHECKIN_TYPE")
	private Integer checkinType;

	@Col(name = "DATE_CHECKIN")
	private String dateCheckin;

	@Col(name = "HOUR_CHECKIN")
	private String hourCheckin;

	@Col(name = "FACE_PROB")
	private Float faceProb = 0f;

	@Col(name = "FACE_MASK")
	private Integer faceMask = 0;

	@Col(name = "FACE_LIVENESS")
	private Integer faceLiveness = 0;

	@Col(name = "MATCH_RATE")
	private Integer matchRate = 0;

	@Col(name = "CHANNEL_ID")
	private Integer channelId;

	@Col(name = "LATITUDE")
	private Float latitude;

	@Col(name = "LONGITUDE")
	private Float longitude;

	@Col(name = "REASON")
	private String reason;
}
