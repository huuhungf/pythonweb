package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class HisCheckinExternalDtoOut {

    private String dateCheckin;

    private String userCode;

    private String uuidHisCheckin;

    private String createdDate;
}
