package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ListDeviceLicenseDtoOut {
	private List<DeviceLicenseDtoOut> data;
}
