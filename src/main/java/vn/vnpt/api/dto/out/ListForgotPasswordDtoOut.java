package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ListForgotPasswordDtoOut {
	private List<AccountEmailUsernameDtoOut> data;
}
