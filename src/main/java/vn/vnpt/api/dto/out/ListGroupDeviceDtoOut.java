package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ListGroupDeviceDtoOut {
	private String uuidDevice;

	private List<GroupNode> groups;
}
