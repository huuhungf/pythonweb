package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ListGroupDtoOut {
	private List<GroupNode> groups;
}
