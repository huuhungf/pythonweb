package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ListGroupTreeDtoOut {
	private List<GroupNode> groups;
}
