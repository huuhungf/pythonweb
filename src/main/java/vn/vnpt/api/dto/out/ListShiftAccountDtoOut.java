package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ListShiftAccountDtoOut {

	private List<ShiftAccountDtoOut> permanentShifts;

	private List<ShiftAccountDtoOut> tmpShifts;
}
