package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.common.Common;

@Data
public class ListWorkingDayDetailDtoOut {

	private String date;

	private int dayId;

	private int workingStatus = Common.WORKING_DAY_STATUS.NO_SHIFT;

}
