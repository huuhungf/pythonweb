package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class MScheduleDtoOut {

	private Integer dayId;

	private String uuidShift;

	private String shiftCode;

}
