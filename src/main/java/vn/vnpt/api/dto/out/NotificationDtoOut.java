package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class NotificationDtoOut {
	private String fullName;
	private String imageUrl;
	private Integer checkinType;
	private String dateCheckin;
	private String userCode;
}
