package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class OrderDtoOut {
	private String fullName;
	private String username;
	private String email;
	private String planName;
	private Integer userAmount;
	private Integer deviceAmount;
	private Integer ottAmount;
	private String orderDate;
	private Integer orderType;
	private String startDate;
	private String endDate;
	private String companyName;
}
