package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class PlanDtoOut {
	private String uuidPlan;
	private Integer type;
	private String name;
	private String description;
}
