package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class PortalAccountDtoOut {
	@Col(name = "UUID_ACCOUNT")
	private String uuidAccount;

	@Col(name = "COMPANY_NAME")
	private String companyName;

	@Col(name = "EMAIL")
	private String email;

	@Col(name = "PLAN_TYPE")
	private Integer planType;

	@Col(name = "PLAN_NAME")
	private String planName;

	@Col(name = "NUM_USER")
	private Integer numUser;

	@Col(name = "NUM_DEVICE")
	private Integer numDevice;

	@Col(name = "NUM_OTT")
	private Integer numOtt;

	@Col(name = "ACTIVE_DATE")
	private String activeDate;

	@Col(name = "EXPIRE_DATE")
	private String expireDate;

	@Col(name = "UUID_AGENT")
	private String uuidAgent;

	@Col(name = "FREE_DEVICE")
	private Integer freeDevice;

	@Col(name = "FREE_OTT")
	private Integer freeOtt;
}
