package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class PortalAccountInfoDtoOut {

	private String uuidAccount;

	private String companyName;

	private String email;

	private String phoneNumber;

	private String address;
}
