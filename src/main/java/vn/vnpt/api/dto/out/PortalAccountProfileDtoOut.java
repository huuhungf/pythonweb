package vn.vnpt.api.dto.out;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class PortalAccountProfileDtoOut {

	private PortalAccountInfoDtoOut account;

	private PortalPlanInfoDtoOut plan;

}
