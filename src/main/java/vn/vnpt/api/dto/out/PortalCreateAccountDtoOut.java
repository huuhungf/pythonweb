package vn.vnpt.api.dto.out;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PortalCreateAccountDtoOut {

	private String uuidCustomerService;

}
