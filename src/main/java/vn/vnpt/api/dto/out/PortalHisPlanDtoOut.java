package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class PortalHisPlanDtoOut {

	@Col(name = "COMPANY_NAME")
	private String companyName;

	@Col(name = "EMAIL")
	private String email;

	@Col(name = "PLAN_TYPE")
	private String planType;

	@Col(name = "PLAN_NAME")
	private String planName;

	@Col(name = "NUM_USER")
	private Integer numUser;

	@Col(name = "NUM_DEVICE")
	private Integer numDevice;

	@Col(name = "NUM_OTT")
	private Integer numOtt;

	@Col(name = "ACTIVE_DATE")
	private String activeDate;

	@Col(name = "EXPIRE_DATE")
	private String expireDate;

	@Col(name = "ORDER_DATE")
	private String orderDate;

	@Col(name = "ORDER_TYPE")
	private Integer orderType;

	@Col(name = "UUID_AGENT")
	private String uuidAgent;

}
