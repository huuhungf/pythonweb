package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class PortalPlanInfoDtoOut {

	private Integer planType;

	private String planName;

	private Integer limitUser;

	private Integer numOtt;

	private Integer freeDevice;

	private Integer addonUser;

	private Integer addonDevice;

	private Integer addonOtt;

	private String activeDate;

	private String expireDate;
}
