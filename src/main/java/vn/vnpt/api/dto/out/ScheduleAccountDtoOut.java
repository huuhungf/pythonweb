package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class ScheduleAccountDtoOut {

	private String uuidAccount;
	private String fullName;
	private String userCode;
	private String imageUrl;
	private String gender;
	private Integer locked;
	private String uuidSchedule;
	private String scheduleName;
	private Integer cycleMode;

}
