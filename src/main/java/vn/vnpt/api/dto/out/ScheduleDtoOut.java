package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class ScheduleDtoOut {

	private String uuidSchedule;
	private String scheduleName;
	private Integer cycleMode;

	private List<WScheduleDtoOut> weekSchedule;

	private List<MScheduleDtoOut> monthSchedule;

	private List<YScheduleDtoOut> yearSchedule;
}
