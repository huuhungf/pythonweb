package vn.vnpt.api.dto.out;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ShiftAccountDtoOut {

	private String uuidShift;

	private String shiftCode;

	private String onDuty;

	private String offDuty;

}
