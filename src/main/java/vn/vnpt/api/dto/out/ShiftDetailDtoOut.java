package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.model.Shift;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;

@Data
public class ShiftDetailDtoOut {

	private String uuidShift;
	private String shiftCode;
	private String onDuty;
	private String offDuty;
	private Integer dayCount;
	private String onTimeIn;
	private String onTimeOut;
	private String cutIn;
	private String cutOut;
	private String onLunch;
	private String offLunch;
	private Integer workingTime;
	private Float workingDay;
	private Integer noInWT;
	private Integer noOutWT;
	private Integer showPosition;
	private Integer isLate;
	private Integer lateGrace;
	private Integer isLateGrace;
	private Integer roundStepLate;
	private Integer roundTypeLate;
	private Integer isEarly;
	private Integer earlyGrace;
	private Integer isEarlyGrace;
	private Integer roundStepEarly;
	private Integer roundTypeEarly;

	public static ShiftDetailDtoOut from(Shift shift) {
		Integer onDuty = Common.getTimeInSec(shift.getOnDuty());
		Integer onTimeIn = onDuty - shift.getOnTimeIn() * 60;
		if (onTimeIn < 0) {
			onTimeIn += ConstantString.SECONDS_IN_A_DAY;
		}
		Integer cutIn = onDuty + shift.getCutIn() * 60;
		if (cutIn > ConstantString.SECONDS_IN_A_DAY) {
			cutIn -= ConstantString.SECONDS_IN_A_DAY;
		}

		Integer offDuty = Common.getTimeInSec(shift.getOffDuty());
		Integer onTimeOut = offDuty - shift.getOnTimeOut() * 60;
		if (onTimeOut < 0) {
			onTimeOut += ConstantString.SECONDS_IN_A_DAY;
		}
		Integer cutOut = offDuty + shift.getCutOut() * 60;
		if (cutOut > ConstantString.SECONDS_IN_A_DAY) {
			cutOut -= ConstantString.SECONDS_IN_A_DAY;
		}

		ShiftDetailDtoOut dto = new ShiftDetailDtoOut();
		dto.setUuidShift(shift.getUuidShift());
		dto.setShiftCode(shift.getShiftCode());
		dto.setOnDuty(shift.getOnDuty());
		dto.setOffDuty(shift.getOffDuty());
		dto.setDayCount(shift.getDayCount());
		dto.setOnTimeIn(Common.convertSecToHhMm(onTimeIn));
		dto.setCutIn(Common.convertSecToHhMm(cutIn));
		dto.setOnTimeOut(Common.convertSecToHhMm(onTimeOut));
		dto.setCutOut(Common.convertSecToHhMm(cutOut));
		dto.setOnLunch(shift.getOnLunch());
		dto.setOffLunch(shift.getOffLunch());
		dto.setWorkingTime(shift.getWorkingTime());
		dto.setWorkingDay(shift.getWorkingDay());
		dto.setShowPosition(shift.getShowPosition());
		dto.setIsLate(shift.getIsLate());
		dto.setLateGrace(shift.getLateGrace());
		dto.setIsLateGrace(shift.getIsLateGrace());
		dto.setRoundStepLate(shift.getRoundStepLate());
		dto.setRoundTypeLate(shift.getRoundTypeLate());
		dto.setIsEarly(shift.getIsEarly());
		dto.setEarlyGrace(shift.getEarlyGrace());
		dto.setIsEarlyGrace(shift.getIsEarlyGrace());
		dto.setRoundStepEarly(shift.getRoundStepEarly());
		dto.setRoundTypeEarly(shift.getRoundTypeEarly());
		return dto;
	}
}
