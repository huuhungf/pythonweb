package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class ShiftStatsDtoOut {
	private String uuidShift;
	private String shiftCode;
	private Long workingTime;
	private Float workingDay;
}
