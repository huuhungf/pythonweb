package vn.vnpt.api.dto.out;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import vn.vnpt.api.repository.Col;
import vn.vnpt.common.constant.ConstantString;

import java.util.Date;

@Data
public class StrangerDtoOut {

	@Col(name = "UUID_STRANGER")
	private String uuidStranger;

	@Col(name = "UUID_DEVICE")
	private String uuidDevice;

	@Col(name = "DEVICE_CODE")
	private String deviceCode;

	@Col(name = "SERIAL_NUMBER")
	private String serialNumber;

	@Col(name = "TABLET_NAME")
	private String tabletName;

	@Col(name = "IMAGE_URL")
	private String imageUrl;

	@Col(name = "DATE_CHECKIN")
	@JsonFormat(pattern = ConstantString.DDMMYYYYHHMM, timezone = ConstantString.TIME_ZONE_GMT7)
	private Date dateCheckin;
}
