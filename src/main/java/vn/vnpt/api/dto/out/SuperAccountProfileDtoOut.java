package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class SuperAccountProfileDtoOut implements AccountProfile {

	private String uuidAccount;

	private String fullName;

	private String userCode;

	private String username;

	private String gender;

	private String email;

	private String imageUrl;

	private String role;

	private String phoneNumber;

	private CompanyDtoOut company;

	private AccountPlanDtoOut accountPlan;
}
