package vn.vnpt.api.dto.out;

import lombok.Data;
import lombok.EqualsAndHashCode;
import vn.vnpt.common.success.model.PagingDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class SyncPagingDTO<T> extends PagingDTO<T> {
	private String lastSyncDate;
}
