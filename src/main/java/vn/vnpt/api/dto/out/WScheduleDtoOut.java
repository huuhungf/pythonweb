package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.model.Shift;

import java.util.List;

@Data
public class WScheduleDtoOut {

	private Integer dayId;

	private String uuidShift;

	private String shiftCode;

}
