package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class WebAccountDtoOut {

	private String uuidAccount;

	private String fullName;

	private String userCode;

	private String username;

	private String imageUrl;

	private String role;

	private String gender;

	private String email;

	private String audioUrl;

	private Integer locked;

	private List<String> channels;

	private Integer isAdmin;

	private String phoneNumber;
}
