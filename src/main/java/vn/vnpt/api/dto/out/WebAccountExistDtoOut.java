package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class WebAccountExistDtoOut {
	private Boolean isExist;
}
