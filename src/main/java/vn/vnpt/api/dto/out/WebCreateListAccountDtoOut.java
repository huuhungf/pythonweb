package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class WebCreateListAccountDtoOut {
	private List<WebErrorDtoOut> errors = new ArrayList<>();
}
