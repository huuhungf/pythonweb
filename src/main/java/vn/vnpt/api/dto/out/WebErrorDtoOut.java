package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class WebErrorDtoOut {
	private String userCode;
	private List<FieldError> fields = new ArrayList<>();
}


