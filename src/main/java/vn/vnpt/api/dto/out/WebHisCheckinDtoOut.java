package vn.vnpt.api.dto.out;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class WebHisCheckinDtoOut {
	@Col(name = "UUID_ACCOUNT")
	private String uuidAccount;

	@Col(name = "FULL_NAME")
	private String fullName;

	@Col(name = "USER_CODE")
	private String userCode;

	@Col(name = "USERNAME")
	private String username;

	@Col(name = "ROLE_NAME")
	private String role;

	@Col(name = "DATE_CHECKIN")
	private String dateCheckin;

	@Col(name = "FIRST_CHECKIN")
	private String firstCheckin;

	@Col(name = "LAST_CHECKIN")
	private String lastCheckin;

	@Col(name = "TOTAL_CHECKIN")
	private Integer totalCheckin = 0;
}
