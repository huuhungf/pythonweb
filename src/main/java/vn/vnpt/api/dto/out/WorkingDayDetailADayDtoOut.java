package vn.vnpt.api.dto.out;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class WorkingDayDetailADayDtoOut {

	private String userCode;
	private String fullName;
	private Integer workingStatus;
	private List<WorkingStatAccountDtoOut> workingStats;
	private List<HisCheckinAccountDtoOut> hisCheckins;

}
