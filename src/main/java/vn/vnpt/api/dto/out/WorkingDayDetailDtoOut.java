package vn.vnpt.api.dto.out;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class WorkingDayDetailDtoOut {

	private String uuidAccount;

	private String userCode;

	private String fullName;

	private List<ListWorkingDayDetailDtoOut> data;

}
