package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class WorkingDayReportDtoOut {

	private String uuidAccount;
	private String fullName;
	private String userCode;
	private String date;
	private Integer dayId;
	private String uuidShift;
	private String shiftCode;
	private String checkinIn;
	private String checkinOut;
	private Long workingTime;
	private Float workingDay;
	private Long lateTime;
	private Long earlyTime;
	// 0: normal, 1: row of sum
	private Integer rowType = 0;

}
