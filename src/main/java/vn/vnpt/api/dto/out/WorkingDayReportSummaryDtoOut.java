package vn.vnpt.api.dto.out;

import lombok.Data;

import java.util.List;

@Data
public class WorkingDayReportSummaryDtoOut {

	private String uuidAccount;
	private String fullName;
	private String userCode;
	private String uuidSchedule;
	private String scheduleName;
	private Long workingTime;
	private Float workingDay;
	private List<ShiftStatsDtoOut> shiftStats;
}
