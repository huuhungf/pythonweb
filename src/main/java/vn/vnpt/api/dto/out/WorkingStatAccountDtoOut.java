package vn.vnpt.api.dto.out;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WorkingStatAccountDtoOut {

	private String uuidShift;
	private String shiftCode;
	private String checkIn;
	private String checkOut;
	private Integer workingTime;
	private Float workingDay;
	private Integer lateTime;
	private Integer earlyTime;

}
