package vn.vnpt.api.dto.out;

import lombok.Data;

@Data
public class YScheduleDtoOut {

	private Integer monthId;

	private Integer dayId;

	private String uuidShift;

	private String shiftCode;

}
