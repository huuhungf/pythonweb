package vn.vnpt.api.mapper;

import vn.vnpt.api.dto.out.CompanyDtoOut;
import vn.vnpt.api.model.Company;

public interface CompanyMapper {
	CompanyDtoOut map(Company company);
}
