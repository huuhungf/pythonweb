package vn.vnpt.api.mapper;

import vn.vnpt.api.dto.out.CompanyDtoOut;
import vn.vnpt.api.model.Company;

public class CompanyMapperImpl implements CompanyMapper {
	@Override
	public CompanyDtoOut map(Company company) {
		CompanyDtoOut companyDtoOut = new CompanyDtoOut();
		companyDtoOut.setUuidCompany(company.getUuidCompany());
		companyDtoOut.setName(company.getName());
		companyDtoOut.setDescription(company.getDescription());
		companyDtoOut.setOwnedAccount(company.getOwnedAccount());
		companyDtoOut.setCareer(company.getCareer());
		companyDtoOut.setNotification(company.getNotification());
		companyDtoOut.setLogoName(company.getLogoName());
		companyDtoOut.setCompanyCode(company.getCompanyCode());

		return companyDtoOut;
	}
}
