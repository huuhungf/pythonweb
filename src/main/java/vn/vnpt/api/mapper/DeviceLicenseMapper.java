package vn.vnpt.api.mapper;


import vn.vnpt.api.dto.out.DeviceLicenseDtoOut;
import vn.vnpt.api.model.DeviceLicense;

public interface DeviceLicenseMapper {
	DeviceLicenseDtoOut map(DeviceLicense deviceLicense);
}
