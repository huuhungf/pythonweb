package vn.vnpt.api.mapper;

import vn.vnpt.api.dto.out.DeviceLicenseDtoOut;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;

public class DeviceLicenseMapperImpl implements DeviceLicenseMapper {
	@Override
	public DeviceLicenseDtoOut map(DeviceLicense deviceLicense) {
		DeviceLicenseDtoOut deviceLicenseDtoOut = new DeviceLicenseDtoOut();
		deviceLicenseDtoOut.setUuidDevice(deviceLicense.getUuidDevice());
		deviceLicenseDtoOut.setSerialNumber(deviceLicense.getSerialNumber());
		deviceLicenseDtoOut.setName(deviceLicense.getName());
		deviceLicenseDtoOut.setStatus(deviceLicense.getStatus());
		deviceLicenseDtoOut.setLicenseKey(deviceLicense.getLicenseKey());
		deviceLicenseDtoOut.setFaceProb(deviceLicense.getFaceProb());
		deviceLicenseDtoOut.setFaceMask(deviceLicense.getFaceMask());
		deviceLicenseDtoOut.setFaceLiveness(deviceLicense.getFaceLiveness());
		deviceLicenseDtoOut.setLastActive(Common.convertDateToString(deviceLicense.getLastUpdate(), ConstantString.DDMMYYYYHHMM));
		deviceLicenseDtoOut.setCollectImage(deviceLicense.getCollectImage());
		deviceLicenseDtoOut.setPinCode(deviceLicense.getPinCode());
		deviceLicenseDtoOut.setVirtualAssistant(deviceLicense.getVirtualAssistant());
		return deviceLicenseDtoOut;
	}
}
