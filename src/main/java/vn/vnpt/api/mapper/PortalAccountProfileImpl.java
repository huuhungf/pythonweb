package vn.vnpt.api.mapper;

import vn.vnpt.api.dto.out.PortalAccountInfoDtoOut;
import vn.vnpt.api.dto.out.PortalAccountProfileDtoOut;
import vn.vnpt.api.dto.out.PortalPlanInfoDtoOut;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.AccountPlan;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Plan;
import vn.vnpt.common.Common;

public class PortalAccountProfileImpl implements PortalAccountProfileMapper{
	@Override
	public PortalAccountInfoDtoOut map(Account account, Company company) {
		PortalAccountInfoDtoOut accountInfo = new PortalAccountInfoDtoOut();
		accountInfo.setUuidAccount(account.getUuidAccount());
		accountInfo.setCompanyName(company.getName());
		accountInfo.setEmail(account.getEmail());
		accountInfo.setAddress(company.getAddress());
		accountInfo.setPhoneNumber(account.getPhoneNumber());
		return accountInfo;
	}

	@Override
	public PortalPlanInfoDtoOut map(Plan plan, AccountPlan accountPlan) {
		PortalPlanInfoDtoOut planInfo = new PortalPlanInfoDtoOut();
		planInfo.setPlanType(plan.getType());
		planInfo.setPlanName(plan.getName());
		planInfo.setLimitUser(accountPlan.getLimitUser());
		planInfo.setNumOtt(accountPlan.getLimitUser());
		planInfo.setFreeDevice(accountPlan.getFreeDevice());
		planInfo.setAddonUser(accountPlan.getAddonUser());
		planInfo.setAddonDevice(accountPlan.getAddonDevice());
		planInfo.setAddonOtt(accountPlan.getAddonOtt());
		planInfo.setActiveDate(accountPlan.getActiveDate().toString());
		planInfo.setExpireDate(accountPlan.getExpireDate().toString());
		return planInfo;
	}
}
