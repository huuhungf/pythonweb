package vn.vnpt.api.mapper;

import vn.vnpt.api.dto.out.PortalAccountInfoDtoOut;
import vn.vnpt.api.dto.out.PortalPlanInfoDtoOut;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.AccountPlan;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Plan;

public interface PortalAccountProfileMapper {
	PortalAccountInfoDtoOut map(Account account, Company company);

	PortalPlanInfoDtoOut map(Plan plan, AccountPlan accountPlan);
}
