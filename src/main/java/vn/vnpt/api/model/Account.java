/*******************************************************************************
 * Copyright (c) 2017 ANHTCN.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package vn.vnpt.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import vn.vnpt.api.repository.Col;

/**
 * @author phuongha
 */
@Data
public class Account {

	@Col(name = "UUID_ACCOUNT")
	private String uuidAccount;

	@Col(name = "USERNAME")
	private String username;

	@Col(name = "PASSWORD")
	@JsonIgnore
	private String password;

	@Col(name = "BIRTHDAY")
	private String birthday;

	@Col(name = "FIRST_NAME")
	private String firstName;

	@Col(name = "LAST_NAME")
	private String lastName;

	@Col(name = "FULL_NAME")
	private String fullName;

	@Col(name = "LOCATION")
	private String location = "";

	@Col(name = "PHONE_NUMBER")
	private String phoneNumber;

	@Col(name = "GENDER")
	private String gender;

	@Col(name = "PROVIDER")
	private String provider = "WEB";

	@Col(name = "ENABLED")
	private boolean enabled = false;

	@Col(name = "CREDENTIALS_EXPIRED")
	private boolean credentialsExpired = false;

	@Col(name = "EXPIRED")
	private boolean expired = false;

	@Col(name = "LOCKED")
	private boolean locked = false;

	@Col(name = "USING2FA")
	private boolean using2FA = false;

	@Col(name = "IMAGE_URL")
	private String imageUrl;

	@Col(name = "IMAGE_EMBED")
	private String imageEmbed;

	@Col(name = "UUID_DEVICE")
	private String uuidDevice;

	@Col(name = "USER_CODE")
	private String userCode;

	@Col(name = "EMAIL")
	private String email;

	@Col(name = "AUDIO_URL")
	private String audioUrl;

	@Col(name = "UUID_GREETING")
	private String uuidGreeting;

	@Col(name = "UUID_POST_GREETING")
	private String uuidPostGreeting;

	@Col(name = "IMAGE_TYPE")
	private Integer imageType;
}
