package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class AccountChannel {
	private String uuidAccount;
	private String uuidChannelCategory;
	private String channelName;
	private Integer status;
	private Integer userOtt;
	private Date lastUpdate;
}
