package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

/*
UUID_GROUP, UUID_ACCOUNT, CREATE_DATE
 */
@Data
public class AccountGroup {
	private String uuidGroup;
	private String uuidAccount;
	private Date createDate;
}
