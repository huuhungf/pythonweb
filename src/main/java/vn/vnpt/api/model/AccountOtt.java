package vn.vnpt.api.model;

import lombok.Data;

@Data
public class AccountOtt {
	private String uuidAccount;
	private String channel;
	private String botId;
	private String userId;
	private Integer status;
}
