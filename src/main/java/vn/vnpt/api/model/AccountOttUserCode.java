package vn.vnpt.api.model;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class AccountOttUserCode {
	@Col(name = "UUID_ACCOUNT")
	private String uuidAccount;

	@Col(name = "BOT_ID")
	private String botId;

	@Col(name = "USER_ID")
	private String userId;

	@Col(name = "STATUS")
	private Integer status;

	@Col(name = "USER_CODE")
	private String userCode;

	@Col(name = "CHANNEL_NAME")
	private String channelName;
}
