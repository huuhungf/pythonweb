package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

/*
UUID_ACCOUNT, UUID_PLAN, STATUS, CREATED_DATE, UPDATED_DATE, ACTIVE_DATE
 */
@Data
public class AccountPlan {
	private String uuidAccountPlan;
	private String uuidAccount;
	private String uuidPlan;
	private Integer status;
	private Date createdDate;
	private Date updatedDate;
	private Date activeDate;
	private Date expireDate;
	private String uuidPlanVersion;
	private Integer limitUser;
	private Integer addonDevice;
	private Integer freeDevice;
	private Integer addonOtt;
	private Integer addonUser;
}
