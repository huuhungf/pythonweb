package vn.vnpt.api.model;

import lombok.Data;

@Data
public class AccountSchedule {
	private String uuidAccount;
	private String uuidSchedule;
}
