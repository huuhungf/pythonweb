package vn.vnpt.api.model;

import lombok.Data;

@Data
public class AccountTempShift {
	private String uuidAccount;
	private String uuidShift;
	private String tempShiftDate;
}
