package vn.vnpt.api.model;

import lombok.Data;
import vn.vnpt.api.repository.Col;

import java.util.Date;

/*
UUID_GROUP, UUID_ACCOUNT
 */
@Data
public class AdminGroup {
	@Col(name = "UUID_GROUP")
	private String uuidGroup;

	@Col(name = "UUID_ACCOUNT")
	private String uuidAccount;
}
