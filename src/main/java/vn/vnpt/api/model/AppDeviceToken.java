package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class AppDeviceToken {
	private String uuidAccount;
	private String deviceId;
	private String deviceToken;
	private Integer status;
	private Date lastUpdate;
}
