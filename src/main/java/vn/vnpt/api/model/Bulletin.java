package vn.vnpt.api.model;

import lombok.Data;

@Data
public class Bulletin {
	private String uuidBulletin;
	private String uuidCompany;
	private Integer status;
	private String startDate;
	private String endDate;
	private String startHour;
	private String endHour;
	private String title;
	private String content;
	private String pageInfo;
}
