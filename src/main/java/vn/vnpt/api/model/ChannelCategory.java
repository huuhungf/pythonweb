package vn.vnpt.api.model;

import lombok.Data;

@Data
public class ChannelCategory {
	private String uuidChannelCategory;
	private String name;
	private String description;
}
