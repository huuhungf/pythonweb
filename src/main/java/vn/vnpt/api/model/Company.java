package vn.vnpt.api.model;

import lombok.Data;

@Data
public class Company {
	private String uuidCompany;
	private String name;
	private String description;
	private String ownedAccount;
	private Integer alwayActive;
	private String career;
	private Integer notification;
	private String logoName;
	private String companyCode;
	private String address;
}
