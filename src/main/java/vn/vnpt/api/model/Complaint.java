package vn.vnpt.api.model;

import lombok.Data;
import vn.vnpt.api.repository.Col;

@Data
public class Complaint {

	@Col(name = "UUID_COMPLAINT")
	private String uuidComplaint;

	@Col(name = "TYPE")
	private Integer type;

	@Col(name = "STATUS")
	private Integer status;

	@Col(name = "DATE_CHECKIN")
	private String dateCheckin;

	@Col(name = "UUID_COMPLAINER")
	private String uuidComplainer;

	@Col(name = "FULL_NAME_COMPLAINER")
	private String fullNameComplainer;

	@Col(name = "IMAGE_URL_COMPLAINER")
	private String imageUrlComplainer;

	@Col(name = "GENDER_COMPLAINER")
	private String genderComplainer;

	@Col(name = "USER_CODE_COMPLAINER")
	private String userCodeComplainer;

	@Col(name = "EMAIL_COMPLAINER")
	private String emailComplainer;

	@Col(name = "UUID_RESOLVER")
	private String uuidResolver;

	@Col(name = "FULL_NAME_RESOLVER")
	private String fullNameResolver;

	@Col(name = "USER_CODE_RESOLVER")
	private String userCodeResolver;

	@Col(name = "USERNAME_RESOLVER")
	private String usernameResolver;

	@Col(name = "EMAIL_RESOLVER")
	private String emailResolver;

	@Col(name = "ROLE_RESOLVER")
	private String roleResolver;

	@Col(name = "COMPLAINT_CODE")
	private String complaintCode;

	@Col(name = "TRANS_ID")
	private String transId;
}
