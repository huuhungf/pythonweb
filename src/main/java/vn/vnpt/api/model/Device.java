package vn.vnpt.api.model;

import lombok.Data;

@Data
public class Device {
	private String uuidDevice;
	private String name;
	private String deviceCode;
	private String type;
	private Integer status;
	private String deviceId;
	private String uuidCompany;
	private String ownedAccount;
	private String publicKey;
	private String privateKey;
	private Integer limitDeviceLicense;
}
