package vn.vnpt.api.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DeviceLicense implements Serializable {
	private String uuidDevice;
	private String serialNumber;
	private String licenseKey;
	private Integer status;
	private Date lastUpdate;
	private String deviceId;
	private String deviceToken;
	private String name;
	private Float faceProb;
	private Integer faceMask;
	private Integer faceLiveness;
	private Integer collectImage;
	private Integer runMode;
	private String pinCode;
	private Integer virtualAssistant;
	private String os;
	private Integer battery;
	private Float latitude;
	private Float longitude;
	private String wifi;
	private String appVersion;
}
