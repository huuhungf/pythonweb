package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class ForgotConfirmationToken {
	private Long tokenId;
	private String confirmationToken;
	private Date createdDate;
	private String uuidAccount;
}
