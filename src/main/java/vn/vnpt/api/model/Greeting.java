package vn.vnpt.api.model;

import lombok.Data;

@Data
public class Greeting {
	private String uuidGreeting;
	private String text;
}
