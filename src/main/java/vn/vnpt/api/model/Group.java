package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

/*
UUID_GROUP, NAME, UUID_DEVICE, UUID_PARENT_GROUP, CREATE_DATE, UPDATE_DATE
 */
@Data
public class Group {
	private String uuidGroup;
	private String name;
	private String uuidDevice;
	private String uuidParentGroup;
	private Date createDate;
	private Date updateDate;
	private String uuidCompany;
}
