package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class LogCheckin {
	private String deviceCode;
	private String serialNumber;
	private String uuidAccount;
	private Date dateCheckin;
	private String imageUrl;
	private Integer checkinType;
	private Float faceProb;
	private Integer faceMask;
	private Integer faceLiveness;
	private String uuidDevice;
	private Integer matchRate;
	private String extraInfo;
}
