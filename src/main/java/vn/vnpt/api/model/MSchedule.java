package vn.vnpt.api.model;

import lombok.Data;

@Data
public class MSchedule {
	private String uuidMSchedule;
	private Integer dayId;
	private String uuidSchedule;
	private String uuidShift;

	private Shift shift;
}
