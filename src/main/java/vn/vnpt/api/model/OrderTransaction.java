package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

/*
UUID_ORDER_TRANSACTION	VARCHAR2(255 BYTE)
ORDER_TYPE	NUMBER
UUID_PLAN	VARCHAR2(255 BYTE)
UUID_ACCOUNT	VARCHAR2(255 BYTE)
UUID_PLAN_VERSION	VARCHAR2(255 BYTE)
ORDER_DATE	DATE
ORDER_STATUS	NUMBER
NUM_USER	NUMBER
ADD_ON_DEVICE	NUMBER
ADD_ON_OTT	NUMBER
TOTAL_AMOUNT	NUMBER
START_DATE	DATE
END_DATE	DATE
USER_AMOUNT	NUMBER
DEVICE_AMOUNT	NUMBER
OTT_AMOUNT	NUMBER
CUS_NAME	VARCHAR2(255 BYTE)
CUS_CODE	VARCHAR2(255 BYTE)
CUS_ORGANIZATION_TYPE	NUMBER
CUS_WORK_UNIT	VARCHAR2(255 BYTE)
CUS_ADDRESS	VARCHAR2(255 BYTE)
CUS_PHONE	VARCHAR2(255 BYTE)
CUS_EMAIL	VARCHAR2(255 BYTE)
EXPORT_INVOICE	NUMBER
VAT_AMOUNT	NUMBER
TOTAL	NUMBER
UUID_PAYMENT_TRANSACTION	VARCHAR2(255 BYTE)
PAYMENT_STATUS	VARCHAR2(255 BYTE)
PAYMENT_DATE	DATE
UUID_ACCOUNT_PLAN	VARCHAR2(255 BYTE)
 */
@Data
public class OrderTransaction {
	private String uuidOrderTransaction;
	private Integer orderType;
	private String uuidPlan;
	private String uuidAccount;
	private String uuidPlanVersion;
	private Date orderDate;
	private Integer orderStatus;
	private Integer numUser;
	private Integer addOnDevice;
	private Integer addOnOtt;
	private Long totalAmount;
	private Date startDate;
	private Date endDate;
	private Long userAmount;
	private Long deviceAmount;
	private Long ottAmount;
	private String cusName;
	private String cusCode;
	private Integer cusOrganizationType;
	private String cusWorkUnit;
	private String cusAddress;
	private String cusPhone;
	private String cusEmail;
	private Integer exportInvoice;
	private Long vatAmount;
	private Long total;
	private String uuidPaymentTransaction;
	private String paymentStatus;
	private Date paymentDate;
	private String uuidAccountPlan;
}
