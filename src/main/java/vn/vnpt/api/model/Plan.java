package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class Plan {
	private String uuidPlan;
	private Integer type;
	private Integer status;
	private String name;
	private String description;
	private Date createdDate;
	private Date updatedDate;
}
