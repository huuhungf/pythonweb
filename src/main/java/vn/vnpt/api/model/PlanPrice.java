package vn.vnpt.api.model;

import lombok.Data;

@Data
public class PlanPrice {
	private String uuidPlan;
	private String item;
}
