package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class PlanVersion {
	private String uuidPlanVersion;
	private Integer version;
	private Integer status;
	private String uuidPlan;
	private Date createdDate;
	private Date updatedDate;
}
