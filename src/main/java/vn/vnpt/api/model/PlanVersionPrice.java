package vn.vnpt.api.model;

import lombok.Data;

@Data
public class PlanVersionPrice {
	private String uuidPlanVersionPrice;
	private String uuidPlanVersion;
	private String item;
	private Integer unitPrice;
}
