package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class Policy {
	private String uuidPolicy;
	private String configuration;
	private String uuidPlan;
	private String uuidPlanVersion;
	private String uuidAccount;
	private String uuidPolicyDef;
	private Date createdDate;
	private Date updatedDate;

	private PlanVersionPrice planVersionPrice;
	private PolicyDef policyDef;
}
