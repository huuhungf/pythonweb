package vn.vnpt.api.model;

import lombok.Data;

@Data
public class PolicyDef {
	private String uuidPolicyDef;
	private String name;
	private Integer status;
	private String description;
}
