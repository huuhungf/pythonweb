package vn.vnpt.api.model;

import lombok.Data;

@Data
public class Price {
	private String item;
	private Integer unitPrice;
}
