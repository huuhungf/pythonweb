package vn.vnpt.api.model;

import lombok.Data;

/**
 * @author phuongha
 *
 */
@Data
public class Role {

	private String uuidRole;

	private String name;

	private String description;

	private String uuidApplication;
}
