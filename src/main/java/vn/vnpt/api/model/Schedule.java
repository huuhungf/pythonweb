package vn.vnpt.api.model;

import lombok.Data;

import java.util.List;

@Data
public class Schedule {
	private String uuidSchedule;
	private String scheduleName;
	private Integer cycleMode; // 0: tuan, 1: thang, 2: nam
	private String uuidCompany;

	private List<WSchedule> weekSchedules;

	private List<MSchedule> monthSchedules;

	private List<YSchedule> yearSchedules;
}
