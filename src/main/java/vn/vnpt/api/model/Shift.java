package vn.vnpt.api.model;

import lombok.Data;

@Data
public class Shift {
	private String uuidShift;
	private String shiftCode;
	private String onDuty;
	private String offDuty;
	private Integer dayCount;
	private Integer onTimeIn;
	private Integer onTimeOut;
	private Integer cutIn;
	private Integer cutOut;
	private String onLunch;
	private String offLunch;
	private Integer workingTime;
	private Float workingDay;
	private Integer noInWT;
	private Integer noOutWT;
	private Integer showPosition;
	private String uuidCompany;
	private Integer isLate;
	private Integer lateGrace;
	private Integer isLateGrace;
	private Integer roundStepLate;
	private Integer roundTypeLate;
	private Integer isEarly;
	private Integer earlyGrace;
	private Integer isEarlyGrace;
	private Integer roundStepEarly;
	private Integer roundTypeEarly;
}
