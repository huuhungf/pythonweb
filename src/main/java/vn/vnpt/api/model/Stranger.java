package vn.vnpt.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class Stranger {
	private String uuidStranger;
	private String uuidDevice;
	private String serialNumber;
	private Date dateCheckin;
	private String imageUrl;
	private Date createdDate;
}
