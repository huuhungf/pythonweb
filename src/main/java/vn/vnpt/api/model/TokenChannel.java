package vn.vnpt.api.model;

import lombok.Data;

@Data
public class TokenChannel {
	private String uuidTokenChannel;
	private String secretKey;
	private String description;
}
