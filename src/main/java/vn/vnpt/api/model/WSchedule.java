package vn.vnpt.api.model;

import lombok.Data;

@Data
public class WSchedule {
	private String uuidWSchedule;
	private Integer dayId;
	private String uuidSchedule;
	private String uuidShift;

	private Shift shift;
}
