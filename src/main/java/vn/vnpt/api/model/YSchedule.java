package vn.vnpt.api.model;

import lombok.Data;

@Data
public class YSchedule {
	private String uuidYSchedule;
	private Integer monthId;
	private Integer dayId;
	private String uuidSchedule;
	private String uuidShift;

	private Shift shift;
}
