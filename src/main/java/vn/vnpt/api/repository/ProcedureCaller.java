package vn.vnpt.api.repository;

import org.hibernate.procedure.ProcedureOutputs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ProcedureCaller {

	private final EntityManager entityManager;

	@Autowired
	public ProcedureCaller(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<Object> call(String procedureName, List<ProcedureParameter> procedureParameters) {
		StoredProcedureQuery query = null;
		try {
			query = entityManager.createStoredProcedureQuery(procedureName);

			ProcedureParameter refCursorParameter = null;
			List<ProcedureParameter> outParameters = new ArrayList<>();

			for (ProcedureParameter parameter : procedureParameters) {

				query.registerStoredProcedureParameter(parameter.getName(), parameter.getType(), parameter.getParameterMode());

				if (parameter.getParameterMode().equals(ParameterMode.IN)) {
					query.setParameter(parameter.getName(), parameter.getValue());
				}

				if (parameter.getParameterMode().equals(ParameterMode.REF_CURSOR)) {
					refCursorParameter = parameter;
				}

				if (parameter.getParameterMode().equals(ParameterMode.OUT)) {
					outParameters.add(parameter);
				}
			}

			query.execute();

			List<Object[]> resultList = null;

			if (refCursorParameter != null) {
				try {
					resultList = query.getResultList();
				} catch (Exception e) {
					resultList = new ArrayList<>();
				}
			}

			List<Object> finalRes = new ArrayList<>();

			for (ProcedureParameter oParameter : outParameters) {
				finalRes.add(query.getOutputParameterValue(oParameter.getName()));
			}

			if (resultList != null) {
				finalRes.add(resultList);
			}

			return finalRes;
		} finally {
			if (query != null) {
				query.unwrap(ProcedureOutputs.class).release();
			}
			entityManager.close();
		}
	}
}
