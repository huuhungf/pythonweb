package vn.vnpt.api.repository;

import org.hibernate.procedure.ProcedureOutputs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * support multi ref cursor
 */
@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ProcedureCallerV3 {

	private final EntityManager entityManager;

	@Autowired
	public ProcedureCallerV3(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * support no ref cursor
	 *
	 * @param procedureName
	 * @param procedureParameters
	 * @return
	 */
	public Map<String, Object> callNoRefCursor(String procedureName, List<ProcedureParameter> procedureParameters) {
		StoredProcedureQuery query = null;
		try {
			query = entityManager.createStoredProcedureQuery(procedureName);

			List<ProcedureParameter> outParameters = new ArrayList<>();

			for (ProcedureParameter parameter : procedureParameters) {

				query.registerStoredProcedureParameter(parameter.getName(), parameter.getType(), parameter.getParameterMode());

				if (parameter.getParameterMode().equals(ParameterMode.IN)) {
					query.setParameter(parameter.getName(), parameter.getValue());
				}

				if (parameter.getParameterMode().equals(ParameterMode.OUT)) {
					outParameters.add(parameter);
				}

				if (parameter.getParameterMode().equals(ParameterMode.REF_CURSOR)) {
					throw new IllegalArgumentException("not support parameter ref_cursor");
				}
			}

			query.execute();

			Map<String, Object> finalRes = new HashMap<>();

			for (ProcedureParameter oParameter : outParameters) {
				finalRes.put(oParameter.getName(), query.getOutputParameterValue(oParameter.getName()));
			}

			return finalRes;
		} finally {
			if (query != null) {
				query.unwrap(ProcedureOutputs.class).release();
			}
			entityManager.close();
		}
	}

	/**
	 * support only one ref cursor
	 *
	 * @param procedureName
	 * @param procedureParameters
	 * @param outputClass
	 * @return
	 */
	public Map<String, Object> callOneRefCursor(String procedureName, List<ProcedureParameter> procedureParameters,
												Class<?> outputClass) {
		StoredProcedureQuery query = null;
		try {
			query = entityManager.createStoredProcedureQuery(procedureName);

			List<ProcedureParameter> outParameters = new ArrayList<>();
			List<ProcedureParameter> refCursorParameters = new ArrayList<>();

			for (ProcedureParameter parameter : procedureParameters) {

				query.registerStoredProcedureParameter(parameter.getName(), parameter.getType(), parameter.getParameterMode());

				if (parameter.getParameterMode().equals(ParameterMode.IN)) {
					query.setParameter(parameter.getName(), parameter.getValue());
				}

				if (parameter.getParameterMode().equals(ParameterMode.OUT)) {
					outParameters.add(parameter);
				}

				if (parameter.getParameterMode().equals(ParameterMode.REF_CURSOR)) {
					refCursorParameters.add(parameter);
				}
			}

			query.execute();

			Map<String, Object> finalRes = new HashMap<>();

			for (ProcedureParameter oParameter : outParameters) {
				finalRes.put(oParameter.getName(), query.getOutputParameterValue(oParameter.getName()));
			}

			for (ProcedureParameter oParameter : refCursorParameters) {
				ResultSet resultSet = (ResultSet) query.getOutputParameterValue(oParameter.getName());
				if (resultSet == null) {
					finalRes.put(oParameter.getName(), new ArrayList<>());
				} else {
					finalRes.put(oParameter.getName(), new ResultProcessor<>().processRs(resultSet, outputClass));
				}
			}

			return finalRes;
		} finally {
			if (query != null) {
				query.unwrap(ProcedureOutputs.class).release();
			}
			entityManager.close();
		}
	}

	/**
	 * support multiple ref cursor
	 *
	 * @param procedureName
	 * @param procedureParameters
	 * @param outClasses
	 * @return
	 */
	public Map<String, Object> callMultiRefCursor(String procedureName, List<ProcedureParameter> procedureParameters,
												  Map<String, Class<?>> outClasses) {
		StoredProcedureQuery query = null;
		try {
			query = entityManager.createStoredProcedureQuery(procedureName);

			List<ProcedureParameter> outParameters = new ArrayList<>();
			List<ProcedureParameter> refCursorParameters = new ArrayList<>();

			for (ProcedureParameter parameter : procedureParameters) {

				query.registerStoredProcedureParameter(parameter.getName(), parameter.getType(), parameter.getParameterMode());

				if (parameter.getParameterMode().equals(ParameterMode.IN)) {
					query.setParameter(parameter.getName(), parameter.getValue());
				}

				if (parameter.getParameterMode().equals(ParameterMode.OUT)) {
					outParameters.add(parameter);
				}

				if (parameter.getParameterMode().equals(ParameterMode.REF_CURSOR)) {
					refCursorParameters.add(parameter);
				}
			}

			query.execute();

			Map<String, Object> finalRes = new HashMap<>();

			for (ProcedureParameter oParameter : outParameters) {
				finalRes.put(oParameter.getName(), query.getOutputParameterValue(oParameter.getName()));
			}

			for (ProcedureParameter oParameter : refCursorParameters) {
				Class<?> aClass = outClasses.get(oParameter.getName());
				if (aClass == null) {
					throw new RuntimeException(String.format("could not find class for param: %s",
							oParameter.getName()));
				}

				ResultSet resultSet = (ResultSet) query.getOutputParameterValue(oParameter.getName());
				if (resultSet == null) {
					finalRes.put(oParameter.getName(), new ArrayList<>());
				} else {
					finalRes.put(oParameter.getName(), new ResultProcessor<>().processRs(resultSet, aClass));
				}
			}

			return finalRes;
		} finally {
			if (query != null) {
				query.unwrap(ProcedureOutputs.class).release();
			}
			entityManager.close();
		}
	}
}
