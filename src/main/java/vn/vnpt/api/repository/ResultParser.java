package vn.vnpt.api.repository;

public interface ResultParser<T> {
	T parse(Object[] r);
}
