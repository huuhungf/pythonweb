package vn.vnpt.api.repository.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.out.AppVersionDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetAllAppVersionRepository {
	private static final String PROC_GET_ALL_APP_VERSION = "PKG_APP.PGET_ALL_APP_VERSION";

	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public GetAllAppVersionRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<AppVersionDtoOut> getAll() {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_ALL_APP_VERSION, Arrays.asList(
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_APP_VERSION")),
				AppVersionDtoOut.class
		);

		@SuppressWarnings("unchecked")
		List<AppVersionDtoOut> appVersionDtoOuts = (List<AppVersionDtoOut>) outputs.get("OREF_APP_VERSION");

		return appVersionDtoOuts;
	}
}
