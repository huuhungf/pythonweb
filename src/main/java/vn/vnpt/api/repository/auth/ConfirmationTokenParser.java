package vn.vnpt.api.repository.auth;

import vn.vnpt.api.model.ConfirmationToken;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class ConfirmationTokenParser {
	public static ConfirmationToken parse(ResultSet rs) throws SQLException {
		Long tokenId = rs.getLong("TOKEN_ID");
		String confirmationToken = rs.getString("CONFIRMATION_TOKEN");
		String uuidAccount = rs.getString("UUID_ACCOUNT");
		Date createdDate = rs.getDate("CREATED_DATE");

		ConfirmationToken ct = new ConfirmationToken();
		ct.setTokenId(tokenId);
		ct.setConfirmationToken(confirmationToken);
		ct.setUuidAccount(uuidAccount);
		ct.setCreatedDate(createdDate);
		return ct;
	}
}
