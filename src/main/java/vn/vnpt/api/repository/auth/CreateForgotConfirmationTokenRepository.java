package vn.vnpt.api.repository.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CreateForgotConfirmationTokenRepository {
	private static final String PROC_CREATE_FORGOT_TOKEN = "PKG_AUTH.PCREATE_FORGOT_CONFIRMATION_TOKEN";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CreateForgotConfirmationTokenRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void create(String token, String uuidAccount) {
		procedureCaller.callNoRefCursor(PROC_CREATE_FORGOT_TOKEN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_CONFIRMATION_TOKEN", String.class, token),
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OUT_RESULT", String.class)
		));
	}
}
