package vn.vnpt.api.repository.auth;

import vn.vnpt.api.model.ForgotConfirmationToken;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class ForgotConfirmationTokenParser {
	public static ForgotConfirmationToken parse(ResultSet rs) throws SQLException {
		Long tokenId = rs.getLong("TOKEN_ID");
		String confirmationToken = rs.getString("CONFIRMATION_TOKEN");
		String uuidAccount = rs.getString("UUID_ACCOUNT");
		Date createdDate = rs.getDate("CREATED_DATE");

		ForgotConfirmationToken fct = new ForgotConfirmationToken();
		fct.setTokenId(tokenId);
		fct.setConfirmationToken(confirmationToken);
		fct.setUuidAccount(uuidAccount);
		fct.setCreatedDate(createdDate);
		return fct;
	}
}
