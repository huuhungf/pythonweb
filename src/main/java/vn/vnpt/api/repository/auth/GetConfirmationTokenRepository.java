package vn.vnpt.api.repository.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.ConfirmationToken;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetConfirmationTokenRepository {
	private static final String PROC_GET_TOKEN = "PKG_AUTH.PGET_CONFIRMATION_TOKEN";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetConfirmationTokenRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public ConfirmationToken get(String token) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_TOKEN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_CONFIRMATION_TOKEN", String.class, token),
				ProcedureParameter.outputParam("OUT_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_REF")),
				ConfirmationTokenParser::parse
		);

		String res = (String) outputs.get("OUT_RESULT");

		if (ConstantString.STATUS_DB.CONFIRMATION_TOKEN_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("confirmation token not found %s", token));
		}

		@SuppressWarnings("unchecked")
		List<ConfirmationToken> confirmationTokens = (List<ConfirmationToken>) outputs.get("OUT_REF");

		return confirmationTokens.get(0);
	}
}
