package vn.vnpt.api.repository.bulletin;

import vn.vnpt.api.model.Bulletin;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BulletinParser {
	public static Bulletin parse(ResultSet rs) throws SQLException {
		String uuidBulletin = rs.getString("UUID_BULLETIN");
		String uuidCompany = rs.getString("UUID_COMPANY");
		Integer status = rs.getInt("STATUS");
		String startDate = rs.getString("START_DATE");
		String endDate = rs.getString("END_DATE");
		String startHour = rs.getString("START_HOUR");
		String endHour = rs.getString("END_HOUR");
		String title = rs.getString("TITLE");
		String content = rs.getString("CONTENT");
		String pageInfo = rs.getString("PAGE_INFO");

		Bulletin bulletin = new Bulletin();
		bulletin.setUuidBulletin(uuidBulletin);
		bulletin.setUuidCompany(uuidCompany);
		bulletin.setStatus(status);
		bulletin.setStartDate(startDate);
		bulletin.setEndDate(endDate);
		bulletin.setStartHour(startHour);
		bulletin.setEndHour(endHour);
		bulletin.setTitle(title);
		bulletin.setContent(content);
		bulletin.setPageInfo(pageInfo);

		return bulletin;
	}
}
