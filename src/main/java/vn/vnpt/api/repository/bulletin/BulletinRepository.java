package vn.vnpt.api.repository.bulletin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.vnpt.api.model.Bulletin;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class BulletinRepository {

	@Autowired
	private ProcedureCallerV2 procedureCaller;

	public List<Bulletin> list(String uuidCompany) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_BULLETIN.PLIST_BULLETIN",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_CURSOR")
				),
				BulletinParser::parse
		);

		List<Bulletin> bulletins = (List<Bulletin>) outputs.get("OREF_CURSOR");
		return bulletins;
	}

	public void insert(String uuidCompany, Integer status, String startDate, String endDate,
	                   String startHour, String endHour, String title, String content, String pageInfo) {
		procedureCaller.callOneRefCursor(
				"PKG_BULLETIN.PINSERT_BULLETIN",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
						ProcedureParameter.inputParam("PRN_STATUS", Integer.class, status),
						ProcedureParameter.inputParam("PRS_START_DATE", String.class, startDate),
						ProcedureParameter.inputParam("PRS_END_DATE", String.class, endDate),
						ProcedureParameter.inputParam("PRS_START_HOUR", String.class, startHour),
						ProcedureParameter.inputParam("PRS_END_HOUR", String.class, endHour),
						ProcedureParameter.inputParam("PRS_TITLE", String.class, title),
						ProcedureParameter.inputParam("PRS_CONTENT", String.class, content),
						ProcedureParameter.inputParam("PRS_PAGE_INFO", String.class, pageInfo),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_CURSOR")
				),
				BulletinParser::parse
		);
	}
}
