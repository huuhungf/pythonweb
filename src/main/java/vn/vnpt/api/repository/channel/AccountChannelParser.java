package vn.vnpt.api.repository.channel;

import vn.vnpt.api.model.AccountChannel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class AccountChannelParser {
	public static AccountChannel parse(ResultSet rs) throws SQLException {
		String uuidAccount = rs.getString("UUID_ACCOUNT");
		String uuidChannelCategory = rs.getString("UUID_CHANNEL_CATEGORY");
		Integer status = rs.getInt("STATUS");
		Integer userOtt = rs.getInt("USER_OTT");
		Date lastUpdate = rs.getDate("LAST_UPDATE");
		String channelName = rs.getString("NAME");

		AccountChannel channel = new AccountChannel();
		channel.setUuidAccount(uuidAccount);
		channel.setUuidChannelCategory(uuidChannelCategory);
		channel.setStatus(status);
		channel.setUserOtt(userOtt);
		channel.setLastUpdate(lastUpdate);
		channel.setChannelName(channelName);

		return channel;
	}
}
