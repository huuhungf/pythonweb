package vn.vnpt.api.repository.channel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AccountChannel;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CreateAccountChannelRepository {
	private static final String PROC_CREATE_ACCOUNT_CHANNEL = "PKG_CHANNEL.PCREATE_ACCOUNT_CHANNEL";
	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CreateAccountChannelRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public AccountChannel createAccountChannel(String uuidAccount, String uuidChannelCategory, Integer userOtt, Integer status) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_CREATE_ACCOUNT_CHANNEL, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.inputParam("PRS_UUID_CHANNEL_CATEGORY", String.class, uuidChannelCategory),
				ProcedureParameter.inputParam("PRN_USER_OTT", Integer.class, userOtt),
				ProcedureParameter.inputParam("PRN_STATUS", Integer.class, status),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_ACCOUNT_CHANNEL")
				),
				AccountChannelParser::parse);

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.CHANNEL_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("channel not exist: %s", uuidChannelCategory));
		}

		@SuppressWarnings("unchecked")
		List<AccountChannel> accountChannels = (List<AccountChannel>) outputs.get("OREF_ACCOUNT_CHANNEL");
		return accountChannels.get(0);
	}
}
