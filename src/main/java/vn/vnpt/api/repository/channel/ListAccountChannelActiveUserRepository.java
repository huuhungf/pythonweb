package vn.vnpt.api.repository.channel;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.out.ChannelOttDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListAccountChannelActiveUserRepository {
	private static final String PROC_LIST_ACCOUNT_CHANNEL = "PKG_CHANNEL.PLIST_ACCOUNT_CHANNEL_USER_ACTIVE";

	private final ProcedureCallerV2 procedureCaller;

	public ListAccountChannelActiveUserRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<ChannelOttDtoOut> list(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_ACCOUNT_CHANNEL, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.refCursorParam("OREF_ACCOUNT_CHANNEL")),
				r -> {
					String uuidChannelCategory = r.getString("UUID_CHANNEL_CATEGORY");
					int status = r.getInt("STATUS");
					int userOtt = r.getInt("USER_OTT");
					String channelName = r.getString("NAME");
					int activeUser = r.getInt("ACTIVE_USER");

					ChannelOttDtoOut channelOttDtoOut = new ChannelOttDtoOut();
					channelOttDtoOut.setUuidChannelCategory(uuidChannelCategory);
					channelOttDtoOut.setChannelName(channelName);
					channelOttDtoOut.setStatus(status);
					channelOttDtoOut.setUserOTT(userOtt);
					channelOttDtoOut.setActiveUser(activeUser);

					return channelOttDtoOut;
				}
		);

		@SuppressWarnings("unchecked")
		List<ChannelOttDtoOut> accountChannels = (List<ChannelOttDtoOut>) outputs.get("OREF_ACCOUNT_CHANNEL");
		return accountChannels;
	}
}
