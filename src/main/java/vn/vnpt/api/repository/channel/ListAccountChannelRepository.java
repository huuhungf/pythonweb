package vn.vnpt.api.repository.channel;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AccountChannel;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListAccountChannelRepository {
	private static final String PROC_LIST_ACCOUNT_CHANNEL = "PKG_CHANNEL.PLIST_ACCOUNT_CHANNEL";

	private final ProcedureCallerV2 procedureCaller;

	public ListAccountChannelRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<AccountChannel> list(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_ACCOUNT_CHANNEL, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.refCursorParam("OREF_ACCOUNT_CHANNEL")),
				AccountChannelParser::parse);

		@SuppressWarnings("unchecked")
		List<AccountChannel> accountChannels = (List<AccountChannel>) outputs.get("OREF_ACCOUNT_CHANNEL");
		return accountChannels;
	}
}
