package vn.vnpt.api.repository.channel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.ChannelCategory;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebListChannelCategoryRepository {
	private static final String PROC_LIST_CHANNEL_CATEGORY = "PKG_CHANNEL.PWEB_GET_LIST_CHANNEL_CATEGORY";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebListChannelCategoryRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<ChannelCategory> list() {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_CHANNEL_CATEGORY, Arrays.asList(
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_CHANNEL_CATEGORY")),
				r -> {
					String uuidChannelCategory = r.getString("UUID_CHANNEL_CATEGORY");
					String name = r.getString("NAME");

					ChannelCategory channelCategory = new ChannelCategory();
					channelCategory.setUuidChannelCategory(uuidChannelCategory);
					channelCategory.setName(name);
					return channelCategory;
				}
		);

		@SuppressWarnings("unchecked")
		List<ChannelCategory> channelCategories = (List<ChannelCategory>) outputs.get("OREF_CHANNEL_CATEGORY");

		return channelCategories;
	}
}
