package vn.vnpt.api.repository.channel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebUpdateAccountChannelRepository {
	private static final String PROC_UPDATE_ACCOUNT_CHANNEL = "PKG_CHANNEL.PWEB_UPDATE_CHANNEL_CATEGORY_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebUpdateAccountChannelRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String uuidAccount, String uuidChannelCategory, Integer status, Integer userOtt) {
		procedureCaller.callNoRefCursor(PROC_UPDATE_ACCOUNT_CHANNEL, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.inputParam("PRS_UUID_CHANNEL_CATEGORY", String.class, uuidChannelCategory),
				ProcedureParameter.inputParam("PRN_STATUS", Integer.class, status),
				ProcedureParameter.inputParam("PRN_USER_OTT", Integer.class, userOtt),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
