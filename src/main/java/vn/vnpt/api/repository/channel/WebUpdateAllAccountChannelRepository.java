package vn.vnpt.api.repository.channel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebUpdateAllAccountChannelRepository {
	private static final String PROC_UPDATE_ACCOUNT_CHANNEL = "PKG_CHANNEL.PWEB_UPDATE_ALL_CHANNEL_CATEGORY_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebUpdateAllAccountChannelRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String uuidAccount, Integer status) {
		procedureCaller.callNoRefCursor(PROC_UPDATE_ACCOUNT_CHANNEL, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.inputParam("PRN_STATUS", Integer.class, status),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
