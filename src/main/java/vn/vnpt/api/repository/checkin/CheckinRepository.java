package vn.vnpt.api.repository.checkin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.vnpt.api.model.HisCheckin;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class CheckinRepository {

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CheckinRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<HisCheckin> listCheckinAccounts(String uuidAccounts, String startDate, String endDate) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_CHECKIN.PLIST_HIS_CHECKIN_ACCOUNTS",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNTS", String.class, uuidAccounts),
						ProcedureParameter.inputParam("PRS_START_DATE", String.class, startDate),
						ProcedureParameter.inputParam("PRS_END_DATE", String.class, endDate),
						ProcedureParameter.refCursorParam("OREF")
				),
				rs -> {
					HisCheckin hisCheckin = new HisCheckin();
					hisCheckin.setUuidAccount(rs.getString("UUID_ACCOUNT"));
					hisCheckin.setDateCheckin(rs.getDate("DATE_CHECKIN"));
					return hisCheckin;
				}
		);
		List<HisCheckin> results = (List<HisCheckin>) outputs.get("OREF");
		return results;
	}
}
