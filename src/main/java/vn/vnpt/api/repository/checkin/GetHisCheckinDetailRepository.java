package vn.vnpt.api.repository.checkin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.out.HisCheckinDetailDtoOut;
import vn.vnpt.api.dto.out.HisCheckinAccountDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetHisCheckinDetailRepository {
	private static final String PROC_GET_DETAIL = "PKG_CHECKIN.PGET_DETAIL_CHECKIN";

	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public GetHisCheckinDetailRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<HisCheckinDetailDtoOut> list(String uuidAccount, String dateCheckin) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_DETAIL, Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
						ProcedureParameter.inputParam("PRS_DATE_CHECKIN", String.class, dateCheckin),
						ProcedureParameter.outputParam("OUT_RESULT", String.class),
						ProcedureParameter.refCursorParam("OUT_CUR")),
				HisCheckinDetailDtoOut.class
		);

		@SuppressWarnings("unchecked")
		List<HisCheckinDetailDtoOut> detailDtoOutList = (List<HisCheckinDetailDtoOut>) outputs.get("OUT_CUR");

		return detailDtoOutList;
	}

	public List<HisCheckinAccountDtoOut> listCheckinAccount(String uuidAccount, String startDate, String endDate, Integer channelId) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_CHECKIN.PLIST_DETAIL_CHECKIN_ACCOUNT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
						ProcedureParameter.inputParam("PRS_START_DATE", String.class, startDate),
						ProcedureParameter.inputParam("PRS_END_DATE", String.class, endDate),
						ProcedureParameter.inputParam("PRN_CHANNEL_ID", Integer.class, channelId),
						ProcedureParameter.refCursorParam("OREF")),
				HisCheckinAccountDtoOut.class
		);

		@SuppressWarnings("unchecked")
		List<HisCheckinAccountDtoOut> detailDtoOutList = (List<HisCheckinAccountDtoOut>) outputs.get("OREF");

		return detailDtoOutList;
	}
}
