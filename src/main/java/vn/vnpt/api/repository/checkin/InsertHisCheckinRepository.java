package vn.vnpt.api.repository.checkin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.HisCheckin;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.Date;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class InsertHisCheckinRepository {

	private static final String PROC_INSERT_HIS_CHECKIN = "PKG_CHECKIN.PINSERT_HIS_CHECKIN";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public InsertHisCheckinRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void insert(HisCheckin hisCheckin) {
		procedureCaller.call(PROC_INSERT_HIS_CHECKIN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_DEVICE_CODE", String.class, hisCheckin.getDeviceCode()),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, hisCheckin.getSerialNumber()),
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, hisCheckin.getUuidAccount()),
				ProcedureParameter.inputParam("PRS_DATE_CHECKIN", Date.class, hisCheckin.getDateCheckin()),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, hisCheckin.getImageUrl()),
				ProcedureParameter.inputParam("PRN_CHECKIN_TYPE", Integer.class, hisCheckin.getCheckinType()),
				ProcedureParameter.inputParam("PRN_FACE_PROB", Float.class, hisCheckin.getFaceProb()),
				ProcedureParameter.inputParam("PRN_FACE_MASK", Integer.class, hisCheckin.getFaceMask()),
				ProcedureParameter.inputParam("PRN_FACE_LIVENESS", Integer.class, hisCheckin.getFaceLiveness()),
				ProcedureParameter.inputParam("PRN_MATCH_RATE", Integer.class, hisCheckin.getMatchRate()),
				ProcedureParameter.inputParam("PRS_EXTRA_INFO", String.class, hisCheckin.getExtraInfo()),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}

	public void addHisCheckin(String uuidAccount, String dateCheckin, String uuidCreator) {
		procedureCaller.call(
				"PKG_CHECKIN.PWEB_MANUAL_INSERT_HIS_CHECKIN",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
						ProcedureParameter.inputParam("PRS_DATE_CHECKIN", String.class, dateCheckin),
						ProcedureParameter.inputParam("PRS_UUID_CREATOR", String.class, uuidCreator),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}
}
