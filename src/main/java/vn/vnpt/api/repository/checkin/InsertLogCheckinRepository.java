package vn.vnpt.api.repository.checkin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.LogCheckin;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.Date;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class InsertLogCheckinRepository {

	private static final String PROC_INSERT_LOG_CHECKIN = "PKG_CHECKIN.PINSERT_LOG_CHECKIN";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public InsertLogCheckinRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void insert(LogCheckin logCheckin) {
		procedureCaller.call(PROC_INSERT_LOG_CHECKIN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_DEVICE_CODE", String.class, logCheckin.getDeviceCode()),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, logCheckin.getSerialNumber()),
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, logCheckin.getUuidAccount()),
				ProcedureParameter.inputParam("PRS_DATE_CHECKIN", Date.class, logCheckin.getDateCheckin()),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, logCheckin.getImageUrl()),
				ProcedureParameter.inputParam("PRN_CHECKIN_TYPE", Integer.class, logCheckin.getCheckinType()),
				ProcedureParameter.inputParam("PRN_FACE_PROB", Float.class, logCheckin.getFaceProb()),
				ProcedureParameter.inputParam("PRN_FACE_MASK", Integer.class, logCheckin.getFaceMask()),
				ProcedureParameter.inputParam("PRN_FACE_LIVENESS", Integer.class, logCheckin.getFaceLiveness()),
				ProcedureParameter.inputParam("PRN_MATCH_RATE", Integer.class, logCheckin.getMatchRate()),
				ProcedureParameter.inputParam("PRS_EXTRA_INFO", String.class, logCheckin.getExtraInfo()),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
