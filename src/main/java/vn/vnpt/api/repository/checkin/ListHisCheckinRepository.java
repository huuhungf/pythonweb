package vn.vnpt.api.repository.checkin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.vnpt.api.dto.out.HisCheckinDtoOut;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Repository
public class ListHisCheckinRepository {
	private static final String PROC_LIST_HIS_CHECKIN = "PKG_CHECKIN.PTABLET_GET_LIST_FILTER";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public ListHisCheckinRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<HisCheckinDtoOut> listHisCheckin(String adminUsername, PagingDtoIn pagingDtoIn) {
		List<Object> outputs = procedureCaller.call(PROC_LIST_HIS_CHECKIN, Arrays.asList(
				ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
				ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
				ProcedureParameter.inputParam("PRS_ADMIN_USERNAME", String.class, adminUsername),
				ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
				ProcedureParameter.refCursorParam("OUT_CUR")
		));

		Long totalElement = (Long) outputs.get(0);
		long totalPages = totalElement / pagingDtoIn.getMaxSize();
		if (totalElement % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}

		PagingDTO<HisCheckinDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalElement(totalElement);
		pagingDTO.setTotalPages(totalPages);

		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<HisCheckinDtoOut> data = new ResultProcessor<HisCheckinDtoOut>().process(resultList, r -> {
			String fullName = Common.castObject(r[1], null);
			String imageUrl = Common.castObject(r[2], null);
			BigDecimal checkinType = Common.castObject(r[3], BigDecimal.ONE);
			Date dateCheckin = Common.castObject(r[4], null);
			String userCode = Common.castObject(r[5], null);

			HisCheckinDtoOut hisCheckinDtoOut = new HisCheckinDtoOut();
			hisCheckinDtoOut.setFullName(fullName);
			hisCheckinDtoOut.setImageUrl(imageUrl);
			hisCheckinDtoOut.setCheckinType(checkinType.intValue());
			hisCheckinDtoOut.setDateCheckin(Common.convertDateToString(dateCheckin, ConstantString.DDMMYYYYHHMMSS));
			hisCheckinDtoOut.setUserCode(userCode);

			return hisCheckinDtoOut;
		});

		pagingDTO.setData(data);

		return pagingDTO;
	}
}
