package vn.vnpt.api.repository.checkin;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.out.WebHisCheckinDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebListHisCheckinByGroupRepository {
	private static final String PROC_LIST_CHECKIN = "PKG_CHECKIN.PWEB_GET_LIST_HIS_CHECKIN_BY_GROUP";

	private final ProcedureCallerV3 procedureCaller;

	public WebListHisCheckinByGroupRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<WebHisCheckinDtoOut> listHisCheckin(String uuidGroup, String startDate, String endDate,
														 String keySearch, PagingDtoIn pagingDtoIn,
														 String minFirstCheckin, String maxFirstCheckin,
														 String minLastCheckin, String maxLastCheckin) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_CHECKIN, Arrays.asList(
				ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
				ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
				ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
				ProcedureParameter.inputParam("PRS_START_DATE", String.class, startDate),
				ProcedureParameter.inputParam("PRS_END_DATE", String.class, endDate),
				ProcedureParameter.inputParam("PRS_KEY_SEARCH", String.class, keySearch),
				ProcedureParameter.inputParam("PRS_MIN_FIRST_CHECKIN", String.class, minFirstCheckin),
				ProcedureParameter.inputParam("PRS_MAX_FIRST_CHECKIN", String.class, maxFirstCheckin),
				ProcedureParameter.inputParam("PRS_MIN_LAST_CHECKIN", String.class, minLastCheckin),
				ProcedureParameter.inputParam("PRS_MAX_LAST_CHECKIN", String.class, maxLastCheckin),
				ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
				ProcedureParameter.refCursorParam("OUT_CUR")),
				WebHisCheckinDtoOut.class
		);

		Long total = (Long) outputs.get("OUT_TOTAL");
		long totalPages = total / pagingDtoIn.getMaxSize();
		if (total % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}
		PagingDTO<WebHisCheckinDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalPages(totalPages);
		pagingDTO.setTotalElement(total);
		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<WebHisCheckinDtoOut> data = (List<WebHisCheckinDtoOut>) outputs.get("OUT_CUR");

		pagingDTO.setData(data);
		return pagingDTO;
	}
}
