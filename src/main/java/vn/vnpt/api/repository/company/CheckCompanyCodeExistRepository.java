package vn.vnpt.api.repository.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CheckCompanyCodeExistRepository {

	private final static String PROC_CHECK_COMPANY_CODE_EXIST = "PKG_COMPANY.PCHECK_COMPANY_CODE_EXIST";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CheckCompanyCodeExistRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Boolean isExist(String companyCode) {

		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_CHECK_COMPANY_CODE_EXIST, Arrays.asList(
				ProcedureParameter.inputParam("PRS_COMPANY_CODE", String.class, companyCode),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get("OS_RESULT");

		return ConstantString.STATUS_DB.COMPANY_CODE_ALREADY_EXIST.equals(res);
	}
}
