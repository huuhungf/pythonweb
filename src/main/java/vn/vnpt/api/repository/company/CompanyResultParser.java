package vn.vnpt.api.repository.company;

import vn.vnpt.api.model.Company;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyResultParser {
	public static Company parse(ResultSet rs) throws SQLException {
		String uuidCompany = rs.getString("UUID_COMPANY");
		String name = rs.getString("NAME");
		String description = rs.getString("DESCRIPTION");
		String ownedAccount = rs.getString("OWNED_ACCOUNT");
		Integer alwayActive = rs.getInt("ALWAY_ACTIVE");
		String career = rs.getString("CAREER");
		Integer notification = rs.getInt("NOTIFICATION");
		String logoName = rs.getString("LOGO_NAME");
		String companyCode = rs.getString("COMPANY_CODE");
		String address = rs.getString("ADDRESS");

		Company company = new Company();
		company.setUuidCompany(uuidCompany);
		company.setName(name);
		company.setDescription(description);
		company.setOwnedAccount(ownedAccount);
		company.setAlwayActive(alwayActive);
		company.setCareer(career);
		company.setNotification(notification);
		company.setLogoName(logoName);
		company.setCompanyCode(companyCode);
		company.setAddress(address);
		return company;
	}
}
