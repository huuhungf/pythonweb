package vn.vnpt.api.repository.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetCompanyByAccountRepository {
	private static final String PROC_GET_COMPANY = "PKG_COMPANY.PGET_COMPANY_INFO";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetCompanyByAccountRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Company getCompany(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_COMPANY, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_COMPANY")),
				CompanyResultParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Company> companies = (List<Company>) outputs.get("OREF_COMPANY");
		return companies.get(0);
	}
}
