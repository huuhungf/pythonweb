package vn.vnpt.api.repository.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetCompanyRepository {
	private static final String PROC_GET_COMPANY = "PKG_COMPANY.PGET_COMPANY";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetCompanyRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Company getCompany(String uuidCompany) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_COMPANY, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_COMPANY")),
				CompanyResultParser::parse
		);

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.COMPANY_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("company not found: %s", uuidCompany));
		}

		@SuppressWarnings("unchecked")
		List<Company> companies = (List<Company>) outputs.get("OREF_COMPANY");
		return companies.get(0);
	}
}
