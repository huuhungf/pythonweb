package vn.vnpt.api.repository.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateCompanyLogoRepository {
	private static final String PROC_UPDATE_COMPANY_LOGO = "PKG_COMPANY.PUPDATE_COMPANY_LOGO";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public UpdateCompanyLogoRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String superUsername, String logoName) {
		procedureCaller.callNoRefCursor(PROC_UPDATE_COMPANY_LOGO, Arrays.asList(
				ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, superUsername),
				ProcedureParameter.inputParam("PRS_LOGO_NAME", String.class, logoName),
				ProcedureParameter.outputParam("OS_RESULT", String.class)));
	}
}
