package vn.vnpt.api.repository.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateCompanyRepository {
	private static final String PROC_UPDATE_COMPANY = "PKG_COMPANY.PUPDATE_COMPANY";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public UpdateCompanyRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String uuidCompany, String name, String companyCode) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_UPDATE_COMPANY, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
				ProcedureParameter.inputParam("PRS_NAME", String.class, name),
				ProcedureParameter.inputParam("PRS_COMPANY_CODE", String.class, companyCode),
				ProcedureParameter.outputParam("OS_RESULT", String.class)));

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.COMPANY_CODE_ALREADY_EXIST.equals(res)) {
			throw new BadRequestException(String.format("company code already exist: %s", companyCode));
		}
	}
}
