package vn.vnpt.api.repository.complaint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.vnpt.api.dto.in.ComplaintDtoIn;
import vn.vnpt.api.model.Complaint;
import vn.vnpt.api.model.ComplaintDetails;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class ComplaintRepository {

	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public ComplaintRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<Complaint> listComplaints(ComplaintDtoIn complaintDtoIn, PagingDtoIn pagingDtoIn) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_COMPLAINT.PLIST_COMPAINT_PAGING", Arrays.asList(
						ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
						ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
						ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, complaintDtoIn.getUuidGroup()),
						ProcedureParameter.inputParam("PRN_STATUS", Integer.class, complaintDtoIn.getStatus()),
						ProcedureParameter.inputParam("PRS_FROM_DATE", String.class, complaintDtoIn.getStartDate()),
						ProcedureParameter.inputParam("PRS_TO_DATE", String.class, complaintDtoIn.getEndDate()),
						ProcedureParameter.inputParam("PRS_KEY_SEARCH", String.class, complaintDtoIn.getKeySearch()),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OR_CUR"),
						ProcedureParameter.outputParam("OUT_TOTAL", Long.class)
				), Complaint.class);

		Long totalElement = (Long) outputs.get("OUT_TOTAL");
		long totalPages = totalElement / pagingDtoIn.getMaxSize();
		if (totalElement % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}

		PagingDTO<Complaint> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalElement(totalElement);
		pagingDTO.setTotalPages(totalPages);

		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<Complaint> data = (List<Complaint>) outputs.get("OR_CUR");
		pagingDTO.setData(data);
		return pagingDTO;
	}

	public PagingDTO<Complaint> listComplaints(ComplaintDtoIn complaintDtoIn,String uuidComplainer ,PagingDtoIn pagingDtoIn) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_COMPLAINT.PCOMPLAINER_LIST_COMPAINT", Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPLAINER", String.class, uuidComplainer),
						ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
						ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
						ProcedureParameter.inputParam("PRN_STATUS", Integer.class, complaintDtoIn.getStatus()),
						ProcedureParameter.inputParam("PRS_FROM_DATE", String.class, complaintDtoIn.getStartDate()),
						ProcedureParameter.inputParam("PRS_TO_DATE", String.class, complaintDtoIn.getEndDate()),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OR_CUR"),
						ProcedureParameter.outputParam("OUT_TOTAL", Long.class)
				), Complaint.class);

		Long totalElement = (Long) outputs.get("OUT_TOTAL");
		long totalPages = totalElement / pagingDtoIn.getMaxSize();
		if (totalElement % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}

		PagingDTO<Complaint> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalElement(totalElement);
		pagingDTO.setTotalPages(totalPages);

		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<Complaint> data = (List<Complaint>) outputs.get("OR_CUR");
		pagingDTO.setData(data);
		return pagingDTO;
	}

	public ComplaintDetails get(String uuidComplaint) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_COMPLAINT.PGET_COMPLAINT_DETAIL",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPLAINT", String.class, uuidComplaint),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OR_CUR")
				), ComplaintDetails.class
		);

		List<ComplaintDetails> rs = (List<ComplaintDetails>) outputs.get("OR_CUR");
		if (rs.isEmpty()) {
			throw new NotFoundException("Complaint not found!");
		}
		return rs.get(0);
	}

	public void resolverComplaint(String uuidComplaint, String uuidResolver, Integer status) {
		procedureCaller.callNoRefCursor(
				"PKG_COMPLAINT.PRESOLVE_COMPLAINT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPLAINT", String.class, uuidComplaint),
						ProcedureParameter.inputParam("PRN_STATUS", Integer.class, status),
						ProcedureParameter.inputParam("PRS_UUID_RESOLVER", String.class, uuidResolver),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

}
