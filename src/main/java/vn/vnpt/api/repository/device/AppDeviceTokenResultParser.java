package vn.vnpt.api.repository.device;

import vn.vnpt.api.model.AppDeviceToken;
import vn.vnpt.common.Common;

import java.math.BigDecimal;
import java.util.Date;

public class AppDeviceTokenResultParser {

	public static AppDeviceToken parse(Object[] r) {
		String uuidAccount = Common.castObject(r[0], null);
		String deviceId = Common.castObject(r[1], null);
		String deviceToken = Common.castObject(r[2], null);
		BigDecimal status = Common.castObject(r[3], BigDecimal.ZERO);
		Date lastUpdate = Common.castObject(r[4], null);

		AppDeviceToken appDeviceToken = new AppDeviceToken();
		appDeviceToken.setUuidAccount(uuidAccount);
		appDeviceToken.setDeviceId(deviceId);
		appDeviceToken.setDeviceToken(deviceToken);
		appDeviceToken.setStatus(status.intValue());
		appDeviceToken.setLastUpdate(lastUpdate);

		return appDeviceToken;
	}
}
