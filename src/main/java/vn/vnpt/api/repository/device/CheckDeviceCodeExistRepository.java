package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CheckDeviceCodeExistRepository {

	private final static String PROC_CHECK_DEVICE_CODE_EXIST = "PKG_DEVICE.PCHECK_DEVICE_CODE_EXIST";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CheckDeviceCodeExistRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Boolean isExist(String deviceCode) {

		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_CHECK_DEVICE_CODE_EXIST, Arrays.asList(
				ProcedureParameter.inputParam("PRS_DEVICE_CODE", String.class, deviceCode),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get("OS_RESULT");

		return ConstantString.STATUS_DB.DEVICE_CODE_ALREADY_EXIST.equals(res);
	}
}
