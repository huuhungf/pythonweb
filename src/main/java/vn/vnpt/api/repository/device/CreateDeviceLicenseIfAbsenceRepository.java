package vn.vnpt.api.repository.device;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CreateDeviceLicenseIfAbsenceRepository {

	private static final String PROC_CREATE_DEVICE_LICENSE = "PKG_DEVICE.PCREATE_DEVICE_LICENSE_IF_ABSENCE";

	private final ProcedureCallerV2 procedureCaller;

	public CreateDeviceLicenseIfAbsenceRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public DeviceLicense create(String uuidDevice, String serialNumber) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_CREATE_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.outputParam("OUT_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_DEVICE_LICENSE")),
				DeviceLicenseParser::parse
		);

		@SuppressWarnings("unchecked")
		List<DeviceLicense> deviceLicenses = (List<DeviceLicense>) outputs.get("OREF_DEVICE_LICENSE");

		if (deviceLicenses.isEmpty()) {
			throw new NotFoundException(String.format("device license not found %s, %s", uuidDevice, serialNumber));
		}
		return deviceLicenses.get(0);
	}
}
