package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.DeviceDtoIn;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CreateDeviceRepository {
	private static final String PROC_CREATE_DEVICE = "PKG_DEVICE.PCREATE_DEVICE";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public CreateDeviceRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Device createDevice(String superUsername, DeviceDtoIn deviceDtoIn, String privateKey, String publicKey) {
		List<Object> outputs = procedureCaller.call(PROC_CREATE_DEVICE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_NAME", String.class, deviceDtoIn.getName()),
				ProcedureParameter.inputParam("PRS_DEVICE_CODE", String.class, deviceDtoIn.getDeviceCode()),
				ProcedureParameter.inputParam("PRS_OWNER_PASSWORD", String.class, Common.encryptPassword(deviceDtoIn.getPassword())),
				ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, Common.replaceDotInUserName(superUsername)),
				ProcedureParameter.inputParam("PRS_PRIVATE_KEY", String.class, privateKey),
				ProcedureParameter.inputParam("PRS_PUBLIC_KEY", String.class, publicKey),
				ProcedureParameter.inputParam("PRN_LIMIT_DEVICE_LICENSE", Integer.class, deviceDtoIn.getLimitDeviceLicense()),
				ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, deviceDtoIn.getUuidGroup()),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_DEVICE")
		));

		String res = (String) outputs.get(0);
		if (ConstantString.STATUS_DB.SUPERADMIN_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("username not found %s", superUsername));
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<Device> devices = new ResultProcessor<Device>().process(resultList, DeviceResultParser::parse);

		return devices.get(0);
	}
}
