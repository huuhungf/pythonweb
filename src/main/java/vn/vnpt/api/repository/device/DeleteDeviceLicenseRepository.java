package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class DeleteDeviceLicenseRepository {
	private static final String PROC_DELETE_DEVICE_LICENSE = "PKG_DEVICE.PDELETE_DEVICE_LICENSE";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public DeleteDeviceLicenseRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void deleteDeviceLicense(String superUsername, String uuidDevice, String serialNumber) {
		procedureCaller.call(PROC_DELETE_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, Common.replaceDotInUserName(superUsername)),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.outputParam("OUT_RESULT", String.class)
		));
	}
}
