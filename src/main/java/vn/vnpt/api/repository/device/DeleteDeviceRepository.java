package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class DeleteDeviceRepository {
	private static final String PROC_DELETE_DEVICE = "PKG_DEVICE.PDELETE_DEVICE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public DeleteDeviceRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void deleteDevice(String superUsername, String uuidDevice) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_DELETE_DEVICE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, Common.replaceDotInUserName(superUsername)),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.outputParam("OUT_RESULT", String.class)
		));

		String res = (String) outputs.get("OUT_RESULT");

		if (ConstantString.STATUS_DB.SUPERADMIN_NOT_HAVE_PRIVILEGE.equals(res)) {
			throw new AccessDeniedException("access denied");
		}

		if (ConstantString.STATUS_DB.CHILD_GROUP_ALREADY_ASSIGNED.equals(res)) {
			throw new BadRequestException("this node has a child is a device");
		}
	}
}
