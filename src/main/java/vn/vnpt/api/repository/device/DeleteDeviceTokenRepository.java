package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class DeleteDeviceTokenRepository {

	private static final String PROC_DELETE_DEVICE_TOKEN = "PKG_DEVICE.PDELETE_DEVICE_TOKEN";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public DeleteDeviceTokenRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void delete(String username, String deviceId) {
		procedureCaller.call(PROC_DELETE_DEVICE_TOKEN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, Common.replaceDotInUserName(username)),
				ProcedureParameter.inputParam("PRS_APP_DEVICE_ID", String.class, deviceId),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
