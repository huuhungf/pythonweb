package vn.vnpt.api.repository.device;

import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.common.Common;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class DeviceLicenseParser {

	public static DeviceLicense parse(Object[] r) {
		String uuidDevice = Common.castObject(r[0], null);
		String serialNumber = Common.castObject(r[1], null);
		String licenseKey = Common.castObject(r[2], null);

		BigDecimal statusInDb = Common.castObject(r[3], BigDecimal.ZERO);
		Integer status = statusInDb.intValue();

		Date lastUpdate = Common.castObject(r[4], null);
		String deviceId = Common.castObject(r[5], null);
		String deviceToken = Common.castObject(r[6], null);
		String name = Common.castObject(r[7], null);

		DeviceLicense deviceLicense = new DeviceLicense();
		deviceLicense.setUuidDevice(uuidDevice);
		deviceLicense.setSerialNumber(serialNumber);
		deviceLicense.setLicenseKey(licenseKey);
		deviceLicense.setStatus(status);
		deviceLicense.setLastUpdate(lastUpdate);
		deviceLicense.setDeviceId(deviceId);
		deviceLicense.setDeviceToken(deviceToken);
		deviceLicense.setName(name);

		return deviceLicense;
	}

	public static DeviceLicense parse(ResultSet rs) throws SQLException {
		String uuidDevice = rs.getString("UUID_DEVICE");
		String serialNumber = rs.getString("SERIAL_NUMBER");
		String licenseKey = rs.getString("LICENSE_KEY");
		BigDecimal status = Common.getDefaultIfNull(rs.getBigDecimal("STATUS"), BigDecimal.ZERO);
		Date lastUpdate = rs.getDate("LAST_UPDATE");
		String deviceId = rs.getString("DEVICE_ID");
		String deviceToken = rs.getString("DEVICE_TOKEN");
		String name = rs.getString("NAME");
		BigDecimal faceProb = Common.getDefaultIfNull(rs.getBigDecimal("FACE_PROB"), BigDecimal.ZERO);
		BigDecimal faceMask = Common.getDefaultIfNull(rs.getBigDecimal("FACE_MASK"), BigDecimal.ZERO);
		BigDecimal faceLiveness = Common.getDefaultIfNull(rs.getBigDecimal("FACE_LIVENESS"), BigDecimal.ZERO);
		BigDecimal collectImage = Common.getDefaultIfNull(rs.getBigDecimal("COLLECT_IMAGE"), BigDecimal.ZERO);
		BigDecimal runMode = Common.getDefaultIfNull(rs.getBigDecimal("RUN_MODE"), BigDecimal.ZERO);
		String pinCode = rs.getString("PIN_CODE");
		BigDecimal virtualAssistant = Common.getDefaultIfNull(rs.getBigDecimal("VIRTUAL_ASSISTANT"), BigDecimal.ZERO);
		String os = rs.getString("OS");
		Integer battery = rs.getInt("BATTERY");
		Float latitude = rs.getFloat("LATITUDE");
		Float longitude = rs.getFloat("LONGITUDE");
		String wifi = rs.getString("WIFI");
		String appVersion = rs.getString("APP_VERSION");

		DeviceLicense deviceLicense = new DeviceLicense();
		deviceLicense.setUuidDevice(uuidDevice);
		deviceLicense.setSerialNumber(serialNumber);
		deviceLicense.setLicenseKey(licenseKey);
		deviceLicense.setStatus(status.intValue());
		deviceLicense.setLastUpdate(lastUpdate);
		deviceLicense.setDeviceId(deviceId);
		deviceLicense.setDeviceToken(deviceToken);
		deviceLicense.setName(name);
		deviceLicense.setFaceProb(faceProb.floatValue());
		deviceLicense.setFaceMask(faceMask.intValue());
		deviceLicense.setFaceLiveness(faceLiveness.intValue());
		deviceLicense.setCollectImage(collectImage.intValue());
		deviceLicense.setRunMode(runMode.intValue());
		deviceLicense.setPinCode(pinCode);
		deviceLicense.setVirtualAssistant(virtualAssistant.intValue());
		deviceLicense.setOs(os);
		deviceLicense.setBattery(battery);
		deviceLicense.setLatitude(latitude);
		deviceLicense.setLongitude(longitude);
		deviceLicense.setWifi(wifi);
		deviceLicense.setAppVersion(appVersion);

		return deviceLicense;
	}
}
