package vn.vnpt.api.repository.device;

import vn.vnpt.api.model.Device;
import vn.vnpt.common.Common;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DeviceResultParser {
	public static Device parse(Object[] r) {
		Device device = new Device();

		String uuidDevice = Common.castObject(r[0], null);
		String name = Common.castObject(r[1], null);
		String deviceCode = Common.castObject(r[2], null);
		String type = Common.castObject(r[3], null);
		BigDecimal status = Common.castObject(r[4], BigDecimal.ZERO);
		String deviceId = Common.castObject(r[5], null);
		String uuidCompany = Common.castObject(r[6], null);
		String ownedAccount = Common.castObject(r[7], null);
		String publicKey = Common.castObject(r[8], null);
		String privateKey = Common.castObject(r[9], null);
		BigDecimal limitDeviceLicense = Common.castObject(r[10], BigDecimal.ZERO);

		device.setUuidDevice(uuidDevice);
		device.setName(name);
		device.setDeviceCode(deviceCode);
		device.setType(type);
		device.setStatus(status.intValue());
		device.setDeviceId(deviceId);
		device.setUuidCompany(uuidCompany);
		device.setOwnedAccount(ownedAccount);
		device.setPublicKey(publicKey);
		device.setPrivateKey(privateKey);
		device.setLimitDeviceLicense(limitDeviceLicense.intValue());

		return device;
	}

	public static Device parse(ResultSet r) throws SQLException {
		Device device = new Device();

		String uuidDevice = r.getString("UUID_DEVICE");
		String name = r.getString("NAME");
		String deviceCode = r.getString("DEVICE_CODE");
		String type = r.getString("TYPE");
		BigDecimal status = Common.getDefaultIfNull(r.getBigDecimal("STATUS"), BigDecimal.ZERO);
		String deviceId = r.getString("DEVICE_ID");
		String uuidCompany = r.getString("UUID_COMPANY");
		String ownedAccount = r.getString("OWNED_ACCOUNT");
		String publicKey = r.getString("PUBLIC_KEY");
		String privateKey = r.getString("PRIVATE_KEY");
		Integer limitDeviceLicense = r.getInt("LIMIT_DEVICE_LICENSE");

		device.setUuidDevice(uuidDevice);
		device.setName(name);
		device.setDeviceCode(deviceCode);
		device.setType(type);
		device.setStatus(status.intValue());
		device.setDeviceId(deviceId);
		device.setUuidCompany(uuidCompany);
		device.setOwnedAccount(ownedAccount);
		device.setPublicKey(publicKey);
		device.setPrivateKey(privateKey);
		device.setLimitDeviceLicense(limitDeviceLicense);

		return device;
	}
}
