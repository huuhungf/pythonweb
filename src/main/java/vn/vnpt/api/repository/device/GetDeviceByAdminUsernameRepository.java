package vn.vnpt.api.repository.device;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetDeviceByAdminUsernameRepository {

	private static final String PROC_GET_DEVICE_BY_ADMIN_USERNAME = "PKG_DEVICE.PGET_DEVICE_BY_ADMIN_USERNAME";

	private final ProcedureCaller procedureCaller;

	public GetDeviceByAdminUsernameRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Device get(String adminUsername) {
		List<Object> outputs = procedureCaller.call(PROC_GET_DEVICE_BY_ADMIN_USERNAME, Arrays.asList(
			ProcedureParameter.inputParam("PRS_ADMIN_USERNAME", String.class, Common.replaceDotInUserName(adminUsername)),
			ProcedureParameter.outputParam("OS_RESULT", String.class),
			ProcedureParameter.refCursorParam("OREF_DEVICE")
		));

		String res = (String) outputs.get(0);

		if (ConstantString.STATUS_DB.DEVICE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("device not found for username: %s", adminUsername));
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);

		List<Device> devices = new ResultProcessor<Device>().process(resultList, DeviceResultParser::parse);

		return devices.get(0);
	}
}
