package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetDeviceByDeviceCodeRepository {

	private final static String PROC_GET_DEVICE_BY_DEVICE_CODE = "PKG_DEVICE.PGET_DEVICE_BY_CODE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetDeviceByDeviceCodeRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Device getDevice(String deviceCode) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_DEVICE_BY_DEVICE_CODE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_DEVICE_CODE", String.class, deviceCode),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_DEVICE")),
				DeviceResultParser::parse
		);

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.DEVICE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("Device not found: %s", deviceCode), ErrorCode.IDG_00002001);
		}

		@SuppressWarnings("unchecked")
		List<Device> devices = (List<Device>) outputs.get("OREF_DEVICE");
		return devices.get(0);
	}
}
