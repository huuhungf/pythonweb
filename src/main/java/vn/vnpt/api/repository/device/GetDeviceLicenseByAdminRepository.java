package vn.vnpt.api.repository.device;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetDeviceLicenseByAdminRepository {

	private static final String PROC_GET_DEVICE_LICENSE_BY_ADMIN = "PKG_DEVICE.PGET_DEVICE_LICENSE_BY_ADMIN";

	private final ProcedureCallerV2 procedureCaller;

	public GetDeviceLicenseByAdminRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public DeviceLicense get(String adminUsername, String serialNumber) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_DEVICE_LICENSE_BY_ADMIN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_ADMIN_USERNAME", String.class, adminUsername),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_DEVICE_LICENSE")),
				DeviceLicenseParser::parse
		);

		@SuppressWarnings("unchecked")
		List<DeviceLicense> deviceLicenses = (List<DeviceLicense>) outputs.get("OREF_DEVICE_LICENSE");

		if (deviceLicenses.isEmpty()) {
			throw new NotFoundException(String.format("device license not found %s, %s", adminUsername, serialNumber));
		}

		return deviceLicenses.get(0);
	}
}
