package vn.vnpt.api.repository.device;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AppDeviceToken;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetDeviceTokenRepository {
	private static final String PROC_GET_DEVICE_TOKEN = "PKG_DEVICE.PGET_DEVICE_TOKEN";

	private final ProcedureCaller procedureCaller;

	public GetDeviceTokenRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public AppDeviceToken getDeviceToken(String username, String deviceId) {
		List<Object> outputs = procedureCaller.call(PROC_GET_DEVICE_TOKEN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, Common.replaceDotInUserName(username)),
				ProcedureParameter.inputParam("PRS_DEVICE_ID", String.class, deviceId),
				ProcedureParameter.outputParam("OUT_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_DEVICE_TOKEN")
		));

		String res = (String) outputs.get(0);
		if (ConstantString.STATUS_DB.DEVICE_TOKEN_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("device token not found with username %s, deviceId %s", username, deviceId));
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<AppDeviceToken> appDeviceTokens = new ResultProcessor<AppDeviceToken>().process(resultList, AppDeviceTokenResultParser::parse);
		return appDeviceTokens.get(0);
	}
}
