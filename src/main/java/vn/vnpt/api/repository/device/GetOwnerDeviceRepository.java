package vn.vnpt.api.repository.device;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.api.repository.user.AccountResultParser;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetOwnerDeviceRepository {
	private static final String PROC_GET_OWNER_DEVICE = "PKG_DEVICE.PGET_OWNER_DEVICE";

	private final ProcedureCaller procedureCaller;

	public GetOwnerDeviceRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account getOwner(String uuidDevice) {
		List<Object> outputs = procedureCaller.call(PROC_GET_OWNER_DEVICE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.outputParam("OUT_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_OWNER_DEVICE")
		));

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<Account> accounts = new ResultProcessor<Account>().process(resultList, AccountResultParser::parse);

		if (accounts.isEmpty()) {
			throw new NotFoundException(String.format("owner not found with uuidDevice %s", uuidDevice));
		}

		return accounts.get(0);
	}
}
