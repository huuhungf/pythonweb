package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetTotalDeviceLicenseRepository {
	private static final String PROC_COUNT_CURRENT_DEVICE_LICENSE = "PKG_DEVICE.PGET_TOTAL_DEVICE_LICENSE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetTotalDeviceLicenseRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Integer count(String uuidDevice) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_COUNT_CURRENT_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.outputParam("ON_RESULT", Integer.class)
		));

		Integer count = (Integer) outputs.get("ON_RESULT");

		return count;
	}
}
