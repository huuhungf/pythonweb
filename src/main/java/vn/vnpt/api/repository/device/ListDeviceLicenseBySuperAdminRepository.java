package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class ListDeviceLicenseBySuperAdminRepository {
	private static final String PROC_LIST_DEVICE_LICENSE = "PKG_DEVICE.PGET_LIST_DEVICE_LICENSE_BY_SUPERADMIN";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ListDeviceLicenseBySuperAdminRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<DeviceLicense> listDeviceLicense(String superUsername) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, superUsername),
				ProcedureParameter.refCursorParam("OREF_DEVICE_LICENSE")),
				DeviceLicenseParser::parse
		);

		@SuppressWarnings("unchecked")
		List<DeviceLicense> deviceLicenses = (List<DeviceLicense>) outputs.get("OREF_DEVICE_LICENSE");

		return deviceLicenses;
	}
}
