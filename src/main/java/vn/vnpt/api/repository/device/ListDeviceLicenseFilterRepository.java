package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListDeviceLicenseFilterRepository {
	private static final String PROC_LIST_DEVICE_LICENSE_FILTER = "PKG_DEVICE.PGET_LIST_DEVICE_LICENSE_FILTER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ListDeviceLicenseFilterRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<DeviceLicense> listDeviceLicense(String uuidDevice) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_DEVICE_LICENSE_FILTER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.refCursorParam("OUT_CUR")),
				DeviceLicenseParser::parse
				);

		@SuppressWarnings("unchecked")
		List<DeviceLicense> deviceLicenses = (List<DeviceLicense>) outputs.get("OUT_CUR");

		return deviceLicenses;
	}
}
