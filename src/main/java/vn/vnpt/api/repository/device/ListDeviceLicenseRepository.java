package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class ListDeviceLicenseRepository {
	private static final String PROC_LIST_DEVICE_LICENSE = "PKG_DEVICE.PGET_LIST_DEVICE_LICENSE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ListDeviceLicenseRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<DeviceLicense> listDeviceLicense(String uuidDevice) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.refCursorParam("OREF_DEVICE_LICENSE")),
				DeviceLicenseParser::parse
		);

		@SuppressWarnings("unchecked")
		List<DeviceLicense> deviceLicenses = (List<DeviceLicense>) outputs.get("OREF_DEVICE_LICENSE");
		return deviceLicenses;
	}
}
