package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.out.DeviceDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListDeviceRepository {
	private static final String PROC_LIST_DEVICE = "PKG_DEVICE.PGET_LIST_DEVICE_FILTER";

	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public ListDeviceRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

//	public PagingDTO<DeviceDtoOut> listDevice(String superUsername, PagingDtoIn pagingDtoIn) {
//		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_DEVICE, Arrays.asList(
//				ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
//				ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
//				ProcedureParameter.inputParam("PRS_USERNAME", String.class, Common.replaceDotInUserName(superUsername)),
//				ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
//				ProcedureParameter.refCursorParam("OUT_CUR")),
//				r -> {
//					String uuidDevice = r.getString("UUID_DEVICE");
//					String name = r.getString("NAME");
//					String deviceCode = r.getString("DEVICE_CODE");
//					Integer deviceLicenseTotal = r.getInt("TOTAL_DEVICE_LICENSE");
//					Integer limitDeviceLicense = r.getInt("LIMIT_DEVICE_LICENSE");
//
//					DeviceDtoOut deviceDtoOut = new DeviceDtoOut();
//					deviceDtoOut.setUuidDevice(uuidDevice);
//					deviceDtoOut.setName(name);
//					deviceDtoOut.setDeviceCode(deviceCode);
//					deviceDtoOut.setDeviceLicenseTotal(deviceLicenseTotal);
//					deviceDtoOut.setLimitDeviceLicense(limitDeviceLicense);
//
//					return deviceDtoOut;
//				}
//				);
//
//		Long total = (Long) outputs.get("OUT_TOTAL");
//		long totalPages = total / pagingDtoIn.getMaxSize();
//		if (total % pagingDtoIn.getMaxSize() != 0) {
//			totalPages++;
//		}
//
//		PagingDTO<DeviceDtoOut> pagingDTO = new PagingDTO<>();
//		pagingDTO.setPage(pagingDtoIn.getPage());
//		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
//		pagingDTO.setTotalElement(total);
//		pagingDTO.setTotalPages(totalPages);
//
//		if (pagingDtoIn.getPage() > totalPages) {
//			pagingDTO.setData(new ArrayList<>());
//			return pagingDTO;
//		}
//
//		@SuppressWarnings("unchecked")
//		List<DeviceDtoOut> data = (List<DeviceDtoOut>) outputs.get("OUT_CUR");
//
//		pagingDTO.setData(data);
//		return pagingDTO;
//	}

	public PagingDTO<DeviceDtoOut> listDevice(String superUsername, PagingDtoIn pagingDtoIn) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_DEVICE, Arrays.asList(
				ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
				ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, Common.replaceDotInUserName(superUsername)),
				ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
				ProcedureParameter.refCursorParam("OUT_CUR")),
				DeviceDtoOut.class
		);

		Long total = (Long) outputs.get("OUT_TOTAL");
		long totalPages = total / pagingDtoIn.getMaxSize();
		if (total % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}

		PagingDTO<DeviceDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalElement(total);
		pagingDTO.setTotalPages(totalPages);

		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<DeviceDtoOut> data = (List<DeviceDtoOut>) outputs.get("OUT_CUR");

		pagingDTO.setData(data);
		return pagingDTO;
	}
}
