package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AppDeviceToken;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListDeviceTokenRepository {

	private static final String PROC_LIST_DEVICE_TOKEN = "PKG_DEVICE.PGET_LIST_DEVICE_TOKEN";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public ListDeviceTokenRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<AppDeviceToken> list(String uuidAccount) {
		List<Object> outputs = procedureCaller.call(PROC_LIST_DEVICE_TOKEN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.refCursorParam("OREF_DEVICE_TOKEN")
		));

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(0);
		List<AppDeviceToken> appDeviceTokens = new ResultProcessor<AppDeviceToken>().process(resultList,
				AppDeviceTokenResultParser::parse);

		return appDeviceTokens;
	}
}
