package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.ResetPasswordDeviceDtoIn;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ResetPasswordDeviceRepository {
	private static final String PROC_RESET_PASSWORD_DEVICE = "PKG_DEVICE.PRESET_PASSWORD_DEVICE";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public ResetPasswordDeviceRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void resetPasswordDevice(String superUsername, ResetPasswordDeviceDtoIn resetPasswordDeviceDtoIn) {
		procedureCaller.call(PROC_RESET_PASSWORD_DEVICE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, Common.replaceDotInUserName(superUsername)),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, resetPasswordDeviceDtoIn.getUuidDevice()),
				ProcedureParameter.inputParam("PRS_NEW_PASSWORD", String.class, Common.encryptPassword(resetPasswordDeviceDtoIn.getPassword())),
				ProcedureParameter.outputParam("OUT_RESULT", String.class)
		));
	}
}
