package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class TabletCreateDeviceLicenseRepository {
	private static final String PROC_CREATE_DEVICE_LICENSE = "PKG_DEVICE.PCREATE_DEVICE_LICENSE_TABLET";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public TabletCreateDeviceLicenseRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public DeviceLicense createDeviceLicense(String superUsername, String serialNumber, String uuidDevice, Integer status) {
		List<Object> outputs = procedureCaller.call(PROC_CREATE_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, superUsername),
				ProcedureParameter.inputParam("PRN_STATUS", Integer.class, status),
				ProcedureParameter.outputParam("OUT_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_DEVICE_LICENSE")
		));

		String res = (String) outputs.get(0);
		if (ConstantString.STATUS_DB.SUPERADMIN_NOT_HAVE_PRIVILEGE.equals(res)) {
			throw new AccessDeniedException("access denied");
		}
		if (ConstantString.STATUS_DB.DEVICE_LICENSE_EXIST.equals(res)) {
			throw new BadRequestException("device license already exist");
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<DeviceLicense> deviceLicenses = new ResultProcessor<DeviceLicense>().process(resultList, DeviceLicenseParser::parse);
		return deviceLicenses.get(0);
	}
}
