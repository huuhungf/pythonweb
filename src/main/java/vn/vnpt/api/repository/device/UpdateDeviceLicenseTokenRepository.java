package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateDeviceLicenseTokenRepository {
	private static final String PROC_UPDATE_DEVICE_LICENSE_TOKEN = "PKG_DEVICE.PUPDATE_DEVICE_LICENSE_TOKEN";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public UpdateDeviceLicenseTokenRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String uuidDevice, String serialNumber, String deviceId, String deviceToken) {
		procedureCaller.call(PROC_UPDATE_DEVICE_LICENSE_TOKEN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.inputParam("PRS_DEVICE_ID", String.class, deviceId),
				ProcedureParameter.inputParam("PRS_DEVICE_TOKEN", String.class, deviceToken),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
