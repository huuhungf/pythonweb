package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateDeviceRepository {
	private static final String PROC_UPDATE_DEVICE = "PKG_DEVICE.PUPDATE_DEVICE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public UpdateDeviceRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Device updateDevice(String uuidDevice, String name, Integer limitDeviceLicense) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_UPDATE_DEVICE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_NAME", String.class, name),
				ProcedureParameter.inputParam("PRN_LIMIT_DEVICE_LICENSE", Integer.class, limitDeviceLicense),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_DEVICE")),
				DeviceResultParser::parse
		);

		String res = (String) outputs.get("");

		if (ConstantString.STATUS_DB.DEVICE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("device not found %s", uuidDevice));
		}

		if (ConstantString.STATUS_DB.NOT_ALLOWED.equals(res)) {
			throw new BadRequestException("limitDeviceLicense less than num of current device license");
		}

		@SuppressWarnings("unchecked")
		List<Device> devices = (List<Device>) outputs.get("OREF_DEVICE");
		return devices.get(0);
	}
}
