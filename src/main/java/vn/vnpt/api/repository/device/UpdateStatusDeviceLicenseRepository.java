package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateStatusDeviceLicenseRepository {
	private static final String PROC_UPDATE_STATUS_DEVICE_LICENSE = "PKG_DEVICE.PUPDATE_STATUS_DEVICE_LICENSE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public UpdateStatusDeviceLicenseRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String ownerUsername, String serialNumber, Integer status) {
		procedureCaller.callNoRefCursor(PROC_UPDATE_STATUS_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_OWNER_USERNAME", String.class, Common.replaceDotInUserName(ownerUsername)),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.inputParam("PRS_STATUS", Integer.class, status),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
