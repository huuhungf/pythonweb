package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateStatusDeviceTokenRepository {

	private static final String PROC_UPDATE_STATUS_DEVICE_TOKEN = "PKG_DEVICE.PUPDATE_STATUS_DEVICE_TOKEN";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public UpdateStatusDeviceTokenRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String uuidAccount, Integer status) {
		procedureCaller.call(PROC_UPDATE_STATUS_DEVICE_TOKEN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.inputParam("PRN_STATUS", Integer.class, status),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
