package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.exception.BadRequestException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebUpdateCollectImageDeviceLicenseRepository {
	private static final String PROC_UPDATE_COLLECT_IMAGE_DEVICE_LICENSE = "PKG_DEVICE.PWEB_UPDATE_COLLECT_IMAGE_DEVICE_LICENSE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebUpdateCollectImageDeviceLicenseRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public DeviceLicense update(String uuidDevice, String serialNumber, Integer collectImage) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_UPDATE_COLLECT_IMAGE_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.inputParam("PRN_COLLECT_IMAGE", Integer.class, collectImage),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_CUR")),
				r -> {
					String uuidDeviceInDb = r.getString("UUID_DEVICE");
					String serialNumberInDb = r.getString("SERIAL_NUMBER");
					BigDecimal status = Common.getDefaultIfNull(r.getBigDecimal("STATUS"), BigDecimal.ZERO);
					String name = r.getString("NAME");
					BigDecimal faceProb = Common.getDefaultIfNull(r.getBigDecimal("FACE_PROB"), BigDecimal.ZERO);
					BigDecimal faceMask = Common.getDefaultIfNull(r.getBigDecimal("FACE_MASK"), BigDecimal.ZERO);
					BigDecimal faceLiveness = Common.getDefaultIfNull(r.getBigDecimal("FACE_LIVENESS"), BigDecimal.ZERO);
					BigDecimal collectImageDb = Common.getDefaultIfNull(r.getBigDecimal("COLLECT_IMAGE"), BigDecimal.ZERO);
					String deviceToken = r.getString("DEVICE_TOKEN");

					DeviceLicense deviceLicense = new DeviceLicense();
					deviceLicense.setUuidDevice(uuidDeviceInDb);
					deviceLicense.setSerialNumber(serialNumberInDb);
					deviceLicense.setStatus(status.intValue());
					deviceLicense.setName(name);
					deviceLicense.setFaceProb(faceProb.floatValue());
					deviceLicense.setFaceMask(faceMask.intValue());
					deviceLicense.setFaceLiveness(faceLiveness.intValue());
					deviceLicense.setDeviceToken(deviceToken);
					deviceLicense.setCollectImage(collectImageDb.intValue());

					return deviceLicense;
				}
		);

		@SuppressWarnings("unchecked")
		List<DeviceLicense> deviceLicenseList = (List<DeviceLicense>) outputs.get("OREF_CUR");

		if (deviceLicenseList.isEmpty()) {
			throw new BadRequestException(String.format("serial number not found %s", serialNumber));
		}

		return deviceLicenseList.get(0);
	}
}
