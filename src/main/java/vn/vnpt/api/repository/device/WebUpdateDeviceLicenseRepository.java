package vn.vnpt.api.repository.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.UpdateDeviceLicenseDtoIn;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebUpdateDeviceLicenseRepository {
	private static final String PROC_UPDATE_DEVICE_LICENSE = "PKG_DEVICE.PWEB_UPDATE_DEVICE_LICENSE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebUpdateDeviceLicenseRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public DeviceLicense update(UpdateDeviceLicenseDtoIn updateDeviceLicenseDtoIn) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_UPDATE_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, updateDeviceLicenseDtoIn.getUuidDevice()),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, updateDeviceLicenseDtoIn.getSerialNumber()),
				ProcedureParameter.inputParam("PRS_NAME", String.class, updateDeviceLicenseDtoIn.getName()),
				ProcedureParameter.inputParam("PRN_FACE_PROB", Float.class, updateDeviceLicenseDtoIn.getFaceProb()),
				ProcedureParameter.inputParam("PRN_FACE_MASK", Integer.class, updateDeviceLicenseDtoIn.getFaceMask()),
				ProcedureParameter.inputParam("PRN_FACE_LIVENESS", Integer.class, updateDeviceLicenseDtoIn.getFaceLiveness()),
				ProcedureParameter.inputParam("PRS_PIN_CODE", String.class, updateDeviceLicenseDtoIn.getPinCode()),
				ProcedureParameter.inputParam("PRN_VIRTUAL_ASSISTANT", Integer.class, updateDeviceLicenseDtoIn.getVirtualAssistant()),
				ProcedureParameter.inputParam("PRN_COLLECT_IMAGE", Integer.class, updateDeviceLicenseDtoIn.getCollectImage()),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_DEVICE_LICENSE")),
				DeviceLicenseParser::parse
		);

		@SuppressWarnings("unchecked")
		List<DeviceLicense> deviceLicenseList = (List<DeviceLicense>) outputs.get("OREF_DEVICE_LICENSE");

		if (deviceLicenseList.isEmpty()) {
			throw new BadRequestException(String.format("serial number not found %s", updateDeviceLicenseDtoIn.getSerialNumber()));
		}

		return deviceLicenseList.get(0);
	}
}
