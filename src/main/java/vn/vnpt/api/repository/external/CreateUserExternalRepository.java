package vn.vnpt.api.repository.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.ExternalCreateAccountDtoIn;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.PrivilegeUpdateUserException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CreateUserExternalRepository {

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CreateUserExternalRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void create(String userName, ExternalCreateAccountDtoIn createAccountDtoIn, String rawPassword) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(
				"PKG_USER.PEXTERNAL_CREATE_USER",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_USER_CODE", String.class, createAccountDtoIn.getUserCode()),
						ProcedureParameter.inputParam("PRS_FULL_NAME", String.class, createAccountDtoIn.getFullName()),
						ProcedureParameter.inputParam("PRS_EMAIL", String.class, createAccountDtoIn.getEmail()),
						ProcedureParameter.inputParam("PRS_GENDER", String.class, createAccountDtoIn.getGender()),
						ProcedureParameter.inputParam("PRS_PASSWORD", String.class, Common.encryptPassword(rawPassword)),
						ProcedureParameter.inputParam("PRS_USER_SUPER_ADMIN", String.class, userName),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);

		String result = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.C_RESULT_PRIVILEGE_UPDATE_USER.equals(result)) {
			throw new PrivilegeUpdateUserException(ConstantString.STATUS_DB.C_RESULT_PRIVILEGE_UPDATE_USER);
		} else if (ConstantString.STATUS_DB.C_RESULT_SYSTEM_ERROR.equals(result)) {
			throw new RuntimeException(ConstantString.STATUS_DB.C_RESULT_SYSTEM_ERROR);
		}
	}
}
