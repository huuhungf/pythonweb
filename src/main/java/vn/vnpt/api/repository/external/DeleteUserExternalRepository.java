package vn.vnpt.api.repository.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.exception.PrivilegeUpdateUserException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class DeleteUserExternalRepository {
	private static final String PEXTERNAL_DELETE_USER = "PKG_USER.PEXTERNAL_DELETE_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public DeleteUserExternalRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void delete(String userName, String userCode) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PEXTERNAL_DELETE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USER_CODE", String.class, userCode),
				ProcedureParameter.inputParam("PRS_USER_SUPER_ADMIN", String.class, userName),
				ProcedureParameter.outputParam("OS_RESULT", String.class))
		);

		String result = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.C_RESULT_PRIVILEGE_UPDATE_USER.equals(result)) {
			throw new PrivilegeUpdateUserException(ConstantString.STATUS_DB.C_RESULT_PRIVILEGE_UPDATE_USER);
		} else if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(result)) {
			throw new NotFoundException(ConstantString.STATUS_DB.USER_NOT_EXIST);
		} else if (ConstantString.STATUS_DB.C_RESULT_SYSTEM_ERROR.equals(result)) {
			throw new RuntimeException(ConstantString.STATUS_DB.C_RESULT_SYSTEM_ERROR);
		}
	}
}
