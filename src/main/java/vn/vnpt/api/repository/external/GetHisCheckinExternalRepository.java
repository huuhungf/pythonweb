package vn.vnpt.api.repository.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.ExternalListCheckinDtoIn;
import vn.vnpt.api.dto.out.HisCheckinExternalDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetHisCheckinExternalRepository {
	private static final String PROC_LIST_EXTERNAL = "PKG_CHECKIN.PGET_LIST_FILTER_BY_COMPANY";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetHisCheckinExternalRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<HisCheckinExternalDtoOut> list(String userName, ExternalListCheckinDtoIn listCheckinDtoIn, PagingDtoIn pagingDtoIn) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_EXTERNAL, Arrays.asList(
				ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, userName),
				ProcedureParameter.inputParam("PRS_START_DATE", String.class, listCheckinDtoIn.getStartDate()),
				ProcedureParameter.inputParam("PRS_END_DATE", String.class, listCheckinDtoIn.getEndDate()),
				ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
				ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
				ProcedureParameter.inputParam("PRS_LIST_USER_CODE", String.class, listCheckinDtoIn.getUserCodes()),
				ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
				ProcedureParameter.refCursorParam("OUT_CUR")),
				rs -> {
					String userCode = rs.getString("USER_CODE");
					String dateCheckin = rs.getString("DATE_CHECKIN");
					String uuidHisCheckin = rs.getString("UUID_HIS_CHECKIN");
					String createdDate = rs.getString("CREATED_DATE");

					HisCheckinExternalDtoOut hisCheckinExternalDtoOut = new HisCheckinExternalDtoOut();
					hisCheckinExternalDtoOut.setUserCode(userCode);
					hisCheckinExternalDtoOut.setDateCheckin(dateCheckin);
					hisCheckinExternalDtoOut.setUuidHisCheckin(uuidHisCheckin);
					hisCheckinExternalDtoOut.setCreatedDate(createdDate);

					return hisCheckinExternalDtoOut;
				}
		);

		Long totalElement = (Long) outputs.get("OUT_TOTAL");
		long totalPages = totalElement / pagingDtoIn.getMaxSize();
		if (totalElement % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}

		PagingDTO<HisCheckinExternalDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalElement(totalElement);
		pagingDTO.setTotalPages(totalPages);

		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<HisCheckinExternalDtoOut> data = (List<HisCheckinExternalDtoOut>) outputs.get("OUT_CUR");

		pagingDTO.setData(data);

		return pagingDTO;
	}
}
