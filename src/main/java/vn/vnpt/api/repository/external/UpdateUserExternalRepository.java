package vn.vnpt.api.repository.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.ExternalUpdateAccountDtoIn;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.PrivilegeUpdateUserException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateUserExternalRepository {
    private static final String PEXTERNAL_UPDATE_USER = "PKG_USER.PEXTERNAL_UPDATE_USER";

    private final ProcedureCallerV2 procedureCaller;

    @Autowired
    public UpdateUserExternalRepository(ProcedureCallerV2 procedureCaller) {
        this.procedureCaller = procedureCaller;
    }

    public void update(String userName, ExternalUpdateAccountDtoIn externalUpdateAccountDtoIn) {
        Map<String, Object> outputs = procedureCaller.callNoRefCursor(PEXTERNAL_UPDATE_USER, Arrays.asList(
                ProcedureParameter.inputParam("PRS_USER_CODE", String.class, externalUpdateAccountDtoIn.getUserCode()),
                ProcedureParameter.inputParam("PRS_FULL_NAME", String.class, externalUpdateAccountDtoIn.getFullName()),
                ProcedureParameter.inputParam("PRS_EMAIL", String.class, externalUpdateAccountDtoIn.getEmail()),
                ProcedureParameter.inputParam("PRS_GENDER", String.class, externalUpdateAccountDtoIn.getGender()),
                ProcedureParameter.inputParam("PRS_NEW_USER_CODE", String.class, externalUpdateAccountDtoIn.getNewUserCode()),
                ProcedureParameter.inputParam("PRS_USER_SUPER_ADMIN", String.class, userName),
                ProcedureParameter.outputParam("OS_RESULT", String.class))
        );

        String result = (String)outputs.get("OS_RESULT");
        if (ConstantString.STATUS_DB.C_RESULT_PRIVILEGE_UPDATE_USER.equals(result)){
            throw new PrivilegeUpdateUserException(ConstantString.STATUS_DB.C_RESULT_PRIVILEGE_UPDATE_USER);
        }else if(ConstantString.STATUS_DB.C_RESULT_SYSTEM_ERROR.equals(result)){
            throw new RuntimeException(ConstantString.STATUS_DB.C_RESULT_SYSTEM_ERROR);
        }
    }
}
