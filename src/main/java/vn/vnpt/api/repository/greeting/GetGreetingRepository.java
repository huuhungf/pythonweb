package vn.vnpt.api.repository.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Greeting;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetGreetingRepository {

	private final static String PROC_GET_GREETING = "PKG_GREETING.PGET_GREETING";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetGreetingRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Greeting get(String uuidGreeting) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_GREETING, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_GREETING", String.class, uuidGreeting),
				ProcedureParameter.refCursorParam("OUT_REF")),
				GreetingResultParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Greeting> greetings = (List<Greeting>) outputs.get("OUT_REF");

		return greetings.get(0);
	}
}
