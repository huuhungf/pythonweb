package vn.vnpt.api.repository.greeting;

import vn.vnpt.api.model.Greeting;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GreetingResultParser {
	public static Greeting parse(ResultSet rs) throws SQLException {
		String uuidGreeting = rs.getString("UUID_GREETING");
		String text = rs.getString("TEXT");

		Greeting greeting = new Greeting();
		greeting.setUuidGreeting(uuidGreeting);
		greeting.setText(text);
		return greeting;
	}
}
