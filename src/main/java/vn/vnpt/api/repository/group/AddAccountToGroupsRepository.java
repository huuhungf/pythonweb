package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class AddAccountToGroupsRepository {
	private static final String PROC_CREATE_GROUP = "PKG_GROUP.PADD_USER_TO_GROUPS";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public AddAccountToGroupsRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void addUserToGroups(String uuidAccount, String uuidGroups) {
		procedureCaller.callNoRefCursor(PROC_CREATE_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.inputParam("PRS_LIST_UUID_GROUP", String.class, uuidGroups),
				ProcedureParameter.outputParam("OS_RESULT", String.class)));
	}
}
