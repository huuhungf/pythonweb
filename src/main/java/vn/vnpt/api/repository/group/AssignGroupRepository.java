package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class AssignGroupRepository {
	private static final String PROC_ASSIGN_GROUP = "PKG_GROUP.PASSIGN_GROUP_DEVICE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public AssignGroupRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void assign(String uuidGroup, String uuidDevice) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_ASSIGN_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.GROUP_ALREADY_ASSIGNED.equals(res)) {
			throw new BadRequestException("group already assigned to device");
		}
	}
}
