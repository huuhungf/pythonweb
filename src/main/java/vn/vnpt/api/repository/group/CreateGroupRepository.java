package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Group;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CreateGroupRepository {
	private static final String PROC_CREATE_GROUP = "PKG_GROUP.PCREATE_GROUP";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CreateGroupRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Group create(String username, String uuidDevice, String groupName, String uuidParentGroup) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_CREATE_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_GROUP_NAME", String.class, groupName),
				ProcedureParameter.inputParam("PRS_UUID_PARENT_GROUP", String.class, uuidParentGroup),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_RESULT")),
				GroupResultParser::parse
		);

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.NOT_ALLOWED.equals(res)) {
			throw new BadRequestException("not allowed");
		}

		@SuppressWarnings("unchecked")
		List<Group> groups = (List<Group>) outputs.get("OREF_RESULT");

		return groups.get(0);
	}
}
