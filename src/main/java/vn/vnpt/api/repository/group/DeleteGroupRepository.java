package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class DeleteGroupRepository {
	private static final String PROC_DELETE_GROUP = "PKG_GROUP.PDELETE_GROUP";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public DeleteGroupRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void delete(String username, String uuidGroup) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_DELETE_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_ACTOR_USERNAME", String.class, username),
				ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.CHILD_GROUP_ALREADY_ASSIGNED.equals(res)) {
			throw new BadRequestException("this node has a child is a device");
		}
	}
}
