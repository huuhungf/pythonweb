package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Group;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetGroupByDeviceRepository {
	private static final String PROC_GET_GROUP = "PKG_GROUP.PGET_GROUP_BY_DEVICE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetGroupByDeviceRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Group get(String uuidDevice) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_REF")),
				GroupResultParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Group> groups = (List<Group>) outputs.get("OUT_REF");
		if (groups.isEmpty()) {
			throw new NotFoundException(String.format("group not found for device: %s", uuidDevice));
		}

		return groups.get(0);
	}
}
