package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Group;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetGroupRepository {
	private static final String PROC_GET_GROUP = "PKG_GROUP.PGET_GROUP";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetGroupRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Group get(String uuidGroup) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_REF")),
				GroupResultParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Group> groups = (List<Group>) outputs.get("OUT_REF");
		if (groups.isEmpty()) {
			throw new NotFoundException(String.format("group not found: %s", uuidGroup));
		}

		return groups.get(0);
	}
}
