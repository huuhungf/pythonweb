package vn.vnpt.api.repository.group;

import vn.vnpt.api.model.Group;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class GroupResultParser {
	public static Group parse(ResultSet r) throws SQLException {
		String uuidGroup = r.getString("UUID_GROUP");
		String name = r.getString("NAME");
		String uuidDevice = r.getString("UUID_DEVICE");
		String uuidParentGroup = r.getString("UUID_PARENT_GROUP");
		Date createDate = r.getDate("CREATE_DATE");
		Date updateDate = r.getDate("UPDATE_DATE");
		String uuidCompany = r.getString("UUID_COMPANY");

		Group group = new Group();
		group.setUuidGroup(uuidGroup);
		group.setName(name);
		group.setUuidDevice(uuidDevice);
		group.setUuidParentGroup(uuidParentGroup);
		group.setCreateDate(createDate);
		group.setUpdateDate(updateDate);
		group.setUuidCompany(uuidCompany);

		return group;
	}
}
