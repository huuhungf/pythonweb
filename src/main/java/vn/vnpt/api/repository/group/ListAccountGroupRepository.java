package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AccountGroup;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListAccountGroupRepository {
	private static final String PROC_LIST_GROUP = "PKG_GROUP.PLIST_ACCOUNT_GROUP";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ListAccountGroupRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<AccountGroup> list(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_RESULT")),
				rs -> {
					String uuidGroup = rs.getString("UUID_GROUP");
					String uuidAccountInDb = rs.getString("UUID_ACCOUNT");
					Date createDate = rs.getDate("CREATE_DATE");

					AccountGroup accountGroup = new AccountGroup();
					accountGroup.setUuidGroup(uuidGroup);
					accountGroup.setUuidAccount(uuidAccountInDb);
					accountGroup.setCreateDate(createDate);
					return accountGroup;
				}
		);

		@SuppressWarnings("unchecked")
		List<AccountGroup> groups = (List<AccountGroup>) outputs.get("OREF_RESULT");
		return groups;
	}
}
