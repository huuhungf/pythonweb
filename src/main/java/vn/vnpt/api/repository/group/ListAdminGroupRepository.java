package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AdminGroup;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListAdminGroupRepository {
	private static final String PROC_LIST_GROUP = "PKG_GROUP.PLIST_ADMIN_GROUP";

	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public ListAdminGroupRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<AdminGroup> list(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_RESULT")),
				AdminGroup.class
		);

		@SuppressWarnings("unchecked")
		List<AdminGroup> groups = (List<AdminGroup>) outputs.get("OREF_RESULT");
		return groups;
	}
}
