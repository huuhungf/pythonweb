package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.out.GroupNode;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListGroupByCompanyRepository {
	private static final String PROC_LIST_GROUP = "PKG_GROUP.PLIST_GROUP_BY_COMPANY";

	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public ListGroupByCompanyRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<GroupNode> list(String uuidCompany) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_RESULT")),
				GroupNode.class
		);

		@SuppressWarnings("unchecked")
		List<GroupNode> groups = (List<GroupNode>) outputs.get("OREF_RESULT");
		return groups;
	}
}
