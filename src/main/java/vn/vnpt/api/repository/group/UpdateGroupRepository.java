package vn.vnpt.api.repository.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateGroupRepository {
	private static final String PROC_UPDATE_GROUP = "PKG_GROUP.PUPDATE_GROUP";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public UpdateGroupRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String uuidGroup, String name) {
		procedureCaller.callNoRefCursor(PROC_UPDATE_GROUP, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
				ProcedureParameter.inputParam("PRS_NEW_NAME", String.class, name),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
