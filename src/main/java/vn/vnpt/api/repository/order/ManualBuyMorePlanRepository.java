package vn.vnpt.api.repository.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.controller.PortalController;
import vn.vnpt.api.model.OrderTransaction;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.Map;


@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ManualBuyMorePlanRepository {
	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public ManualBuyMorePlanRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public static final Logger logger = LogManager.getLogger(ManualBuyMorePlanRepository.class);

	public void buyMorePlan(String userName, Integer numUser, Integer numDevice, Integer numOtt) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor("PKG_ORDER.PMANUAL_BUY_MORE_PLAN", Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, userName),
				ProcedureParameter.inputParam("PRN_NUM_USER", Integer.class, numUser),
				ProcedureParameter.inputParam("PRN_NUM_DEVICE", Integer.class, numDevice),
				ProcedureParameter.inputParam("PRN_NUM_OTT", Integer.class, numOtt),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_ORDER_TRANS")
				), OrderTransaction.class
		);
		String rs = (String) outputs.get("OS_RESULT");
		logger.info("Status handle request portal agent buy-more plan: " + rs);
	}
}
