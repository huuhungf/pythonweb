package vn.vnpt.api.repository.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.OrderTransaction;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;


@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ManualRegisterPlanRepository {
	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public ManualRegisterPlanRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void register(Integer planType, String username, Integer numUser, Integer addonDevice,
						 Integer addonOtt, Integer addonUser, Integer duration) {
		procedureCaller.callOneRefCursor(
				"PKG_ORDER.PMANUAL_REG_PLAN",
				Arrays.asList(
						ProcedureParameter.inputParam("PRN_PLAN_TYPE", Integer.class, planType),
						ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
						ProcedureParameter.inputParam("PRN_NUM_USER", Integer.class, numUser),
						ProcedureParameter.inputParam("PRN_ADD_ON_DEVICE", Integer.class, addonDevice),
						ProcedureParameter.inputParam("PRN_ADD_ON_OTT", Integer.class, addonOtt),
						ProcedureParameter.inputParam("PRN_ADD_ON_USER", Integer.class, addonUser),
						ProcedureParameter.inputParam("PRN_DURATION", Integer.class, duration),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_ORDER_TRANS")
				),
				OrderTransaction.class
		);
	}
}
