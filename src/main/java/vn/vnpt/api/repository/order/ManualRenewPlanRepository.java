package vn.vnpt.api.repository.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.OrderTransaction;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.Map;


@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ManualRenewPlanRepository {
	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public ManualRenewPlanRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public static final Logger logger = LogManager.getLogger(ManualRenewPlanRepository.class);

	public void renewPlan(String userName, Integer numDuration) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor("PKG_ORDER.PMANUAL_RENEW_PLAN", Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, userName),
				ProcedureParameter.inputParam("PRN_DURATION", Integer.class, numDuration),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_ORDER_TRANS")
				), OrderTransaction.class
		);
		String rs = (String) outputs.get("OS_RESULT");
		logger.info("Status handle request portal agent renew plan: " + rs);
	}
}
