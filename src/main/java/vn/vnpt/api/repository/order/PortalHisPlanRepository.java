package vn.vnpt.api.repository.order;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.PortalHisPlanDtoIn;
import vn.vnpt.api.dto.out.PortalHisPlanDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class PortalHisPlanRepository {

	private static final String PROC_LIST_HIS_PLAN = "PKG_ORDER.PAGENT_GET_ORDER_HISTORY";

	private final ProcedureCallerV3 procedureCallerV3;

	public PortalHisPlanRepository(ProcedureCallerV3 procedureCallerV3) {
		this.procedureCallerV3 = procedureCallerV3;
	}

	public PagingDTO<PortalHisPlanDtoOut> listHisPlan(PortalHisPlanDtoIn hisPlanDtoIn, PagingDtoIn pagingDtoIn) {
		Map<String, Object> outputs = procedureCallerV3.callOneRefCursor(PROC_LIST_HIS_PLAN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, hisPlanDtoIn.getUuidAccount()),
				ProcedureParameter.inputParam("PRS_START_DATE", String.class, hisPlanDtoIn.getStartDate()),
				ProcedureParameter.inputParam("PRS_END_DATE", String.class, hisPlanDtoIn.getEndDate()),
				ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
				ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
				ProcedureParameter.inputParam("PRS_KEY_SEARCH", String.class, hisPlanDtoIn.getKeySearch()),
				ProcedureParameter.inputParam("PRS_UUID_AGENTS", String.class, hisPlanDtoIn.getUuidAgent()),
				ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
				ProcedureParameter.refCursorParam("OUT_CUR")
		), PortalHisPlanDtoOut.class);

		Long total = (Long) outputs.get("OUT_TOTAL");
		long totalPages = total / pagingDtoIn.getMaxSize();
		if (total % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}
		PagingDTO<PortalHisPlanDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalPages(totalPages);
		pagingDTO.setTotalElement(total);
		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<PortalHisPlanDtoOut> data = (List<PortalHisPlanDtoOut>) outputs.get("OUT_CUR");

		pagingDTO.setData(data);
		return pagingDTO;
	}
}
