package vn.vnpt.api.repository.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.ListOrderDtoIn;
import vn.vnpt.api.dto.out.OrderDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebGetReportListOrder {
	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebGetReportListOrder(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<OrderDtoOut> list(PagingDtoIn pagingDtoIn, ListOrderDtoIn listOrderDtoIn) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_ORDER.PWEB_GET_REPORT_ORDER_FILTER",
				Arrays.asList(
						ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
						ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
						ProcedureParameter.inputParam("PRS_START_DATE", String.class, listOrderDtoIn.getStartDate()),
						ProcedureParameter.inputParam("PRS_END_DATE", String.class, listOrderDtoIn.getEndDate()),
						ProcedureParameter.inputParam("PRS_KEY_SEARCH", String.class, listOrderDtoIn.getKeySearch()),
						ProcedureParameter.outputParam("OUT_TOTAL", Integer.class),
						ProcedureParameter.refCursorParam("OUT_CUR")
				),
				rs -> {
					String fullName = rs.getString("FULL_NAME");
					String username = rs.getString("USERNAME");
					String email = rs.getString("EMAIL");
					String planName = rs.getString("PLAN_NAME");
					Integer numUser = rs.getInt("NUM_USER");
					Integer numDevice = rs.getInt("ADD_ON_DEVICE");
					Integer numOtt = rs.getInt("ADD_ON_OTT");
					String orderDate = rs.getString("ORDER_DATE");
					Integer orderType = rs.getInt("ORDER_TYPE");
					String startDate = rs.getString("START_DATE");
					String endDate = rs.getString("END_DATE");
					String companyName = rs.getString("COMPANY_NAME");

					OrderDtoOut orderDtoOut = new OrderDtoOut();
					orderDtoOut.setFullName(fullName);
					orderDtoOut.setUsername(username);
					orderDtoOut.setEmail(email);
					orderDtoOut.setPlanName(planName);
					orderDtoOut.setUserAmount(numUser);
					orderDtoOut.setDeviceAmount(numDevice);
					orderDtoOut.setOttAmount(numOtt);
					orderDtoOut.setOrderDate(orderDate);
					orderDtoOut.setOrderType(orderType);
					orderDtoOut.setStartDate(startDate);
					orderDtoOut.setEndDate(endDate);
					orderDtoOut.setCompanyName(companyName);

					return orderDtoOut;
				}
		);

		Integer totalElement = (Integer) outputs.get("OUT_TOTAL");

		@SuppressWarnings("unchecked")
		List<OrderDtoOut> dtoOutList = (List<OrderDtoOut>) outputs.get("OUT_CUR");

		long totalPages = totalElement / pagingDtoIn.getMaxSize();
		if (totalElement % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}

		PagingDTO<OrderDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalElement(totalElement);
		pagingDTO.setTotalPages(totalPages);

		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		pagingDTO.setData(dtoOutList);

		return pagingDTO;
	}
}
