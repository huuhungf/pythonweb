package vn.vnpt.api.repository.plan;

import vn.vnpt.api.model.AccountPlan;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/*
UUID_ACCOUNT_PLAN, UUID_ACCOUNT, UUID_PLAN, STATUS, CREATED_DATE, UPDATED_DATE, ACTIVE_DATE, EXPIRE_DATE, UUID_PLAN_VERSION
 */
public class AccountPlanResultParser {
	public static AccountPlan parse(ResultSet rs) throws SQLException {

		String uuidAccountPlan = rs.getString("UUID_ACCOUNT_PLAN");
		String uuidAccount = rs.getString("UUID_ACCOUNT");
		String uuidPlan = rs.getString("UUID_PLAN");
		Integer status = rs.getInt("STATUS");
		Date createdDate = rs.getDate("CREATED_DATE");
		Date updatedDate = rs.getDate("UPDATED_DATE");
		Date activeDate = rs.getDate("ACTIVE_DATE");
		Date expireDate = rs.getDate("EXPIRE_DATE");
		String uuidPlanVersion = rs.getString("UUID_PLAN_VERSION");
		Integer limitUser = rs.getInt("LIMIT_USER");
		Integer addonDevice = rs.getInt("ADDON_DEVICE");
		Integer freeDevice = rs.getInt("FREE_DEVICE");
		Integer addonOtt = rs.getInt("ADDON_OTT");
		Integer addonUser = rs.getInt("ADDON_USER");

		AccountPlan accountPlan = new AccountPlan();
		accountPlan.setUuidAccountPlan(uuidAccountPlan);
		accountPlan.setUuidAccount(uuidAccount);
		accountPlan.setUuidPlan(uuidPlan);
		accountPlan.setStatus(status);
		accountPlan.setCreatedDate(createdDate);
		accountPlan.setUpdatedDate(updatedDate);
		accountPlan.setActiveDate(activeDate);
		accountPlan.setExpireDate(expireDate);
		accountPlan.setUuidPlanVersion(uuidPlanVersion);
		accountPlan.setLimitUser(limitUser);
		accountPlan.setAddonDevice(addonDevice);
		accountPlan.setFreeDevice(freeDevice);
		accountPlan.setAddonOtt(addonOtt);
		accountPlan.setAddonUser(addonUser);

		return accountPlan;
	}
}
