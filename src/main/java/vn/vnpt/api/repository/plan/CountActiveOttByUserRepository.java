package vn.vnpt.api.repository.plan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CountActiveOttByUserRepository {
	private static final String PROC_COUNT_ACTIVE_OTT_BY_USER = "PKG_PLAN.PCOUNT_ACTIVE_OTT_BY_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CountActiveOttByUserRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Integer count(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_COUNT_ACTIVE_OTT_BY_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OUT_TOTAL_ACTIVE_OTT", Integer.class)
		));

		Integer count = (Integer) outputs.get("OUT_TOTAL_ACTIVE_OTT");

		return count;
	}
}
