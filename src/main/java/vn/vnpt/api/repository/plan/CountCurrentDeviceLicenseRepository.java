package vn.vnpt.api.repository.plan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CountCurrentDeviceLicenseRepository {
	private static final String PROC_COUNT_CURRENT_DEVICE_LICENSE = "PKG_PLAN.PCOUNT_CURRENT_DEVICE_LICENSE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CountCurrentDeviceLicenseRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Integer count(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_COUNT_CURRENT_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OUT_TOTAL_DEVICE_LICENSE", Integer.class)
		));

		Integer count = (Integer) outputs.get("OUT_TOTAL_DEVICE_LICENSE");

		return count;
	}
}
