package vn.vnpt.api.repository.plan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ExternalUpdatePlanRepository {
	private static final String PROC_UPDATE_PLAN = "PKG_PLAN.PEXTERNAL_UPDATE_PLAN";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ExternalUpdatePlanRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String username, Integer addonUser, Integer addonDevice, Integer addonOtt) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_UPDATE_PLAN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.inputParam("PRN_ADDON_USER", Integer.class, addonUser),
				ProcedureParameter.inputParam("PRN_ADDON_DEVICE", Integer.class, addonDevice),
				ProcedureParameter.inputParam("PRN_ADDON_OTT", Integer.class, addonOtt),
				ProcedureParameter.outputParam("OUT_RESULT", String.class)
		));

		String res = (String) outputs.get("OUT_RESULT");
		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("account not found %s", username));
		}

		if (ConstantString.STATUS_DB.NOT_ALLOWED.equals(res)) {
			throw new BadRequestException("NOT ALLOWED");
		}
	}
}
