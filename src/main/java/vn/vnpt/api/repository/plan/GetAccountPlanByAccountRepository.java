package vn.vnpt.api.repository.plan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AccountPlan;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetAccountPlanByAccountRepository {
	private static final String PROC_GET_ACCOUNT_PLAN_BY_ACCOUNT = "PKG_PLAN.PGET_ACCOUNT_PLAN_BY_ACCOUNT";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetAccountPlanByAccountRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public AccountPlan get(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_ACCOUNT_PLAN_BY_ACCOUNT, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_ACCOUNT_PLAN")),
				rs -> {
					String uuidAccountPlan = rs.getString("UUID_ACCOUNT_PLAN");
					String uuidAccountInDb = rs.getString("UUID_ACCOUNT");
					String uuidPlan = rs.getString("UUID_PLAN");
					Integer status = rs.getInt("STATUS");
					Date createdDate = rs.getDate("CREATED_DATE");
					Date updatedDate = rs.getDate("UPDATED_DATE");
					Date activeDate = rs.getDate("ACTIVE_DATE");
					Date expireDate = rs.getDate("EXPIRE_DATE");
					String uuidPlanVersion = rs.getString("UUID_PLAN_VERSION");

					AccountPlan ap = new AccountPlan();
					ap.setUuidAccountPlan(uuidAccountPlan);
					ap.setUuidAccount(uuidAccountInDb);
					ap.setUuidPlan(uuidPlan);
					ap.setStatus(status);
					ap.setCreatedDate(createdDate);
					ap.setUpdatedDate(updatedDate);
					ap.setActiveDate(activeDate);
					ap.setExpireDate(expireDate);
					ap.setUuidPlanVersion(uuidPlanVersion);

					return ap;
				}
		);

		@SuppressWarnings("unchecked")
		List<AccountPlan> accountPlans = (List<AccountPlan>) outputs.get("OREF_ACCOUNT_PLAN");
		if (accountPlans.isEmpty()) {
			throw new NotFoundException(String.format("account plan not found for account %s", uuidAccount));
		}
		return accountPlans.get(0);
	}
}
