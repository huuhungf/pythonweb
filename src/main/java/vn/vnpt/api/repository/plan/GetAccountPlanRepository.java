package vn.vnpt.api.repository.plan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AccountPlan;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetAccountPlanRepository {
	private static final String PROC_GET_ACCOUNT_PLAN = "PKG_PLAN.PGET_ACCOUNT_PLAN";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetAccountPlanRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public AccountPlan get(String username) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_ACCOUNT_PLAN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_ACCOUNT_PLAN")),
				AccountPlanResultParser::parse
		);

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("account not found %s", username));
		}

		@SuppressWarnings("unchecked")
		List<AccountPlan> accountPlans = (List<AccountPlan>) outputs.get("OREF_ACCOUNT_PLAN");
		if (accountPlans.isEmpty()) {
			throw new NotFoundException(String.format("account plan not found for user %s", username));
		}
		return accountPlans.get(0);
	}
}
