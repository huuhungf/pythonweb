package vn.vnpt.api.repository.plan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Plan;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetPlanRepository {
	private static final String PROC_GET_PLAN = "PKG_PLAN.PGET_PLAN";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetPlanRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Plan get(String uuidPlan) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_PLAN, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_PLAN", String.class, uuidPlan),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_PLAN")),
				PlanParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Plan> plans = (List<Plan>) outputs.get("OREF_PLAN");
		if (plans.isEmpty()) {
			throw new NotFoundException(String.format("plan not found %s", uuidPlan));
		}
		return plans.get(0);
	}
}
