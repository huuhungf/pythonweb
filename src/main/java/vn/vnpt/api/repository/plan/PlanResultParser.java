package vn.vnpt.api.repository.plan;

import vn.vnpt.api.model.Plan;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/*
UUID_PLAN, TYPE, STATUS, NAME, DESCRIPTION, CREATED_DATE, UPDATED_DATE
 */
public class PlanResultParser {
	public static Plan parse(ResultSet rs) throws SQLException {
		String uuidPlan = rs.getString("UUID_PLAN");
		Integer type = rs.getInt("TYPE");
		Integer status = rs.getInt("STATUS");
		String name = rs.getString("NAME");
		String description = rs.getString("DESCRIPTION");
		Date createdDate = rs.getDate("CREATED_DATE");
		Date updatedDate = rs.getDate("UPDATED_DATE");

		Plan plan = new Plan();
		plan.setUuidPlan(uuidPlan);
		plan.setType(type);
		plan.setStatus(status);
		plan.setName(name);
		plan.setDescription(description);
		plan.setCreatedDate(createdDate);
		plan.setUpdatedDate(updatedDate);
		return plan;
	}
}
