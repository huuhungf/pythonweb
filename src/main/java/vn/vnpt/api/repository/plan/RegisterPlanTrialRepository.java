package vn.vnpt.api.repository.plan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class RegisterPlanTrialRepository {
	private static final String PROC_REGISTER_PLAN_TRIAL = "PKG_PLAN.PREGISTER_PLAN_TRIAL";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public RegisterPlanTrialRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void register(String username) {
		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_REGISTER_PLAN_TRIAL, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.outputParam("OUT_RESULT", String.class)
		));

		String res = (String) outputs.get("OUT_RESULT");
		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("account not found %s", username));
		}

		if (ConstantString.STATUS_DB.NOT_ALLOWED.equals(res)) {
			throw new BadRequestException("NOT ALLOWED");
		}
	}
}
