package vn.vnpt.api.repository.planversion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.PlanVersion;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetPlanVersionRepository {
	private static final String PROC_GET_PLAN_VERSION = "PKG_PLAN_VERSION.PGET_PLAN_VERSION";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetPlanVersionRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PlanVersion get(String uuidPlan) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_PLAN_VERSION, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_PLAN", String.class, uuidPlan),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_PLAN_VERSION")),
				PlanVersionResultParser::parse);

		@SuppressWarnings("unchecked")
		List<PlanVersion> planVersions = (List<PlanVersion>) outputs.get("OREF_PLAN_VERSION");
		if (planVersions.isEmpty()) {
			throw new NotFoundException(String.format("plan version not found by plan: %s", uuidPlan));
		}
		return planVersions.get(0);
	}
}
