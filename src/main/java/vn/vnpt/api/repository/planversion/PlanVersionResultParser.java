package vn.vnpt.api.repository.planversion;

import vn.vnpt.api.model.PlanVersion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/*
UUID_PLAN_VERSION, VERSION, STATUS, UUID_PLAN, CREATED_DATE, UPDATED_DATE
 */
public class PlanVersionResultParser {
	public static PlanVersion parse(ResultSet rs) throws SQLException {
		String uuidPlanVersion = rs.getString("UUID_PLAN_VERSION");
		Integer version = rs.getInt("VERSION");
		Integer status = rs.getInt("STATUS");
		String uuidPlan = rs.getString("UUID_PLAN");
		Date createdDate = rs.getDate("CREATED_DATE");
		Date updatedDate = rs.getDate("UPDATED_DATE");

		PlanVersion planVersion = new PlanVersion();
		planVersion.setUuidPlanVersion(uuidPlanVersion);
		planVersion.setVersion(version);
		planVersion.setStatus(status);
		planVersion.setUuidPlan(uuidPlan);
		planVersion.setCreatedDate(createdDate);
		planVersion.setUpdatedDate(updatedDate);
		return planVersion;
	}
}
