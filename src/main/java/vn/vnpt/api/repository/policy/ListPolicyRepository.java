package vn.vnpt.api.repository.policy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Policy;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListPolicyRepository {
	private static final String PROC_LIST_POLICIES = "PKG_POLICY.PGET_LIST_POLICY_BY_ACCOUNT_PLAN";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ListPolicyRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<Policy> list(String uuidAccountPlan) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_POLICIES, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT_PLAN", String.class, uuidAccountPlan),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_POLICY")),
				PolicyResultParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Policy> policies = (List<Policy>) outputs.get("OREF_POLICY");
		return policies;
	}
}
