package vn.vnpt.api.repository.policy;

import vn.vnpt.api.model.Policy;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/*
UUID_POLICY, CONFIGURATION, UUID_PLAN, UUID_PLAN_VERSION, UUID_POLICYDEF, CREATED_DATE, UPDATED_DATE, UUID_ACCOUNT, UUID_PLAN_VERSION_PRICE
 */
public class PolicyResultParser {
	public static Policy parse(ResultSet rs) throws SQLException {
		String uuidPolicy = rs.getString("UUID_POLICY");
		String configuration = rs.getString("CONFIGURATION");
		String uuidPlan = rs.getString("UUID_PLAN");
		String uuidPlanVersion = rs.getString("UUID_PLAN_VERSION");
		String uuidPolicyDef = rs.getString("UUID_POLICYDEF");
		Date createdDate = rs.getDate("CREATED_DATE");
		Date updatedDate = rs.getDate("UPDATED_DATE");
		String uuidAccount = rs.getString("UUID_ACCOUNT");

		Policy policy = new Policy();
		policy.setUuidPolicy(uuidPolicy);
		policy.setConfiguration(configuration);
		policy.setUuidPlan(uuidPlan);
		policy.setUuidPlanVersion(uuidPlanVersion);
		policy.setUuidPolicyDef(uuidPolicyDef);
		policy.setCreatedDate(createdDate);
		policy.setUpdatedDate(updatedDate);
		policy.setUuidAccount(uuidAccount);
		return policy;
	}
}
