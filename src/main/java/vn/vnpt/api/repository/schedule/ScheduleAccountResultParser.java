package vn.vnpt.api.repository.schedule;

import vn.vnpt.api.dto.out.ScheduleAccountDtoOut;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ScheduleAccountResultParser {
	public static ScheduleAccountDtoOut parser(ResultSet rs) throws SQLException {
		String uuidAccount = rs.getString("UUID_ACCOUNT");
		String fullName = rs.getString("FULL_NAME");
		String userCode = rs.getString("USER_CODE");
		String imageUrl = rs.getString("IMAGE_URL");
		String gender = rs.getString("GENDER");
		Integer locked = rs.getInt("LOCKED");
		String uuidSchedule = rs.getString("UUID_SCHEDULE");
		String scheduleName = rs.getString("SCHEDULE_NAME");
		Integer cycleMode = rs.getInt("CYCLE_MODE");

		ScheduleAccountDtoOut result = new ScheduleAccountDtoOut();
		result.setUuidAccount(uuidAccount);
		result.setFullName(fullName);
		result.setUserCode(userCode);
		result.setImageUrl(imageUrl);
		result.setGender(gender);
		result.setLocked(locked);
		result.setUuidSchedule(uuidSchedule);
		result.setScheduleName(scheduleName);
		result.setCycleMode(cycleMode);

		return result;

	}
}
