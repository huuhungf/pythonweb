package vn.vnpt.api.repository.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.ScheduleDtoIn;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.model.AccountSchedule;
import vn.vnpt.api.model.Schedule;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ScheduleRepository {

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ScheduleRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Schedule createSchedule(String uuidCompany, ScheduleDtoIn scheduleDtoIn) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PINSERT_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
						ProcedureParameter.inputParam("PRS_SCHEDULE_NAME", String.class, scheduleDtoIn.getScheduleName()),
						ProcedureParameter.inputParam("PRN_CYCLE_MODE", Integer.class, scheduleDtoIn.getCycleMode()),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")),
				ScheduleResultParser::parser
		);

		List<Schedule> results = (List<Schedule>) outputs.get("OREF_RESULT");
		return results.get(0);
	}

	public void createWSchedule(String uuidSchedule, Integer dayId, String shiftIds) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PINSERT_W_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRN_DAY_ID", Integer.class, dayId),
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.inputParam("PRS_UUID_SHIFT", String.class, shiftIds),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public void createMSchedule(String uuidSchedule, Integer dayId, String uuidShift) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PINSERT_M_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRN_DAY_ID", Integer.class, dayId),
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.inputParam("PRS_UUID_SHIFT", String.class, uuidShift),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public void createYSchedule(String uuidSchedule, Integer dayId, Integer monthId, String uuidShift) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PINSERT_Y_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRN_MONTH_ID", Integer.class, monthId),
						ProcedureParameter.inputParam("PRN_DAY_ID", Integer.class, dayId),
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.inputParam("PRS_UUID_SHIFT", String.class, uuidShift),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public List<ScheduleDtoOut> listSchedules(String uuidCompany) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				), rs -> {
					String uuidSchedule = rs.getString("UUID_SCHEDULE");
					String scheduleName = rs.getString("SCHEDULE_NAME");
					Integer cycleMode = rs.getInt("CYCLE_MODE");

					ScheduleDtoOut schedule = new ScheduleDtoOut();
					schedule.setUuidSchedule(uuidSchedule);
					schedule.setScheduleName(scheduleName);
					schedule.setCycleMode(cycleMode);
					return schedule;
				}
		);
		List<ScheduleDtoOut> results = (List<ScheduleDtoOut>) outputs.get("OREF_RESULT");
		return results;
	}

	public Schedule getSchedule(String uuidSchedule) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PGET_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				), ScheduleResultParser::parser
		);
		List<Schedule> results = (List<Schedule>) outputs.get("OREF_RESULT");

		if (results.isEmpty()) {
			throw new NotFoundException("schedule not found");
		}
		return results.get(0);
	}


	public List<WScheduleDtoOut> listWSchedule(String uuidSchedule) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_W_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				), rs -> {
					Integer dayId = rs.getInt("DAY_ID");
					String uuidShift = rs.getString("UUID_SHIFT");
					String shiftCode = rs.getString("SHIFT_CODE");

					WScheduleDtoOut out = new WScheduleDtoOut();
					out.setDayId(dayId);
					out.setShiftCode(shiftCode);
					out.setUuidShift(uuidShift);
					return out;
				}
		);

		List<WScheduleDtoOut> results = (List<WScheduleDtoOut>) outputs.get("OREF_RESULT");
		return results;
	}

	public void deleteWSchedule(String uuidSchedule) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PDELETE_W_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public void deleteSchedule(String uuidSchedule) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PDELETE_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public void updateSchedule(String uuidSchedule, String scheduleName, Integer cycleMode) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PUPDATE_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.inputParam("PRS_SCHEDULE_NAME", String.class, scheduleName),
						ProcedureParameter.inputParam("PRN_CYCLE_MODE", Integer.class, cycleMode),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public void assignScheduleAccount(String uuidSchedule, String uuidListAccount, String uuidGroup) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PASSIGN_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.inputParam("PRS_LIST_UUID_ACCOUNT", String.class, uuidListAccount),
						ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public PagingDTO<ScheduleAccountDtoOut> listScheduleAccount(PagingDtoIn pagingDtoIn, String uuidGroup, String keySearch) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_ACCOUNT_SCHEDULE_BY_GROUP",
				Arrays.asList(
						ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
						ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
						ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
						ProcedureParameter.inputParam("PRS_KEY_SEARCH", String.class, keySearch),
						ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
						ProcedureParameter.refCursorParam("OUT_CUR")
				), ScheduleAccountResultParser::parser
		);

		Long total = (Long) outputs.get("OUT_TOTAL");
		long totalPages = total / pagingDtoIn.getMaxSize();
		if (total % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}
		PagingDTO<ScheduleAccountDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalPages(totalPages);
		pagingDTO.setTotalElement(total);
		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<ScheduleAccountDtoOut> data = (List<ScheduleAccountDtoOut>) outputs.get("OUT_CUR");
		pagingDTO.setData(data);
		return pagingDTO;
	}

	public AccountSchedule getAccountSchedule(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PGET_ACCOUNT_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				), rs -> {
					AccountSchedule accountSchedule = new AccountSchedule();
					accountSchedule.setUuidAccount(rs.getString("UUID_ACCOUNT"));
					accountSchedule.setUuidSchedule(rs.getString("UUID_SCHEDULE"));
					return accountSchedule;
				}
		);
		List<AccountSchedule> results = (List<AccountSchedule>) outputs.get("OREF_RESULT");
		if (results.isEmpty()) {
			throw new NotFoundException("account schedule not found");
		}
		return results.get(0);
	}

	public List<MScheduleDtoOut> listMSchedule(String uuidSchedule) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_M_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				), rs -> {
					Integer dayId = rs.getInt("DAY_ID");
					String uuidShift = rs.getString("UUID_SHIFT");
					String shiftCode = rs.getString("SHIFT_CODE");

					MScheduleDtoOut out = new MScheduleDtoOut();
					out.setDayId(dayId);
					out.setShiftCode(shiftCode);
					out.setUuidShift(uuidShift);
					return out;
				}
		);

		List<MScheduleDtoOut> results = (List<MScheduleDtoOut>) outputs.get("OREF_RESULT");
		return results;
	}

	public List<YScheduleDtoOut> listYSchedule(String uuidSchedule) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_Y_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				), rs -> {
					Integer monthId = rs.getInt("MONTH_ID");
					Integer dayId = rs.getInt("DAY_ID");
					String uuidShift = rs.getString("UUID_SHIFT");
					String shiftCode = rs.getString("SHIFT_CODE");

					YScheduleDtoOut out = new YScheduleDtoOut();
					out.setMonthId(monthId);
					out.setDayId(dayId);
					out.setShiftCode(shiftCode);
					out.setUuidShift(uuidShift);
					return out;
				}
		);

		List<YScheduleDtoOut> results = (List<YScheduleDtoOut>) outputs.get("OREF_RESULT");
		return results;
	}

	public void deleteMSchedule(String uuidSchedule) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PDELETE_M_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public void deleteYSchedule(String uuidSchedule) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PDELETE_Y_SCHEDULE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SCHEDULE", String.class, uuidSchedule),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}
}
