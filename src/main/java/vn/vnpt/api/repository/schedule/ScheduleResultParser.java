package vn.vnpt.api.repository.schedule;

import vn.vnpt.api.model.Schedule;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ScheduleResultParser {
	public static Schedule parser(ResultSet rs) throws SQLException {
		String uuidSchedule = rs.getString("UUID_SCHEDULE");
		String scheduleName = rs.getString("SCHEDULE_NAME");
		Integer cycleMode = rs.getInt("CYCLE_MODE");
		String uuidCompany = rs.getString("UUID_COMPANY");

		Schedule schedule = new Schedule();
		schedule.setUuidSchedule(uuidSchedule);
		schedule.setScheduleName(scheduleName);
		schedule.setCycleMode(cycleMode);
		schedule.setUuidCompany(uuidCompany);
		return schedule;
	}
}
