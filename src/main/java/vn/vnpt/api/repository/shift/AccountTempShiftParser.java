package vn.vnpt.api.repository.shift;

import vn.vnpt.api.model.AccountTempShift;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountTempShiftParser {
	public static AccountTempShift parse(ResultSet rs) throws SQLException {
		String uuidAccount = rs.getString("UUID_ACCOUNT");
		String uuidShift = rs.getString("UUID_SHIFT");
		String tempShiftDate = rs.getString("TEMP_SHIFT_DATE");
		AccountTempShift accountTempShift = new AccountTempShift();
		accountTempShift.setUuidAccount(uuidAccount);
		accountTempShift.setUuidShift(uuidShift);
		accountTempShift.setTempShiftDate(tempShiftDate);
		return accountTempShift;
	}
}
