package vn.vnpt.api.repository.shift;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.ShiftDtoIn;
import vn.vnpt.api.model.AccountTempShift;
import vn.vnpt.api.model.Shift;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ShiftRepository {

	private ProcedureCallerV2 procedureCaller;

	@Autowired
	public ShiftRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void updateShift(String uuidShift, ShiftDtoIn shiftDtoIn) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PUPDATE_SHIFT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SHIFT", String.class, uuidShift),
						ProcedureParameter.inputParam("PRS_SHIFT_CODE", String.class, shiftDtoIn.getShiftCode()),
						ProcedureParameter.inputParam("PRS_ON_DUTY", String.class, shiftDtoIn.getOnDuty()),
						ProcedureParameter.inputParam("PRS_OFF_DUTY", String.class, shiftDtoIn.getOffDuty()),
						ProcedureParameter.inputParam("PRN_DAY_COUNT", Integer.class, shiftDtoIn.getDayCount()),
						ProcedureParameter.inputParam("PRS_ON_TIME_IN", String.class, shiftDtoIn.getOnTimeIn()),
						ProcedureParameter.inputParam("PRS_ON_TIME_OUT", String.class, shiftDtoIn.getOnTimeOut()),
						ProcedureParameter.inputParam("PRS_CUT_IN", String.class, shiftDtoIn.getCutIn()),
						ProcedureParameter.inputParam("PRS_CUT_OUT", String.class, shiftDtoIn.getCutOut()),
						ProcedureParameter.inputParam("PRS_ON_LUNCH", String.class, shiftDtoIn.getOnLunch()),
						ProcedureParameter.inputParam("PRS_OFF_LUNCH", String.class, shiftDtoIn.getOffLunch()),
						ProcedureParameter.inputParam("PRN_WORKING_TIME", Integer.class, shiftDtoIn.getWorkingTime()),
						ProcedureParameter.inputParam("PRN_WORKING_DAY", Float.class, shiftDtoIn.getWorkingDay()),
						ProcedureParameter.inputParam("PRN_NO_IN_WORK_TIME", Integer.class, 0),
						ProcedureParameter.inputParam("PRN_NO_OUT_WORK_TIME", Integer.class, 0),
						ProcedureParameter.inputParam("PRN_SHOW_POSITION", Integer.class, shiftDtoIn.getShowPosition()),
						ProcedureParameter.inputParam("PRN_IS_LATE", Integer.class, shiftDtoIn.getIsLate()),
						ProcedureParameter.inputParam("PRN_IS_EARLY", Integer.class, shiftDtoIn.getIsEarly()),
						ProcedureParameter.inputParam("PRN_LATE_GRACE", Integer.class, shiftDtoIn.getLateGrace()),
						ProcedureParameter.inputParam("PRN_IS_LATE_GRACE", Integer.class, shiftDtoIn.getIsLateGrace()),
						ProcedureParameter.inputParam("PRN_EARLY_GRACE", Integer.class, shiftDtoIn.getEarlyGrace()),
						ProcedureParameter.inputParam("PRN_IS_EARLY_GRACE", Integer.class, shiftDtoIn.getIsEarlyGrace()),
						ProcedureParameter.inputParam("PRN_ROUND_TYPE_LATE", Integer.class, shiftDtoIn.getRoundTypeLate()),
						ProcedureParameter.inputParam("PRN_ROUND_STEP_LATE", Integer.class, shiftDtoIn.getRoundStepLate()),
						ProcedureParameter.inputParam("PRN_ROUND_TYPE_EARLY", Integer.class, shiftDtoIn.getRoundTypeEarly()),
						ProcedureParameter.inputParam("PRN_ROUND_STEP_EARLY", Integer.class, shiftDtoIn.getRoundStepEarly()),
						ProcedureParameter.outputParam("OS_RESULT", String.class))
		);
	}

	public void deleteShift(String uuidShift) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PDELETE_SHIFT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SHIFT", String.class, uuidShift),
						ProcedureParameter.outputParam("OS_RESULT", String.class))
		);
	}

	public List<Shift> list(String uuidCompany) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_SHIFT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")),
				ShiftResultParser::parse
		);

		List<Shift> shifts = (List<Shift>) outputs.get("OREF_RESULT");
		return shifts;
	}

	public List<Shift> listShiftByGroup(String uuidGroup) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_SHIFT_BY_GROUP",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				),
				ShiftResultParser::parse
		);

		List<Shift> shifts = (List<Shift>) outputs.get("OREF_RESULT");
		return shifts;
	}

	public Shift get(String uuidShift) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PGET_SHIFT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_SHIFT", String.class, uuidShift),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				),
				ShiftResultParser::parse
		);

		List<Shift> shifts = (List<Shift>) outputs.get("OREF_RESULT");
		if (shifts.isEmpty()) {
			throw new NotFoundException(String.format("shift not found: %s", uuidShift));
		}

		return shifts.get(0);
	}

	public void createShift(String uuidCompany, ShiftDtoIn shiftDtoIn) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PINSERT_SHIFT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_COMPANY", String.class, uuidCompany),
						ProcedureParameter.inputParam("PRS_SHIFT_CODE", String.class, shiftDtoIn.getShiftCode()),
						ProcedureParameter.inputParam("PRS_ON_DUTY", String.class, shiftDtoIn.getOnDuty()),
						ProcedureParameter.inputParam("PRS_OFF_DUTY", String.class, shiftDtoIn.getOffDuty()),
						ProcedureParameter.inputParam("PRN_DAY_COUNT", Integer.class, shiftDtoIn.getDayCount()),
						ProcedureParameter.inputParam("PRS_ON_TIME_IN", String.class, shiftDtoIn.getOnTimeIn()),
						ProcedureParameter.inputParam("PRS_ON_TIME_OUT", String.class, shiftDtoIn.getOnTimeOut()),
						ProcedureParameter.inputParam("PRS_CUT_IN", String.class, shiftDtoIn.getCutIn()),
						ProcedureParameter.inputParam("PRS_CUT_OUT", String.class, shiftDtoIn.getCutOut()),
						ProcedureParameter.inputParam("PRS_ON_LUNCH", String.class, shiftDtoIn.getOnLunch()),
						ProcedureParameter.inputParam("PRS_OFF_LUNCH", String.class, shiftDtoIn.getOffLunch()),
						ProcedureParameter.inputParam("PRN_WORKING_TIME", Integer.class, shiftDtoIn.getWorkingTime()),
						ProcedureParameter.inputParam("PRN_WORKING_DAY", Float.class, shiftDtoIn.getWorkingDay()),
						ProcedureParameter.inputParam("PRN_NO_IN_WORK_TIME", Integer.class, 0),
						ProcedureParameter.inputParam("PRN_NO_OUT_WORK_TIME", Integer.class, 0),
						ProcedureParameter.inputParam("PRN_SHOW_POSITION", Integer.class, shiftDtoIn.getShowPosition()),
						ProcedureParameter.inputParam("PRN_IS_LATE", Integer.class, shiftDtoIn.getIsLate()),
						ProcedureParameter.inputParam("PRN_IS_EARLY", Integer.class, shiftDtoIn.getIsEarly()),
						ProcedureParameter.inputParam("PRN_LATE_GRACE", Integer.class, shiftDtoIn.getLateGrace()),
						ProcedureParameter.inputParam("PRN_IS_LATE_GRACE", Integer.class, shiftDtoIn.getIsLateGrace()),
						ProcedureParameter.inputParam("PRN_EARLY_GRACE", Integer.class, shiftDtoIn.getEarlyGrace()),
						ProcedureParameter.inputParam("PRN_IS_EARLY_GRACE", Integer.class, shiftDtoIn.getIsEarlyGrace()),
						ProcedureParameter.inputParam("PRN_ROUND_TYPE_LATE", Integer.class, shiftDtoIn.getRoundTypeLate()),
						ProcedureParameter.inputParam("PRN_ROUND_STEP_LATE", Integer.class, shiftDtoIn.getRoundStepLate()),
						ProcedureParameter.inputParam("PRN_ROUND_TYPE_EARLY", Integer.class, shiftDtoIn.getRoundTypeEarly()),
						ProcedureParameter.inputParam("PRN_ROUND_STEP_EARLY", Integer.class, shiftDtoIn.getRoundStepEarly()),
						ProcedureParameter.outputParam("OS_RESULT", String.class))
		);
	}

	public void assignShiftTmp(String uuidAccount, String uuidShifts, String tmpDate) {
		procedureCaller.callNoRefCursor(
				"PKG_SHIFT.PASSIGN_ACCOUNT_TEMP_SHIFT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
						ProcedureParameter.inputParam("PRS_LIST_UUID_SHIFT", String.class, uuidShifts),
						ProcedureParameter.inputParam("PRS_TEMP_SHIFT_DATE", String.class, tmpDate),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}

	public List<Shift> getShiftTmp(String uuidAccount, String tmpShiftDate) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_TEMP_SHIFT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
						ProcedureParameter.inputParam("PRS_TEMP_SHIFT_DATE", String.class, tmpShiftDate),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				),
				ShiftResultParser::parse
		);

		List<Shift> shifts = (List<Shift>) outputs.get("OREF_RESULT");
		return shifts;
	}

	public List<AccountTempShift> listAccountTempShift(String uuidAccount, String startDate, String endDate) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_SHIFT.PLIST_ACCOUNT_TEMP_SHIFT",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
						ProcedureParameter.inputParam("PRS_START_DATE", String.class, startDate),
						ProcedureParameter.inputParam("PRS_END_DATE", String.class, endDate),
						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OREF_RESULT")
				),
				AccountTempShiftParser::parse
		);

		List<AccountTempShift> shifts = (List<AccountTempShift>) outputs.get("OREF_RESULT");
		return shifts;
	}

}
