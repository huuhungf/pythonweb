package vn.vnpt.api.repository.shift;

import vn.vnpt.api.model.Shift;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ShiftResultParser {
	public static Shift parse(ResultSet rs) throws SQLException {
		String uuidShift = rs.getString("UUID_SHIFT");
		String shiftCode = rs.getString("SHIFT_CODE");
		String onDuty = rs.getString("ON_DUTY");
		String offDuty = rs.getString("OFF_DUTY");
		Integer dayCount = rs.getInt("DAY_COUNT");
		Integer onTimeIn = rs.getInt("ON_TIME_IN");
		Integer onTimeOut = rs.getInt("ON_TIME_OUT");
		Integer cutIn = rs.getInt("CUT_IN");
		Integer cutOut = rs.getInt("CUT_OUT");
		String onLunch = rs.getString("ON_LUNCH");
		String offLunch = rs.getString("OFF_LUNCH");
		Integer workingTime = rs.getInt("WORKING_TIME");
		Float workingDay = rs.getFloat("WORKING_DAY");
		Integer showPosition = rs.getInt("SHOW_POSITION");
		String uuidCompany = rs.getString("UUID_COMPANY");
		Integer isLate = rs.getInt("IS_LATE");
		Integer isEarly = rs.getInt("IS_EARLY");
		Integer lateGrace = rs.getInt("LATE_GRACE");
		Integer isLateGrace = rs.getInt("IS_LATE_GRACE");
		Integer earlyGrace = rs.getInt("EARLY_GRACE");
		Integer isEarlyGrace = rs.getInt("IS_EARLY_GRACE");
		Integer roundTypeLate = rs.getInt("ROUND_TYPE_LATE");
		Integer roundStepLate = rs.getInt("ROUND_STEP_LATE");
		Integer roundTypeEarly = rs.getInt("ROUND_TYPE_EARLY");
		Integer roundStepEarly = rs.getInt("ROUND_STEP_EARLY");

		Shift shift = new Shift();
		shift.setUuidShift(uuidShift);
		shift.setShiftCode(shiftCode);
		shift.setOnDuty(onDuty);
		shift.setOffDuty(offDuty);
		shift.setDayCount(dayCount);
		shift.setOnTimeIn(onTimeIn);
		shift.setOnTimeOut(onTimeOut);
		shift.setCutIn(cutIn);
		shift.setCutOut(cutOut);
		shift.setOnLunch(onLunch);
		shift.setOffLunch(offLunch);
		shift.setWorkingTime(workingTime);
		shift.setWorkingDay(workingDay);
		shift.setShowPosition(showPosition);
		shift.setUuidCompany(uuidCompany);
		shift.setIsLate(isLate);
		shift.setIsEarly(isEarly);
		shift.setLateGrace(lateGrace);
		shift.setIsLateGrace(isLateGrace);
		shift.setEarlyGrace(earlyGrace);
		shift.setIsEarlyGrace(isEarlyGrace);
		shift.setRoundTypeLate(roundTypeLate);
		shift.setRoundStepLate(roundStepLate);
		shift.setRoundTypeEarly(roundTypeEarly);
		shift.setRoundStepEarly(roundStepEarly);

		return shift;
	}
}
