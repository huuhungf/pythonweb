package vn.vnpt.api.repository.stranger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class DeleteStrangerByDeviceRepository {
	private static final String PROC_DELETE_STRANGER_BY_DEVICE = "PKG_STRANGER.PDELETE_STRANGER_BY_DEVICE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public DeleteStrangerByDeviceRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void delete(String uuidDevice, String serialNumber) {
		procedureCaller.callNoRefCursor(PROC_DELETE_STRANGER_BY_DEVICE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.outputParam("OUT_RESULT", String.class)
		));
	}
}
