package vn.vnpt.api.repository.stranger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class DeleteStrangerRepository {
	private static final String PROC_DELETE_STRANGER = "PKG_STRANGER.PDELETE_STRANGER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public DeleteStrangerRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void delete(String uuidStranger) {
		procedureCaller.callNoRefCursor(PROC_DELETE_STRANGER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_STRANGER", String.class, uuidStranger),
				ProcedureParameter.outputParam("OUT_RESULT", String.class)
		));
	}
}
