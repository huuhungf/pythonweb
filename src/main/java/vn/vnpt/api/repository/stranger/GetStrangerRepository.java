package vn.vnpt.api.repository.stranger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Stranger;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetStrangerRepository {
	private static final String PROC_GET_STRANGER = "PKG_STRANGER.PGET_STRANGER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetStrangerRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Stranger get(String uuidStranger) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_STRANGER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_STRANGER", String.class, uuidStranger),
				ProcedureParameter.outputParam("OUT_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_CUR")),
				StrangerParser::parse
		);

		String res = (String) outputs.get("OUT_RESULT");
		if (ConstantString.STATUS_DB.STRANGER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("stranger not found %s", uuidStranger));
		}

		@SuppressWarnings("unchecked")
		List<Stranger> strangers = (List<Stranger>) outputs.get("OUT_CUR");
		return strangers.get(0);
	}
}
