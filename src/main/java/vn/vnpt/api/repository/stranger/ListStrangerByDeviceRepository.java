package vn.vnpt.api.repository.stranger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Stranger;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListStrangerByDeviceRepository {
	private static final String PROC_LIST_STRANGER = "PKG_STRANGER.PLIST_STRANGER_BY_DEVICE";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ListStrangerByDeviceRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<Stranger> list(String uuidDevice, String serialNumber) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_STRANGER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.refCursorParam("OUT_CUR")),
				StrangerParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Stranger> strangers = (List<Stranger>) outputs.get("OUT_CUR");
		return strangers;
	}
}
