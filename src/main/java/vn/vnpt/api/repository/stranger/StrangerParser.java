package vn.vnpt.api.repository.stranger;

import vn.vnpt.api.model.Stranger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class StrangerParser {
	public static Stranger parse(ResultSet rs) throws SQLException {
		String uuidStranger = rs.getString("UUID_STRANGER");
		String uuidDevice = rs.getString("UUID_DEVICE");
		String serialNumber = rs.getString("SERIAL_NUMBER");
		Date dateCheckin = rs.getDate("DATE_CHECKIN");
		String imageUrl = rs.getString("IMAGE_URL");
		Date createdDate = rs.getDate("CREATED_DATE");

		Stranger stranger = new Stranger();
		stranger.setUuidStranger(uuidStranger);
		stranger.setUuidDevice(uuidDevice);
		stranger.setSerialNumber(serialNumber);
		stranger.setDateCheckin(dateCheckin);
		stranger.setImageUrl(imageUrl);
		stranger.setCreatedDate(createdDate);
		return stranger;
	}
}
