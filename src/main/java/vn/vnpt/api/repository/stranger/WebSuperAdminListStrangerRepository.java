package vn.vnpt.api.repository.stranger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.WebListStrangerDtoIn;
import vn.vnpt.api.dto.out.StrangerDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.*;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebSuperAdminListStrangerRepository {
	private static final String PROC_LIST_STRANGER = "PKG_STRANGER.PWEB_SUPERADMIN_GET_LIST_FILTER_STRANGER";

	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public WebSuperAdminListStrangerRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<StrangerDtoOut> list(String superUsername, WebListStrangerDtoIn webListStrangerDtoIn, PagingDtoIn pagingDtoIn) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_LIST_STRANGER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_SUPERADMIN_USERNAME", String.class, superUsername),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, webListStrangerDtoIn.getUuidDevice()),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, webListStrangerDtoIn.getSerialNumbers()),
				ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
				ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
				ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
				ProcedureParameter.refCursorParam("OUT_CUR")),
				StrangerDtoOut.class
		);

		Long totalElement = (Long) outputs.get("OUT_TOTAL");
		long totalPages = totalElement / pagingDtoIn.getMaxSize();
		if (totalElement % pagingDtoIn.getMaxSize() != 0) {
			totalPages++;
		}

		PagingDTO<StrangerDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalElement(totalElement);
		pagingDTO.setTotalPages(totalPages);

		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<StrangerDtoOut> data = (List<StrangerDtoOut>) outputs.get("OUT_CUR");

		pagingDTO.setData(data);

		return pagingDTO;
	}
}
