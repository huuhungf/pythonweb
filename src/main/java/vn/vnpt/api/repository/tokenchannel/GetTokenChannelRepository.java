package vn.vnpt.api.repository.tokenchannel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.TokenChannel;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetTokenChannelRepository {

	private final static String PROC_GET_TOKEN_CHANNEL = "PKG_TOKEN_CHANNEL.PGET_TOKEN_CHANNEL";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetTokenChannelRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public TokenChannel get(String uuidTokenChannel) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_TOKEN_CHANNEL, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_TOKEN_CHANNEL", String.class, uuidTokenChannel),
				ProcedureParameter.refCursorParam("OUT_REF")),
				rs -> {
					String uuidTokenChannelInDb = rs.getString("UUID_TOKEN_CHANNEL");
					String secretKey = rs.getString("SECRET_KEY");
					String description = rs.getString("DESCRIPTION");

					TokenChannel tc = new TokenChannel();
					tc.setUuidTokenChannel(uuidTokenChannelInDb);
					tc.setSecretKey(secretKey);
					tc.setDescription(description);
					return tc;
				}
		);

		@SuppressWarnings("unchecked")
		List<TokenChannel> tokenChannels = (List<TokenChannel>) outputs.get("OUT_REF");
		if (tokenChannels.isEmpty()) {
			throw new NotFoundException(String.format("token channel not exist: %s", uuidTokenChannel));
		}

		return tokenChannels.get(0);
	}
}
