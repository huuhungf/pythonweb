package vn.vnpt.api.repository.user;

import vn.vnpt.api.model.AccountOtt;
import vn.vnpt.common.Common;

import java.math.BigDecimal;

public class AccountOttParser {

	public static AccountOtt parse(Object[] r) {
		String uuidAccount = Common.castObject(r[0], null);
		String channel = Common.castObject(r[1], null);
		String botId = Common.castObject(r[2], null);
		String userId = Common.castObject(r[3], null);
		BigDecimal status = Common.castObject(r[4], BigDecimal.ZERO);

		AccountOtt accountOtt = new AccountOtt();

		accountOtt.setUuidAccount(uuidAccount);
		accountOtt.setChannel(channel);
		accountOtt.setBotId(botId);
		accountOtt.setUserId(userId);
		accountOtt.setStatus(status.intValue());

		return accountOtt;
	}
}
