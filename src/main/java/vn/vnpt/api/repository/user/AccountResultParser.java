package vn.vnpt.api.repository.user;

import vn.vnpt.api.model.Account;
import vn.vnpt.common.Common;

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountResultParser {

	public static Account parse(Object[] r) {
		Account account = new Account();
		String uuidAccount = Common.castObject(r[0], null);
		String username = Common.castObject(r[14], null);
		String password = Common.castObject(r[11], null);
		String fullName = Common.castObject(r[6], null);
		String image = Common.castObject(r[16], null);

		String imageEmbed = null;
		Clob clob = Common.castObject(r[17], null);
		if (clob != null) {
			try {
				imageEmbed = clob.getSubString(1, (int) clob.length());
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException("could not get image embed");
			}
		}
		String uuidDevice = Common.castObject(r[18], null);

		String userCode = Common.castObject(r[19], null);

		BigDecimal enabledInDb = Common.castObject(r[3], BigDecimal.ZERO);
		boolean enabled = enabledInDb.intValue() != 0;

		account.setUuidAccount(uuidAccount);
		account.setUsername(username);
		account.setPassword(password);
		account.setFullName(fullName);
		account.setImageUrl(image);
		account.setImageEmbed(imageEmbed);
		account.setUuidDevice(uuidDevice);
		account.setEnabled(enabled);
		account.setUserCode(userCode);

		return account;
	}

	public static Account parse(ResultSet rs) throws SQLException {
		Account account = new Account();
		String uuidAccount = rs.getString("UUID_ACCOUNT");
		String username = rs.getString("USERNAME");
		String password = rs.getString("PASSWORD");
		String fullName = rs.getString("FULL_NAME");
		String image = rs.getString("IMAGE_URL");

		String imageEmbed = null;
		Clob clob = rs.getClob("IMAGE_EMBED");
		if (clob != null) {
			try {
				imageEmbed = clob.getSubString(1, (int) clob.length());
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException("could not get image embed");
			}
		}
		String uuidDevice = rs.getString("UUID_DEVICE");

		String userCode = rs.getString("USER_CODE");

		BigDecimal enabledInDb = Common.getDefaultIfNull(rs.getBigDecimal("ENABLED"), BigDecimal.ZERO);
		boolean enabled = enabledInDb.intValue() != 0;

		String email = rs.getString("EMAIL");
		String gender = rs.getString("GENDER");
		String audioUrl = rs.getString("AUDIO_URL");
		String phoneNumber = rs.getString("PHONE_NUMBER");
		String uuidGreeting = rs.getString("UUID_GREETING");
		String postUuidGreeting = rs.getString("UUID_POST_GREETING");

		account.setUuidAccount(uuidAccount);
		account.setUsername(username);
		account.setPassword(password);
		account.setFullName(fullName);
		account.setImageUrl(image);
		account.setImageEmbed(imageEmbed);
		account.setUuidDevice(uuidDevice);
		account.setEnabled(enabled);
		account.setUserCode(userCode);
		account.setEmail(email);
		account.setGender(gender);
		account.setAudioUrl(audioUrl);
		account.setPhoneNumber(phoneNumber);
		account.setUuidGreeting(uuidGreeting);
		account.setUuidPostGreeting(postUuidGreeting);

		return account;
	}
}
