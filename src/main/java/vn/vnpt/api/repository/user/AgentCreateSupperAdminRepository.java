package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class AgentCreateSupperAdminRepository {

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public AgentCreateSupperAdminRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account create(
			String uuidAgent, String email, String phoneNumber, String password,
			String companyName, String companyAddress,
			Integer planType, Integer planDuration, Integer numUser,
			Integer addonDevice, Integer addonOtt, Integer addonUser) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_USER.PAGENT_CREATE_SUPERADMIN_USER",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_AGENT", String.class, uuidAgent),
						ProcedureParameter.inputParam("PRS_EMAIL", String.class, email),
						ProcedureParameter.inputParam("PRS_PHONE_NUMBER", String.class, phoneNumber),
						ProcedureParameter.inputParam("PRS_PASSWORD", String.class, password),
						ProcedureParameter.inputParam("PRS_COMPANY_NAME", String.class, companyName),
						ProcedureParameter.inputParam("PRS_COMPANY_ADDRESS", String.class, companyAddress),
						ProcedureParameter.inputParam("PRN_PLAN_TYPE", Integer.class, planType),
						ProcedureParameter.inputParam("PRN_PLAN_DURATION", Integer.class, planDuration),
						ProcedureParameter.inputParam("PRN_NUM_USER", Integer.class, numUser),
						ProcedureParameter.inputParam("PRN_ADDON_DEVICE", Integer.class, addonDevice),
						ProcedureParameter.inputParam("PRN_ADDON_OTT", Integer.class, addonOtt),
						ProcedureParameter.inputParam("PRN_ADDON_USER", Integer.class, addonUser),

						ProcedureParameter.outputParam("OS_RESULT", String.class),
						ProcedureParameter.refCursorParam("OUT_REF")),
				rs -> {
					Account account = new Account();
					String uuidAccount = rs.getString("UUID_ACCOUNT");
					String usernameInDb = rs.getString("USERNAME");
					String fullNameDb = rs.getString("FULL_NAME");
					String emailDb = rs.getString("EMAIL");
					String phoneNumberDb = rs.getString("PHONE_NUMBER");

					account.setUuidAccount(uuidAccount);
					account.setUsername(usernameInDb);
					account.setFullName(fullNameDb);
					account.setEmail(emailDb);
					account.setPhoneNumber(phoneNumberDb);

					return account;
				}
		);

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.USERNAME_ALREADY_EXIST.equalsIgnoreCase(res)) {
			throw new BadRequestException(String.format("username already exist %s", email), ErrorCode.IDG_00000002);
		}
		if (ConstantString.STATUS_DB.SUPERADMIN_EMAIL_ALREADY_EXIST.equalsIgnoreCase(res)) {
			throw new BadRequestException(String.format("email already exist %s", email), ErrorCode.IDG_00002012);
		}

		if (ConstantString.STATUS_DB.NOT_ALLOWED.equals(res)) {
			throw new BadRequestException("NOT ALLOWED");
		}

		if (!ConstantString.STATUS_DB.SUCCESS.equals(res)) {
			throw new BadRequestException("NOT ALLOWED");
		}

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OUT_REF");

		return accounts.get(0);
	}
}
