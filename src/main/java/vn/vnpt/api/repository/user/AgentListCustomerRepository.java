package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.vnpt.api.dto.out.PortalAccountDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.success.model.PagingDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class AgentListCustomerRepository {
	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public AgentListCustomerRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<PortalAccountDtoOut> list(Integer page, Integer maxSize, String uuidAgents, String keySearch) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_USER.PAGENT_LIST_FILTER_CUSTOMER",
				Arrays.asList(
						ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, page),
						ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, maxSize),
						ProcedureParameter.inputParam("PRS_UUID_AGENTS", String.class, uuidAgents),
						ProcedureParameter.inputParam("PRS_KEY_SEARCH", String.class, keySearch),
						ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
						ProcedureParameter.refCursorParam("OUT_CUR")
				),
				PortalAccountDtoOut.class
		);

		Long totalElement = (Long) outputs.get("OUT_TOTAL");
		long totalPages = totalElement / maxSize;
		if (totalElement % maxSize != 0) {
			totalPages++;
		}

		PagingDTO<PortalAccountDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(page);
		pagingDTO.setMaxSize(maxSize);
		pagingDTO.setTotalElement(totalElement);
		pagingDTO.setTotalPages(totalPages);

		if (page > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<PortalAccountDtoOut> data = (List<PortalAccountDtoOut>) outputs.get("OUT_CUR");

		pagingDTO.setData(data);

		return pagingDTO;
	}
}
