package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ChangeAccountPasswordRepository {

	private static final String PROC_CHANGE_PASSWORD = "PKG_USER.PCHANGE_PASSWORD_USER";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public ChangeAccountPasswordRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void changePassword(String uuidAccount, String newPassword) {
		List<Object> outputs = procedureCaller.call(PROC_CHANGE_PASSWORD, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.inputParam("PRS_NEW_PASSWORD", String.class, Common.encryptPassword(newPassword)),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get(0);
		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("account not exist: %s", uuidAccount));
		}
	}
}
