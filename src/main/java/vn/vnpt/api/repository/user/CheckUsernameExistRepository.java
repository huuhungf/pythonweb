package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;

import java.util.Arrays;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CheckUsernameExistRepository {

	private static final String PROC_CHECK_USERNAME_EXIST = "PKG_USER.PCHECK_USERNAME_EXIST";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CheckUsernameExistRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Boolean isExist(String username) {

		Map<String, Object> outputs = procedureCaller.callNoRefCursor(PROC_CHECK_USERNAME_EXIST, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get("OS_RESULT");

		return ConstantString.STATUS_DB.USERNAME_ALREADY_EXIST.equals(res);
	}
}
