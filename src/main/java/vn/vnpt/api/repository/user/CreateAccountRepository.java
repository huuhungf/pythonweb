package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.AccountDtoIn;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CreateAccountRepository {

	private static final String PROC_CREATE_USER = "PKG_USER.PCREATE_USER";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public CreateAccountRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account createAccount(AccountDtoIn accountDtoIn, String adminUsername) {

		List<Object> outputs = procedureCaller.call(PROC_CREATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_BIRTHDAY", String.class, null),
				ProcedureParameter.inputParam("PRN_CREDENTIALS_EXPIRED", Integer.class, 0),
				ProcedureParameter.inputParam("PRN_ENABLED", Integer.class, 1),
				ProcedureParameter.inputParam("PRN_EXPIRED", Integer.class, 0),
				ProcedureParameter.inputParam("PRS_FIRST_NAME", String.class, null),
				ProcedureParameter.inputParam("PRS_FULL_NAME", String.class, accountDtoIn.getFullName()),
				ProcedureParameter.inputParam("PRS_GENDER", String.class, "MALE"),
				ProcedureParameter.inputParam("PRS_LAST_NAME", String.class, null),
				ProcedureParameter.inputParam("PRS_LOCATION", String.class, null),
				ProcedureParameter.inputParam("PRN_LOCKED", Integer.class, 0),
				ProcedureParameter.inputParam("PRS_PASSWORD", String.class, Common.encryptPassword(accountDtoIn.getPassword().trim())),
				ProcedureParameter.inputParam("PRS_PHONE_NUMBER", String.class, null),
				ProcedureParameter.inputParam("PRS_PROVIDER", String.class, "WEB"),
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, Common.replaceDotInUserName(accountDtoIn.getUsername())),
				ProcedureParameter.inputParam("PRN_USING2FA", Integer.class, 0),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, accountDtoIn.getImageUrl()),
				ProcedureParameter.inputParam("PRS_IMAGE_EMBED", String.class, accountDtoIn.getImageEmbed()),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, accountDtoIn.getDevice().getUuidDevice()),
				ProcedureParameter.inputParam("PRS_UUID_ROLE", String.class, accountDtoIn.getRole().getUuidRole()),
				ProcedureParameter.inputParam("PRS_USER_CODE", String.class, accountDtoIn.getUserCode()),
				ProcedureParameter.inputParam("PRS_ADMIN_USERNAME", String.class, Common.replaceDotInUserName(adminUsername)),

				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_USER")
		));

		String res = (String) outputs.get(0);

		if (ConstantString.STATUS_DB.ROLE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("uuid role not found: %s", accountDtoIn.getRole().getUuidRole()));
		}

		if (ConstantString.STATUS_DB.DEVICE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("uuid device not found: %s", accountDtoIn.getDevice().getUuidDevice()));
		}

		if (ConstantString.STATUS_DB.ADMIN_NOT_OWN_DEVICE.equals(res)) {
			throw new AccessDeniedException("access denied");
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<Account> accounts = new ResultProcessor<Account>().process(resultList, AccountResultParser::parse);

		return accounts.get(0);
	}
}
