package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class CreateSupperAdminAccountRepository {

	private static final String PROC_CREATE_USER = "PKG_USER.PCREATE_SUPERADMIN_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public CreateSupperAdminAccountRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account create(String username, String fullName, String companyName, String password, String email, String phoneNumber,
						  String career, String imageUrl, Integer notification) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_CREATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.inputParam("PRS_FULL_NAME", String.class, fullName),
				ProcedureParameter.inputParam("PRS_COMPANY_NAME", String.class, companyName),
				ProcedureParameter.inputParam("PRS_PASSWORD", String.class, Common.encryptPassword(password)),
				ProcedureParameter.inputParam("PRS_EMAIL", String.class, email),
				ProcedureParameter.inputParam("PRS_PHONE_NUMBER", String.class, phoneNumber),
				ProcedureParameter.inputParam("PRS_CAREER", String.class, career),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, imageUrl),
				ProcedureParameter.inputParam("PRN_NOTIFICATION", Integer.class, notification),

				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OR_CURSOR")),
				rs -> {
					Account account = new Account();
					String uuidAccount = rs.getString("UUID_ACCOUNT");
					String usernameInDb = rs.getString("USERNAME");
					String fullNameDb = rs.getString("FULL_NAME");
					String emailDb = rs.getString("EMAIL");
					String phoneNumberDb = rs.getString("PHONE_NUMBER");

					account.setUuidAccount(uuidAccount);
					account.setUsername(usernameInDb);
					account.setFullName(fullNameDb);
					account.setEmail(emailDb);
					account.setPhoneNumber(phoneNumberDb);

					return account;
				}
				);

		String res = (String) outputs.get("OS_RESULT");
		if (ConstantString.STATUS_DB.USERNAME_ALREADY_EXIST.equalsIgnoreCase(res)) {
			throw new BadRequestException(String.format("username already exist %s", username), ErrorCode.IDG_00000002);
		}
		if (ConstantString.STATUS_DB.SUPERADMIN_EMAIL_ALREADY_EXIST.equalsIgnoreCase(res)) {
			throw new BadRequestException(String.format("email already exist %s", email), ErrorCode.IDG_00002012);
		}

		if (ConstantString.STATUS_DB.NOT_ALLOWED.equals(res)) {
			throw new BadRequestException("NOT ALLOWED");
		}

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OR_CURSOR");

		return accounts.get(0);
	}
}
