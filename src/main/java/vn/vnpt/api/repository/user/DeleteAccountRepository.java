package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class DeleteAccountRepository {
	private static final String PROC_DELETE_ACCOUNT = "PKG_USER.PDELETE_USER";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public DeleteAccountRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void delete(String uuidAccount) {
		List<Object> outputs = procedureCaller.call(PROC_DELETE_ACCOUNT, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get(0);
		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("account not exist: %s", uuidAccount));
		}
	}
}
