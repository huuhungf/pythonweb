package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class EnableAccountRepository {
	private static final String PROC_ENABLE_ACCOUNT = "PKG_USER.PENABLE_USER";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public EnableAccountRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void enable(String uuidAccount) {
		procedureCaller.call(PROC_ENABLE_ACCOUNT, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
