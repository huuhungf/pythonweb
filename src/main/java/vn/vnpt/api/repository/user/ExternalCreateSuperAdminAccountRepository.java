package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.out.ExternalRegisterDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.BadRequestException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ExternalCreateSuperAdminAccountRepository {

	private static final String PROC_CREATE_USER = "PKG_USER.PEXTERNAL_CREATE_SUPERADMIN_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ExternalCreateSuperAdminAccountRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public ExternalRegisterDtoOut create(
			String externalCustomerId, String email, String phoneNumber, String rawPassword, String createdBy,
			String uuidSmpPackage, Integer numAccount, Integer numDevice, Integer numOtt,
			String activeDate, String expireDate
	) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_CREATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_EXTERNAL_CUSTOMER_ID", String.class, externalCustomerId),
				ProcedureParameter.inputParam("PRS_EMAIL", String.class, email),
				ProcedureParameter.inputParam("PRS_PHONE_NUMBER", String.class, phoneNumber),
				ProcedureParameter.inputParam("PRS_PASSWORD", String.class, Common.encryptPassword(rawPassword)),
				ProcedureParameter.inputParam("PRS_CREATED_BY", String.class, createdBy),
				ProcedureParameter.inputParam("PRS_UUID_SMP_PACKAGE", String.class, uuidSmpPackage),
				ProcedureParameter.inputParam("PRN_NUM_ACCOUNT", Integer.class, numAccount),
				ProcedureParameter.inputParam("PRN_NUM_DEVICE", Integer.class, numDevice),
				ProcedureParameter.inputParam("PRN_NUM_OTT", Integer.class, numOtt),
				ProcedureParameter.inputParam("PRS_ACTIVE_DATE", String.class, activeDate),
				ProcedureParameter.inputParam("PRS_EXPIRE_DATE", String.class, expireDate),

				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_REF")),
				rs -> {
					String uuidAccount = rs.getString("UUID_ACCOUNT");
					String emailDb = rs.getString("EMAIL");
					String uuidPlan = rs.getString("UUID_PLAN");
					String planName = rs.getString("PLAN_NAME");
					Integer status = rs.getInt("STATUS");
					String activeDateInDb = rs.getString("ACTIVE_DATE");
					String expireDateInDb = rs.getString("EXPIRE_DATE");

					ExternalRegisterDtoOut dtoOut = new ExternalRegisterDtoOut();
					dtoOut.setUuidAccount(uuidAccount);
					dtoOut.setEmail(emailDb);
					dtoOut.setUuidPlan(uuidPlan);
					dtoOut.setPlanName(planName);
					dtoOut.setStatus(status);
					dtoOut.setActiveDate(activeDateInDb);
					dtoOut.setExpireDate(expireDateInDb);

					return dtoOut;
				}
		);

		String res = (String) outputs.get("OS_RESULT");

		if(ConstantString.STATUS_DB.PLAN_NOT_EXIST.equalsIgnoreCase(res)) {
			throw new BadRequestException(String.format("plan not exist %s", uuidSmpPackage));
		}

		if (ConstantString.STATUS_DB.USERNAME_ALREADY_EXIST.equalsIgnoreCase(res)) {
			throw new BadRequestException(String.format("username already exist %s", email), ErrorCode.IDG_00000002);
		}
		if (ConstantString.STATUS_DB.SUPERADMIN_EMAIL_ALREADY_EXIST.equalsIgnoreCase(res)) {
			throw new BadRequestException(String.format("email already exist %s", email), ErrorCode.IDG_00002012);
		}

		if (ConstantString.STATUS_DB.NOT_ALLOWED.equals(res)) {
			throw new BadRequestException("vui lòng hủy gói cước hiện tại trước khi đăng ký gói cước mới");
		}

		@SuppressWarnings("unchecked")
		List<ExternalRegisterDtoOut> dtoOuts = (List<ExternalRegisterDtoOut>) outputs.get("OUT_REF");

		return dtoOuts.get(0);
	}
}
