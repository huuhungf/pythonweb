package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ExternalGetAccountRepository {

	private static final String PROC_EXTERNAL_GET_USER = "PKG_USER.PEXTERNAL_GET_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public ExternalGetAccountRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account get(String externalCustomerId) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_EXTERNAL_GET_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_EXTERNAL_CUSTOMER_ID", String.class, externalCustomerId),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_REF")),
				AccountResultParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OUT_REF");
		if (accounts.isEmpty()) {
			throw new NotFoundException(String.format("account not found: %s", externalCustomerId));
		}
		return accounts.get(0);
	}
}
