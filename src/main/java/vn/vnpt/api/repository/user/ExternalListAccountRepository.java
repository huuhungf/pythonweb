package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.vnpt.api.dto.out.ExternalAccountDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.success.model.PagingDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class ExternalListAccountRepository {
	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public ExternalListAccountRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<ExternalAccountDtoOut> list(Integer page, Integer maxSize, String startDate, String endDate, String username) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_USER.PEXTERNAL_LIST_USER_BY_UPDATE_TIME",
				Arrays.asList(
						ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, page),
						ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, maxSize),
						ProcedureParameter.inputParam("PRS_SUPER_USERNAME", String.class, username),
						ProcedureParameter.inputParam("PRS_FROM_DATE", String.class, startDate),
						ProcedureParameter.inputParam("PRS_TO_DATE", String.class, endDate),
						ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
						ProcedureParameter.refCursorParam("OUT_CUR")
				),
				ExternalAccountDtoOut.class
		);

		Long totalElement = (Long) outputs.get("OUT_TOTAL");
		long totalPages = totalElement / maxSize;
		if (totalElement % maxSize != 0) {
			totalPages++;
		}

		PagingDTO<ExternalAccountDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(page);
		pagingDTO.setMaxSize(maxSize);
		pagingDTO.setTotalElement(totalElement);
		pagingDTO.setTotalPages(totalPages);

		if (page > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<ExternalAccountDtoOut> data = (List<ExternalAccountDtoOut>) outputs.get("OUT_CUR");

		pagingDTO.setData(data);

		return pagingDTO;
	}
}
