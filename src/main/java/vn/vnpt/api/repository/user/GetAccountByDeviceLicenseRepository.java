package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetAccountByDeviceLicenseRepository {

	private static final String PROC_GET_ACCOUNT_BY_DEVICE_LICENSE = "PKG_DEVICE.PGET_USER_BY_DEVICE_LICENSE";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public GetAccountByDeviceLicenseRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account getAccount(String uuidDevice, String serialNumber, String licenseKey) {
		List<Object> outputs = procedureCaller.call(PROC_GET_ACCOUNT_BY_DEVICE_LICENSE, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, uuidDevice),
				ProcedureParameter.inputParam("PRS_SERIAL_NUMBER", String.class, serialNumber),
				ProcedureParameter.inputParam("PRS_LICENSE_KEY", String.class, licenseKey),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_USER")
		));

		String res = (String) outputs.get(0);
		if(ConstantString.STATUS_DB.DEVICE_LICENSE_NOT_EXIST.equals(res)) {
			throw new NotFoundException("device license incorrect or not found");
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<Account> accounts = new ResultProcessor<Account>().process(resultList, AccountResultParser::parse);

		return accounts.get(0);
	}
}
