package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetAccountByUsernameRepository {

	private static final String PROC_GET_USER_BY_USERNAME = "PKG_USER.PGET_USER_BY_USERNAME";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public GetAccountByUsernameRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account getAccountByUserName(String username) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_USER_BY_USERNAME, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_USER")),
				AccountResultParser::parse
		);

		String res = (String) outputs.get("OS_RESULT");

		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("account not found: %s", username));
		}

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OREF_USER");
		return accounts.get(0);
	}
}
