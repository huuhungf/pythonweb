package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetAccountRepository {

	private static final String PROC_GET_ACCOUNT = "PKG_USER.PGET_USER_BY_UUID_ACCOUNT";

	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public GetAccountRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account get(String uuidAccount) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_ACCOUNT, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_USER")),
				Account.class
		);

		String res = (String) outputs.get("OS_RESULT");

		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("uuid account not found: %s", uuidAccount));
		}

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OREF_USER");

		return accounts.get(0);
	}
}
