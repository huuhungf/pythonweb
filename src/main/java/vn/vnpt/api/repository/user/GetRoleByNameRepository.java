package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Role;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class GetRoleByNameRepository {

	private final ProcedureCaller procedureCaller;

	private static final String PROC_GET_ROLE_BY_NAME = "PKG_USER.PGET_ROLE_BY_NAME";

	@Autowired
	public GetRoleByNameRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Role getRole(String roleName) {

		List<Object> outputs = procedureCaller.call(PROC_GET_ROLE_BY_NAME, Arrays.asList(
				ProcedureParameter.inputParam("PRS_ROLE_NAME", String.class, roleName),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_ROLE")
		));

		String res = (String) outputs.get(0);
		if (ConstantString.STATUS_DB.ROLE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("Role name not found: %s", roleName));
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<Role> roles = new ResultProcessor<Role>().process(resultList, RoleResultParser::parse);
		return roles.get(0);
	}
}
