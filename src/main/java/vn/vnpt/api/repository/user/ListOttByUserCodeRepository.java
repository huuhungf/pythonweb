package vn.vnpt.api.repository.user;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AccountOttUserCode;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListOttByUserCodeRepository {

	private final ProcedureCallerV3 procedureCaller;

	public ListOttByUserCodeRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<AccountOttUserCode> listOtt(String superUsername, String userCodes, String channels) {
		Map<String, Object> outputs = procedureCaller.callOneRefCursor(
				"PKG_USER.PGET_LIST_OTT_BY_USERCODE",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_SUPPER_USERNAME", String.class, superUsername),
						ProcedureParameter.inputParam("PRS_USER_CODES", String.class, userCodes),
						ProcedureParameter.inputParam("PRS_CHANNELS", String.class, channels),
						ProcedureParameter.refCursorParam("OREF_OTT")
				),
				AccountOttUserCode.class
		);

		@SuppressWarnings("unchecked")
		List<AccountOttUserCode> accountOtts = (List<AccountOttUserCode>) outputs.get("OREF_OTT");

		return accountOtts;
	}
}
