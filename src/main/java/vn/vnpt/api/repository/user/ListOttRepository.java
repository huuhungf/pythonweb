package vn.vnpt.api.repository.user;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.AccountOtt;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListOttRepository {
	private static final String PROC_LIST_OTT = "PKG_USER.PGET_LIST_OTT";

	private final ProcedureCaller procedureCaller;

	public ListOttRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<AccountOtt> listOtt(String uuidAccount) {
		List<Object> outputs = procedureCaller.call(PROC_LIST_OTT, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.refCursorParam("OREF_OTT")
		));

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(0);

		List<AccountOtt> accountOtts = new ResultProcessor<AccountOtt>().process(resultList, AccountOttParser::parse);
		return accountOtts;
	}
}
