package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Role;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.constant.ConstantString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ListRoleByUuidAccountRepository {

	private static final String PROC_GET_LIST_ROLE = "PKG_USER.PGET_ROLE_BY_ACCOUNT_UUID";

	private final ProcedureCaller procedureCaller;

	@Autowired
	public ListRoleByUuidAccountRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<Role> listRole(String uuidAccount) {

		List<Object> outputs = procedureCaller.call(PROC_GET_LIST_ROLE, Arrays.asList(
			ProcedureParameter.inputParam("PRS_ACCOUNT_UUID", String.class, uuidAccount),
			ProcedureParameter.outputParam("OS_RESULT", String.class),
			ProcedureParameter.refCursorParam("OREF_ROLE")
		));

		String res = (String) outputs.get(0);

		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			return new ArrayList<>();
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<Role> roles = new ResultProcessor<Role>().process(resultList, RoleResultParser::parse);
		return roles;
	}
}
