package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV3;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class LockAccountRepository {
	private final ProcedureCallerV3 procedureCaller;

	@Autowired
	public LockAccountRepository(ProcedureCallerV3 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void lock(String uuidAccount, Integer locked) {
		procedureCaller.callNoRefCursor(
				"PKG_USER.PLOCK_USER",
				Arrays.asList(
						ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
						ProcedureParameter.inputParam("PRN_LOCKED", Integer.class, locked),
						ProcedureParameter.outputParam("OS_RESULT", String.class)
				)
		);
	}
}
