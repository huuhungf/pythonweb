package vn.vnpt.api.repository.user;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.AccountChannelInactiveException;
import vn.vnpt.common.exception.ExceedMaxUserOttException;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.exception.PasswordInvalidException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class RegisterOttRepository {
	private static final String PROC_REGISTER_OTT = "PKG_USER.PREGISTER_OTT";

	private final ProcedureCaller procedureCaller;

	public RegisterOttRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void registerOtt(String username, String channel, String botId, String userId) {
		List<Object> outputs = procedureCaller.call(PROC_REGISTER_OTT, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.inputParam("PRS_CHANNEL", String.class, channel),
				ProcedureParameter.inputParam("PRS_BOT_ID", String.class, botId),
				ProcedureParameter.inputParam("PRS_USER_ID", String.class, userId),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get(0);
		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("username not exist %s", username));
		}

		if (ConstantString.STATUS_DB.PASSWORD_INVALID.equals(res)) {
			throw new PasswordInvalidException("password invalid");
		}

		if (ConstantString.STATUS_DB.ACCOUNT_CHANNEL_INACTIVE.equals(res)) {
			throw new AccountChannelInactiveException(String.format("account channel inactive %s", channel));
		}

		if (ConstantString.STATUS_DB.EXCEED_MAX_USER_OTT.equals(res)) {
			throw new ExceedMaxUserOttException("exceed max user ott");
		}
	}
}
