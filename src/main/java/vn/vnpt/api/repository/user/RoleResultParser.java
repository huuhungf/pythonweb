package vn.vnpt.api.repository.user;

import vn.vnpt.api.model.Role;
import vn.vnpt.common.Common;

public class RoleResultParser {
	public static Role parse(Object[] r) {
		Role role = new Role();

		String uuidRole = Common.castObject(r[0], null);
		String uuidApplication = Common.castObject(r[1], null);
		String description = Common.castObject(r[2], null);
		String name = Common.castObject(r[3], null);

		role.setUuidRole(uuidRole);
		role.setUuidApplication(uuidApplication);
		role.setDescription(description);
		role.setName(name);

		return role;
	}
}
