package vn.vnpt.api.repository.user;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.exception.UnregisteredUserOttException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UnregisterOttRepository {
	private static final String PROC_UNREGISTER_OTT = "PKG_USER.PUNREGISTER_OTT";

	private final ProcedureCaller procedureCaller;

	public UnregisterOttRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void unregisterOtt(String username, String channel, String botId, String userId) {
		List<Object> outputs = procedureCaller.call(PROC_UNREGISTER_OTT, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username.toUpperCase()),
				ProcedureParameter.inputParam("PRS_CHANNEL", String.class, channel),
				ProcedureParameter.inputParam("PRS_BOT_ID", String.class, botId),
				ProcedureParameter.inputParam("PRS_USER_ID", String.class, userId),
				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));

		String res = (String) outputs.get(0);
		if (ConstantString.STATUS_DB.USER_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("username not exist %s", username));
		}
		if (ConstantString.STATUS_DB.UNREGISTERED_ACCOUNT_OTT.equals(res)) {
			throw new UnregisteredUserOttException(String.format("unregistered user %s %s", username, userId));
		}
	}
}
