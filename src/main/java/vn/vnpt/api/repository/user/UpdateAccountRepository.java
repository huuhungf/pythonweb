package vn.vnpt.api.repository.user;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCaller;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.api.repository.ResultProcessor;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateAccountRepository {

	private static final String PROC_UPDATE_USER = "PKG_USER.PUPDATE_USER";

	private final ProcedureCaller procedureCaller;

	public UpdateAccountRepository(ProcedureCaller procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account update(String adminUsername, Account accountUpdate, String uuidRole) {

		List<Object> outputs = procedureCaller.call(PROC_UPDATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, accountUpdate.getUuidAccount()),
				ProcedureParameter.inputParam("PRS_FULL_NAME", String.class, accountUpdate.getFullName()),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, accountUpdate.getImageUrl()),
				ProcedureParameter.inputParam("PRS_IMAGE_EMBED", String.class, accountUpdate.getImageEmbed()),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, accountUpdate.getUuidDevice()),
				ProcedureParameter.inputParam("PRS_UUID_ROLE", String.class, uuidRole),
				ProcedureParameter.inputParam("PRS_USER_CODE", String.class, accountUpdate.getUserCode()),
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, Common.replaceDotInUserName(accountUpdate.getUsername())),
				ProcedureParameter.inputParam("PRS_ADMIN_USERNAME", String.class, Common.replaceDotInUserName(adminUsername)),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_USER")
		));

		String res = (String) outputs.get(0);

		if (ConstantString.STATUS_DB.ACCOUNT_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("account not exist: %s", accountUpdate.getUuidAccount()));
		}

		if (ConstantString.STATUS_DB.DEVICE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("uuid device not found: %s", accountUpdate.getUuidDevice()));
		}

		if (ConstantString.STATUS_DB.ADMIN_NOT_OWN_DEVICE.equals(res)) {
			throw new AccessDeniedException("access denied");
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = (List<Object[]>) outputs.get(1);
		List<Account> accounts = new ResultProcessor<Account>().process(resultList, AccountResultParser::parse);

		return accounts.get(0);
	}
}
