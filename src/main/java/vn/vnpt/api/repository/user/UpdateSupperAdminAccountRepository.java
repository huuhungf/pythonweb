package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UpdateSupperAdminAccountRepository {

	private static final String PROC_UPDATE_USER = "PKG_USER.PUPDATE_SUPERADMIN_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public UpdateSupperAdminAccountRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String username, String fullName, String companyName, String phoneNumber,
					   String career, String imageUrl, Integer notification) {

		procedureCaller.callNoRefCursor(PROC_UPDATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, username),
				ProcedureParameter.inputParam("PRS_FULL_NAME", String.class, fullName),
				ProcedureParameter.inputParam("PRS_COMPANY_NAME", String.class, companyName),
				ProcedureParameter.inputParam("PRS_PHONE_NUMBER", String.class, phoneNumber),
				ProcedureParameter.inputParam("PRS_CAREER", String.class, career),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, imageUrl),
				ProcedureParameter.inputParam("PRN_NOTIFICATION", Integer.class, notification),

				ProcedureParameter.outputParam("OS_RESULT", String.class)
		));
	}
}
