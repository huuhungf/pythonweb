package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.in.AccountDtoIn;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebCreateAccountRepository {

	private static final String PROC_CREATE_USER = "PKG_USER.PWEB_CREATE_USER";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebCreateAccountRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account createAccount(AccountDtoIn accountDtoIn, String listUuidGroup) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_CREATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_BIRTHDAY", String.class, null),
				ProcedureParameter.inputParam("PRN_CREDENTIALS_EXPIRED", Integer.class, 0),
				ProcedureParameter.inputParam("PRN_ENABLED", Integer.class, 1),
				ProcedureParameter.inputParam("PRN_EXPIRED", Integer.class, 0),
				ProcedureParameter.inputParam("PRS_FIRST_NAME", String.class, null),
				ProcedureParameter.inputParam("PRS_FULL_NAME", String.class, accountDtoIn.getFullName()),
				ProcedureParameter.inputParam("PRS_GENDER", String.class, accountDtoIn.getGender()),
				ProcedureParameter.inputParam("PRS_LAST_NAME", String.class, null),
				ProcedureParameter.inputParam("PRS_LOCATION", String.class, null),
				ProcedureParameter.inputParam("PRN_LOCKED", Integer.class, 0),
				ProcedureParameter.inputParam("PRS_PASSWORD", String.class, Common.encryptPassword(accountDtoIn.getPassword().trim())),
				ProcedureParameter.inputParam("PRS_PHONE_NUMBER", String.class, accountDtoIn.getPhoneNumber()),
				ProcedureParameter.inputParam("PRS_PROVIDER", String.class, "WEB"),
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, Common.replaceDotInUserName(accountDtoIn.getUsername())),
				ProcedureParameter.inputParam("PRN_USING2FA", Integer.class, 0),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, accountDtoIn.getImageUrl()),
				ProcedureParameter.inputParam("PRS_IMAGE_EMBED", String.class, accountDtoIn.getImageEmbed()),
				ProcedureParameter.inputParam("PRS_UUID_DEVICE", String.class, null),
				ProcedureParameter.inputParam("PRS_UUID_ROLE", String.class, accountDtoIn.getRole().getUuidRole()),
				ProcedureParameter.inputParam("PRS_USER_CODE", String.class, accountDtoIn.getUserCode()),
				ProcedureParameter.inputParam("PRS_EMAIL", String.class, Common.replaceDotInUserName(accountDtoIn.getEmail())),
				ProcedureParameter.inputParam("PRS_AUDIO_URL", String.class, accountDtoIn.getAudioUrl()),
				ProcedureParameter.inputParam("PRS_LIST_UUID_GROUP", String.class, listUuidGroup),
				ProcedureParameter.inputParam("PRN_IMAGE_TYPE", Integer.class, accountDtoIn.getImageType()),

				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_USER")
				),
				AccountResultParser::parse
		);

		String res = (String) outputs.get("OS_RESULT");

		if (ConstantString.STATUS_DB.ROLE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("uuid role not found: %s", accountDtoIn.getRole().getUuidRole()));
		}

		if (ConstantString.STATUS_DB.ADMIN_NOT_OWN_DEVICE.equals(res)) {
			throw new AccessDeniedException("access denied");
		}

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OREF_USER");

		return accounts.get(0);
	}
}
