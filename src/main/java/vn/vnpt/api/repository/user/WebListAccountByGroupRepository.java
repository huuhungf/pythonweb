package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.dto.out.WebAccountDtoOut;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebListAccountByGroupRepository {

	private static final String PROC_GET_LIST_USER = "PKG_USER.PWEB_GET_LIST_USER_BY_GROUP";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebListAccountByGroupRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public PagingDTO<WebAccountDtoOut> listAccount(String uuidGroup, String keySearch, Integer imageMode, PagingDtoIn pagingDtoIn, Integer adminGroup) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_LIST_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRN_PAGE_INDEX", Integer.class, pagingDtoIn.getPage()),
				ProcedureParameter.inputParam("PRN_PAGE_SIZE", Integer.class, pagingDtoIn.getMaxSize()),
				ProcedureParameter.inputParam("PRS_UUID_GROUP", String.class, uuidGroup),
				ProcedureParameter.inputParam("PRS_KEY_SEARCH", String.class, keySearch),
				ProcedureParameter.inputParam("PRN_IMAGE_MODE", Integer.class, imageMode),
				ProcedureParameter.inputParam("PRN_ADMIN_GROUP", Integer.class, adminGroup),
				ProcedureParameter.outputParam("OUT_TOTAL", Long.class),
				ProcedureParameter.refCursorParam("OUT_CUR")),
				r -> {
					String uuidAccount = r.getString("UUID_ACCOUNT");
					String fullName = r.getString("FULL_NAME");
					String userCode = r.getString("USER_CODE");
					String username = r.getString("USERNAME");
					String imageUrl = r.getString("IMAGE_URL");
					String role = r.getString("ROLE_NAME");
					String gender = r.getString("GENDER");
					String email = r.getString("EMAIL");
					String channels = r.getString("LIST_CHANNEL_NAME");
					Integer locked = r.getInt("LOCKED");
					String phoneNumber = r.getString("PHONE_NUMBER");
					List<String> channelList = new ArrayList<>();
					if (channels != null && !channels.isEmpty()) {
						channelList = Arrays.asList(channels.split(","));
					}
					Integer isAdmin = r.getInt("IS_ADMIN");

					WebAccountDtoOut accountDtoOut = new WebAccountDtoOut();
					accountDtoOut.setUuidAccount(uuidAccount);
					accountDtoOut.setFullName(fullName);
					accountDtoOut.setUserCode(userCode);
					accountDtoOut.setUsername(username);
					accountDtoOut.setImageUrl(imageUrl);
					accountDtoOut.setRole(role);
					accountDtoOut.setGender(gender);
					accountDtoOut.setEmail(email);
					accountDtoOut.setLocked(locked);
					accountDtoOut.setChannels(channelList);
					accountDtoOut.setIsAdmin(isAdmin);
					accountDtoOut.setPhoneNumber(phoneNumber);

					return accountDtoOut;
				}
		);

		Long total = (Long) outputs.get("OUT_TOTAL");
		long totalPages = total / pagingDtoIn.getMaxSize();
		if (total % pagingDtoIn.getMaxSize() != 0) {
			totalPages += 1;
		}

		PagingDTO<WebAccountDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalPages(totalPages);
		pagingDTO.setTotalElement(total);

		if (pagingDtoIn.getPage() > totalPages) {
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		@SuppressWarnings("unchecked")
		List<WebAccountDtoOut> data = (List<WebAccountDtoOut>) outputs.get("OUT_CUR");

		pagingDTO.setData(data);

		return pagingDTO;
	}
}
