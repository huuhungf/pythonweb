package vn.vnpt.api.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebListAccountByUsernameOrEmailRepository {

	private static final String PROC_GET_LIST_USER = "PKG_USER.WEB_LIST_ACC_BY_USERNAME_OR_EMAIL";

	private final ProcedureCallerV2 procedureCaller;

	@Autowired
	public WebListAccountByUsernameOrEmailRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public List<Account> list(String keyword) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_GET_LIST_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_USERNAME_OR_EMAIL", String.class, keyword),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OUT_REF")),
				AccountResultParser::parse
		);

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OUT_REF");

		return accounts;
	}
}
