package vn.vnpt.api.repository.user;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebUpdateAccountAudioUrlRepository {

	private static final String PROC_UPDATE_USER = "PKG_USER.PWEB_UPDATE_USER_AUDIO_URL";

	private final ProcedureCallerV2 procedureCaller;

	public WebUpdateAccountAudioUrlRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public void update(String uuidAccount, String audioUrl) {

		procedureCaller.callNoRefCursor(PROC_UPDATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.inputParam("PRS_AUDIO_URL", String.class, audioUrl),
				ProcedureParameter.outputParam("OS_RESULT", String.class)));
	}
}
