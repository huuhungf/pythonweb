package vn.vnpt.api.repository.user;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebUpdateAccountFromStrangerRepository {

	private static final String PROC_UPDATE_USER = "PKG_USER.PWEB_UPDATE_USER_FROM_STRANGER";

	private final ProcedureCallerV2 procedureCaller;

	public WebUpdateAccountFromStrangerRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account update(String uuidAccount, String imageUrl, String imageEmbed) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_UPDATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, uuidAccount),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, imageUrl),
				ProcedureParameter.inputParam("PRS_IMAGE_EMBED", String.class, imageEmbed),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_USER")),
				AccountResultParser::parse);

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OREF_USER");

		return accounts.get(0);
	}
}
