package vn.vnpt.api.repository.user;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.ProcedureCallerV2;
import vn.vnpt.api.repository.ProcedureParameter;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class WebUpdateAccountRepository {

	private static final String PROC_UPDATE_USER = "PKG_USER.PWEB_UPDATE_USER";

	private final ProcedureCallerV2 procedureCaller;

	public WebUpdateAccountRepository(ProcedureCallerV2 procedureCaller) {
		this.procedureCaller = procedureCaller;
	}

	public Account update(Account accountUpdate, String uuidRole, String listUuidGroup, String listUuidAdminGroup) {

		Map<String, Object> outputs = procedureCaller.callOneRefCursor(PROC_UPDATE_USER, Arrays.asList(
				ProcedureParameter.inputParam("PRS_UUID_ACCOUNT", String.class, accountUpdate.getUuidAccount()),
				ProcedureParameter.inputParam("PRS_FULL_NAME", String.class, accountUpdate.getFullName()),
				ProcedureParameter.inputParam("PRS_IMAGE_URL", String.class, accountUpdate.getImageUrl()),
				ProcedureParameter.inputParam("PRS_IMAGE_EMBED", String.class, accountUpdate.getImageEmbed()),
				ProcedureParameter.inputParam("PRS_UUID_ROLE", String.class, uuidRole),
				ProcedureParameter.inputParam("PRS_USER_CODE", String.class, accountUpdate.getUserCode()),
				ProcedureParameter.inputParam("PRS_USERNAME", String.class, Common.replaceDotInUserName(accountUpdate.getUsername())),
				ProcedureParameter.inputParam("PRS_EMAIL", String.class, Common.replaceDotInUserName(accountUpdate.getEmail())),
				ProcedureParameter.inputParam("PRS_GENDER", String.class, accountUpdate.getGender()),
				ProcedureParameter.inputParam("PRS_AUDIO_URL", String.class, accountUpdate.getAudioUrl()),
				ProcedureParameter.inputParam("PRS_LIST_UUID_GROUP", String.class, listUuidGroup),
				ProcedureParameter.inputParam("PRS_LIST_UUID_ADMIN_GROUP", String.class, listUuidAdminGroup),
				ProcedureParameter.inputParam("PRS_PHONE_NUMBER", String.class, accountUpdate.getPhoneNumber()),
				ProcedureParameter.inputParam("PRN_IMAGE_TYPE", Integer.class, accountUpdate.getImageType()),
				ProcedureParameter.outputParam("OS_RESULT", String.class),
				ProcedureParameter.refCursorParam("OREF_USER")),
				AccountResultParser::parse);

		String res = (String) outputs.get("OS_RESULT");

		if (ConstantString.STATUS_DB.ACCOUNT_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("account not exist: %s", accountUpdate.getUuidAccount()));
		}

		if (ConstantString.STATUS_DB.DEVICE_NOT_EXIST.equals(res)) {
			throw new NotFoundException(String.format("uuid device not found: %s", accountUpdate.getUuidDevice()));
		}

		if (ConstantString.STATUS_DB.ADMIN_NOT_OWN_DEVICE.equals(res)) {
			throw new AccessDeniedException("access denied");
		}

		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) outputs.get("OREF_USER");

		return accounts.get(0);
	}
}
