/*******************************************************************************
 * Copyright (c) 2017 ANHTCN.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package vn.vnpt.api.service;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

/**
 * @author trananh
 */
public interface AccountService {

	PagingDTO<WebAccountDtoOut> webListAccount(WebListAccountDtoIn webListAccountDtoIn, PagingDtoIn pagingDtoIn);

	AccountProfile getAccountProfile(String username, String role);

	void resetPassword(ResetPasswordDtoIn resetPasswordDtoIn);

	void changePassword(String username, ChangePasswordDtoIn changePasswordDtoIn);

	void deleteAccount(String uuidAccount);

	OAuth2AccessToken login(LoginDtoIn loginDtoIn);

	WebAccountDtoOut createAccount(WebAccountDtoIn webAccountDtoIn);

	AccountProfileDtoOut getAccount(String adminUsername, String role, String uuidAccount);

	WebAccountDtoOut updateAccount(WebUpdateAccountDtoIn webUpdateAccountDtoIn);

	void createForgotPassword(CreateForgotPasswordDtoIn createForgotPasswordDtoIn);

	void confirmForgotPassword(ConfirmForgotPasswordDtoIn confirmForgotPasswordDtoIn);

	WebCreateListAccountDtoOut createListAccount(WebCreateListAccountDtoIn webCreateListAccountDtoIn);

	WebAccountDtoOut createAccountFromStranger(WebAccountFromStrangerDtoIn accountFromStrangerDtoIn);

	WebAccountDtoOut updateAccountFromStranger(String adminUsername, String adminRole, WebUpdateAccountFromStrangerDtoIn updateAccountFromStrangerDtoIn);

	WebAccountDtoOut register(WebRegisterDtoIn webRegisterDtoIn);

	void confirmRegister(WebConfirmRegisterDtoIn webConfirmRegisterDtoIn);

	WebAccountExistDtoOut checkAccountExist(String adminUsername, String role, String userCode);

	void updateSuperAdmin(String username, WebUpdateSuperAccountDtoIn webUpdateSuperAccountDtoIn);

	ListForgotPasswordDtoOut listAccountByUsernameOrEmail(String keyword);

	void lockAccount(LockAccountDtoIn lockAccountDtoIn);
}
