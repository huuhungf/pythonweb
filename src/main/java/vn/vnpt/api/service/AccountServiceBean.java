/*******************************************************************************
 * Copyright (c) 2017 ANHTCN.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package vn.vnpt.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.mapper.CompanyMapper;
import vn.vnpt.api.mapper.CompanyMapperImpl;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.auth.CreateConfirmationTokenRepository;
import vn.vnpt.api.repository.auth.CreateForgotConfirmationTokenRepository;
import vn.vnpt.api.repository.auth.GetConfirmationTokenRepository;
import vn.vnpt.api.repository.auth.GetForgotConfirmationTokenRepository;
import vn.vnpt.api.repository.channel.*;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.device.UpdateStatusDeviceTokenRepository;
import vn.vnpt.api.repository.greeting.GetGreetingRepository;
import vn.vnpt.api.repository.greeting.GetPostGreetingRepository;
import vn.vnpt.api.repository.group.*;
import vn.vnpt.api.repository.plan.GetAccountPlanRepository;
import vn.vnpt.api.repository.plan.GetPlanRepository;
import vn.vnpt.api.repository.stranger.DeleteStrangerRepository;
import vn.vnpt.api.repository.stranger.GetStrangerRepository;
import vn.vnpt.api.repository.user.*;
import vn.vnpt.authentication.MyEncryptPassword;
import vn.vnpt.authentication.WebLoginService;
import vn.vnpt.common.Common;
import vn.vnpt.common.ConstantsString;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.IncorrectPasswordException;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;
import vn.vnpt.context.DataContextHelper;
import vn.vnpt.embedding.EmbeddingService;
import vn.vnpt.policy.PolicyReviewerService;
import vn.vnpt.text2speech.AddTextDtoIn;
import vn.vnpt.text2speech.AudioDownloadMessage;
import vn.vnpt.text2speech.DownloadDtoOut;
import vn.vnpt.text2speech.Text2SpeechService;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author trananh
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AccountServiceBean implements AccountService {

	@Autowired
	private GetAccountByUsernameRepository accountByUsernameRepository;

	@Autowired
	private ListRoleByUuidAccountRepository listRoleByUuidAccountRepository;

	@Autowired
	private GetAccountRepository getAccountRepository;

	@Autowired
	private GetDeviceRepository getDeviceRepository;

	@Autowired
	private ChangeAccountPasswordRepository changeAccountPasswordRepository;

	@Autowired
	private DeleteAccountRepository deleteAccountRepository;

	@Autowired
	private WebLoginService webLoginService;

	@Autowired
	private UpdateStatusDeviceTokenRepository updateStatusDeviceTokenRepository;

	@Autowired
	private GetRoleByNameRepository roleByNameRepository;

	@Autowired
	private UploadService uploadService;

	@Value("${storage.bucket}")
	private String BUCKET_NAME;

	@Autowired
	private EmbeddingService embeddingService;

	@Autowired
	private GetAccountByUsernameRepository getAccountByUsernameRepository;

	@Autowired
	private GetCompanyRepository getCompanyRepository;

	@Autowired
	private CreateAccountChannelRepository createAccountChannelRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ListAccountChannelRepository listAccountChannelRepository;

	@Autowired
	private WebUpdateAccountChannelRepository webUpdateAccountChannelRepository;

	@Autowired
	private WebUpdateAllAccountChannelRepository webUpdateAllAccountChannelRepository;

	@Autowired
	private WebUpdateAccountRepository webUpdateAccountRepository;

	@Autowired
	private WebCreateAccountRepository webCreateAccountRepository;

	@Autowired
	private CreateForgotConfirmationTokenRepository createForgotConfirmationTokenRepository;

	@Autowired
	private GetForgotConfirmationTokenRepository getForgotConfirmationTokenRepository;

	@Autowired
	private NotifyService notifyService;

	@Autowired
	private ListAccountChannelActiveUserRepository listAccountChannelActiveUserRepository;

	@Autowired
	private GetStrangerRepository getStrangerRepository;

	@Autowired
	private DeleteStrangerRepository deleteStrangerRepository;

	@Autowired
	private Validator validator;

	@Autowired
	private CreateSupperAdminAccountRepository createSupperAdminAccountRepository;

	@Autowired
	private CreateConfirmationTokenRepository createConfirmationTokenRepository;

	@Autowired
	private GetConfirmationTokenRepository getConfirmationTokenRepository;

	@Autowired
	private EnableAccountRepository enableAccountRepository;

	@Autowired
	private Text2SpeechService text2SpeechService;

	@Autowired
	private GetCompanyByAccountRepository getCompanyByAccountRepository;

	@Autowired
	private UpdateSupperAdminAccountRepository updateSupperAdminAccountRepository;

	@Autowired
	private PolicyReviewerService policyReviewerService;

	@Autowired
	private GetAccountPlanRepository getAccountPlanRepository;

	@Autowired
	private GetGreetingRepository getGreetingRepository;

	@Autowired
	private GetPostGreetingRepository getPostGreetingRepository;

	@Autowired
	private WebListAccountByGroupRepository webListAccountByGroupRepository;

	@Autowired
	private ListAccountGroupRepository listAccountGroupRepository;

	@Autowired
	private GetGroupByCompanyRepository getGroupByCompanyRepository;

	@Autowired
	private GetGroupByDeviceRepository getGroupByDeviceRepository;

	@Autowired
	private ListGroupByAccountRepository listGroupByAccountRepository;

	@Autowired
	private WebUpdateAccountFromStrangerRepository webUpdateAccountFromStrangerRepository;

	@Autowired
	private CheckUsernameExistRepository checkUsernameExistRepository;

	@Autowired
	private CheckPermissionService checkPermissionService;

	@Autowired
	private WebListAccountByUsernameOrEmailRepository webListAccountByUsernameOrEmailRepository;

	@Autowired
	private GetPlanRepository getPlanRepository;

	@Autowired
	private LockAccountRepository lockAccountRepository;

	@Autowired
	private ListAdminGroupByAccountRepository listAdminGroupByAccountRepository;

	@Autowired
	private ListAdminGroupRepository listAdminGroupRepository;

	@Autowired
	private DataContextHelper dataContextHelper;

	private final MyEncryptPassword myEncryptPassword = new MyEncryptPassword();

	private static final Logger logger = LogManager.getLogger(AccountServiceBean.class);

	@Override
	public PagingDTO<WebAccountDtoOut> webListAccount(WebListAccountDtoIn webListAccountDtoIn,
	                                                  PagingDtoIn pagingDtoIn) {
		if (pagingDtoIn.getPage() < ConstantString.ONE) {
			pagingDtoIn.setPage(ConstantString.ONE);
		}
		if (pagingDtoIn.getMaxSize() < ConstantString.ONE || pagingDtoIn.getMaxSize() > ConstantString.MAX_PAGE_SIZE) {
			pagingDtoIn.setMaxSize(ConstantString.MAX_PAGE_SIZE);
		}

		String role = dataContextHelper.getRole();
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {

			String uuidGroup = webListAccountDtoIn.getUuidGroup();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				// get uuid group of company
				Company company = dataContextHelper.getCompany();
				Group group = getGroupByCompanyRepository.get(company.getUuidCompany());
				uuidGroup = group.getUuidGroup();
			}

			return webListAccountByGroupRepository.listAccount(
					uuidGroup, webListAccountDtoIn.getKeySearch(), webListAccountDtoIn.getImageMode(), pagingDtoIn,
					webListAccountDtoIn.getAdminGroup());
		} else if (ConstantString.ROLE.OWNER.equals(role)) {

			String uuidGroup = webListAccountDtoIn.getUuidGroup();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				// get uuid group of device
				Device device = dataContextHelper.getDevice();
				Group group = getGroupByDeviceRepository.get(device.getUuidDevice());
				uuidGroup = group.getUuidGroup();
			}
			return webListAccountByGroupRepository.listAccount(uuidGroup,
					webListAccountDtoIn.getKeySearch(),
					webListAccountDtoIn.getImageMode(), pagingDtoIn, webListAccountDtoIn.getAdminGroup());
		} else {
			Account user = dataContextHelper.getContextAccount();
			String uuidGroup = webListAccountDtoIn.getUuidGroup();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				List<AdminGroup> adminGroups = listAdminGroupRepository.list(user.getUuidAccount());
				uuidGroup = adminGroups.stream().map(AdminGroup::getUuidGroup).collect(Collectors.joining(","));
			}

			return webListAccountByGroupRepository.listAccount(uuidGroup,
					webListAccountDtoIn.getKeySearch(),
					webListAccountDtoIn.getImageMode(), pagingDtoIn, webListAccountDtoIn.getAdminGroup());
		}
	}

	@Override
	public AccountProfile getAccountProfile(String username, String role) {

		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			return getSuperAccountProfile(username);
		}

		return getNormalAccountProfile(username, role);
	}

	@Override
	public void resetPassword(ResetPasswordDtoIn resetPasswordDtoIn) {

		checkPermissionService.resetPasswordAccount(resetPasswordDtoIn.getUuidAccount());

		Account account = getAccountRepository.get(resetPasswordDtoIn.getUuidAccount());

		String newPassword = Common.generatePassword();
		if (!Common.isNullOrEmpty(account.getEmail())) {
			String token = UUID.randomUUID().toString();
			createForgotConfirmationTokenRepository.create(token, account.getUuidAccount());
			Map<String, String> properties = new HashMap<>();
			properties.put("fullName", Common.isNullOrEmpty(account.getFullName()) ? account.getUsername() : account.getFullName());
			properties.put("username", account.getUsername());
			properties.put("confirmationToken", token);
			notifyService.sendEmail(MessageNotification.builder()
					.destEmails(account.getEmail())
					.channelCode(ConstantsString.EMAIL_CHANNEL_CODE)
					.emailCategory(ConstantsString.EMAIL_CATEGORY_FORGOT_PASSWORD)
					.messageProperties(objectMapper, properties)
					.build());
		} else {
			changeAccountPasswordRepository.changePassword(account.getUuidAccount(), newPassword);
			notifyService.sendNotifyResetPasswordTele(account, newPassword);
		}
	}

	@Override
	public void changePassword(String username, ChangePasswordDtoIn changePasswordDtoIn) {

		Account account = accountByUsernameRepository.getAccountByUserName(username);
		if (account == null) {
			throw new NotFoundException("Not found account with username = " + username);
		}

		if (!myEncryptPassword.matches(changePasswordDtoIn.getOldPassword(), account.getPassword())) {
			throw new IncorrectPasswordException("oldPassword is incorrect");
		}

		changeAccountPasswordRepository.changePassword(account.getUuidAccount(), changePasswordDtoIn.getPassword());
		updateStatusDeviceTokenRepository.update(account.getUuidAccount(), Common.DEVICE_TOKEN_STATUS.LOCK);
	}

	@Override
	public void deleteAccount(String uuidAccount) {
		Account account = getAccountRepository.get(uuidAccount);

		List<Role> roles = listRoleByUuidAccountRepository.listRole(account.getUuidAccount());
		if (!roles.isEmpty() && ConstantString.ROLE.OWNER.equals(roles.get(0).getName())) {
			throw new AccessDeniedException("access denied");
		}

		if (!roles.isEmpty() && ConstantString.ROLE.SUPERADMIN.equals(roles.get(0).getName())) {
			throw new AccessDeniedException("access denied");
		}

		checkPermissionService.deleteAccount(uuidAccount);

		deleteAccountRepository.delete(account.getUuidAccount());
		updateStatusDeviceTokenRepository.update(account.getUuidAccount(), Common.DEVICE_TOKEN_STATUS.LOCK);

		notifyService.sendNotifyToTablet(account.getUuidAccount());
	}

	@Override
	public OAuth2AccessToken login(LoginDtoIn loginDtoIn) {
		return webLoginService.login(loginDtoIn.getUsername().toUpperCase(), loginDtoIn.getPassword());
	}

	@Override
	public WebAccountDtoOut createAccount(WebAccountDtoIn webAccountDtoIn) {

		checkPermissionService.createAccount(webAccountDtoIn.getUuidGroups());
		Account superAccount = dataContextHelper.getSuperAccount();
		Company company = dataContextHelper.getCompany();
		String listUuidGroup = (String) dataContextHelper.get("listUuidGroup");

		List<AccountChannelDtoIn> accountChannels = parseAccountChannels(webAccountDtoIn.getAccountChannels());

		policyReviewerService.reviewCreateAccount(superAccount, 1, accountChannels);

		String password = Common.generatePassword();
		String username = Common.getUsername(company.getCompanyCode(), webAccountDtoIn.getUserCode());

		Boolean isExist = checkUsernameExistRepository.isExist(username);
		if (isExist) {
			throw new BadRequestException("username already exist", "IDG-00000002");
		}

		String subFolder = company.getUuidCompany() + "/" + Common.getCurrentDay();

		String imageUrl = null;
		String imageEmbed = null;
		Integer imageType = Common.IMAGE_TYPE.UPLOAD;
		if (webAccountDtoIn.getImageFile() != null) {
			imageUrl = uploadImage(webAccountDtoIn.getImageFile(), subFolder);
			String hashFile = uploadService.extractHashFile(imageUrl);
			imageEmbed = embeddingService.getEmbedding(hashFile);
		}

		AddTextDtoIn addTextDtoIn = new AddTextDtoIn();
		addTextDtoIn.setText(Common.getText2Speech(webAccountDtoIn.getGender(), webAccountDtoIn.getFullName()));
		DownloadDtoOut downloadDtoOut = text2SpeechService.addTextAndDownload(addTextDtoIn);
		String audioUrl = null;
		if (downloadDtoOut != null) {
			audioUrl = uploadService.uploadAudio(downloadDtoOut.getContent(), webAccountDtoIn.getFullName(), BUCKET_NAME,
					subFolder, downloadDtoOut.getContentType());
		}

		Role role = roleByNameRepository.getRole(webAccountDtoIn.getRole().toUpperCase());

		AccountDtoIn accountDtoIn = new AccountDtoIn();
		accountDtoIn.setUsername(username);
		accountDtoIn.setFullName(webAccountDtoIn.getFullName());
		accountDtoIn.setUserCode(webAccountDtoIn.getUserCode().toUpperCase());
		accountDtoIn.setPassword(password);
		accountDtoIn.setImageEmbed(imageEmbed);
		accountDtoIn.setImageUrl(imageUrl);
		accountDtoIn.setRole(role);
		accountDtoIn.setEmail(webAccountDtoIn.getEmail());
		accountDtoIn.setGender(webAccountDtoIn.getGender());
		accountDtoIn.setAudioUrl(audioUrl);
		accountDtoIn.setImageType(imageType);
		accountDtoIn.setPhoneNumber(webAccountDtoIn.getPhoneNumber());

		Account account = webCreateAccountRepository.createAccount(accountDtoIn, listUuidGroup);

		// create channel ott
		for (AccountChannelDtoIn accountChannelDtoIn : accountChannels) {
			createAccountChannelRepository.createAccountChannel(
					account.getUuidAccount(),
					accountChannelDtoIn.getUuidChannelCategory(),
					accountChannelDtoIn.getUserOTT(),
					accountChannelDtoIn.getStatus()
			);
		}

		List<AccountChannel> channels = listAccountChannelRepository.list(account.getUuidAccount());

		WebAccountDtoOut accountDtoOut = new WebAccountDtoOut();

		accountDtoOut.setUuidAccount(account.getUuidAccount());
		accountDtoOut.setUsername(account.getUsername());
		accountDtoOut.setFullName(account.getFullName());
		accountDtoOut.setImageUrl(account.getImageUrl());
		accountDtoOut.setUserCode(account.getUserCode());
		accountDtoOut.setRole(role.getName());
		accountDtoOut.setEmail(account.getEmail());
		accountDtoOut.setGender(account.getGender());
		accountDtoOut.setAudioUrl(account.getAudioUrl());
		accountDtoOut.setChannels(channels.stream()
				.filter(ac -> ac.getStatus() == Common.ACCOUNT_CHANNEL_STATUS.ACTIVE)
				.map(AccountChannel::getChannelName).collect(Collectors.toList()));

		if (!Common.isNullOrEmpty(webAccountDtoIn.getEmail())) {
			// send email to user
			notifyService.sendEmail(account, password, company, superAccount, channels);
		} else {
			// TODO: Thêm phần gửi tài khoản qua sdt nếu có
			notifyService.sendNotifySignupTele(account, password);
		}

		// Send notification to tablet for sync
		List<DeviceLicense> deviceLicenses = notifyService.listNotifyDeviceLicense(account.getUuidAccount());
		notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_ACCOUNT, deviceLicenses);
		if (audioUrl == null) {
			retryDownloadAudio(account, addTextDtoIn, subFolder, null, deviceLicenses);
		}

		return accountDtoOut;
	}

	@Override
	public AccountProfileDtoOut getAccount(String adminUsername, String role, String uuidAccount) {
		Account account = getAccountRepository.get(uuidAccount);

		Account adminAccount = accountByUsernameRepository.getAccountByUserName(adminUsername);
		if (adminAccount == null) {
			throw new NotFoundException(String.format("account not found %s", adminUsername));
		}

		List<Role> roles = listRoleByUuidAccountRepository.listRole(account.getUuidAccount());
		String roleName = null;
		if (!roles.isEmpty()) {
			roleName = roles.get(0).getName();
		}
		List<ChannelOttDtoOut> accountChannels = listAccountChannelActiveUserRepository.list(account.getUuidAccount());

		// list groups of account
		List<Group> accountGroups = listGroupByAccountRepository.list(account.getUuidAccount());

		List<Group> adminGroups = listAdminGroupByAccountRepository.list(account.getUuidAccount());

		AccountProfileDtoOut profileDtoOut = new AccountProfileDtoOut();
		profileDtoOut.setUuidAccount(account.getUuidAccount());
		profileDtoOut.setFullName(account.getFullName());
		profileDtoOut.setUserCode(account.getUserCode());
		profileDtoOut.setUsername(account.getUsername());
		profileDtoOut.setRole(roleName);
		profileDtoOut.setEmail(account.getEmail());
		profileDtoOut.setImageUrl(account.getImageUrl());
		profileDtoOut.setGender(account.getGender());
		profileDtoOut.setPhoneNumber(account.getPhoneNumber());
		profileDtoOut.setAccountChannels(accountChannels);
		profileDtoOut.setGroups(accountGroups.stream().map(ag -> {
			GroupNode groupNode = new GroupNode();
			groupNode.setUuidGroup(ag.getUuidGroup());
			groupNode.setName(ag.getName());
			return groupNode;
		}).collect(Collectors.toList()));

		profileDtoOut.setAdminGroups(adminGroups.stream().map(ag -> {
			GroupNode groupNode = new GroupNode();
			groupNode.setUuidGroup(ag.getUuidGroup());
			groupNode.setName(ag.getName());
			return groupNode;
		}).collect(Collectors.toList()));

		return profileDtoOut;
	}

	@Override
	public WebAccountDtoOut updateAccount(WebUpdateAccountDtoIn webUpdateAccountDtoIn) {
		List<AccountChannelDtoIn> accountChannels = parseAccountChannels(webUpdateAccountDtoIn.getAccountChannels());

		checkPermissionService.updateAccount(webUpdateAccountDtoIn.getUuidAccount(),
				webUpdateAccountDtoIn.getUuidGroups(), true, webUpdateAccountDtoIn.getUuidAdminGroups());

		Account account = getAccountRepository.get(webUpdateAccountDtoIn.getUuidAccount());
		Company company = dataContextHelper.getCompany();
		Account superAccount = dataContextHelper.getSuperAccount();
		String listUuidGroup = (String) dataContextHelper.get("listUuidGroup");
		if (Common.isNullOrEmpty(webUpdateAccountDtoIn.getUuidGroups())) {
			listUuidGroup = "";
		}

		policyReviewerService.reviewUpdateAccount(superAccount, account, accountChannels);

		List<Role> roles = listRoleByUuidAccountRepository.listRole(account.getUuidAccount());
		if (!roles.isEmpty() && ConstantString.ROLE.OWNER.equals(roles.get(0).getName())) {
			throw new AccessDeniedException("access denied");
		}

		String subFolder = company.getUuidCompany() + "/" + Common.getCurrentDay();

		String imageUrl = null;
		String imageEmbed = null;
		Integer imageType = account.getImageType();
		if (webUpdateAccountDtoIn.getImageFile() != null) {
			imageUrl = uploadImage(webUpdateAccountDtoIn.getImageFile(), subFolder);
			String hashFile = uploadService.extractHashFile(imageUrl);
			imageEmbed = embeddingService.getEmbedding(hashFile);
			imageType = Common.IMAGE_TYPE.UPLOAD;
		}

		String uuidRole = null;
		if (webUpdateAccountDtoIn.getRole() != null) {
			Role role = roleByNameRepository.getRole(webUpdateAccountDtoIn.getRole().toUpperCase());
			uuidRole = role.getUuidRole();
		}

		String audioUrl = null;
		AddTextDtoIn addTextDtoIn = null;
		String oldAudioUrl = null;
		boolean isUpdateAudio = false;
		String gender = account.getGender();
		String fullName = account.getFullName();

		if (account.getUuidGreeting() != null) {
			isUpdateAudio = true;
		} else {
			if (webUpdateAccountDtoIn.getFullName() != null &&
					(!webUpdateAccountDtoIn.getFullName().equalsIgnoreCase(account.getFullName())
							|| account.getAudioUrl() == null)) {
				isUpdateAudio = true;
				fullName = webUpdateAccountDtoIn.getFullName();
			}
			if (webUpdateAccountDtoIn.getGender() != null &&
					!webUpdateAccountDtoIn.getGender().equalsIgnoreCase(account.getGender())) {
				isUpdateAudio = true;
				gender = webUpdateAccountDtoIn.getGender();
			}
		}

		if (isUpdateAudio) {
			oldAudioUrl = account.getAudioUrl();
			addTextDtoIn = new AddTextDtoIn();
			String postGreet = null;
			if (account.getUuidPostGreeting() != null) {
				Greeting greeting = getPostGreetingRepository.get(account.getUuidPostGreeting());
				postGreet = greeting.getText();
			}
			if (account.getUuidGreeting() != null) {
				Greeting greeting = getGreetingRepository.get(account.getUuidGreeting());
				String txt = greeting.getText() + " " + fullName;
				if (postGreet != null) {
					txt += " " + postGreet;
				}
				addTextDtoIn.setText(txt);
			} else {
				addTextDtoIn.setText(Common.getText2Speech(gender, fullName));
			}
			DownloadDtoOut downloadDtoOut = text2SpeechService.addTextAndDownload(addTextDtoIn);
			if (downloadDtoOut != null) {
				audioUrl = uploadService.uploadAudio(downloadDtoOut.getContent(), webUpdateAccountDtoIn.getFullName(),
						BUCKET_NAME, subFolder, downloadDtoOut.getContentType());

				// delete old audio file in minio
				if (account.getAudioUrl() != null) {
					uploadService.deleteFile(uploadService.extractHashFile(account.getAudioUrl()));
				}
			}
		}

		Account updateAccount = new Account();
		updateAccount.setUuidAccount(webUpdateAccountDtoIn.getUuidAccount());
		updateAccount.setFullName(webUpdateAccountDtoIn.getFullName());
		updateAccount.setImageUrl(imageUrl);
		updateAccount.setImageEmbed(imageEmbed);
		updateAccount.setUuidDevice(account.getUuidDevice());
		updateAccount.setUserCode(account.getUserCode());
		updateAccount.setUsername(account.getUsername());
		updateAccount.setEmail(webUpdateAccountDtoIn.getEmail());
		updateAccount.setGender(webUpdateAccountDtoIn.getGender());
		updateAccount.setAudioUrl(audioUrl);
		updateAccount.setImageType(imageType);
		updateAccount.setPhoneNumber(webUpdateAccountDtoIn.getPhoneNumber());

		account = webUpdateAccountRepository.update(updateAccount, uuidRole, listUuidGroup, webUpdateAccountDtoIn.getUuidAdminGroups());

		// update channel ott
		if (!accountChannels.isEmpty()) {
			webUpdateAllAccountChannelRepository.update(account.getUuidAccount(), Common.ACCOUNT_CHANNEL_STATUS.INACTIVE);

			for (AccountChannelDtoIn accountChannelDtoIn : accountChannels) {
				webUpdateAccountChannelRepository.update(
						account.getUuidAccount(),
						accountChannelDtoIn.getUuidChannelCategory(),
						accountChannelDtoIn.getStatus(),
						accountChannelDtoIn.getUserOTT()
				);
			}
		}

		roles = listRoleByUuidAccountRepository.listRole(account.getUuidAccount());

		List<AccountChannel> channels = listAccountChannelRepository.list(account.getUuidAccount());

		WebAccountDtoOut accountDtoOut = new WebAccountDtoOut();
		accountDtoOut.setUuidAccount(account.getUuidAccount());
		accountDtoOut.setFullName(account.getFullName());
		accountDtoOut.setUsername(account.getUsername());
		accountDtoOut.setUserCode(account.getUserCode());
		accountDtoOut.setImageUrl(account.getImageUrl());
		if (!roles.isEmpty()) {
			accountDtoOut.setRole(roles.get(0).getName());
		}
		accountDtoOut.setEmail(account.getEmail());
		accountDtoOut.setGender(account.getGender());
		accountDtoOut.setAudioUrl(account.getAudioUrl());
		accountDtoOut.setChannels(channels.stream()
				.filter(ac -> ac.getStatus() == Common.ACCOUNT_CHANNEL_STATUS.ACTIVE)
				.map(AccountChannel::getChannelName).collect(Collectors.toList()));

		// send notification to tablet for sync
		List<DeviceLicense> deviceLicenses = notifyService.listNotifyDeviceLicense(account.getUuidAccount());
		notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_ACCOUNT, deviceLicenses);
		if (audioUrl == null && addTextDtoIn != null) {
			retryDownloadAudio(account, addTextDtoIn, subFolder, oldAudioUrl, deviceLicenses);
		}

		return accountDtoOut;
	}

	@Override
	public void createForgotPassword(CreateForgotPasswordDtoIn createForgotPasswordDtoIn) {
		Account account = getAccountByUsernameRepository.getAccountByUserName(createForgotPasswordDtoIn.getUsername());
		if (!createForgotPasswordDtoIn.getEmail().equalsIgnoreCase(account.getEmail())) {
			throw new BadRequestException(String.format("email %s not match username %s", createForgotPasswordDtoIn.getEmail(), account.getUsername()));
		}

		String token = UUID.randomUUID().toString();
		createForgotConfirmationTokenRepository.create(token, account.getUuidAccount());

		Map<String, String> properties = new HashMap<>();
		properties.put("fullName", Common.isNullOrEmpty(account.getFullName()) ? account.getUsername() : account.getFullName());
		properties.put("username", account.getUsername());
		properties.put("confirmationToken", token);
		notifyService.sendEmail(MessageNotification.builder()
				.destEmails(account.getEmail())
				.channelCode(ConstantsString.EMAIL_CHANNEL_CODE)
				.emailCategory(ConstantsString.EMAIL_CATEGORY_FORGOT_PASSWORD)
				.messageProperties(objectMapper, properties)
				.build());
	}

	@Override
	public void confirmForgotPassword(ConfirmForgotPasswordDtoIn confirmForgotPasswordDtoIn) {
		Account account = getAccountByUsernameRepository.getAccountByUserName(confirmForgotPasswordDtoIn.getUsername());
		ForgotConfirmationToken confirmationToken = getForgotConfirmationTokenRepository.get(confirmForgotPasswordDtoIn.getToken());
		if (!confirmationToken.getUuidAccount().equals(account.getUuidAccount())) {
			throw new BadRequestException(String.format("confirm token not match account %s", confirmForgotPasswordDtoIn.getUsername()));
		}
		changeAccountPasswordRepository.changePassword(account.getUuidAccount(), confirmForgotPasswordDtoIn.getPassword());
		updateStatusDeviceTokenRepository.update(account.getUuidAccount(), Common.DEVICE_TOKEN_STATUS.LOCK);
	}

	@Override
	public WebCreateListAccountDtoOut createListAccount(WebCreateListAccountDtoIn webCreateListAccountDtoIn) {
		Account superAccount = dataContextHelper.getSuperAccount();
		policyReviewerService.reviewCreateListAccount(superAccount, webCreateListAccountDtoIn);
		WebCreateListAccountDtoOut listAccountDtoOut = new WebCreateListAccountDtoOut();
		List<WebElementAccountDtoIn> validAccounts = new ArrayList<>();
		for (WebElementAccountDtoIn accountDtoIn : webCreateListAccountDtoIn.getAccounts()) {
			Set<ConstraintViolation<WebElementAccountDtoIn>> violations = validator.validate(accountDtoIn);
			// if valid -> add to validAccounts
			if (violations.isEmpty()) {
				validAccounts.add(accountDtoIn);
				continue;
			}

			// in case invalid -> report errors
			WebErrorDtoOut webErrorDtoOut = reportError(accountDtoIn, violations);
			listAccountDtoOut.getErrors().add(webErrorDtoOut);
		}

		for (WebElementAccountDtoIn accountDtoIn : validAccounts) {
			WebAccountDtoIn webAccountDtoIn = new WebAccountDtoIn();
			webAccountDtoIn.setFullName(accountDtoIn.getFullName());
			webAccountDtoIn.setUserCode(accountDtoIn.getUserCode());
			webAccountDtoIn.setRole(accountDtoIn.getRole());
			webAccountDtoIn.setEmail(accountDtoIn.getEmail());
			webAccountDtoIn.setImageFile(null);
			webAccountDtoIn.setGender(accountDtoIn.getGender());
			webAccountDtoIn.setPhoneNumber(accountDtoIn.getPhoneNumber());
			String channels = null;
			try {
				channels = objectMapper.writeValueAsString(accountDtoIn.getAccountChannels());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				throw new RuntimeException("could not write json");
			}
			webAccountDtoIn.setAccountChannels(channels);
			webAccountDtoIn.setUuidGroups(accountDtoIn.getUuidGroups());
			try {
				createAccount(webAccountDtoIn);
			} catch (Exception e) {
				// report errors
				WebErrorDtoOut webErrorDtoOut = reportError(accountDtoIn, e);
				listAccountDtoOut.getErrors().add(webErrorDtoOut);
			}
			try {
				TimeUnit.MILLISECONDS.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return listAccountDtoOut;
	}

	@Override
	public WebAccountDtoOut createAccountFromStranger(WebAccountFromStrangerDtoIn accountFromStrangerDtoIn) {
		Stranger stranger = getStrangerRepository.get(accountFromStrangerDtoIn.getUuidStranger());
		Role role = roleByNameRepository.getRole(accountFromStrangerDtoIn.getRole().toUpperCase());

		String uuidGroups = null;
		if (accountFromStrangerDtoIn.getUuidGroups() != null && !accountFromStrangerDtoIn.getUuidGroups().isEmpty()) {
			uuidGroups = String.join(",", accountFromStrangerDtoIn.getUuidGroups());
		}

		checkPermissionService.createAccount(uuidGroups);
		Account superAccount = dataContextHelper.getSuperAccount();
		Company company = dataContextHelper.getCompany();
		String listUuidGroup = (String) dataContextHelper.get("listUuidGroup");

		policyReviewerService.reviewCreateAccount(superAccount, 1,
				accountFromStrangerDtoIn.getAccountChannels());

		String hashFile = uploadService.extractHashFile(stranger.getImageUrl());
		String imageEmbed = embeddingService.getEmbedding(hashFile);
		Integer imageType = Common.IMAGE_TYPE.TABLET;
		String imageUrl = null;
		try {
			imageUrl = uploadService.copyFile(hashFile, BUCKET_NAME);
		} catch (Exception e) {
			throw new BadRequestException("invalid image url");
		}

		String subFolder = company.getUuidCompany() + "/" + Common.getCurrentDay();

		String audioUrl = null;
		AddTextDtoIn addTextDtoIn = new AddTextDtoIn();
		addTextDtoIn.setText(Common.getText2Speech(accountFromStrangerDtoIn.getGender(), accountFromStrangerDtoIn.getFullName()));
		DownloadDtoOut downloadDtoOut = text2SpeechService.addTextAndDownload(addTextDtoIn);
		if (downloadDtoOut != null) {
			audioUrl = uploadService.uploadAudio(downloadDtoOut.getContent(), accountFromStrangerDtoIn.getFullName(),
					BUCKET_NAME, subFolder, downloadDtoOut.getContentType());
		}

		String password = Common.generatePassword();
		String username = Common.getUsername(company.getCompanyCode(), accountFromStrangerDtoIn
				.getUserCode().toUpperCase());
		Boolean isExist = checkUsernameExistRepository.isExist(username);
		if (isExist) {
			throw new BadRequestException("username already exist", "IDG-00000002");
		}

		AccountDtoIn accountDtoIn = new AccountDtoIn();
		accountDtoIn.setUsername(username);
		accountDtoIn.setFullName(accountFromStrangerDtoIn.getFullName());
		accountDtoIn.setUserCode(accountFromStrangerDtoIn.getUserCode().toUpperCase());
		accountDtoIn.setPassword(password);
		accountDtoIn.setImageEmbed(imageEmbed);
		accountDtoIn.setImageUrl(imageUrl);
		accountDtoIn.setRole(role);
		accountDtoIn.setEmail(accountFromStrangerDtoIn.getEmail());
		accountDtoIn.setGender(accountFromStrangerDtoIn.getGender());
		accountDtoIn.setAudioUrl(audioUrl);
		accountDtoIn.setImageType(imageType);
		accountDtoIn.setPhoneNumber(accountFromStrangerDtoIn.getPhoneNumber());

		Account account = webCreateAccountRepository.createAccount(accountDtoIn, listUuidGroup);

		// create channel ott
		for (AccountChannelDtoIn accountChannelDtoIn : accountFromStrangerDtoIn.getAccountChannels()) {
			createAccountChannelRepository.createAccountChannel(
					account.getUuidAccount(),
					accountChannelDtoIn.getUuidChannelCategory(),
					accountChannelDtoIn.getUserOTT(),
					accountChannelDtoIn.getStatus()
			);
		}

		// delete stranger
		deleteStrangerRepository.delete(stranger.getUuidStranger());

		// delete stranger image
		uploadService.deleteFile(hashFile);

		List<AccountChannel> channels = listAccountChannelRepository.list(account.getUuidAccount());

		WebAccountDtoOut accountDtoOut = new WebAccountDtoOut();

		accountDtoOut.setUuidAccount(account.getUuidAccount());
		accountDtoOut.setUsername(account.getUsername());
		accountDtoOut.setFullName(account.getFullName());
		accountDtoOut.setImageUrl(account.getImageUrl());
		accountDtoOut.setUserCode(account.getUserCode());
		accountDtoOut.setRole(role.getName());
		accountDtoOut.setEmail(account.getEmail());
		accountDtoOut.setGender(account.getGender());
		accountDtoOut.setAudioUrl(account.getAudioUrl());
		accountDtoOut.setChannels(channels.stream()
				.filter(ac -> ac.getStatus() == Common.ACCOUNT_CHANNEL_STATUS.ACTIVE)
				.map(AccountChannel::getChannelName).collect(Collectors.toList()));

		if (!Common.isNullOrEmpty(accountFromStrangerDtoIn.getEmail())) {
			// send email to user
			notifyService.sendEmail(account, password, company, superAccount, channels);
		} else {
			// TODO: Thêm phần gửi tài khoản qua sdt nếu có
			notifyService.sendNotifySignupTele(account, password);
		}
		// Send notification to tablet for sync
		List<DeviceLicense> deviceLicenses = notifyService.listNotifyDeviceLicense(account.getUuidAccount());
		notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_ACCOUNT, deviceLicenses);
		if (audioUrl == null) {
			retryDownloadAudio(account, addTextDtoIn, subFolder, null, deviceLicenses);
		}

		return accountDtoOut;
	}

	@Override
	public WebAccountDtoOut updateAccountFromStranger(String adminUsername, String adminRole, WebUpdateAccountFromStrangerDtoIn updateAccountFromStrangerDtoIn) {
		Stranger stranger = getStrangerRepository.get(updateAccountFromStrangerDtoIn.getUuidStranger());

		checkPermissionService.updateAccount(updateAccountFromStrangerDtoIn.getUuidAccount(),
				null, false, null);
		Account account = getAccountRepository.get(updateAccountFromStrangerDtoIn.getUuidAccount());

		List<Role> roles = listRoleByUuidAccountRepository.listRole(account.getUuidAccount());
		if (!roles.isEmpty() && ConstantString.ROLE.OWNER.equals(roles.get(0).getName())) {
			throw new AccessDeniedException("access denied");
		}

		String hashFile = uploadService.extractHashFile(stranger.getImageUrl());
		String imageEmbed = embeddingService.getEmbedding(hashFile);
		String imageUrl = null;
		try {
			imageUrl = uploadService.copyFile(hashFile, BUCKET_NAME);
		} catch (Exception e) {
			throw new BadRequestException("invalid image url");
		}

		account = webUpdateAccountFromStrangerRepository.update(account.getUuidAccount(), imageUrl,
				imageEmbed);

		// delete stranger
		deleteStrangerRepository.delete(stranger.getUuidStranger());

		// delete stranger image
		uploadService.deleteFile(hashFile);

		List<AccountChannel> channels = listAccountChannelRepository.list(account.getUuidAccount());

		WebAccountDtoOut accountDtoOut = new WebAccountDtoOut();
		accountDtoOut.setUuidAccount(account.getUuidAccount());
		accountDtoOut.setFullName(account.getFullName());
		accountDtoOut.setUsername(account.getUsername());
		accountDtoOut.setUserCode(account.getUserCode());
		accountDtoOut.setImageUrl(account.getImageUrl());
		if (!roles.isEmpty()) {
			accountDtoOut.setRole(roles.get(0).getName());
		}
		accountDtoOut.setEmail(account.getEmail());
		accountDtoOut.setGender(account.getGender());
		accountDtoOut.setChannels(channels.stream()
				.filter(ac -> ac.getStatus() == Common.ACCOUNT_CHANNEL_STATUS.ACTIVE)
				.map(AccountChannel::getChannelName).collect(Collectors.toList()));

		// send notification to tablet for sync
		notifyService.sendNotifyToTablet(account.getUuidAccount());

		return accountDtoOut;
	}

	@Override
	public WebAccountDtoOut register(WebRegisterDtoIn webRegisterDtoIn) {
		String subFolder = ConstantString.SUB_FOLDER_SUPER_ADMIN + "/" + Common.getCurrentDay();

		String imageUrl = null;
		if (webRegisterDtoIn.getImageFile() != null) {
			byte[] imageContents = null;
			try {
				imageContents = webRegisterDtoIn.getImageFile().getBytes();
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("could not get image contents");
			}

			imageUrl = uploadService.uploadImage(imageContents, webRegisterDtoIn.getImageFile().getName(), BUCKET_NAME, subFolder);
		}

		Account account = createSupperAdminAccountRepository.create(webRegisterDtoIn.getUsername(),
				null, null,
				webRegisterDtoIn.getPassword(), webRegisterDtoIn.getEmail(), webRegisterDtoIn.getPhoneNumber(),
				null, imageUrl, 0);

		// create confirmation token
		String token = Common.GenerateUUID();
		createConfirmationTokenRepository.create(token, account.getUuidAccount());

		// send email
		Map<String, String> properties = new HashMap<>();
		properties.put("confirmationToken", token);
		properties.put("email", account.getEmail());
		notifyService.sendEmail(MessageNotification.builder()
				.destEmails(account.getEmail())
				.channelCode(ConstantsString.EMAIL_CHANNEL_CODE)
				.emailCategory(ConstantsString.EMAIL_CATEGORY_REGISTER_ACCOUNT)
				.messageProperties(objectMapper, properties)
				.build());

		WebAccountDtoOut webAccountDtoOut = new WebAccountDtoOut();
		webAccountDtoOut.setUuidAccount(account.getUuidAccount());
		webAccountDtoOut.setFullName(account.getFullName());
		webAccountDtoOut.setUsername(account.getUsername());
		webAccountDtoOut.setEmail(account.getEmail());
		return webAccountDtoOut;
	}

	@Override
	public void confirmRegister(WebConfirmRegisterDtoIn webConfirmRegisterDtoIn) {
		ConfirmationToken ct = getConfirmationTokenRepository.get(webConfirmRegisterDtoIn.getConfirmationToken());
		Account account = getAccountRepository.get(ct.getUuidAccount());
		if (!account.getEmail().equalsIgnoreCase(webConfirmRegisterDtoIn.getEmail())) {
			throw new BadRequestException(String.format("confirmation token not match email %s", webConfirmRegisterDtoIn.getEmail()));
		}
		// enable account
		enableAccountRepository.enable(account.getUuidAccount());
	}

	@Override
	public WebAccountExistDtoOut checkAccountExist(String adminUsername, String role, String userCode) {
		Company company = dataContextHelper.getCompany();
		if (Common.isNullOrEmpty(company.getCompanyCode())) {
			throw new BadRequestException("company must have company code");
		}

		String username = Common.getUsername(company.getCompanyCode(), userCode);
		Boolean isExist = checkUsernameExistRepository.isExist(username);

		WebAccountExistDtoOut webAccountExistDtoOut = new WebAccountExistDtoOut();
		webAccountExistDtoOut.setIsExist(isExist);
		return webAccountExistDtoOut;
	}

	@Override
	public void updateSuperAdmin(String username, WebUpdateSuperAccountDtoIn webUpdateSuperAccountDtoIn) {

		String imageUrl = null;
		if (webUpdateSuperAccountDtoIn.getImageFile() != null) {
			String subFolder = ConstantString.SUB_FOLDER_SUPER_ADMIN + "/" + Common.getCurrentDay();
			imageUrl = uploadImage(webUpdateSuperAccountDtoIn.getImageFile(), subFolder);
		}

		updateSupperAdminAccountRepository.update(username,
				webUpdateSuperAccountDtoIn.getFullName(),
				webUpdateSuperAccountDtoIn.getCompany(),
				webUpdateSuperAccountDtoIn.getPhoneNumber(),
				webUpdateSuperAccountDtoIn.getCareer(),
				imageUrl,
				webUpdateSuperAccountDtoIn.getNotification()
		);
	}

	@Override
	public ListForgotPasswordDtoOut listAccountByUsernameOrEmail(String keyword) {
		List<Account> accounts = webListAccountByUsernameOrEmailRepository.list(keyword);

		List<AccountEmailUsernameDtoOut> data = accounts.stream().map(account -> {
			AccountEmailUsernameDtoOut dto = new AccountEmailUsernameDtoOut();
			dto.setEmail(account.getEmail());
			dto.setUsername(account.getUsername());
			return dto;
		}).collect(Collectors.toList());

		ListForgotPasswordDtoOut dtoOut = new ListForgotPasswordDtoOut();
		dtoOut.setData(data);
		return dtoOut;
	}

	@Override
	public void lockAccount(LockAccountDtoIn lockAccountDtoIn) {
		Account account = getAccountRepository.get(lockAccountDtoIn.getUuidAccount());

		checkPermissionService.lockAccount(lockAccountDtoIn.getUuidAccount());
		lockAccountRepository.lock(lockAccountDtoIn.getUuidAccount(), lockAccountDtoIn.getLocked());

		// send notification to tablet for sync
		List<DeviceLicense> deviceLicenses = notifyService.listNotifyDeviceLicense(account.getUuidAccount());
		notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_ACCOUNT, deviceLicenses);
	}

	private WebErrorDtoOut reportError(WebElementAccountDtoIn accountDtoIn, Set<ConstraintViolation<WebElementAccountDtoIn>> violations) {
		WebErrorDtoOut webErrorDtoOut = new WebErrorDtoOut();
		webErrorDtoOut.setUserCode(accountDtoIn.getUserCode());
		Map<String, List<String>> fieldMap = new HashMap<>();
		for (ConstraintViolation<WebElementAccountDtoIn> violation : violations) {
			String name = violation.getPropertyPath().toString();
			if (name.isEmpty()) {
				name = "userCode";
			}
			String errorCode = violation.getMessage();

			List<String> errorCodes = fieldMap.getOrDefault(name, new ArrayList<>());
			errorCodes.add(errorCode);
			fieldMap.put(name, errorCodes);
		}

		webErrorDtoOut.setFields(fieldMap.entrySet().stream().map(e -> {
			FieldError fe = new FieldError();
			fe.setName(e.getKey());
			fe.setErrorCodes(e.getValue());
			return fe;
		}).collect(Collectors.toList()));

		return webErrorDtoOut;
	}

	private WebErrorDtoOut reportError(WebElementAccountDtoIn accountDtoIn, Exception e) {
		WebErrorDtoOut webErrorDtoOut = new WebErrorDtoOut();
		webErrorDtoOut.setUserCode(accountDtoIn.getUserCode());

		String errorCode = ErrorCode.IDG_00000500;
		if (e instanceof AccessDeniedException) {
			errorCode = ErrorCode.IDG_00000403;
		} else if (e instanceof BadRequestException) {
			errorCode = ((BadRequestException) e).getErrorCode();
		}
		FieldError fieldError = new FieldError();
		fieldError.setName("");
		fieldError.setErrorCodes(Collections.singletonList(errorCode));

		webErrorDtoOut.setFields(Collections.singletonList(fieldError));
		return webErrorDtoOut;
	}

	private List<AccountChannelDtoIn> parseAccountChannels(String accountChannelsJson) {
		List<AccountChannelDtoIn> accountChannels = new ArrayList<>();

		if (accountChannelsJson != null && !accountChannelsJson.isEmpty()) {
			TypeReference<List<AccountChannelDtoIn>> typeReference = new TypeReference<List<AccountChannelDtoIn>>() {
			};
			try {
				accountChannels = objectMapper.readValue(accountChannelsJson,
						typeReference);
			} catch (IOException e) {
				e.printStackTrace();
				throw new BadRequestException("accountChannels invalid");
			}
		}

		for (AccountChannelDtoIn accDtoIn : accountChannels) {
			if (accDtoIn.getStatus() < 0 || accDtoIn.getStatus() > 2) {
				throw new BadRequestException("accountChannels invalid");
			}
			if (accDtoIn.getUserOTT() < 0) {
				throw new BadRequestException("accountChannels invalid");
			}
		}

		return accountChannels;
	}

	/**
	 * retry download audio in background
	 *
	 * @param account
	 * @param addTextDtoIn
	 * @param subFolder
	 * @param deviceLicenses
	 */
	private void retryDownloadAudio(Account account, AddTextDtoIn addTextDtoIn, String subFolder,
	                                String oldAudioUrl, List<DeviceLicense> deviceLicenses) {
		AudioDownloadMessage adm = new AudioDownloadMessage();
		adm.setUuidAccount(account.getUuidAccount());
		adm.setFullName(account.getFullName());
		adm.setText(addTextDtoIn.getText());

		adm.setBucketName(BUCKET_NAME);
		adm.setSubFolder(subFolder);

		adm.setOldAudioUrl(oldAudioUrl);

		adm.setDeviceLicenses(deviceLicenses);

		try {
			AudioDownloadScheduler.AUDIO_MESSAGE_QUEUE.add(adm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private SuperAccountProfileDtoOut getSuperAccountProfile(String username) {
		Account account = accountByUsernameRepository.getAccountByUserName(username);
		if (account == null) {
			throw new NotFoundException(String.format("account not found: %s", username));
		}

		Company company = getCompanyByAccountRepository.getCompany(account.getUuidAccount());

		CompanyMapper companyMapper = new CompanyMapperImpl();
		CompanyDtoOut companyDtoOut = companyMapper.map(company);

		AccountPlanDtoOut accountPlanDtoOut = getAccountPlan(account);

		SuperAccountProfileDtoOut profileDtoOut = new SuperAccountProfileDtoOut();
		profileDtoOut.setUuidAccount(account.getUuidAccount());
		profileDtoOut.setFullName(account.getFullName());
		profileDtoOut.setUsername(account.getUsername());
		profileDtoOut.setUserCode(account.getUserCode());
		profileDtoOut.setGender(account.getGender());
		profileDtoOut.setEmail(account.getEmail());
		profileDtoOut.setImageUrl(account.getImageUrl());
		profileDtoOut.setRole(ConstantString.ROLE.SUPERADMIN);
		profileDtoOut.setPhoneNumber(account.getPhoneNumber());
		profileDtoOut.setCompany(companyDtoOut);
		profileDtoOut.setAccountPlan(accountPlanDtoOut);

		return profileDtoOut;
	}

	private AccountProfileDtoOut getNormalAccountProfile(String username, String role) {
		Account account = accountByUsernameRepository.getAccountByUserName(username);
		if (account == null) {
			throw new NotFoundException(String.format("account not found: %s", username));
		}

		List<ChannelOttDtoOut> accountChannels = listAccountChannelActiveUserRepository.list(account.getUuidAccount());

		AccountProfileDtoOut profileDtoOut = new AccountProfileDtoOut();
		profileDtoOut.setUuidAccount(account.getUuidAccount());
		profileDtoOut.setFullName(account.getFullName());
		profileDtoOut.setUserCode(account.getUserCode());
		profileDtoOut.setUsername(account.getUsername());
		profileDtoOut.setRole(role);
		profileDtoOut.setEmail(account.getEmail());
		profileDtoOut.setImageUrl(account.getImageUrl());
		profileDtoOut.setGender(account.getGender());
		profileDtoOut.setPhoneNumber(account.getPhoneNumber());

		if (account.getUuidDevice() != null) {
			Device device = getDeviceRepository.getDevice(account.getUuidDevice());
			profileDtoOut.setUuidDevice(device.getUuidDevice());
			profileDtoOut.setDeviceCode(device.getDeviceCode());
			profileDtoOut.setDeviceName(device.getName());
		}

		profileDtoOut.setAccountChannels(accountChannels);

		if (ConstantString.ROLE.EXPORTER.equalsIgnoreCase(role)) {
			return profileDtoOut;
		}

		Company company = null;
		if (ConstantString.ROLE.OWNER.equals(role)) {
			Device device = getDeviceRepository.getDevice(account.getUuidDevice());
			company = getCompanyRepository.getCompany(device.getUuidCompany());
		} else {
			Group companyGroup = listGroupByAccountRepository.list(account.getUuidAccount()).stream()
					.filter(group -> group.getUuidCompany() != null)
					.findFirst()
					.get();
			company = getCompanyRepository.getCompany(companyGroup.getUuidCompany());

			List<Group> adminGroups = listAdminGroupByAccountRepository.list(account.getUuidAccount());

			profileDtoOut.setAdminGroups(adminGroups.stream().map(ag -> {
				GroupNode groupNode = new GroupNode();
				groupNode.setUuidGroup(ag.getUuidGroup());
				groupNode.setName(ag.getName());
				return groupNode;
			}).collect(Collectors.toList()));
		}

		Account superAccount = getAccountRepository.get(company.getOwnedAccount());
		AccountPlanDtoOut accountPlanDtoOut = getAccountPlan(superAccount);

		profileDtoOut.setAccountPlan(accountPlanDtoOut);

		return profileDtoOut;
	}

	private AccountPlanDtoOut getAccountPlan(Account superAccount) {
		AccountPlan accountPlan;
		try {
			accountPlan = getAccountPlanRepository.get(superAccount.getUsername());
		} catch (NotFoundException e) {
			return null;
		}

		Plan plan = getPlanRepository.get(accountPlan.getUuidPlan());

		AccountPlanDtoOut accountPlanDtoOut = new AccountPlanDtoOut();
		accountPlanDtoOut.setUuidAccountPlan(accountPlan.getUuidAccountPlan());
		accountPlanDtoOut.setUuidAccount(superAccount.getUuidAccount());
		accountPlanDtoOut.setUuidPlan(accountPlan.getUuidPlan());
		accountPlanDtoOut.setStatus(accountPlan.getStatus());
		accountPlanDtoOut.setActiveDate(Common.convertDateToString(accountPlan.getActiveDate(), ConstantString.DDMMYYYY));
		accountPlanDtoOut.setExpireDate(Common.convertDateToString(accountPlan.getExpireDate(), ConstantString.DDMMYYYY));
		accountPlanDtoOut.setRemainDay(Common.remainDays(Common.getCurrentTime(), accountPlan.getExpireDate()));
		accountPlanDtoOut.setUuidPlanVersion(accountPlan.getUuidPlanVersion());

		PlanDtoOut planDtoOut = new PlanDtoOut();
		planDtoOut.setUuidPlan(plan.getUuidPlan());
		planDtoOut.setName(plan.getName());
		planDtoOut.setDescription(plan.getDescription());
		planDtoOut.setType(plan.getType());

		accountPlanDtoOut.setPlan(planDtoOut);

		return accountPlanDtoOut;
	}

	private String uploadImage(MultipartFile imageFile, String subFolder) {
		try {
			byte[] imageContents = imageFile.getBytes();
			return uploadService.uploadImage(imageContents, imageFile.getName(), BUCKET_NAME, subFolder);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("could not get image contents");
		}
	}
}
