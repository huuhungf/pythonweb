package vn.vnpt.api.service;

import vn.vnpt.api.dto.out.AppVersionDtoOut;

import java.util.List;

public interface AppVersionService {
	List<AppVersionDtoOut> listLatest();
}
