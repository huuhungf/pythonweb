package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.out.AppVersionDtoOut;
import vn.vnpt.api.repository.app.GetAllAppVersionRepository;

import java.util.List;

@Service
public class AppVersionServiceBean implements AppVersionService {

	private final GetAllAppVersionRepository getAllAppVersionRepository;

	@Autowired
	public AppVersionServiceBean(GetAllAppVersionRepository getAllAppVersionRepository) {
		this.getAllAppVersionRepository = getAllAppVersionRepository;
	}

	@Override
	public List<AppVersionDtoOut> listLatest() {
		return getAllAppVersionRepository.getAll();
	}
}
