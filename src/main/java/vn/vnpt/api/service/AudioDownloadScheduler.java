package vn.vnpt.api.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import vn.vnpt.api.repository.user.WebUpdateAccountAudioUrlRepository;
import vn.vnpt.common.Common;
import vn.vnpt.text2speech.AddTextDtoIn;
import vn.vnpt.text2speech.AudioDownloadMessage;
import vn.vnpt.text2speech.DownloadDtoOut;
import vn.vnpt.text2speech.Text2SpeechService;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

@Service
public class AudioDownloadScheduler {
	private static final Logger logger = LogManager.getLogger(AudioDownloadScheduler.class);

	public static final BlockingQueue<AudioDownloadMessage> AUDIO_MESSAGE_QUEUE = new LinkedBlockingQueue<>();

	private final Text2SpeechService text2SpeechService;
	private final WebUpdateAccountAudioUrlRepository webUpdateAccountAudioUrlRepository;
	private final UploadService uploadService;
	private final NotifyService notifyService;

	@Autowired
	public AudioDownloadScheduler(Text2SpeechService text2SpeechService,
								  WebUpdateAccountAudioUrlRepository webUpdateAccountAudioUrlRepository, UploadService uploadService,
								  NotifyService notifyService) {
		this.text2SpeechService = text2SpeechService;
		this.webUpdateAccountAudioUrlRepository = webUpdateAccountAudioUrlRepository;
		this.uploadService = uploadService;
		this.notifyService = notifyService;
	}

	@Scheduled(fixedDelay = 200)
	public void download() {
		try {
			AudioDownloadMessage adm = AUDIO_MESSAGE_QUEUE.poll(100, TimeUnit.MILLISECONDS);
			if (adm == null) {
				return;
			}
			int retry = 0;
			AddTextDtoIn addTextDtoIn = new AddTextDtoIn();
			addTextDtoIn.setText(adm.getText());
			DownloadDtoOut downloadDtoOut = null;
			logger.info("AudioDownloadScheduler: -------START RETRY DOWNLOAD------: " + adm);
			while (downloadDtoOut == null && retry < 30) {
				retry++;
				TimeUnit.MILLISECONDS.sleep(300);
				try {
					downloadDtoOut = text2SpeechService.addTextAndDownload(addTextDtoIn);
					// update account and send notify
					if (downloadDtoOut != null) {
						String audioUrl = uploadService.uploadAudio(downloadDtoOut.getContent(), adm.getFullName(),
								adm.getBucketName(), adm.getSubFolder(), downloadDtoOut.getContentType());
						webUpdateAccountAudioUrlRepository.update(adm.getUuidAccount(), audioUrl);

						// delete old audio file in minio
						if (adm.getOldAudioUrl() != null) {
							uploadService.deleteFile(uploadService.extractHashFile(adm.getOldAudioUrl()));
						}

						// Send notification to tablet for sync
						notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_ACCOUNT, adm.getDeviceLicenses());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			logger.info(String.format("AudioDownloadScheduler: -------FINISH RETRY DOWNLOAD------: RETRY = %s, SUCCESS = %s, adm = %s",
					retry, downloadDtoOut != null, adm));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
