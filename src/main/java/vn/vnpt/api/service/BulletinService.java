package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.BulletinDtoIn;
import vn.vnpt.api.dto.out.BulletinDtoOut;

public interface BulletinService {
	void save(BulletinDtoIn bulletinDtoIn);

	BulletinDtoOut get();
}
