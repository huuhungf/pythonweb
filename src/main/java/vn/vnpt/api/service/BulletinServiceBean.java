package vn.vnpt.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.BulletinDtoIn;
import vn.vnpt.api.dto.out.BulletinDtoOut;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Bulletin;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.bulletin.BulletinRepository;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.device.ListDeviceLicenseBySuperAdminRepository;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.exception.BadRequestException;

import java.io.IOException;
import java.util.List;

@Service
public class BulletinServiceBean implements BulletinService {

	@Autowired
	private GetAccountByUsernameRepository getAccountByUsernameRepository;
	@Autowired
	private GetCompanyByAccountRepository getCompanyByAccountRepository;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private BulletinRepository bulletinRepository;
	@Autowired
	private NotifyService notifyService;
	@Autowired
	private ListDeviceLicenseBySuperAdminRepository listDeviceLicenseBySuperAdminRepository;

	@Override
	public void save(BulletinDtoIn bulletinDtoIn) {
		String username = getUsername();
		Account superAdmin = getAccountByUsernameRepository.getAccountByUserName(username);
		Company company = getCompanyByAccountRepository.getCompany(superAdmin.getUuidAccount());

		String pageInfo;
		try {
			pageInfo = objectMapper.writeValueAsString(bulletinDtoIn.getPageInfo());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new BadRequestException("invalid page info");
		}

		bulletinRepository.insert(company.getUuidCompany(), bulletinDtoIn.getStatus(), bulletinDtoIn.getStartDate(),
				bulletinDtoIn.getEndDate(), bulletinDtoIn.getStartHour(), bulletinDtoIn.getEndHour(),
				bulletinDtoIn.getTitle(), bulletinDtoIn.getContent(), pageInfo);

		// send firebase to tablet
		List<DeviceLicense> deviceLicenses = listDeviceLicenseBySuperAdminRepository.listDeviceLicense(username);
		notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_CONFIG, deviceLicenses);
	}

	@Override
	public BulletinDtoOut get() {
		String username = getUsername();
		Account superAdmin = getAccountByUsernameRepository.getAccountByUserName(username);
		Company company = getCompanyByAccountRepository.getCompany(superAdmin.getUuidAccount());

		List<Bulletin> bulletins = bulletinRepository.list(company.getUuidCompany());
		if (bulletins.isEmpty()) {
			return new BulletinDtoOut();
		}

		Bulletin bulletin = bulletins.get(0);

		JsonNode pageInfo = null;
		if (!Common.isNullOrEmpty(bulletin.getPageInfo())) {
			try {
				pageInfo = objectMapper.readValue(bulletin.getPageInfo(), JsonNode.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		BulletinDtoOut bulletinDtoOut = new BulletinDtoOut();
		bulletinDtoOut.setStatus(bulletin.getStatus());
		bulletinDtoOut.setStartDate(bulletin.getStartDate());
		bulletinDtoOut.setEndDate(bulletin.getEndDate());
		bulletinDtoOut.setStartHour(bulletin.getStartHour());
		bulletinDtoOut.setEndHour(bulletin.getEndHour());
		bulletinDtoOut.setTitle(bulletin.getTitle());
		bulletinDtoOut.setContent(bulletin.getContent());
		bulletinDtoOut.setPageInfo(pageInfo);

		return bulletinDtoOut;
	}

	private String getUsername() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}

}
