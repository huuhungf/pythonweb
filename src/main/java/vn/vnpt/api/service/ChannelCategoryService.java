package vn.vnpt.api.service;

import vn.vnpt.api.dto.out.ChannelCategoryDtoOut;

import java.util.List;

public interface ChannelCategoryService {
	List<ChannelCategoryDtoOut> listChannelCategory();
}
