package vn.vnpt.api.service;

import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.out.ChannelCategoryDtoOut;
import vn.vnpt.api.model.ChannelCategory;
import vn.vnpt.api.repository.channel.WebListChannelCategoryRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChannelCategoryServiceBean implements ChannelCategoryService {
	private final WebListChannelCategoryRepository webListChannelCategoryRepository;

	public ChannelCategoryServiceBean(WebListChannelCategoryRepository webListChannelCategoryRepository) {
		this.webListChannelCategoryRepository = webListChannelCategoryRepository;
	}

	@Override
	public List<ChannelCategoryDtoOut> listChannelCategory() {
		List<ChannelCategory> channelCategories = webListChannelCategoryRepository.list();
		return channelCategories.stream().map(channelCategory -> {
			ChannelCategoryDtoOut categoryDtoOut = new ChannelCategoryDtoOut();

			categoryDtoOut.setUuidChannelCategory(channelCategory.getUuidChannelCategory());
			categoryDtoOut.setName(channelCategory.getName());
			return categoryDtoOut;
		}).collect(Collectors.toList());
	}
}
