package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.ChatBotDtoIn;

public interface ChatBotService {
	void receive(ChatBotDtoIn chatBotDtoIn);
	void registerOtt(ChatBotDtoIn chatBotDtoIn);
	void unregisterOtt(ChatBotDtoIn ottUnregisterDtoIn);
}
