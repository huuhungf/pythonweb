package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.ChatBotDtoIn;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.api.repository.user.RegisterOttRepository;
import vn.vnpt.api.repository.user.UnregisterOttRepository;
import vn.vnpt.authentication.MyEncryptPassword;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.*;
import vn.vnpt.common.pattern.Patterns;
import vn.vnpt.ott.CardData;
import vn.vnpt.ott.OttNotifyDtoIn;
import vn.vnpt.ott.OttNotifyService;

import java.util.Collections;

@Service
public class ChatBotServiceBean implements ChatBotService {

	private final RegisterOttRepository registerOttRepository;

	private final UnregisterOttRepository unregisterOttRepository;

	private final OttNotifyService ottNotifyService;

	private final GetAccountByUsernameRepository getAccountByUsernameRepository;

	private final MyEncryptPassword myEncryptPassword = new MyEncryptPassword();

	@Autowired
	public ChatBotServiceBean(RegisterOttRepository registerOttRepository,
							  UnregisterOttRepository unregisterOttRepository,
							  OttNotifyService ottNotifyService,
							  GetAccountByUsernameRepository getAccountByUsernameRepository) {
		this.registerOttRepository = registerOttRepository;
		this.unregisterOttRepository = unregisterOttRepository;
		this.ottNotifyService = ottNotifyService;
		this.getAccountByUsernameRepository = getAccountByUsernameRepository;
	}

	@Override
	public void receive(ChatBotDtoIn chatBotDtoIn) {
		String text = chatBotDtoIn.getText().trim().toLowerCase();
		if (text.startsWith(ConstantString.OTT_SYNTAX.REGISTER)) {
			registerOtt(chatBotDtoIn);
			return;
		}
		if (text.startsWith(ConstantString.OTT_SYNTAX.UNREGISTER)) {
			unregisterOtt(chatBotDtoIn);
			return;
		}

		// send notify cu phap ko hop le
		CardData cardData = CardData.builder()
				.type(ConstantString.OTT_CARD_DATA_TYPE.TEXT)
				.text(ConstantString.OTT_MESSAGE.SYNTAX_INCORRECT)
				.build();

		OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
		ottNotifyDtoIn.setUserId(ottNotifyDtoIn.getUserId());
		ottNotifyDtoIn.setBotId(ottNotifyDtoIn.getBotId());
		ottNotifyDtoIn.setChannel(ottNotifyDtoIn.getChannel());
		ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
		ottNotifyService.sendNotify(ottNotifyDtoIn);
		throw new BadRequestException("Syntax is incorrect");
	}

	@Override
	public void registerOtt(ChatBotDtoIn chatBotDtoIn) {
		OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
		ottNotifyDtoIn.setUserId(chatBotDtoIn.getUserId());
		ottNotifyDtoIn.setBotId(chatBotDtoIn.getBotId());
		ottNotifyDtoIn.setChannel(chatBotDtoIn.getChannel());

		CardData.Builder cdBuilder = CardData.builder()
				.type(ConstantString.OTT_CARD_DATA_TYPE.TEXT);

		String text = chatBotDtoIn.getText().trim();

		if (!Common.isMatchPattern(text, Patterns.OTT_REGISTER_TEXT)) {
			// send notify cu phap ko hop le
			CardData cardData = cdBuilder.text(ConstantString.OTT_MESSAGE.REGISTER_SYNTAX_INCORRECT).build();

			ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
			ottNotifyService.sendNotify(ottNotifyDtoIn);
			throw new BadRequestException("Register syntax is incorrect");
		}

		String[] arr = text.split("\\s+");
		String username = arr[1].toUpperCase();
		String password = arr[2];

		try {
			Account account = getAccountByUsernameRepository.getAccountByUserName(username);

			if (!myEncryptPassword.matches(password, account.getPassword())) {
				throw new PasswordInvalidException("password invalid");
			}

			registerOttRepository.registerOtt(username,
					chatBotDtoIn.getChannel(),
					chatBotDtoIn.getBotId(),
					chatBotDtoIn.getUserId());
		} catch (NotFoundException e) {
			// send notify username khong ton tai
			CardData cardData = cdBuilder.text(String.format(ConstantString.OTT_MESSAGE.USERNAME_NOT_EXIST, username)).build();
			ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
			ottNotifyService.sendNotify(ottNotifyDtoIn);
			throw e;
		} catch (PasswordInvalidException e) {
			// send notify mat khau khong dung
			CardData cardData = cdBuilder.text(ConstantString.OTT_MESSAGE.PASSWORD_INCORRECT).build();
			ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
			ottNotifyService.sendNotify(ottNotifyDtoIn);
			throw e;
		} catch (AccountChannelInactiveException e) {
			// send notify kenh dang ky chua duoc kich hoat
			CardData cardData = cdBuilder.text(ConstantString.OTT_MESSAGE.ACCOUNT_CHANNEL_INACTIVE).build();
			ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
			ottNotifyService.sendNotify(ottNotifyDtoIn);
			throw e;
		} catch (ExceedMaxUserOttException e) {
			// send notify vuot qua so luong user dang ky
			CardData cardData = cdBuilder.text(ConstantString.OTT_MESSAGE.EXCEED_MAX_USER_OTT).build();
			ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
			ottNotifyService.sendNotify(ottNotifyDtoIn);
			throw e;
		}

		// send notify đăng ký thành công
		CardData cardData = cdBuilder.text(ConstantString.OTT_MESSAGE.REGISTER_SUCCESS).build();
		ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
		ottNotifyService.sendNotify(ottNotifyDtoIn);
	}

	@Override
	public void unregisterOtt(ChatBotDtoIn chatBotDtoIn) {
		OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
		ottNotifyDtoIn.setUserId(chatBotDtoIn.getUserId());
		ottNotifyDtoIn.setBotId(chatBotDtoIn.getBotId());
		ottNotifyDtoIn.setChannel(chatBotDtoIn.getChannel());

		CardData.Builder cdBuilder = CardData.builder().type(ConstantString.OTT_CARD_DATA_TYPE.TEXT);

		String text = chatBotDtoIn.getText().trim();
		if (!Common.isMatchPattern(text, Patterns.OTT_UNREGISTER_TEXT)) {
			// send notify cu phap ko hop le
			CardData cardData = cdBuilder.text(ConstantString.OTT_MESSAGE.UNREGISTER_SYNTAX_INCORRECT).build();

			ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
			ottNotifyService.sendNotify(ottNotifyDtoIn);
			throw new BadRequestException("Unregister syntax is incorrect");
		}

		String[] arr = text.split("\\s+");
		String username = arr[1].toUpperCase();

		try {
			unregisterOttRepository.unregisterOtt(username, chatBotDtoIn.getChannel(),
					chatBotDtoIn.getBotId(),
					chatBotDtoIn.getUserId());
		} catch (NotFoundException e) {
			// send notify username khong ton tai
			CardData cardData = cdBuilder.text(String.format(ConstantString.OTT_MESSAGE.USERNAME_NOT_EXIST, username)).build();
			ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
			ottNotifyService.sendNotify(ottNotifyDtoIn);
			throw e;
		} catch (UnregisteredUserOttException e) {
			// send notify chua dang ky
			CardData cardData = cdBuilder.text(ConstantString.OTT_MESSAGE.UNREGISTERED_ACCOUNT_OTT).build();
			ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
			ottNotifyService.sendNotify(ottNotifyDtoIn);
			return;
		}

		// send notify hủy đăng ký thành công
		CardData cardData = cdBuilder.text(ConstantString.OTT_MESSAGE.UNREGISTER_SUCCESS).build();
		ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));
		ottNotifyService.sendNotify(ottNotifyDtoIn);
	}
}
