package vn.vnpt.api.service;

public interface CheckPermissionService {

	void updateAccount(String uuidAccount, String uuidGroups, boolean isCheckGroup, String uuidAdminGroups);

	void createAccount(String uuidGroups);

	void deleteAccount(String uuidAccount);

	void lockAccount(String uuidAccount);

	void assignScheduleAccount(String uuidGroup);

	void resetPasswordAccount(String uuidAccount);

	void updateComplaint(String uuidAccount);

	void updateImgProfile(String uuidAccount);

	void checkPermissionOnAccount(String uuidAccount);
}
