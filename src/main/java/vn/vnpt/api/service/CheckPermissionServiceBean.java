package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.out.GroupNode;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.model.Group;
import vn.vnpt.api.repository.group.GetGroupByCompanyRepository;
import vn.vnpt.api.repository.group.GetGroupByDeviceRepository;
import vn.vnpt.api.repository.group.ListGroupByAccountRepository;
import vn.vnpt.api.repository.group.ListGroupTreeRepository;
import vn.vnpt.api.repository.user.GetAccountRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.context.DataContextHelper;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CheckPermissionServiceBean implements CheckPermissionService {

	@Autowired
	private GetGroupByCompanyRepository getGroupByCompanyRepository;

	@Autowired
	private GetGroupByDeviceRepository getGroupByDeviceRepository;

	@Autowired
	private ListGroupTreeRepository listGroupTreeRepository;

	@Autowired
	private GetAccountRepository getAccountRepository;

	@Autowired
	private ListGroupByAccountRepository listGroupByAccountRepository;

	@Autowired
	private DataContextHelper dataContextHelper;

	@Override
	public void updateAccount(String uuidAccount, String uuidGroups, boolean isCheckGroup, String uuidAdminGroups) {
		onAccount(uuidAccount);

		if (isCheckGroup) {
			String listUuidGroup = checkOnGroups(uuidGroups);
			dataContextHelper.put("listUuidGroup", listUuidGroup);
		}

		if (!Common.isNullOrEmpty(uuidAdminGroups)) {
			checkOnGroups(uuidAdminGroups);
		}
	}

	@Override
	public void createAccount(String uuidGroups) {
		Company company = dataContextHelper.getCompany();
		if (company.getCompanyCode() == null || company.getCompanyCode().isEmpty()) {
			throw new BadRequestException("company must have companyCode");
		}

		String listUuidGroup = checkOnGroups(uuidGroups);
		dataContextHelper.put("listUuidGroup", listUuidGroup);
	}

	@Override
	public void deleteAccount(String uuidAccount) {
		onAccount(uuidAccount);
	}

	@Override
	public void updateComplaint(String uuidAccount) {
		String role = dataContextHelper.getRole();
		if (ConstantString.ROLE.USER.equals(role)) {
			Account account = dataContextHelper.getContextAccount();
			checkPermissionGroupAdminOnAccount(account, uuidAccount);
			return;
		}

		onAccount(uuidAccount);
	}

	@Override
	public void updateImgProfile(String uuidAccount) {
		onAccount(uuidAccount);
	}

	public void checkPermissionOnAccount(String uuidAccount) {
		onAccount(uuidAccount);
	}

	public void assignScheduleAccount(String uuidGroup) {
		checkOnGroups(uuidGroup);
	}

	public void resetPasswordAccount(String uuidAccount) {
		onAccount(uuidAccount);
	}

	@Override
	public void lockAccount(String uuidAccount) {
		onAccount(uuidAccount);
	}

	private void onAccount(String uuidAccount) {
		String role = dataContextHelper.getRole();
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Company company = dataContextHelper.getCompany();
			checkPermissionSuperOnAccount(company, uuidAccount);
			return;
		}
		if (ConstantString.ROLE.OWNER.equals(role)) {
			Device device = dataContextHelper.getDevice();
			checkPermissionOwnerOnAccount(device, uuidAccount);
			return;
		}
		throw new AccessDeniedException("access denied");
	}

	private void checkPermissionSuperOnAccount(Company company, String uuidAccount) {
		Group groupCompany = listGroupByAccountRepository.list(uuidAccount)
				.stream().filter(g -> g.getUuidCompany() != null)
				.findFirst()
				.get();

		// neu account la cua company khac
		if (!groupCompany.getUuidCompany().equals(company.getUuidCompany())) {
			throw new AccessDeniedException("access denied");
		}
	}

	private void checkPermissionOwnerOnAccount(Device device, String uuidAccount) {
		List<String> uuidDevices = listGroupByAccountRepository.list(uuidAccount)
				.stream().filter(g -> g.getUuidDevice() != null)
				.map(Group::getUuidDevice)
				.collect(Collectors.toList());

		// neu la cua nhanh device khac
		if (!uuidDevices.contains(device.getUuidDevice())) {
			throw new AccessDeniedException("access denied");
		}
	}

	private String checkOnGroups(String uuidGroups) {
		String role = dataContextHelper.getRole();
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Company company = dataContextHelper.getCompany();
			return checkPermissionSuperOnGroup(company, uuidGroups);
		}
		if (ConstantString.ROLE.OWNER.equals(role)) {
			Device device = dataContextHelper.getDevice();
			return checkPermissionOwnerOnGroup(device, uuidGroups);
		}
		throw new AccessDeniedException("access denied");
	}

	private String checkPermissionSuperOnGroup(Company company, String uuidGroups) {
		Group group = getGroupByCompanyRepository.get(company.getUuidCompany());
		return checkPermissionOnGroup(group, uuidGroups);
	}

	private String checkPermissionOwnerOnGroup(Device device, String uuidGroups) {
		Group group = getGroupByDeviceRepository.get(device.getUuidDevice());
		return checkPermissionOnGroup(group, uuidGroups);
	}

	private String checkPermissionOnGroup(Group group, String uuidGroups) {
		if (uuidGroups != null && !uuidGroups.isEmpty()) {
			// check quyen tren group
			checkGroupIsParent(group, Arrays.asList(uuidGroups.split(",")));
			return uuidGroups;
		}
		return group.getUuidGroup();
	}

	private void checkGroupIsParent(Group group, List<String> uuidGroups) {
		// check quyen tren group
		List<GroupNode> groupNodes = listGroupTreeRepository.list(group.getUuidGroup());
		List<String> uuidGroupList = groupNodes.stream()
				.map(GroupNode::getUuidGroup)
				.collect(Collectors.toList());
		if (!uuidGroupList.containsAll(uuidGroups)) {
			throw new BadRequestException("group list invalid");
		}
	}

	private void checkPermissionGroupAdminOnAccount(Account groupAdminAccount, String uuidAccount) {
		Set<String> uuidAdminGroup = listGroupByAccountRepository.list(groupAdminAccount.getUuidAccount())
				.stream().map(Group::getUuidGroup).collect(Collectors.toSet());
		Account userAccount = getAccountRepository.get(uuidAccount);
		List<Group> userGroup = listGroupByAccountRepository.list(userAccount.getUuidAccount());

		for (Group ug : userGroup) {
			if (uuidAdminGroup.contains(ug.getUuidGroup())) {
				return;
			}
		}

		throw new AccessDeniedException("access denied");
	}
}
