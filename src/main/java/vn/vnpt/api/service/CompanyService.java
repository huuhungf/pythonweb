/*******************************************************************************
 * Copyright (c) 2017 ANHTCN.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.WebUpdateCompanyDtoIn;
import vn.vnpt.api.dto.in.WebUpdateLogoDtoIn;
import vn.vnpt.api.dto.out.CompanyDtoOut;
import vn.vnpt.api.dto.out.CompanyExistDtoOut;

/**
 * @author phuongha
 */
public interface CompanyService {
	void updateLogo(String username, WebUpdateLogoDtoIn webUpdateLogoDtoIn);
	CompanyDtoOut get(String uuidCompany);
	CompanyExistDtoOut checkExist(String companyCode);
	void update(String uuidCompany, WebUpdateCompanyDtoIn webUpdateCompanyDtoIn);
}
