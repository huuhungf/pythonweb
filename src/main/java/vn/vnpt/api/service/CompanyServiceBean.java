package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.WebUpdateCompanyDtoIn;
import vn.vnpt.api.dto.in.WebUpdateLogoDtoIn;
import vn.vnpt.api.dto.out.CompanyDtoOut;
import vn.vnpt.api.dto.out.CompanyExistDtoOut;
import vn.vnpt.api.mapper.CompanyMapper;
import vn.vnpt.api.mapper.CompanyMapperImpl;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.company.CheckCompanyCodeExistRepository;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.company.UpdateCompanyLogoRepository;
import vn.vnpt.api.repository.company.UpdateCompanyRepository;
import vn.vnpt.api.repository.device.ListDeviceLicenseBySuperAdminRepository;
import vn.vnpt.common.Common;

import java.util.List;

@Service
public class CompanyServiceBean implements CompanyService {

	@Autowired
	private UpdateCompanyLogoRepository updateCompanyLogoRepository;

	@Autowired
	private NotifyService notifyService;

	@Autowired
	private ListDeviceLicenseBySuperAdminRepository listDeviceLicenseBySuperAdminRepository;

	@Autowired
	private GetCompanyRepository getCompanyRepository;

	@Autowired
	private UpdateCompanyRepository updateCompanyRepository;

	@Autowired
	private CheckCompanyCodeExistRepository checkCompanyCodeExistRepository;

	@Override
	public void updateLogo(String username, WebUpdateLogoDtoIn webUpdateLogoDtoIn) {
		// update logoName
		updateCompanyLogoRepository.update(username, webUpdateLogoDtoIn.getLogoName());

		// send firebase to tablet
		List<DeviceLicense> deviceLicenses = listDeviceLicenseBySuperAdminRepository.listDeviceLicense(username);
		notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_CONFIG, deviceLicenses);
	}

	@Override
	public CompanyDtoOut get(String uuidCompany) {
		Company company = getCompanyRepository.getCompany(uuidCompany);

		CompanyMapper companyMapper = new CompanyMapperImpl();
		CompanyDtoOut companyDtoOut = companyMapper.map(company);
		return companyDtoOut;
	}

	@Override
	public CompanyExistDtoOut checkExist(String companyCode) {
		CompanyExistDtoOut companyExistDtoOut = new CompanyExistDtoOut();
		companyExistDtoOut.setIsExist(checkCompanyCodeExistRepository.isExist(companyCode));
		return companyExistDtoOut;
	}

	@Override
	public void update(String uuidCompany, WebUpdateCompanyDtoIn webUpdateCompanyDtoIn) {
		updateCompanyRepository.update(uuidCompany, webUpdateCompanyDtoIn.getName(), webUpdateCompanyDtoIn.getCompanyCode());
	}
}
