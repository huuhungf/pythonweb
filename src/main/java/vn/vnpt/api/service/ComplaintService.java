package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.ComplaintDtoIn;
import vn.vnpt.api.dto.in.ResolveComplaintDtoIn;
import vn.vnpt.api.dto.out.ComplaintDetailsDtoOut;
import vn.vnpt.api.dto.out.ComplaintDtoOut;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

public interface ComplaintService {

	PagingDTO<ComplaintDtoOut> getListComplaints(ComplaintDtoIn complaintDtoIn, PagingDtoIn pagingDtoIn);

	ComplaintDetailsDtoOut getComplaint(String uuidComplaint);

	void resolveComplaint(ResolveComplaintDtoIn resolverComplaint, String uuidComplaint);

	void updateImgProfile(String uuidComplaint);
}
