package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.ComplaintDtoIn;
import vn.vnpt.api.dto.in.ResolveComplaintDtoIn;
import vn.vnpt.api.dto.out.ComplaintDetailsDtoOut;
import vn.vnpt.api.dto.out.ComplaintDtoOut;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.complaint.ComplaintRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.group.GetGroupByCompanyRepository;
import vn.vnpt.api.repository.group.GetGroupByDeviceRepository;
import vn.vnpt.api.repository.group.ListAdminGroupRepository;
import vn.vnpt.api.repository.user.*;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;
import vn.vnpt.context.DataContextHelper;
import vn.vnpt.embedding.EmbeddingService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
public class ComplaintServiceImpl implements ComplaintService {

	@Autowired
	private ComplaintRepository complaintRepository;
	@Autowired
	private GetAccountByUsernameRepository getAccountByUsernameRepository;
	@Autowired
	private CheckPermissionService checkPermissionService;
	@Autowired
	private UploadService uploadService;
	@Autowired
	private EmbeddingService embeddingService;
	@Autowired
	private WebUpdateAccountFromStrangerRepository webUpdateAccountFromStrangerRepository;
	@Autowired
	private GetGroupByCompanyRepository getGroupByCompanyRepository;
	@Autowired
	private GetCompanyByAccountRepository getCompanyByAccountRepository;
	@Autowired
	private GetDeviceRepository getDeviceRepository;
	@Autowired
	private GetGroupByDeviceRepository getGroupByDeviceRepository;
	@Autowired
	private ListAdminGroupRepository listAdminGroupRepository;
	@Autowired
	private ListOttRepository listOttRepository;
	@Autowired
	private NotifyService notifyService;
	@Autowired
	private GetAccountRepository getAccountRepository;
	@Autowired
	private ListRoleByUuidAccountRepository listRoleByUuidAccountRepository;
	@Autowired
	private DataContextHelper dataContextHelper;

	@Value("${storage.bucket}")
	private String BUCKET_NAME;

	@Override
	public PagingDTO<ComplaintDtoOut> getListComplaints(ComplaintDtoIn complaintDtoIn, PagingDtoIn pagingDtoIn) {

		String role = dataContextHelper.getRole();
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			String uuidGroup = complaintDtoIn.getUuidGroup();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Company company = dataContextHelper.getCompany();
				Group group = getGroupByCompanyRepository.get(company.getUuidCompany());
				complaintDtoIn.setUuidGroup(group.getUuidGroup());
			}
			PagingDTO<Complaint> rs = complaintRepository.listComplaints(complaintDtoIn, pagingDtoIn);
			return buildPagingDtoOut(rs);
		}

		if (ConstantString.ROLE.OWNER.equals(role)) {
			if (Common.isNullOrEmpty(complaintDtoIn.getUuidGroup()) || ConstantString.GROUP_ALL.equals(complaintDtoIn.getUuidGroup())) {
				Device device = dataContextHelper.getDevice();
				Group group = getGroupByDeviceRepository.get(device.getUuidDevice());
				complaintDtoIn.setUuidGroup(group.getUuidGroup());
			}
			PagingDTO<Complaint> rs = complaintRepository.listComplaints(complaintDtoIn, pagingDtoIn);
			return buildPagingDtoOut(rs);
		}

		if (ConstantString.ROLE.USER.equals(role)) {
			if (Common.isNullOrEmpty(complaintDtoIn.getUuidGroup())) {
				Account account = dataContextHelper.getContextAccount();
				PagingDTO<Complaint> rs = complaintRepository.listComplaints(complaintDtoIn, account.getUuidAccount(), pagingDtoIn);
				return buildPagingDtoOut(rs);
			}
			if (ConstantString.GROUP_ALL.equals(complaintDtoIn.getUuidGroup())) {
				Account account = dataContextHelper.getContextAccount();
				List<AdminGroup> adminGroups = listAdminGroupRepository.list(account.getUuidAccount());
				complaintDtoIn.setUuidGroup(adminGroups.stream()
						.map(AdminGroup::getUuidGroup)
						.collect(Collectors.joining(",")));
			}
			PagingDTO<Complaint> rs = complaintRepository.listComplaints(complaintDtoIn, pagingDtoIn);
			return buildPagingDtoOut(rs);
		}
		return new PagingDTO<>();
	}

	private PagingDTO<ComplaintDtoOut> buildPagingDtoOut(PagingDTO<Complaint> pagingDTO) {
		List<ComplaintDtoOut> listComplaint = new ArrayList<>();
		for (Complaint complaint : pagingDTO.getData()) {
			ComplaintDtoOut complaintDtoOut = new ComplaintDtoOut(complaint);
			listComplaint.add(complaintDtoOut);
		}

		PagingDTO<ComplaintDtoOut> complaintPagingDTO = new PagingDTO<>();
		complaintPagingDTO.setPage(pagingDTO.getPage());
		complaintPagingDTO.setMaxSize(pagingDTO.getMaxSize());
		complaintPagingDTO.setTotalElement(pagingDTO.getTotalElement());
		complaintPagingDTO.setTotalPages(pagingDTO.getTotalPages());
		complaintPagingDTO.setSort(pagingDTO.getSort());
		complaintPagingDTO.setPropertiesSort(pagingDTO.getPropertiesSort());
		complaintPagingDTO.setData(listComplaint);
		return complaintPagingDTO;
	}

	@Override
	public ComplaintDetailsDtoOut getComplaint(String uuidComplaint) {
		ComplaintDetails complaintDetails = complaintRepository.get(uuidComplaint);

		return new ComplaintDetailsDtoOut(complaintDetails);
	}

	@Override
	public void resolveComplaint(ResolveComplaintDtoIn resolveComplaint, String uuidComplaint) {
		ComplaintDetails complaintDetails = complaintRepository.get(uuidComplaint);
		if (ConstantString.STATUS_COMPLAINT.PENDING != complaintDetails.getStatus()) {
			throw new BadRequestException("Complaint has been handled!");
		}

		checkPermissionService.updateComplaint(complaintDetails.getUuidComplainer());
		Account resolver = dataContextHelper.getContextAccount();
		complaintRepository.resolverComplaint(uuidComplaint, resolver.getUuidAccount(), resolveComplaint.getStatus());
		List<AccountOtt> accountOtts = listOttRepository.listOtt(complaintDetails.getUuidComplainer());

		String fullNameResolver = resolver.getFullName();
		Role accountRole = listRoleByUuidAccountRepository.listRole(resolver.getUuidAccount()).get(0);
		if (ConstantString.ROLE.SUPERADMIN.equals(accountRole.getName())
				|| ConstantString.ROLE.OWNER.equals(accountRole.getName())) {
			fullNameResolver = ConstantString.RESOLVER_ADMIN;
		}

		notifyService.sendNotifyComplaintToUserByOtt(complaintDetails.getFullNameComplainer(), fullNameResolver,
				complaintDetails.getImageUrlComplaint(), accountOtts, resolveComplaint.getStatus(),
				complaintDetails.getTransId());
	}

	@Override
	public void updateImgProfile(String uuidComplaint) {

		ComplaintDetails complaintDetails = complaintRepository.get(uuidComplaint);
		checkPermissionService.updateImgProfile(complaintDetails.getUuidComplainer());
		Account account = getAccountRepository.get(complaintDetails.getUuidComplainer());
		String oldImageUrl = account.getImageUrl();
		String hashFile = uploadService.extractHashFile(complaintDetails.getImageUrlComplaint());

		String imageEmbed = embeddingService.getEmbedding(hashFile);
		String imageUrl;
		try {
			imageUrl = uploadService.copyFile(hashFile, BUCKET_NAME);
		} catch (Exception e) {
			throw new BadRequestException("invalid image url");
		}

		account = webUpdateAccountFromStrangerRepository.update(account.getUuidAccount(), imageUrl,
				imageEmbed);

		if (!Common.isNullOrEmpty(oldImageUrl) && !oldImageUrl.equals(account.getImageUrl())) {
			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(() -> {
				String oldHashFile = uploadService.extractHashFile(oldImageUrl);
				uploadService.deleteFile(oldHashFile);
			});
			executorService.shutdown();
		}

		notifyService.sendNotifyToTablet(account.getUuidAccount());
	}

}
