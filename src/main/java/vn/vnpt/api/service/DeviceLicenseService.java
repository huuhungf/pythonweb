package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.DeviceLicenseDtoOut;
import vn.vnpt.api.dto.out.ListDeviceLicenseDtoOut;

public interface DeviceLicenseService {
	ListDeviceLicenseDtoOut listDeviceLicense(String uuidDevice);
	DeviceLicenseDtoOut createDeviceLicense(String superUsername, DeviceLicenseDtoIn deviceLicenseDtoIn);
	void deleteDeviceLicense(String role, String username, DeleteDeviceLicenseDtoIn deleteDeviceLicenseDtoIn);
	void activeDeviceLicense(String role, String username, ActiveDeviceLicenseDtoIn activeDeviceLicenseDtoIn);
	void lockDeviceLicense(String role, String superUsername, LockDeviceLicenseDtoIn lockDeviceLicenseDtoIn);
	DeviceLicenseDtoOut getDeviceLicense(GetDeviceLicenseDtoIn getDeviceLicenseDtoIn);
	DeviceLicenseDtoOut updateDeviceLicense(String role, String username, UpdateDeviceLicenseDtoIn updateDeviceLicenseDtoIn);
	void updateCollectImageStatus(String role, String username, UpdateDeviceLicenseCollectImageDtoIn updateCollectImageDtoIn);
}
