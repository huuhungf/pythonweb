package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.DeviceLicenseDtoOut;
import vn.vnpt.api.dto.out.ListDeviceLicenseDtoOut;
import vn.vnpt.api.mapper.DeviceLicenseMapper;
import vn.vnpt.api.mapper.DeviceLicenseMapperImpl;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.device.*;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.api.repository.user.GetAccountRepository;
import vn.vnpt.cache.HeartBeatCache;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.interceptor.CacheDeviceLicenseService;
import vn.vnpt.policy.PolicyReviewerService;

import java.util.*;

@Service
public class DeviceLicenseServiceBean implements DeviceLicenseService {

	private final ListDeviceLicenseFilterRepository listDeviceLicenseFilterRepository;
	private final DeleteDeviceLicenseRepository deleteDeviceLicenseRepository;
	private final CreateDeviceLicenseRepository createDeviceLicenseRepository;
	private final GetOwnerDeviceRepository getOwnerDeviceRepository;
	private final CacheDeviceLicenseService cacheDeviceLicenseService;
	private final SuperAdminUpdateStatusDeviceLicenseRepository superAdminUpdateStatusDeviceLicenseRepository;
	private final GetDeviceLicenseRepository getDeviceLicenseRepository;
	private final GetAccountByUsernameRepository accountByUsernameRepository;
	private final GetCompanyRepository getCompanyRepository;
	private final GetDeviceRepository getDeviceRepository;
	private final WebUpdateDeviceLicenseRepository webUpdateDeviceLicenseRepository;
	private final NotifyService notifyService;
	private final WebUpdateCollectImageDeviceLicenseRepository webUpdateCollectImageDeviceLicenseRepository;
	private final HeartbeatService heartbeatService;
	private final PolicyReviewerService policyReviewerService;
	private final GetAccountRepository getAccountRepository;
	private final UpdateStatusDeviceLicenseRepository updateStatusDeviceLicenseRepository;

	@Autowired
	public DeviceLicenseServiceBean(
			ListDeviceLicenseFilterRepository listDeviceLicenseFilterRepository,
			DeleteDeviceLicenseRepository deleteDeviceLicenseRepository,
			CreateDeviceLicenseRepository createDeviceLicenseRepository,
			GetOwnerDeviceRepository getOwnerDeviceRepository,
			CacheDeviceLicenseService cacheDeviceLicenseService,
			SuperAdminUpdateStatusDeviceLicenseRepository superAdminUpdateStatusDeviceLicenseRepository,
			GetDeviceLicenseRepository getDeviceLicenseRepository, GetAccountByUsernameRepository accountByUsernameRepository,
			GetCompanyRepository getCompanyRepository, GetDeviceRepository getDeviceRepository,
			WebUpdateDeviceLicenseRepository webUpdateDeviceLicenseRepository, NotifyService notifyService,
			WebUpdateCollectImageDeviceLicenseRepository webUpdateCollectImageDeviceLicenseRepository,
			HeartbeatService heartbeatService, PolicyReviewerService policyReviewerService,
			GetAccountRepository getAccountRepository,
			UpdateStatusDeviceLicenseRepository updateStatusDeviceLicenseRepository) {
		this.listDeviceLicenseFilterRepository = listDeviceLicenseFilterRepository;
		this.deleteDeviceLicenseRepository = deleteDeviceLicenseRepository;
		this.createDeviceLicenseRepository = createDeviceLicenseRepository;
		this.getOwnerDeviceRepository = getOwnerDeviceRepository;
		this.cacheDeviceLicenseService = cacheDeviceLicenseService;
		this.superAdminUpdateStatusDeviceLicenseRepository = superAdminUpdateStatusDeviceLicenseRepository;
		this.getDeviceLicenseRepository = getDeviceLicenseRepository;
		this.accountByUsernameRepository = accountByUsernameRepository;
		this.getCompanyRepository = getCompanyRepository;
		this.getDeviceRepository = getDeviceRepository;
		this.webUpdateDeviceLicenseRepository = webUpdateDeviceLicenseRepository;
		this.notifyService = notifyService;
		this.webUpdateCollectImageDeviceLicenseRepository = webUpdateCollectImageDeviceLicenseRepository;
		this.heartbeatService = heartbeatService;
		this.policyReviewerService = policyReviewerService;
		this.getAccountRepository = getAccountRepository;
		this.updateStatusDeviceLicenseRepository = updateStatusDeviceLicenseRepository;
	}

	@Override
	public ListDeviceLicenseDtoOut listDeviceLicense(String uuidDevice) {
		// list device license
		List<DeviceLicense> deviceLicenses = listDeviceLicenseFilterRepository.listDeviceLicense(uuidDevice);

		Account owner = getOwnerDeviceRepository.getOwner(uuidDevice);

		List<DeviceLicenseDtoOut> data = new ArrayList<>();
		for (DeviceLicense dl : deviceLicenses) {

			Map<String, Object> stats = getFromCache(dl, owner);

			Integer status = (Integer) stats.get("status");
			Integer battery = (Integer) stats.get("battery");
			String lastActive = (String) stats.get("lastActive");
			Float latitude = (Float) stats.get("latitude");
			Float longitude = (Float) stats.get("longitude");
			String wifi = (String) stats.get("wifi");
			String appVersion = (String) stats.get("appVersion");

			DeviceLicenseDtoOut deviceLicenseDtoOut = new DeviceLicenseDtoOut();
			deviceLicenseDtoOut.setUuidDevice(dl.getUuidDevice());
			deviceLicenseDtoOut.setSerialNumber(dl.getSerialNumber());
			deviceLicenseDtoOut.setName(dl.getName());
			deviceLicenseDtoOut.setLicenseKey(dl.getLicenseKey());
			deviceLicenseDtoOut.setStatus(status);
			deviceLicenseDtoOut.setCollectImage(dl.getCollectImage());
			deviceLicenseDtoOut.setLastActive(lastActive);
			deviceLicenseDtoOut.setBattery(battery);
			deviceLicenseDtoOut.setLatitude(latitude);
			deviceLicenseDtoOut.setLongitude(longitude);
			deviceLicenseDtoOut.setWifi(wifi);
			deviceLicenseDtoOut.setOs(dl.getOs());
			deviceLicenseDtoOut.setAppVersion(appVersion);

			data.add(deviceLicenseDtoOut);
		}
		ListDeviceLicenseDtoOut listDeviceLicenseDtoOut = new ListDeviceLicenseDtoOut();
		listDeviceLicenseDtoOut.setData(data);

		return listDeviceLicenseDtoOut;
	}

	@Override
	public DeviceLicenseDtoOut createDeviceLicense(String superUsername, DeviceLicenseDtoIn deviceLicenseDtoIn) {
		Account superAccount = accountByUsernameRepository.getAccountByUserName(superUsername);
		if (superAccount == null) {
			throw new NotFoundException(String.format("account not found: %s", superUsername));
		}

		policyReviewerService.reviewCreateDeviceLicense(superAccount, 1);

		// create device license
		DeviceLicense deviceLicense = createDeviceLicenseRepository.createDeviceLicense(superUsername, deviceLicenseDtoIn);

		DeviceLicenseMapper mapper = new DeviceLicenseMapperImpl();
		return mapper.map(deviceLicense);
	}

	@Override
	public void deleteDeviceLicense(String role, String username, DeleteDeviceLicenseDtoIn deleteDeviceLicenseDtoIn) {

		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			// delete device license
			deleteDeviceLicenseRepository.deleteDeviceLicense(username, deleteDeviceLicenseDtoIn.getUuidDevice(),
					deleteDeviceLicenseDtoIn.getSerialNumber());
		} else {
			Device device = getDeviceRepository.getDevice(deleteDeviceLicenseDtoIn.getUuidDevice());
			Company company = getCompanyRepository.getCompany(device.getUuidCompany());
			Account superAccount = getAccountRepository.get(company.getOwnedAccount());
			deleteDeviceLicenseRepository.deleteDeviceLicense(superAccount.getUsername(), deleteDeviceLicenseDtoIn.getUuidDevice(),
					deleteDeviceLicenseDtoIn.getSerialNumber());
		}

		// delete device license in cache
		Account owner = null;
		try {
			owner = getOwnerDeviceRepository.getOwner(deleteDeviceLicenseDtoIn.getUuidDevice());
		} catch (NotFoundException e) {
			return;
		}
		cacheDeviceLicenseService.deleteDeviceLicense(owner.getUsername(), deleteDeviceLicenseDtoIn.getSerialNumber());
	}

	@Override
	public void activeDeviceLicense(String role, String username, ActiveDeviceLicenseDtoIn activeDeviceLicenseDtoIn) {
		Account superAccount;
		Device device = getDeviceRepository.getDevice(activeDeviceLicenseDtoIn.getUuidDevice());
		Company company = getCompanyRepository.getCompany(device.getUuidCompany());
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			superAccount = accountByUsernameRepository.getAccountByUserName(username);
			if (superAccount == null) {
				throw new NotFoundException(String.format("account not found %s", username));
			}
			if (!company.getOwnedAccount().equals(superAccount.getUuidAccount())) {
				throw new AccessDeniedException("access denied");
			}
		} else {
			Account owner = accountByUsernameRepository.getAccountByUserName(username);
			if (!owner.getUuidDevice().equals(device.getUuidDevice())) {
				throw new AccessDeniedException("access denied");
			}
			superAccount = getAccountRepository.get(company.getOwnedAccount());
		}

		DeviceLicense dl = getDeviceLicenseRepository.get(activeDeviceLicenseDtoIn.getUuidDevice(), activeDeviceLicenseDtoIn.getSerialNumber());
		policyReviewerService.reviewActiveDeviceLicense(superAccount, dl);

		Account owner = getOwnerDeviceRepository.getOwner(activeDeviceLicenseDtoIn.getUuidDevice());

		superAdminUpdateStatusDeviceLicenseRepository.update(superAccount.getUsername(), activeDeviceLicenseDtoIn.getUuidDevice(),
				activeDeviceLicenseDtoIn.getSerialNumber(), Common.DEVICE_LICENSE_STATUS.PENDING);

		// delete device license in cache
		cacheDeviceLicenseService.deleteDeviceLicense(owner.getUsername(), activeDeviceLicenseDtoIn.getSerialNumber());
	}

	@Override
	public void lockDeviceLicense(String role, String username, LockDeviceLicenseDtoIn lockDeviceLicenseDtoIn) {
		Device device = getDeviceRepository.getDevice(lockDeviceLicenseDtoIn.getUuidDevice());
		Company company = getCompanyRepository.getCompany(device.getUuidCompany());
		Account superAccount;
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			superAccount = accountByUsernameRepository.getAccountByUserName(username);
			if (superAccount == null) {
				throw new NotFoundException(String.format("account not found %s", username));
			}
			if (!company.getOwnedAccount().equals(superAccount.getUuidAccount())) {
				throw new AccessDeniedException("access denied");
			}
		} else {
			Account owner = accountByUsernameRepository.getAccountByUserName(username);
			if (!owner.getUuidDevice().equals(device.getUuidDevice())) {
				throw new AccessDeniedException("access denied");
			}
			superAccount = getAccountRepository.get(company.getOwnedAccount());
		}
		superAdminUpdateStatusDeviceLicenseRepository.update(superAccount.getUsername(), lockDeviceLicenseDtoIn.getUuidDevice(),
				lockDeviceLicenseDtoIn.getSerialNumber(), Common.DEVICE_LICENSE_STATUS.LOCK);

		// delete device license in cache
		Account owner = null;
		try {
			owner = getOwnerDeviceRepository.getOwner(lockDeviceLicenseDtoIn.getUuidDevice());
		} catch (NotFoundException e) {
			return;
		}
		cacheDeviceLicenseService.deleteDeviceLicense(owner.getUsername(), lockDeviceLicenseDtoIn.getSerialNumber());
	}

	@Override
	public DeviceLicenseDtoOut getDeviceLicense(GetDeviceLicenseDtoIn getDeviceLicenseDtoIn) {
		DeviceLicense dl = getDeviceLicenseRepository.get(getDeviceLicenseDtoIn.getUuidDevice(),
				getDeviceLicenseDtoIn.getSerialNumber());

		Account owner = getOwnerDeviceRepository.getOwner(dl.getUuidDevice());

		Map<String, Object> stats = getFromCache(dl, owner);

		Integer status = (Integer) stats.get("status");
		Integer battery = (Integer) stats.get("battery");
		String lastActive = (String) stats.get("lastActive");
		Float latitude = (Float) stats.get("latitude");
		Float longitude = (Float) stats.get("longitude");
		String wifi = (String) stats.get("wifi");
		String appVersion = (String) stats.get("appVersion");

		DeviceLicenseDtoOut deviceLicenseDtoOut = new DeviceLicenseDtoOut();
		deviceLicenseDtoOut.setUuidDevice(dl.getUuidDevice());
		deviceLicenseDtoOut.setSerialNumber(dl.getSerialNumber());
		deviceLicenseDtoOut.setName(dl.getName());
		deviceLicenseDtoOut.setLicenseKey(dl.getLicenseKey());
		deviceLicenseDtoOut.setStatus(status);
		deviceLicenseDtoOut.setCollectImage(dl.getCollectImage());
		deviceLicenseDtoOut.setLastActive(lastActive);
		deviceLicenseDtoOut.setFaceProb(dl.getFaceProb());
		deviceLicenseDtoOut.setFaceMask(dl.getFaceMask());
		deviceLicenseDtoOut.setFaceLiveness(dl.getFaceLiveness());
		deviceLicenseDtoOut.setPinCode(dl.getPinCode());
		deviceLicenseDtoOut.setVirtualAssistant(dl.getVirtualAssistant());
		deviceLicenseDtoOut.setBattery(battery);
		deviceLicenseDtoOut.setLatitude(latitude);
		deviceLicenseDtoOut.setLongitude(longitude);
		deviceLicenseDtoOut.setWifi(wifi);
		deviceLicenseDtoOut.setOs(dl.getOs());
		deviceLicenseDtoOut.setAppVersion(appVersion);

		return deviceLicenseDtoOut;
	}

	@Override
	public DeviceLicenseDtoOut updateDeviceLicense(String role, String username, UpdateDeviceLicenseDtoIn updateDeviceLicenseDtoIn) {
		Device device = getDeviceRepository.getDevice(updateDeviceLicenseDtoIn.getUuidDevice());
		Company company = getCompanyRepository.getCompany(device.getUuidCompany());
		Account superAccount;

		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			superAccount = accountByUsernameRepository.getAccountByUserName(username);
			if (!company.getOwnedAccount().equals(superAccount.getUuidAccount())) {
				throw new AccessDeniedException("access denied");
			}
		} else {
			Account ownerAccount = accountByUsernameRepository.getAccountByUserName(username);
			if (!ownerAccount.getUuidDevice().equals(device.getUuidDevice())) {
				throw new AccessDeniedException("access denied");
			}
			superAccount = getAccountRepository.get(company.getOwnedAccount());
		}

		DeviceLicense dl = getDeviceLicenseRepository.get(updateDeviceLicenseDtoIn.getUuidDevice(), updateDeviceLicenseDtoIn.getSerialNumber());
		policyReviewerService.reviewUpdateDeviceLicense(superAccount, dl, updateDeviceLicenseDtoIn);

		dl = webUpdateDeviceLicenseRepository.update(updateDeviceLicenseDtoIn);

		DeviceLicenseMapper mapper = new DeviceLicenseMapperImpl();
		DeviceLicenseDtoOut deviceLicenseDtoOut = mapper.map(dl);

		// send notify to tablet for update configuration
		notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_CONFIG, Collections.singletonList(dl));

		return deviceLicenseDtoOut;
	}

	@Override
	public void updateCollectImageStatus(String role, String username, UpdateDeviceLicenseCollectImageDtoIn updateCollectImageDtoIn) {
		Device device = getDeviceRepository.getDevice(updateCollectImageDtoIn.getUuidDevice());
		Company company = getCompanyRepository.getCompany(device.getUuidCompany());
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Account superAccount = accountByUsernameRepository.getAccountByUserName(username);
			if (!company.getOwnedAccount().equals(superAccount.getUuidAccount())) {
				throw new AccessDeniedException("access denied");
			}
		} else {
			Account ownerAccount = accountByUsernameRepository.getAccountByUserName(username);
			if (!ownerAccount.getUuidDevice().equals(device.getUuidDevice())) {
				throw new AccessDeniedException("access denied");
			}
		}
		List<DeviceLicense> deviceLicenses = new ArrayList<>();

		for (DeviceLicenseCollectImage config : updateCollectImageDtoIn.getDeviceLicenseConfigs()) {
			//  updateCollectImageStatus
			DeviceLicense dl = webUpdateCollectImageDeviceLicenseRepository.update(device.getUuidDevice(), config.getSerialNumber(),
					config.getCollectImage());

			deviceLicenses.add(dl);
		}

		// send notify to tablet for update configuration
		notifyService.sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_CONFIG, deviceLicenses);
	}

	private Map<String, Object> getFromCache(DeviceLicense dl, Account owner) {
		Integer status = dl.getStatus();
		Integer battery = dl.getBattery();
		String lastActive = Common.convertDateToString(dl.getLastUpdate(), ConstantString.DDMMYYYYHHMM);
		Float latitude = dl.getLatitude();
		Float longitude = dl.getLongitude();
		String wifi = dl.getWifi();
		String appVersion = dl.getAppVersion();

		try {
			HeartBeatCache cache = heartbeatService.getHeartbeat(owner.getUsername(), dl.getSerialNumber());
			if (cache != null) {
				battery = cache.getBattery();
				lastActive = Common.convertDateToString(cache.getTime(), ConstantString.DDMMYYYYHHMM);
				latitude = cache.getLat();
				longitude = cache.getLon();
				wifi = cache.getWifi();
				appVersion = cache.getAppVersion();
			}

			if (status == Common.DEVICE_LICENSE_STATUS.ACTIVE && cache == null) {
				// if not in cache
				updateStatusDeviceLicenseRepository.update(owner.getUsername(), dl.getSerialNumber(), Common.DEVICE_LICENSE_STATUS.DISCONNECTED);
				status = Common.DEVICE_LICENSE_STATUS.DISCONNECTED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		Map<String, Object> res = new HashMap<>();
		res.put("status", status);
		res.put("battery", battery);
		res.put("lastActive", lastActive);
		res.put("latitude", latitude);
		res.put("longitude", longitude);
		res.put("wifi", wifi);
		res.put("appVersion", appVersion);

		return res;
	}
}
