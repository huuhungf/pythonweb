package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.DeviceDtoIn;
import vn.vnpt.api.dto.in.ResetPasswordDeviceDtoIn;
import vn.vnpt.api.dto.in.UpdateDeviceDtoIn;
import vn.vnpt.api.dto.out.DeviceDtoOut;
import vn.vnpt.api.dto.out.DeviceExistDtoOut;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

public interface DeviceService {
	DeviceDtoOut getDeviceOfOwner(String adminUsername);
	DeviceDtoOut createDevice(String superUsername, DeviceDtoIn deviceDtoIn);
	PagingDTO<DeviceDtoOut> listDevice(String superUsername, PagingDtoIn pagingDtoIn);
	void deleteDevice(String superUsername, String uuidDevice);
	void resetPassword(String superUsername, ResetPasswordDeviceDtoIn resetPasswordDeviceDtoIn);
	DeviceDtoOut updateDevice(String superUsername, String uuidDevice, UpdateDeviceDtoIn updateDeviceDtoIn);
	DeviceExistDtoOut checkDeviceExist(String deviceCode);
}
