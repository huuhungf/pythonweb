package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.DeviceDtoIn;
import vn.vnpt.api.dto.in.ResetPasswordDeviceDtoIn;
import vn.vnpt.api.dto.in.UpdateDeviceDtoIn;
import vn.vnpt.api.dto.out.DeviceDtoOut;
import vn.vnpt.api.dto.out.DeviceExistDtoOut;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.device.*;
import vn.vnpt.api.repository.group.GetGroupRepository;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.common.RsaUtil;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;
import vn.vnpt.interceptor.CacheDeviceLicenseService;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

@Service
public class DeviceServiceBean implements DeviceService {

	private final GetDeviceByAdminUsernameRepository deviceByAdminUsernameRepository;
	private final CreateDeviceRepository createDeviceRepository;
	private final ListDeviceRepository listDeviceRepository;
	private final DeleteDeviceRepository deleteDeviceRepository;
	private final ResetPasswordDeviceRepository resetPasswordDeviceRepository;
	private final ListDeviceLicenseRepository listDeviceLicenseRepository;
	private final CacheDeviceLicenseService cacheDeviceLicenseService;
	private final GetOwnerDeviceRepository getOwnerDeviceRepository;
	private final GetDeviceRepository getDeviceRepository;
	private final GetCompanyRepository getCompanyRepository;
	private final GetAccountByUsernameRepository getAccountByUsernameRepository;
	private final UpdateDeviceRepository updateDeviceRepository;
	private final GetDeviceByDeviceCodeRepository getDeviceByDeviceCodeRepository;
	private final GetTotalDeviceLicenseRepository getTotalDeviceLicenseRepository;
	private final GetGroupRepository getGroupRepository;

	@Autowired
	public DeviceServiceBean(
			GetDeviceByAdminUsernameRepository deviceByAdminUsernameRepository,
			CreateDeviceRepository createDeviceRepository, ListDeviceRepository listDeviceRepository,
			DeleteDeviceRepository deleteDeviceRepository, ResetPasswordDeviceRepository resetPasswordDeviceRepository,
			ListDeviceLicenseRepository listDeviceLicenseRepository,
			CacheDeviceLicenseService cacheDeviceLicenseService, GetOwnerDeviceRepository getOwnerDeviceRepository,
			GetDeviceRepository getDeviceRepository, GetCompanyRepository getCompanyRepository,
			GetAccountByUsernameRepository getAccountByUsernameRepository, UpdateDeviceRepository updateDeviceRepository,
			GetDeviceByDeviceCodeRepository getDeviceByDeviceCodeRepository,
			GetTotalDeviceLicenseRepository getTotalDeviceLicenseRepository,
			GetGroupRepository getGroupRepository) {
		this.deviceByAdminUsernameRepository = deviceByAdminUsernameRepository;
		this.createDeviceRepository = createDeviceRepository;
		this.listDeviceRepository = listDeviceRepository;
		this.deleteDeviceRepository = deleteDeviceRepository;
		this.resetPasswordDeviceRepository = resetPasswordDeviceRepository;
		this.listDeviceLicenseRepository = listDeviceLicenseRepository;
		this.cacheDeviceLicenseService = cacheDeviceLicenseService;
		this.getOwnerDeviceRepository = getOwnerDeviceRepository;
		this.getDeviceRepository = getDeviceRepository;
		this.getCompanyRepository = getCompanyRepository;
		this.getAccountByUsernameRepository = getAccountByUsernameRepository;
		this.updateDeviceRepository = updateDeviceRepository;
		this.getDeviceByDeviceCodeRepository = getDeviceByDeviceCodeRepository;
		this.getTotalDeviceLicenseRepository = getTotalDeviceLicenseRepository;
		this.getGroupRepository = getGroupRepository;
	}

	@Override
	public DeviceDtoOut getDeviceOfOwner(String adminUsername) {
		Device device = deviceByAdminUsernameRepository.get(adminUsername);

		DeviceDtoOut deviceDtoOut = new DeviceDtoOut();
		deviceDtoOut.setUuidDevice(device.getUuidDevice());
		deviceDtoOut.setDeviceCode(device.getDeviceCode());
		deviceDtoOut.setName(device.getName());
		deviceDtoOut.setType(device.getType());
		deviceDtoOut.setStatus(device.getStatus());

		Integer totalDeviceLicense = getTotalDeviceLicenseRepository.count(device.getUuidDevice());
		deviceDtoOut.setDeviceLicenseTotal(totalDeviceLicense);

		return deviceDtoOut;
	}

	@Override
	public DeviceDtoOut createDevice(String superUsername, DeviceDtoIn deviceDtoIn) {

		// generate private & public key
		String privateKeyStr;
		String publicKeyStr;
		try {
			KeyPair keyPair = RsaUtil.getKeyPair();
			PrivateKey privateKey = keyPair.getPrivate();
			PublicKey publicKey = keyPair.getPublic();

			privateKeyStr = RsaUtil.encodeKey(privateKey);
			assert privateKey.equals(RsaUtil.decodePrivateKey(privateKeyStr));

			publicKeyStr = RsaUtil.encodeKey(publicKey);
			assert publicKey.equals(RsaUtil.decodePrivateKey(publicKeyStr));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("could not generate key pair rsa");
		}

		if (deviceDtoIn.getUuidGroup() != null) {
			Group group = getGroupRepository.get(deviceDtoIn.getUuidGroup());
			if (group.getUuidDevice() != null) {
				throw new BadRequestException(String.format("group already assigned to device: %s",
						group.getUuidDevice()));
			}
		}

		// create device
		Device device = createDeviceRepository.createDevice(superUsername, deviceDtoIn, privateKeyStr, publicKeyStr);

		DeviceDtoOut deviceDtoOut = new DeviceDtoOut();
		deviceDtoOut.setUuidDevice(device.getUuidDevice());
		deviceDtoOut.setName(device.getName());
		deviceDtoOut.setDeviceCode(device.getDeviceCode());
		deviceDtoOut.setPublicKey(device.getPublicKey());

		return deviceDtoOut;
	}

	@Override
	public PagingDTO<DeviceDtoOut> listDevice(String superUsername, PagingDtoIn pagingDtoIn) {
		if (pagingDtoIn.getPage() < ConstantString.ONE) {
			pagingDtoIn.setPage(ConstantString.ONE);
		}
		// list device
		return listDeviceRepository.listDevice(superUsername, pagingDtoIn);
	}

	@Override
	public void deleteDevice(String superUsername, String uuidDevice) {
		// delete device
		deleteDeviceRepository.deleteDevice(superUsername, uuidDevice);
		// delete device licenses cache
		Account owner = null;
		try {
			owner = getOwnerDeviceRepository.getOwner(uuidDevice);
		} catch (Exception e) {
			return;
		}

		List<DeviceLicense> deviceLicenseList = listDeviceLicenseRepository.listDeviceLicense(uuidDevice);
		for (DeviceLicense dl : deviceLicenseList) {
			cacheDeviceLicenseService.deleteDeviceLicense(owner.getUsername(), dl.getSerialNumber());
		}
	}

	@Override
	public void resetPassword(String superUsername, ResetPasswordDeviceDtoIn resetPasswordDeviceDtoIn) {
		// reset password device
		resetPasswordDeviceRepository.resetPasswordDevice(superUsername, resetPasswordDeviceDtoIn);
	}

	@Override
	public DeviceDtoOut updateDevice(String superUsername, String uuidDevice, UpdateDeviceDtoIn updateDeviceDtoIn) {
		Device device = getDeviceRepository.getDevice(uuidDevice);
		Company company = getCompanyRepository.getCompany(device.getUuidCompany());
		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(superUsername);
		if (superAccount == null) {
			throw new NotFoundException(String.format("username not found %s", superUsername));
		}
		if (!superAccount.getUuidAccount().equals(company.getOwnedAccount())) {
			throw new AccessDeniedException("access denied");
		}

		if (updateDeviceDtoIn.getLimitDeviceLicense() != null) {
			Integer currentDeviceLicense = getTotalDeviceLicenseRepository.count(device.getUuidDevice());
			if (currentDeviceLicense > updateDeviceDtoIn.getLimitDeviceLicense()) {
				throw new BadRequestException("limitDeviceLicense less than num of current device license");
			}
		}

		device = updateDeviceRepository.updateDevice(uuidDevice, updateDeviceDtoIn.getName(),
				updateDeviceDtoIn.getLimitDeviceLicense());
		DeviceDtoOut deviceDtoOut = new DeviceDtoOut();
		deviceDtoOut.setUuidDevice(device.getUuidDevice());
		deviceDtoOut.setDeviceCode(device.getDeviceCode());
		deviceDtoOut.setName(device.getName());
		deviceDtoOut.setLimitDeviceLicense(device.getLimitDeviceLicense());
		return deviceDtoOut;
	}

	@Override
	public DeviceExistDtoOut checkDeviceExist(String deviceCode) {
		boolean isExist = true;
		try {
			getDeviceByDeviceCodeRepository.getDevice(deviceCode.toUpperCase());
		} catch (NotFoundException e) {
			isExist = false;
		}
		DeviceExistDtoOut deviceExistDtoOut = new DeviceExistDtoOut();
		deviceExistDtoOut.setIsExist(isExist);
		return deviceExistDtoOut;
	}
}
