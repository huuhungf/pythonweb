package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.ListOrderDtoIn;
import vn.vnpt.api.dto.out.OrderDtoOut;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

public interface ExportService {
	PagingDTO<OrderDtoOut> listOrders(PagingDtoIn pagingDtoIn, ListOrderDtoIn listOrderDtoIn);
}
