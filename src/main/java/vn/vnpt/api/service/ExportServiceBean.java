package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.ListOrderDtoIn;
import vn.vnpt.api.dto.out.OrderDtoOut;
import vn.vnpt.api.repository.order.WebGetReportListOrder;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

@Service
public class ExportServiceBean implements ExportService {

	private final WebGetReportListOrder webGetReportListOrder;

	@Autowired
	public ExportServiceBean(WebGetReportListOrder webGetReportListOrder) {
		this.webGetReportListOrder = webGetReportListOrder;
	}

	@Override
	public PagingDTO<OrderDtoOut> listOrders(PagingDtoIn pagingDtoIn, ListOrderDtoIn listOrderDtoIn) {
		return webGetReportListOrder.list(pagingDtoIn, listOrderDtoIn);
	}
}
