package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.ExternalAccountDtoOut;
import vn.vnpt.api.dto.out.ExternalNotifyDtoOut;
import vn.vnpt.api.dto.out.ExternalRegisterDtoOut;
import vn.vnpt.api.dto.out.HisCheckinExternalDtoOut;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;


public interface ExternalService {
    PagingDTO<HisCheckinExternalDtoOut> list(String username, ExternalListCheckinDtoIn listCheckinDtoIn, PagingDtoIn pagingDtoIn);

    void updateListUser(String adminUsername, ExternalUpdateAccountList updateAccountList);

    void deleteListUser(String adminUsername, ExternalDeleteAccountListDtoIn externalDeleteAccountListDtoIn);

    ExternalRegisterDtoOut registerOrExtendAccountPlan(ExternalRegisterDtoIn externalRegisterDtoIn);

    void updateAccountPlan(ExternalUpdateAccountPlanDtoIn externalUpdateAccountPlanDtoIn);

    void cancelAccountPlan(ExternalCancelAccountPlanDtoIn externalCancelAccountPlanDtoIn);

    PagingDTO<ExternalAccountDtoOut> listAccount(String username, ExternalListAccountDtoIn listAccountDtoIn, PagingDtoIn pagingDtoIn);

    ExternalNotifyDtoOut notifyAccount(String username, ExternalNotifyDtoIn notifyDtoIn);

    void createListUser(String adminUsername, ExternalCreateAccountList createAccountList);
}
