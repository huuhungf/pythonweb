package vn.vnpt.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.AccountOttUserCode;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.external.CreateUserExternalRepository;
import vn.vnpt.api.repository.external.DeleteUserExternalRepository;
import vn.vnpt.api.repository.external.GetHisCheckinExternalRepository;
import vn.vnpt.api.repository.external.UpdateUserExternalRepository;
import vn.vnpt.api.repository.plan.ExternalCancelPlanRepository;
import vn.vnpt.api.repository.plan.ExternalUpdatePlanRepository;
import vn.vnpt.api.repository.user.*;
import vn.vnpt.common.Common;
import vn.vnpt.common.ConstantsString;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;
import vn.vnpt.interceptor.CacheAccountPlanService;
import vn.vnpt.policy.PolicyReviewerService;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExternalServiceBean implements ExternalService {

	private final GetHisCheckinExternalRepository getHisCheckinExternalRepository;
	private final UpdateUserExternalRepository updateUserExternalRepository;
	private final GetAccountByUsernameRepository getAccountByUsernameRepository;
	private final DeleteUserExternalRepository deleteUserExternalRepository;
	private final GetCompanyByAccountRepository getCompanyByAccountRepository;
	private final CheckUsernameExistRepository checkUsernameExistRepository;
	private final PolicyReviewerService policyReviewerService;
	private final NotifyService notifyService;
	private final ObjectMapper objectMapper;
	private final ExternalCreateSuperAdminAccountRepository externalCreateSuperAdminAccountRepository;
	private final ExternalCancelPlanRepository externalCancelPlanRepository;
	private final ExternalUpdatePlanRepository externalUpdatePlanRepository;
	private final CacheAccountPlanService cacheAccountPlanService;
	private final ExternalGetAccountRepository externalGetAccountRepository;
	private final GetAccountRepository getAccountRepository;
	private final ExternalListAccountRepository externalListAccountRepository;
	private final ListOttByUserCodeRepository listOttByUserCodeRepository;
	private final CreateUserExternalRepository createUserExternalRepository;

	@Autowired
	public ExternalServiceBean(GetHisCheckinExternalRepository getHisCheckinExternalRepository,
							   UpdateUserExternalRepository updateUserExternalRepository,
							   GetAccountByUsernameRepository getAccountByUsernameRepository,
							   DeleteUserExternalRepository deleteUserExternalRepository,
							   GetCompanyByAccountRepository getCompanyByAccountRepository,
							   CheckUsernameExistRepository checkUsernameExistRepository,
							   PolicyReviewerService policyReviewerService,
							   NotifyService notifyService,
							   ObjectMapper objectMapper,
							   ExternalCreateSuperAdminAccountRepository externalCreateSuperAdminAccountRepository,
							   ExternalCancelPlanRepository externalCancelPlanRepository,
							   ExternalUpdatePlanRepository externalUpdatePlanRepository,
							   CacheAccountPlanService cacheAccountPlanService,
							   ExternalGetAccountRepository externalGetAccountRepository,
							   GetAccountRepository getAccountRepository,
							   ExternalListAccountRepository externalListAccountRepository,
							   ListOttByUserCodeRepository listOttByUserCodeRepository,
							   CreateUserExternalRepository createUserExternalRepository) {
		super();
		this.getHisCheckinExternalRepository = getHisCheckinExternalRepository;
		this.updateUserExternalRepository = updateUserExternalRepository;
		this.getAccountByUsernameRepository = getAccountByUsernameRepository;
		this.deleteUserExternalRepository = deleteUserExternalRepository;
		this.getCompanyByAccountRepository = getCompanyByAccountRepository;
		this.checkUsernameExistRepository = checkUsernameExistRepository;
		this.policyReviewerService = policyReviewerService;
		this.notifyService = notifyService;
		this.objectMapper = objectMapper;
		this.externalCreateSuperAdminAccountRepository = externalCreateSuperAdminAccountRepository;
		this.externalCancelPlanRepository = externalCancelPlanRepository;
		this.externalUpdatePlanRepository = externalUpdatePlanRepository;
		this.cacheAccountPlanService = cacheAccountPlanService;
		this.externalGetAccountRepository = externalGetAccountRepository;
		this.getAccountRepository = getAccountRepository;
		this.externalListAccountRepository = externalListAccountRepository;
		this.listOttByUserCodeRepository = listOttByUserCodeRepository;
		this.createUserExternalRepository = createUserExternalRepository;
	}

	@Override
	public PagingDTO<HisCheckinExternalDtoOut> list(String username, ExternalListCheckinDtoIn listCheckinDtoIn, PagingDtoIn pagingDtoIn) {
		return getHisCheckinExternalRepository.list(username, listCheckinDtoIn, pagingDtoIn);
	}

	@Override
	public void updateListUser(String adminUsername, ExternalUpdateAccountList updateAccountList) {
		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(adminUsername);
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		if (Common.isNullOrEmpty(company.getCompanyCode())) {
			throw new BadRequestException("company have no company code");
		}
		for (ExternalUpdateAccountDtoIn updateAccountDtoIn : updateAccountList.getExternalAccounts()) {
			String username = Common.getUsername(company.getCompanyCode(), updateAccountDtoIn.getUserCode());
			Boolean isExist = checkUsernameExistRepository.isExist(username);

			if (!isExist) {
				continue;
			}

			String newUserCode = updateAccountDtoIn.getNewUserCode();
			if (Common.isNullOrEmpty(newUserCode)) {
				newUserCode = updateAccountDtoIn.getUserCode();
			} else {
				String newUsername = Common.getUsername(company.getCompanyCode(), newUserCode);
				Boolean isNewUserCodeExist = checkUsernameExistRepository.isExist(newUsername);
				// Neu ma moi da ton tai va khac ma cu thi khong xu ly
				if (isNewUserCodeExist && !newUserCode.equals(updateAccountDtoIn.getUserCode())) {
					continue;
				}
			}

			updateAccountDtoIn.setNewUserCode(newUserCode);
			updateUserExternalRepository.update(adminUsername, updateAccountDtoIn);

			if (!newUserCode.equals(updateAccountDtoIn.getUserCode())) {
				// send email notify to user in case newUserCode != userCode
				String newUsername = Common.getUsername(company.getCompanyCode(), newUserCode);
				Account account = getAccountByUsernameRepository.getAccountByUserName(newUsername);
				Map<String, String> properties = new HashMap<>();
				properties.put("oldUsername", username);
				properties.put("newUsername", newUsername);
				notifyService.sendEmail(MessageNotification.builder()
						.destEmails(account.getEmail())
						.channelCode(ConstantsString.EMAIL_CHANNEL_CODE)
						.emailCategory(ConstantsString.EMAIL_CATEGORY_CHANGE_USERNAME)
						.messageProperties(objectMapper, properties)
						.build());
			}
		}
	}

	@Override
	public void deleteListUser(String adminUsername, ExternalDeleteAccountListDtoIn externalDeleteAccountListDtoIn) {
		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(adminUsername);
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		if (Common.isNullOrEmpty(company.getCompanyCode())) {
			throw new BadRequestException("company have no company code");
		}
		for (String userCode : externalDeleteAccountListDtoIn.getUserCodes()) {
			deleteUserExternalRepository.delete(adminUsername, userCode);
			String username = Common.getUsername(company.getCompanyCode(), userCode);
			Account account = getAccountByUsernameRepository.getAccountByUserName(username);
			notifyService.sendNotifyToTablet(account.getUuidAccount());
		}
	}

	@Override
	public ExternalRegisterDtoOut registerOrExtendAccountPlan(ExternalRegisterDtoIn externalRegisterDtoIn) {
		// get numAccount, numDevice, numOtt
		int numAccount = 0;
		int numDevice = 0;
		int numOtt = 0;
		for (AttributeDtoIn attr : externalRegisterDtoIn.getAttr()) {
			if (ConstantString.ATTR_UNIT.USER.equalsIgnoreCase(attr.getUnit())) {
				numAccount = attr.getQuantity();
			} else if (ConstantString.ATTR_UNIT.DEVICE.equalsIgnoreCase(attr.getUnit())) {
				numDevice = attr.getQuantity();
			}
		}

		Account superAccount = null;
		boolean isExist = true;
		try {
			superAccount = externalGetAccountRepository.get(externalRegisterDtoIn.getExternalCustomerId());

			// validate current stats
			policyReviewerService.reviewExternalRegisterOrExtendAccountPlan(superAccount, numAccount, numDevice, numOtt);
		} catch (NotFoundException e) {
			isExist = false;
		}

		String rawPassword = Common.generatePassword();

		ExternalRegisterDtoOut externalRegisterDtoOut = externalCreateSuperAdminAccountRepository.create(
				externalRegisterDtoIn.getExternalCustomerId(), externalRegisterDtoIn.getEmail(), externalRegisterDtoIn.getPhoneNumber(),
				rawPassword, externalRegisterDtoIn.getCreatedBy(), externalRegisterDtoIn.getPackageId(),
				numAccount, numDevice, numOtt, externalRegisterDtoIn.getActiveDate(), externalRegisterDtoIn.getExpireDate());

		if (!isExist) {
			// if account not exist
			superAccount = getAccountRepository.get(externalRegisterDtoOut.getUuidAccount());

			// send email
			Map<String, String> properties = new HashMap<>();
			properties.put("username", superAccount.getUsername());
			properties.put("password", rawPassword);
			notifyService.sendEmail(MessageNotification.builder()
					.destEmails(superAccount.getEmail())
					.channelCode(ConstantsString.EMAIL_CHANNEL_CODE)
					.emailCategory(ConstantsString.EMAIL_CATEGORY_REGISTER_ACCOUNT_EXTERNAL)
					.messageProperties(objectMapper, properties)
					.build());
		}

		// clear account plan cache
		cacheAccountPlanService.deleteAccountPlan(superAccount.getUuidAccount());

		return externalRegisterDtoOut;
	}

	@Override
	public void updateAccountPlan(ExternalUpdateAccountPlanDtoIn externalUpdateAccountPlanDtoIn) {
		// get numAccount, numDevice, numOtt
		int numAccount = 0;
		int numDevice = 0;
		int numOtt = 0;
		for (AttributeDtoIn attr : externalUpdateAccountPlanDtoIn.getAttr()) {
			if (ConstantString.ATTR_UNIT.USER.equalsIgnoreCase(attr.getUnit())) {
				numAccount = attr.getQuantity();
			} else if (ConstantString.ATTR_UNIT.DEVICE.equalsIgnoreCase(attr.getUnit())) {
				numDevice = attr.getQuantity();
			} else if (ConstantString.ATTR_UNIT.OTT.equalsIgnoreCase(attr.getUnit())) {
				numOtt = attr.getQuantity();
			}
		}

		Account superAccount = externalGetAccountRepository.get(externalUpdateAccountPlanDtoIn.getExternalCustomerId());

		// update account plan
		externalUpdatePlanRepository.update(superAccount.getUsername(), numAccount, numDevice, numOtt);

		// clear account plan cache
		cacheAccountPlanService.deleteAccountPlan(superAccount.getUuidAccount());
	}

	@Override
	public void cancelAccountPlan(ExternalCancelAccountPlanDtoIn externalCancelAccountPlanDtoIn) {
		Account superAccount = externalGetAccountRepository.get(externalCancelAccountPlanDtoIn.getExternalCustomerId());

		externalCancelPlanRepository.cancel(superAccount.getUsername());

		// clear account plan cache
		cacheAccountPlanService.deleteAccountPlan(superAccount.getUuidAccount());
	}

	@Override
	public PagingDTO<ExternalAccountDtoOut> listAccount(String username, ExternalListAccountDtoIn listAccountDtoIn, PagingDtoIn pagingDtoIn) {
		return externalListAccountRepository.list(pagingDtoIn.getPage(), pagingDtoIn.getMaxSize(),
				listAccountDtoIn.getStartDate(), listAccountDtoIn.getEndDate(), username);
	}

	@Override
	public ExternalNotifyDtoOut notifyAccount(String username, ExternalNotifyDtoIn notifyDtoIn) {
		String userCodes = String.join(",", notifyDtoIn.getUserCodes());
		String channels = String.join(",", notifyDtoIn.getChannels());
		List<AccountOttUserCode> accountOttList = listOttByUserCodeRepository.listOtt(username, userCodes, channels);

		List<ExternalOttDtoOut> ottDtoOuts = accountOttList.stream().map(accountOtt -> {
			ExternalOttDtoOut externalOttDtoOut = new ExternalOttDtoOut();
			externalOttDtoOut.setUserCode(accountOtt.getUserCode());
			externalOttDtoOut.setChannel(accountOtt.getChannelName());
			return externalOttDtoOut;
		}).collect(Collectors.toList());

		notifyService.sendNotifyOtt(notifyDtoIn.getContent(), accountOttList);

		ExternalNotifyDtoOut notifyDtoOut = new ExternalNotifyDtoOut();
		notifyDtoOut.setData(ottDtoOuts);
		return notifyDtoOut;
	}

	@Override
	public void createListUser(String adminUsername, ExternalCreateAccountList createAccountList) {
		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(adminUsername);
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		if (Common.isNullOrEmpty(company.getCompanyCode())) {
			throw new BadRequestException("company have no company code");
		}
		for (ExternalCreateAccountDtoIn accountDtoIn : createAccountList.getExternalAccounts()) {
			String username = Common.getUsername(company.getCompanyCode(), accountDtoIn.getUserCode());
			Boolean isExist = checkUsernameExistRepository.isExist(username);
			if (isExist) {
				continue;
			}

			String rawPassword = Common.generatePassword();
			policyReviewerService.reviewExternalCreateAccount(superAccount, 1);

			createUserExternalRepository.create(adminUsername, accountDtoIn, rawPassword);

			Account account = getAccountByUsernameRepository.getAccountByUserName(username);
			notifyService.sendEmail(account, rawPassword, company, superAccount);
		}
	}
}
