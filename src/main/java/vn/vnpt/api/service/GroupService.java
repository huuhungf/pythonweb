package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.GroupDtoIn;
import vn.vnpt.api.dto.in.ListGroupDeviceDtoIn;
import vn.vnpt.api.dto.in.UpdateGroupDtoIn;
import vn.vnpt.api.dto.out.GroupDtoOut;
import vn.vnpt.api.dto.out.ListGroupDeviceDtoOut;
import vn.vnpt.api.dto.out.ListGroupDtoOut;
import vn.vnpt.api.dto.out.ListGroupTreeDtoOut;

public interface GroupService {
	GroupDtoOut create(String username, GroupDtoIn groupDtoIn);

	ListGroupTreeDtoOut listGroupTree(String username, String role);

	void delete(String username, String uuidGroup);

	void update(String username, String uuidGroup, UpdateGroupDtoIn updateGroupDtoIn);

	ListGroupDeviceDtoOut listGroupDevice(String username, ListGroupDeviceDtoIn listGroupDeviceDtoIn);

	ListGroupDtoOut listGroup(String username, String role);
}
