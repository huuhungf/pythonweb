package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.GroupDtoIn;
import vn.vnpt.api.dto.in.ListGroupDeviceDtoIn;
import vn.vnpt.api.dto.in.UpdateGroupDtoIn;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.model.Group;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.group.*;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;

import java.util.List;

@Service
public class GroupServiceBean implements GroupService {
	private final CreateGroupRepository createGroupRepository;
	private final GetAccountByUsernameRepository getAccountByUsernameRepository;
	private final GetCompanyByAccountRepository getCompanyByAccountRepository;
	private final ListGroupByCompanyRepository listGroupByCompanyRepository;
	private final DeleteGroupRepository deleteGroupRepository;
	private final UpdateGroupRepository updateGroupRepository;
	private final ListGroupByDeviceRepository listGroupByDeviceRepository;
	private final GetDeviceRepository getDeviceRepository;
	private final GetCompanyRepository getCompanyRepository;
	private final ListGroupTreeRepository listGroupTreeRepository;
	private final GetGroupByCompanyRepository getGroupByCompanyRepository;
	private final GetGroupByDeviceRepository getGroupByDeviceRepository;
	private final ListAdminGroupTreeRepository listAdminGroupTreeRepository;

	@Autowired
	public GroupServiceBean(CreateGroupRepository createGroupRepository,
							GetAccountByUsernameRepository getAccountByUsernameRepository,
							GetCompanyByAccountRepository getCompanyByAccountRepository,
							ListGroupByCompanyRepository listGroupByCompanyRepository,
							DeleteGroupRepository deleteGroupRepository,
							UpdateGroupRepository updateGroupRepository,
							ListGroupByDeviceRepository listGroupByDeviceRepository,
							GetDeviceRepository getDeviceRepository,
							GetCompanyRepository getCompanyRepository,
							ListGroupTreeRepository listGroupTreeRepository,
							GetGroupByCompanyRepository getGroupByCompanyRepository,
							GetGroupByDeviceRepository getGroupByDeviceRepository,
							ListAdminGroupTreeRepository listAdminGroupTreeRepository) {
		this.createGroupRepository = createGroupRepository;
		this.getAccountByUsernameRepository = getAccountByUsernameRepository;
		this.getCompanyByAccountRepository = getCompanyByAccountRepository;
		this.listGroupByCompanyRepository = listGroupByCompanyRepository;
		this.deleteGroupRepository = deleteGroupRepository;
		this.updateGroupRepository = updateGroupRepository;
		this.listGroupByDeviceRepository = listGroupByDeviceRepository;
		this.getDeviceRepository = getDeviceRepository;
		this.getCompanyRepository = getCompanyRepository;
		this.listGroupTreeRepository = listGroupTreeRepository;
		this.getGroupByCompanyRepository = getGroupByCompanyRepository;
		this.getGroupByDeviceRepository = getGroupByDeviceRepository;
		this.listAdminGroupTreeRepository = listAdminGroupTreeRepository;
	}

	@Override
	public GroupDtoOut create(String username, GroupDtoIn groupDtoIn) {
		Group group = createGroupRepository.create(username, null, groupDtoIn.getName(),
				groupDtoIn.getUuidParentGroup());

		GroupDtoOut groupDtoOut = new GroupDtoOut();
		groupDtoOut.setUuidGroup(group.getUuidGroup());
		groupDtoOut.setName(group.getName());
		groupDtoOut.setUuidDevice(group.getUuidDevice());
		groupDtoOut.setUuidParentGroup(group.getUuidParentGroup());
		groupDtoOut.setUuidCompany(group.getUuidCompany());

		return groupDtoOut;
	}

	@Override
	public ListGroupTreeDtoOut listGroupTree(String username, String role) {
		Company company = null;
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
			if (superAccount == null) {
				throw new NotFoundException(String.format("account not found %s", username));
			}
			company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		} else {
			Account owner = getAccountByUsernameRepository.getAccountByUserName(username);
			if (owner == null) {
				throw new NotFoundException(String.format("account not found %s", username));
			}
			Device device = getDeviceRepository.getDevice(owner.getUuidDevice());
			company = getCompanyRepository.getCompany(device.getUuidCompany());
		}

		List<GroupNode> groupNodes = listGroupByCompanyRepository.list(company.getUuidCompany());

		ListGroupTreeDtoOut listGroupTreeDtoOut = new ListGroupTreeDtoOut();
		listGroupTreeDtoOut.setGroups(groupNodes);

		return listGroupTreeDtoOut;
	}

	@Override
	public void delete(String username, String uuidGroup) {
		deleteGroupRepository.delete(username, uuidGroup);
	}

	@Override
	public void update(String username, String uuidGroup, UpdateGroupDtoIn updateGroupDtoIn) {
		updateGroupRepository.update(uuidGroup, updateGroupDtoIn.getName());
	}

	@Override
	public ListGroupDeviceDtoOut listGroupDevice(String username, ListGroupDeviceDtoIn listGroupDeviceDtoIn) {
		List<GroupNode> groupNodes = listGroupByDeviceRepository.list(listGroupDeviceDtoIn.getUuidDevice());

		ListGroupDeviceDtoOut listGroupDeviceDtoOut = new ListGroupDeviceDtoOut();
		listGroupDeviceDtoOut.setUuidDevice(listGroupDeviceDtoIn.getUuidDevice());
		listGroupDeviceDtoOut.setGroups(groupNodes);

		return listGroupDeviceDtoOut;
	}

	@Override
	public ListGroupDtoOut listGroup(String username, String role) {
		List<GroupNode> groupNodes;
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
			if (superAccount == null) {
				throw new BadRequestException(String.format("username not exist: %s", username));
			}
			Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
			Group group = getGroupByCompanyRepository.get(company.getUuidCompany());
			groupNodes = listGroupTreeRepository.list(group.getUuidGroup());
		} else if (ConstantString.ROLE.OWNER.equals(role)) {
			Account owner = getAccountByUsernameRepository.getAccountByUserName(username);
			if (owner == null) {
				throw new BadRequestException(String.format("username not exist: %s", username));
			}
			Device device = getDeviceRepository.getDevice(owner.getUuidDevice());
			Group group = getGroupByDeviceRepository.get(device.getUuidDevice());
			groupNodes = listGroupTreeRepository.list(group.getUuidGroup());
		} else {
			Account account = getAccountByUsernameRepository.getAccountByUserName(username);
			groupNodes = listAdminGroupTreeRepository.list(account.getUuidAccount());
		}

		ListGroupDtoOut listGroupDtoOut = new ListGroupDtoOut();
		listGroupDtoOut.setGroups(groupNodes);
		return listGroupDtoOut;
	}
}
