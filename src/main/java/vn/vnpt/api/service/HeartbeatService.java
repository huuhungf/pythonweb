package vn.vnpt.api.service;

import vn.vnpt.cache.HeartBeatCache;

public interface HeartbeatService {
	HeartBeatCache getHeartbeat(String ownerUsername, String serialNumber);
	void deleteCacheHeartbeat(String ownerUsername, String serialNumber);
	void deleteCacheShadowHeartbeat(String ownerUsername, String serialNumber);
}
