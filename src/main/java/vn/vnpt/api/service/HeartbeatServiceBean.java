package vn.vnpt.api.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import vn.vnpt.cache.HeartBeatCache;

@Service
public class HeartbeatServiceBean implements HeartbeatService {
	@Cacheable(value = "Heartbeat", key = "#ownerUsername + '/' + #serialNumber", unless = "#result == null")
	public HeartBeatCache getHeartbeat(String ownerUsername, String serialNumber) {
		return null;
	}

	@CacheEvict(value = "Heartbeat", key = "#ownerUsername + '/' + #serialNumber")
	public void deleteCacheHeartbeat(String ownerUsername, String serialNumber) {
	}

	@CacheEvict(value = "ShadowHeartbeat", key = "#ownerUsername + '/' + #serialNumber")
	@Override
	public void deleteCacheShadowHeartbeat(String ownerUsername, String serialNumber) {
	}
}
