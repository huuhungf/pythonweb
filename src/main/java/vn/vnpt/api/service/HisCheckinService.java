package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.HisCheckinDtoIn;
import vn.vnpt.api.dto.in.GetHisCheckinDtoIn;
import vn.vnpt.api.dto.in.ListHisCheckinDtoIn;
import vn.vnpt.api.dto.out.HisCheckinDetailDtoOut;
import vn.vnpt.api.dto.out.ListDtoOut;
import vn.vnpt.api.dto.out.HisCheckinAccountDtoOut;
import vn.vnpt.api.dto.out.WebHisCheckinDtoOut;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.List;

public interface HisCheckinService {
	PagingDTO<WebHisCheckinDtoOut> listHisCheckin(ListHisCheckinDtoIn listHisCheckinDtoIn,
												  PagingDtoIn pagingDtoIn);

	List<HisCheckinDetailDtoOut> getHisCheckinDetail(GetHisCheckinDtoIn getHisCheckinDtoIn);

	void addHisCheckin(HisCheckinDtoIn hisCheckinDtoIn);

	ListDtoOut<HisCheckinAccountDtoOut> getHisCheckinAccount(GetHisCheckinDtoIn getHisCheckinDtoIn);
}
