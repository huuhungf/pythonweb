package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.GetHisCheckinDtoIn;
import vn.vnpt.api.dto.in.HisCheckinDtoIn;
import vn.vnpt.api.dto.in.ListHisCheckinDtoIn;
import vn.vnpt.api.dto.out.HisCheckinAccountDtoOut;
import vn.vnpt.api.dto.out.HisCheckinDetailDtoOut;
import vn.vnpt.api.dto.out.ListDtoOut;
import vn.vnpt.api.dto.out.WebHisCheckinDtoOut;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.checkin.GetHisCheckinDetailRepository;
import vn.vnpt.api.repository.checkin.InsertHisCheckinRepository;
import vn.vnpt.api.repository.checkin.WebListHisCheckinByGroupRepository;
import vn.vnpt.api.repository.checkin.WebUserListHisCheckinRepository;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.group.GetGroupByCompanyRepository;
import vn.vnpt.api.repository.group.GetGroupByDeviceRepository;
import vn.vnpt.api.repository.group.ListAdminGroupRepository;
import vn.vnpt.api.repository.group.ListGroupByAccountRepository;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.api.repository.user.GetAccountRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;
import vn.vnpt.context.DataContextHelper;
import vn.vnpt.policy.PolicyReviewerService;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HisCheckinServiceBean implements HisCheckinService {
	private final WebUserListHisCheckinRepository webUserListHisCheckinRepository;
	private final GetHisCheckinDetailRepository getHisCheckinDetailRepository;
	private final PolicyReviewerService policyReviewerService;
	private final GetDeviceRepository getDeviceRepository;
	private final GetCompanyRepository getCompanyRepository;
	private final GetAccountRepository getAccountRepository;
	private final GetAccountByUsernameRepository getAccountByUsernameRepository;
	private final WebListHisCheckinByGroupRepository webListHisCheckinByGroupRepository;
	private final ListGroupByAccountRepository listGroupByAccountRepository;
	private final GetCompanyByAccountRepository getCompanyByAccountRepository;
	private final GetGroupByCompanyRepository getGroupByCompanyRepository;
	private final GetGroupByDeviceRepository getGroupByDeviceRepository;
	private final ListAdminGroupRepository listAdminGroupRepository;
	private final InsertHisCheckinRepository insertHisCheckinRepository;
	private final CheckPermissionService checkPermissionService;
	@Autowired
	private DataContextHelper dataContextHelper;

	@Autowired
	public HisCheckinServiceBean(WebUserListHisCheckinRepository webUserListHisCheckinRepository,
	                             GetHisCheckinDetailRepository getHisCheckinDetailRepository,
	                             PolicyReviewerService policyReviewerService, GetDeviceRepository getDeviceRepository,
	                             GetCompanyRepository getCompanyRepository, GetAccountRepository getAccountRepository,
	                             GetAccountByUsernameRepository getAccountByUsernameRepository,
	                             WebListHisCheckinByGroupRepository webListHisCheckinByGroupRepository,
	                             ListGroupByAccountRepository listGroupByAccountRepository,
	                             GetCompanyByAccountRepository getCompanyByAccountRepository,
	                             GetGroupByCompanyRepository getGroupByCompanyRepository,
	                             GetGroupByDeviceRepository getGroupByDeviceRepository,
	                             ListAdminGroupRepository listAdminGroupRepository,
	                             InsertHisCheckinRepository insertHisCheckinRepository,
	                             CheckPermissionService checkPermissionService) {
		this.webUserListHisCheckinRepository = webUserListHisCheckinRepository;
		this.getHisCheckinDetailRepository = getHisCheckinDetailRepository;
		this.policyReviewerService = policyReviewerService;
		this.getDeviceRepository = getDeviceRepository;
		this.getCompanyRepository = getCompanyRepository;
		this.getAccountRepository = getAccountRepository;
		this.getAccountByUsernameRepository = getAccountByUsernameRepository;
		this.webListHisCheckinByGroupRepository = webListHisCheckinByGroupRepository;
		this.listGroupByAccountRepository = listGroupByAccountRepository;
		this.getCompanyByAccountRepository = getCompanyByAccountRepository;
		this.getGroupByCompanyRepository = getGroupByCompanyRepository;
		this.getGroupByDeviceRepository = getGroupByDeviceRepository;
		this.listAdminGroupRepository = listAdminGroupRepository;
		this.insertHisCheckinRepository = insertHisCheckinRepository;
		this.checkPermissionService = checkPermissionService;
	}

	@Override
	public PagingDTO<WebHisCheckinDtoOut> listHisCheckin(ListHisCheckinDtoIn listHisCheckinDtoIn, PagingDtoIn pagingDtoIn) {

		if (pagingDtoIn.getPage() < ConstantString.ONE) {
			pagingDtoIn.setPage(ConstantString.ONE);
		}

		String role = dataContextHelper.getRole();
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			String uuidGroup = listHisCheckinDtoIn.getUuidGroup();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Company company = dataContextHelper.getCompany();
				Group group = getGroupByCompanyRepository.get(company.getUuidCompany());
				uuidGroup = group.getUuidGroup();
			}

			return webListHisCheckinByGroupRepository.listHisCheckin(uuidGroup,
					listHisCheckinDtoIn.getStartDate(), listHisCheckinDtoIn.getEndDate(),
					listHisCheckinDtoIn.getKeySearch(), pagingDtoIn,
					listHisCheckinDtoIn.getMinFirstCheckin(), listHisCheckinDtoIn.getMaxFirstCheckin(),
					listHisCheckinDtoIn.getMinLastCheckin(), listHisCheckinDtoIn.getMaxLastCheckin()
			);
		}

		if (ConstantString.ROLE.OWNER.equals(role)) {
			String uuidGroup = listHisCheckinDtoIn.getUuidGroup();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Device device = dataContextHelper.getDevice();
				Group group = getGroupByDeviceRepository.get(device.getUuidDevice());
				uuidGroup = group.getUuidGroup();
			}
			return webListHisCheckinByGroupRepository.listHisCheckin(uuidGroup,
					listHisCheckinDtoIn.getStartDate(), listHisCheckinDtoIn.getEndDate(),
					listHisCheckinDtoIn.getKeySearch(), pagingDtoIn,
					listHisCheckinDtoIn.getMinFirstCheckin(), listHisCheckinDtoIn.getMaxFirstCheckin(),
					listHisCheckinDtoIn.getMinLastCheckin(), listHisCheckinDtoIn.getMaxLastCheckin()
			);
		}

		// list his checkin in case role is USER
		if (Common.isNullOrEmpty(listHisCheckinDtoIn.getUuidGroup())) {
			Account superAccount = dataContextHelper.getSuperAccount();
			policyReviewerService.reviewListHisCheckin(superAccount);
			String username = dataContextHelper.getUsername();
			return webUserListHisCheckinRepository.listHisCheckin(username, listHisCheckinDtoIn, pagingDtoIn);
		} else {
			String uuidGroup = listHisCheckinDtoIn.getUuidGroup();
			if (ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Account account = dataContextHelper.getContextAccount();
				List<AdminGroup> adminGroups = listAdminGroupRepository.list(account.getUuidAccount());
				uuidGroup = adminGroups.stream().map(AdminGroup::getUuidGroup).collect(Collectors.joining(","));
			}
			return webListHisCheckinByGroupRepository.listHisCheckin(uuidGroup,
					listHisCheckinDtoIn.getStartDate(), listHisCheckinDtoIn.getEndDate(),
					listHisCheckinDtoIn.getKeySearch(), pagingDtoIn,
					listHisCheckinDtoIn.getMinFirstCheckin(), listHisCheckinDtoIn.getMaxFirstCheckin(),
					listHisCheckinDtoIn.getMinLastCheckin(), listHisCheckinDtoIn.getMaxLastCheckin()
			);
		}
	}

	@Override
	public List<HisCheckinDetailDtoOut> getHisCheckinDetail(GetHisCheckinDtoIn getHisCheckinDtoIn) {
		return getHisCheckinDetailRepository.list(
				getHisCheckinDtoIn.getUuidAccount(),
				getHisCheckinDtoIn.getDateCheckin()
		);
	}

	@Override
	public void addHisCheckin(HisCheckinDtoIn hisCheckinDtoIn) {
		Account account = getAccountRepository.get(hisCheckinDtoIn.getUuidAccount());

		checkPermissionService.checkPermissionOnAccount(hisCheckinDtoIn.getUuidAccount());

		Account accountCreator = dataContextHelper.getContextAccount();

		insertHisCheckinRepository.addHisCheckin(account.getUuidAccount(),
				hisCheckinDtoIn.getDateCheckin(), accountCreator.getUuidAccount());
	}

	@Override
	public ListDtoOut<HisCheckinAccountDtoOut> getHisCheckinAccount(GetHisCheckinDtoIn getHisCheckinDtoIn) {

		Account account = getAccountRepository.get(getHisCheckinDtoIn.getUuidAccount());

		Date dateCheckin;
		try {
			dateCheckin = Common.convertStringToDate(getHisCheckinDtoIn.getDateCheckin(), ConstantString.DDMMYYYY);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not parse date: %s", getHisCheckinDtoIn.getDateCheckin()));
		}
		String startDate = Common.convertDateToString(Common.addDate(dateCheckin, -1), ConstantString.DDMMYYYY);
		String endDate = Common.convertDateToString(Common.addDate(dateCheckin, 1), ConstantString.DDMMYYYY);

		List<HisCheckinAccountDtoOut> results =
				getHisCheckinDetailRepository.listCheckinAccount(account.getUuidAccount(), startDate, endDate, Common.HIS_CHECKIN_CHANNEL_ID.ALL);

		return new ListDtoOut<>(results);
	}
}
