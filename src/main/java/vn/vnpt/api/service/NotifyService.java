package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.MessageNotification;
import vn.vnpt.api.dto.out.ExternalOttDtoOut;
import vn.vnpt.api.model.*;
import vn.vnpt.firebase.PushNotificationRequest;
import vn.vnpt.ott.AccessType;

import java.util.List;
import java.util.Map;

public interface NotifyService {
	void sendEmail(MessageNotification messageNotification);

	void sendEmail(Account account, String rawPassword, Company company, Account superAccount, List<AccountChannel> channels);

	void sendEmail(Account account, String rawPassword, Company company, Account superAccount);

	void sendNotifySignupTele(Account account, String rawPassword);

	void sendNotifyResetPasswordTele(Account account, String rawPassword);

	void sendFireBase(List<PushNotificationRequest> requestList);

	void sendFireBaseApp(Map<String, String> data, List<AppDeviceToken> deviceTokens);

	void sendNotifyToTablet(Map<String, String> data, List<DeviceLicense> deviceLicenses);

	void sendNotifyToTablet(String notificationType, List<DeviceLicense> deviceLicenses);

	void sendNotifyToAppByOtt(Account account, String imageUrl, String dateCheckin, List<AccountOtt> accountOtts, AccessType accessType);

	void sendNotifyToTablet(String uuidAccount);

	List<DeviceLicense> listNotifyDeviceLicense(String uuidAccount);

	void sendNotifyOtt(String content, List<AccountOttUserCode> accountOtts);

	void sendNotifyComplaintToUserByOtt(String accountComplainer, String accountResolver, String imageUrl,
	                                    List<AccountOtt> accountOtts, Integer status, String transId);
}
