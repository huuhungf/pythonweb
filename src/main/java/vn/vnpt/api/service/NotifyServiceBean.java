package vn.vnpt.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.RestTemplate;
import vn.vnpt.api.dto.in.MessageNotification;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.device.ListDeviceLicenseRepository;
import vn.vnpt.api.repository.group.ListGroupByAccountRepository;
import vn.vnpt.api.repository.group.ListRemovedGroupByAccountRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.ConstantsString;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.firebase.FirebaseService;
import vn.vnpt.firebase.PushNotificationRequest;
import vn.vnpt.ott.*;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
public class NotifyServiceBean implements NotifyService {
	private static final Logger logger = LogManager.getLogger(AccountServiceBean.class);

	private static final ExecutorService executorService = Executors.newFixedThreadPool(10);

	@Autowired
	private KafkaTemplate<String, MessageNotification> notificationKafkaTemplate;

	@Autowired
	private FirebaseService firebaseService;

	@Value("${kafka.topic.mail-notification}")
	private String TOPIC_NOTIFICATION;

	@Autowired
	private OttNotifyService ottNotifyService;

	@Autowired
	private ListGroupByAccountRepository listGroupByAccountRepository;

	@Autowired
	private ListRemovedGroupByAccountRepository listRemovedGroupByAccountRepository;

	@Autowired
	private ListDeviceLicenseRepository listDeviceLicenseRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Value(value = "${tele.chat-id}")
	private String chatId;

	@Value(value = "${tele.url}")
	private String teleUrl;

	@Value(value = "${sms.enable}")
	private Boolean enable;

	private final RestTemplate restTemplate;

	public NotifyServiceBean(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public void sendEmail(MessageNotification messageNotification) {
		sendMessageToKafka(messageNotification);
	}

	@Override
	public void sendEmail(Account account, String rawPassword, Company company, Account superAccount, List<AccountChannel> channels) {
		// send email to user
		String telegramStatus = ConstantsString.EMAIL_STATUS_UNREGISTERED;
		String zaloStatus = ConstantsString.EMAIL_STATUS_UNREGISTERED;
		String viberStatus = ConstantsString.EMAIL_STATUS_UNREGISTERED;
		String messengerStatus = ConstantsString.EMAIL_STATUS_UNREGISTERED;

		for (AccountChannel ac : channels) {
			if (Common.ACCOUNT_CHANNEL_STATUS.ACTIVE == ac.getStatus()) {
				if (ConstantsString.CHANNEL_NAME.TELEGRAM.equals(ac.getChannelName())) {
					telegramStatus = ConstantsString.EMAIL_STATUS_REGISTERED;
				} else if (ConstantsString.CHANNEL_NAME.ZALO.equals(ac.getChannelName())) {
					zaloStatus = ConstantsString.EMAIL_STATUS_REGISTERED;
				} else if (ConstantsString.CHANNEL_NAME.VIBER.equals(ac.getChannelName())) {
					viberStatus = ConstantsString.EMAIL_STATUS_REGISTERED;
				} else if (ConstantsString.CHANNEL_NAME.FACEBOOK.equals(ac.getChannelName())) {
					messengerStatus = ConstantsString.EMAIL_STATUS_REGISTERED;
				}
			}
		}

		Map<String, String> properties = new HashMap<>();
		properties.put("fullName", account.getFullName());
		properties.put("username", account.getUsername());
		properties.put("password", rawPassword);
		properties.put("telegramStatus", telegramStatus);
		properties.put("zaloStatus", zaloStatus);
		properties.put("viberStatus", viberStatus);
		properties.put("messengerStatus", messengerStatus);
		sendEmail(MessageNotification.builder()
				.destEmails(account.getEmail())
				.channelCode(ConstantsString.EMAIL_CHANNEL_CODE)
				.emailCategory(ConstantsString.EMAIL_CATEGORY_CREATE_ACCOUNT)
				.messageProperties(objectMapper, properties)
				.build());

		if (Common.COMPANY_NOTIFICATION.ENABLE == company.getNotification()) {
			// send email to super admin
			sendEmail(MessageNotification.builder()
					.destEmails(superAccount.getEmail())
					.channelCode(ConstantsString.EMAIL_CHANNEL_CODE)
					.emailCategory(ConstantsString.EMAIL_CATEGORY_CREATE_ACCOUNT_FOR_SUPER_ADMIN)
					.messageProperties(objectMapper, properties)
					.build());
		}
	}

	@Override
	public void sendEmail(Account account, String rawPassword, Company company, Account superAccount) {
		sendEmail(account, rawPassword, company, superAccount, new ArrayList<>());
	}

	@Override
	public void sendNotifySignupTele(Account account, String rawPassword) {
		String username = account.getUsername();
		System.out.printf("------START SEND TO TELEGRAM-------: %s,%s", username, rawPassword);
		Map<String, Object> body = new HashMap<>();
		body.put("chat_id", chatId);
		body.put("text", "Thông tin đăng nhập tài khoản vnFace \n" +
				"Tên đăng nhập: " + username + "\n" +
				"Mật khẩu: " + rawPassword + "\n" +
				"Truy cập tại: https://console-vnface.vnpt.vn/");
		body.put("disable_notification", true);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		restTemplate.postForObject(teleUrl, new HttpEntity<>(body, headers), String.class);
		System.out.printf("------SUCCESS SEND TO TELEGRAM-------: %s,%s", username, rawPassword);
	}

	@Override
	public void sendNotifyResetPasswordTele(Account account, String newPassword) {
		String username = account.getUsername();
		System.out.printf("------START SEND TO TELEGRAM-------: %s,%s", username, newPassword);
		Map<String, Object> body = new HashMap<>();
		body.put("chat_id", chatId);
		body.put("text", "vnFace thông báo đặt lại mật khẩu\n" +
				"Tên đăng nhập: " + username + "\n" +
				"Mật khẩu mới: " + newPassword + "\n" +
				"Truy cập tại: https://console-vnface.vnpt.vn/");
		body.put("disable_notification", true);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		restTemplate.postForObject(teleUrl, new HttpEntity<>(body, headers), String.class);
		System.out.printf("------SUCCESS SEND TO TELEGRAM-------: %s,%s", username, newPassword);
	}

	@Override
	public void sendFireBase(List<PushNotificationRequest> requestList) {
		for (PushNotificationRequest request : requestList) {
			try {
				firebaseService.sendMessage(request);
			} catch (Exception e) {
				logger.error(String.format("could not send notification to token %s", request.getToken()), e);
			}
		}
	}

	@Override
	public void sendFireBaseApp(Map<String, String> data, List<AppDeviceToken> deviceTokens) {
		List<PushNotificationRequest> requests = new ArrayList<>();
		for (AppDeviceToken deviceToken : deviceTokens) {
			PushNotificationRequest notificationRequest = new PushNotificationRequest();
			notificationRequest.setToken(deviceToken.getDeviceToken());
			notificationRequest.setData(data);
			notificationRequest.setWithConfig(true);
			notificationRequest.setTitle("");
			notificationRequest.setInstanceName(ConstantString.FIRE_BASE.INSTANCE_NAME_APP);
			requests.add(notificationRequest);
		}
		sendFireBase(requests);
	}

	@Override
	public void sendNotifyToTablet(Map<String, String> data, List<DeviceLicense> deviceLicenses) {
		// send notification to tablet
		List<PushNotificationRequest> requests = new ArrayList<>();
		for (DeviceLicense dl : deviceLicenses) {
			PushNotificationRequest notificationRequest = new PushNotificationRequest();
			if (ConstantString.OS.IOS.equalsIgnoreCase(dl.getOs())) {
				notificationRequest.setInstanceName(ConstantString.FIRE_BASE.INSTANCE_NAME_IOS_TABLET);
			} else {
				notificationRequest.setInstanceName(ConstantString.FIRE_BASE.INSTANCE_NAME_ANDROID_TABLET);
			}
			notificationRequest.setData(data);
			notificationRequest.setToken(dl.getDeviceToken());
			requests.add(notificationRequest);
		}
		sendFireBase(requests);
	}

	@Override
	public void sendNotifyToTablet(String notificationType, List<DeviceLicense> deviceLicenses) {
		Map<String, String> data = new HashMap<>();
		data.put("notificationType", notificationType);
		sendNotifyToTablet(data, deviceLicenses);
	}

	public void sendNotifyToAppByOtt(Account account, String imageUrl, String dateCheckin, List<AccountOtt> accountOtts, AccessType accessType) {

		for (AccountOtt accountOtt : accountOtts) {
			OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
			ottNotifyDtoIn.setBotId(accountOtt.getBotId());
			ottNotifyDtoIn.setUserId(accountOtt.getUserId());
			ottNotifyDtoIn.setChannel(accountOtt.getChannel());

			// check channel is telegram, zalo ... to get title builder
			TitleBuilder titleBuilder = new DefaultTitleBuilder(account.getFullName(), account.getUsername(), dateCheckin, accessType);

			if (ConstantString.OTT_CHANNEL.FB.equals(accountOtt.getChannel())) {
				CardData imageCardData = CardData.builder()
						.type(ConstantString.OTT_CARD_DATA_TYPE.IMAGE)
						.url(imageUrl)
						.title(ConstantString.OTT_MESSAGE.FB_TITLE).build();

				CardData textCardData = CardData.builder()
						.type("text")
						.text(titleBuilder.build())
						.build();

				ottNotifyDtoIn.setCardData(Arrays.asList(imageCardData, textCardData));
			} else {
				CardData imageCardData = CardData.builder()
						.type(ConstantString.OTT_CARD_DATA_TYPE.IMAGE)
						.url(imageUrl)
						.title(titleBuilder).build();

				ottNotifyDtoIn.setCardData(Collections.singletonList(imageCardData));
			}

			try {
				ottNotifyService.sendNotify(ottNotifyDtoIn);
				logger.info(String.format("send notification ott success to %s", accountOtt));
			} catch (Exception e) {
				logger.error(String.format("could not send notification ott to %s", accountOtt), e);
			}
		}
	}

	@Override
	public void sendNotifyToTablet(String uuidAccount) {
		// list all device licenses related to account
		List<DeviceLicense> deviceLicenses = listNotifyDeviceLicense(uuidAccount);
		// Notify to tablet for sync
		sendNotifyToTablet(Common.NOTIFICATION_TYPE.TABLET_SYNC_ACCOUNT, deviceLicenses);
	}


	@Override
	public List<DeviceLicense> listNotifyDeviceLicense(String uuidAccount) {
		List<Group> groups = listGroupByAccountRepository.list(uuidAccount);
		groups.addAll(listRemovedGroupByAccountRepository.list(uuidAccount));

		List<String> uuidDevices = groups.stream()
				.filter(group -> group.getUuidDevice() != null)
				.map(Group::getUuidDevice)
				.collect(Collectors.toList());

		List<DeviceLicense> deviceLicenses = new ArrayList<>();
		for (String uuidDevice : uuidDevices) {
			deviceLicenses.addAll(listDeviceLicenseRepository.listDeviceLicense(uuidDevice));
		}

		return deviceLicenses;
	}

	@Override
	public void sendNotifyOtt(String content, List<AccountOttUserCode> accountOtts) {
		for (AccountOttUserCode accountOtt : accountOtts) {
			executorService.submit(() -> {
				OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
				ottNotifyDtoIn.setBotId(accountOtt.getBotId());
				ottNotifyDtoIn.setUserId(accountOtt.getUserId());
				ottNotifyDtoIn.setChannel(accountOtt.getChannelName());

				CardData textCardData = CardData.builder()
						.type("text")
						.text(content)
						.build();

				ottNotifyDtoIn.setCardData(Collections.singletonList(textCardData));

				try {
					ottNotifyService.sendNotify(ottNotifyDtoIn);
					logger.info(String.format("send notification ott success to %s", accountOtt));
				} catch (Exception e) {
					logger.error(String.format("could not send notification ott to %s", accountOtt), e);
				}
			});
		}

		logger.info("SUCCESS SEND OTT size: " + accountOtts.size());
	}

	@Override
	public void sendNotifyComplaintToUserByOtt(String accountComplainer, String accountResolver, String imageUrl, List<AccountOtt> accountOtts, Integer status, String transId) {
		for (AccountOtt accountOtt : accountOtts) {
			executorService.submit(() -> {
				TitleBuilder titleBuilder = new ComplaintTitleBuilder(accountComplainer, accountResolver, status, imageUrl, transId);
				OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
				ottNotifyDtoIn.setBotId(accountOtt.getBotId());
				ottNotifyDtoIn.setUserId(accountOtt.getUserId());
				ottNotifyDtoIn.setChannel(accountOtt.getChannel());

				CardData textCardData = CardData.builder()
						.type("text")
						.text(titleBuilder.build())
						.build();

				ottNotifyDtoIn.setCardData(Collections.singletonList(textCardData));
				try {
					ottNotifyService.sendNotify(ottNotifyDtoIn);
					logger.info(String.format("send notification ott success to %s", accountOtt));
				} catch (Exception e) {
					logger.error(String.format("could not send notification ott to %s", accountOtt), e);
				}
			});
		}
	}

	private void sendMessageToKafka(MessageNotification messageNotification) {
		ListenableFuture<SendResult<String, MessageNotification>> future =
				this.notificationKafkaTemplate.send(TOPIC_NOTIFICATION, messageNotification);
		future.addCallback(new ListenableFutureCallback<SendResult<String, MessageNotification>>() {
			@Override
			public void onFailure(Throwable ex) {
				logger.error(String.format("could not send to topic %s with message " + messageNotification,
						TOPIC_NOTIFICATION), ex);
			}

			@Override
			public void onSuccess(SendResult<String, MessageNotification> result) {
				logger.info(String.format("send success to topic %s with message " +
						result.getProducerRecord().value(), TOPIC_NOTIFICATION));
			}
		});
	}
}
