package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.repository.plan.RegisterPlanTrialRepository;

@Service
public class PlanServiceBean implements PlanService{
	private final RegisterPlanTrialRepository registerPlanTrialRepository;

	@Autowired
	public PlanServiceBean(RegisterPlanTrialRepository registerPlanTrialRepository) {
		this.registerPlanTrialRepository = registerPlanTrialRepository;
	}

	@Override
	public void register(String username) {
		this.registerPlanTrialRepository.register(username);
	}
}
