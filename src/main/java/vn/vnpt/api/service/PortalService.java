package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.PortalAccountDtoOut;
import vn.vnpt.api.dto.out.PortalAccountProfileDtoOut;
import vn.vnpt.api.dto.out.PortalCreateAccountDtoOut;
import vn.vnpt.api.dto.out.PortalHisPlanDtoOut;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;


public interface PortalService {
    PortalCreateAccountDtoOut createAccount(PortalCreateAccountDtoIn portalDtoIn);
    PagingDTO<PortalAccountDtoOut> listAccount(PortalListAccountDtoIn listAccountDtoIn, PagingDtoIn pagingDtoIn);
    void registerPlan(PortalRegisterPlanDtoIn registerPlanDtoIn);
    void buyMorePlan(PortalBuyMorePlanDtoIn buyMorePlanDtoIn);
    void renewPlan(PortalPlanRenewDtoIn renewPlan);
    PortalAccountProfileDtoOut getAccountProfile(PortalAccountInfoDtoIn accountInfoDtoIn);
    PagingDTO<PortalHisPlanDtoOut> listPlanHistory(PortalHisPlanDtoIn hisPlanDtoIn, PagingDtoIn pagingDtoIn);
}
