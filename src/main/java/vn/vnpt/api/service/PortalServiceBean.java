package vn.vnpt.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.mapper.PortalAccountProfileImpl;
import vn.vnpt.api.mapper.PortalAccountProfileMapper;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.AccountPlan;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Plan;
import vn.vnpt.api.repository.auth.CreateConfirmationTokenRepository;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.order.ManualBuyMorePlanRepository;
import vn.vnpt.api.repository.order.ManualRegisterPlanRepository;
import vn.vnpt.api.repository.order.ManualRenewPlanRepository;
import vn.vnpt.api.repository.order.PortalHisPlanRepository;
import vn.vnpt.api.repository.plan.GetAccountPlanRepository;
import vn.vnpt.api.repository.plan.GetPlanRepository;
import vn.vnpt.api.repository.user.AgentCreateSupperAdminRepository;
import vn.vnpt.api.repository.user.AgentListCustomerRepository;
import vn.vnpt.api.repository.user.GetAccountRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.ConstantsString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;
import vn.vnpt.policy.PolicyReviewerService;

import java.util.HashMap;
import java.util.Map;

@Service
public class PortalServiceBean implements PortalService {

	private final AgentCreateSupperAdminRepository agentCreateSupperAdminRepository;
	private final CreateConfirmationTokenRepository createConfirmationTokenRepository;
	private final ObjectMapper objectMapper;
	private final NotifyService notifyService;
	private final AgentListCustomerRepository agentListCustomerRepository;
	private final PolicyReviewerService policyReviewerService;
	private final GetAccountRepository getAccountRepository;
	private final ManualRegisterPlanRepository manualRegisterPlanRepository;
	private final ManualBuyMorePlanRepository manualBuyMorePlanRepository;
	private final ManualRenewPlanRepository manualRenewPlanRepository;
	private final GetAccountPlanRepository getAccountPlanRepository;
	private final GetPlanRepository getPlanRepository;
	private final GetCompanyByAccountRepository getCompanyByAccountRepository;
	private final PortalHisPlanRepository portalHisPlanRepository;

	@Autowired
	public PortalServiceBean(AgentCreateSupperAdminRepository agentCreateSupperAdminRepository,
							 CreateConfirmationTokenRepository createConfirmationTokenRepository,
							 ObjectMapper objectMapper,
							 NotifyService notifyService,
							 AgentListCustomerRepository agentListCustomerRepository,
							 PolicyReviewerService policyReviewerService,
							 GetAccountRepository getAccountRepository,
							 ManualRegisterPlanRepository manualRegisterPlanRepository,
							 ManualBuyMorePlanRepository manualBuyMorePlanRepository,
							 ManualRenewPlanRepository manualRenewPlanRepository,
							 GetAccountPlanRepository getAccountPlanRepository,
							 GetPlanRepository getPlanRepository,
							 GetCompanyByAccountRepository getCompanyByAccountRepository,
							 PortalHisPlanRepository portalHisPlanRepository) {
		this.agentCreateSupperAdminRepository = agentCreateSupperAdminRepository;
		this.createConfirmationTokenRepository = createConfirmationTokenRepository;
		this.objectMapper = objectMapper;
		this.notifyService = notifyService;
		this.agentListCustomerRepository = agentListCustomerRepository;
		this.policyReviewerService = policyReviewerService;
		this.getAccountRepository = getAccountRepository;
		this.manualRegisterPlanRepository = manualRegisterPlanRepository;
		this.manualBuyMorePlanRepository = manualBuyMorePlanRepository;
		this.manualRenewPlanRepository = manualRenewPlanRepository;
		this.getAccountPlanRepository = getAccountPlanRepository;
		this.getPlanRepository = getPlanRepository;
		this.getCompanyByAccountRepository = getCompanyByAccountRepository;
		this.portalHisPlanRepository = portalHisPlanRepository;
	}

	@Override
	public PortalCreateAccountDtoOut createAccount(PortalCreateAccountDtoIn portalDtoIn) {
		PortalAccountDtoIn accountIn = portalDtoIn.getAccount();
		PortalCompanyDtoIn company = portalDtoIn.getCompany();
		PortalPlanDtoIn plan = portalDtoIn.getPlan();

		if (Common.PLAN_TYPE.PRO != plan.getPlanType() && Common.PLAN_TYPE.TRIAL != plan.getPlanType()) {
			throw new BadRequestException("wrong plan type");
		}

		if (Common.PLAN_PRO.DEFAULT_USER + plan.getAddonUser() > Common.PLAN_PRO.MAX_USER) {
			throw new BadRequestException("exceed max user");
		}

		String rawPassword = Common.generatePassword();
		String hashPassword = Common.encryptPassword(rawPassword);
		Account account = agentCreateSupperAdminRepository.create(
				portalDtoIn.getUuidAgent(), accountIn.getEmail(), accountIn.getPhoneNumber(), hashPassword,
				company.getName(), company.getAddress(),
				plan.getPlanType(), Common.PLAN_PRO.DEFAULT_DURATION, Common.PLAN_PRO.DEFAULT_USER,
				plan.getAddonDevice(), plan.getAddonOtt(), plan.getAddonUser()
		);

		// create confirmation token
		String token = Common.GenerateUUID();
		createConfirmationTokenRepository.create(token, account.getUuidAccount());

		// send email
		Map<String, String> properties = new HashMap<>();
		properties.put("confirmationToken", token);
		properties.put("email", accountIn.getEmail());
		properties.put("password", rawPassword);
		notifyService.sendEmail(MessageNotification.builder()
				.destEmails(account.getEmail())
				.channelCode(ConstantsString.EMAIL_CHANNEL_CODE)
				.emailCategory(ConstantsString.EMAIL_CATEGORY_CONFIRM_REGISTER_ACCOUNT_AGENT)
				.messageProperties(objectMapper, properties)
				.build());
		return new PortalCreateAccountDtoOut(account.getUuidAccount());
	}

	@Override
	public PagingDTO<PortalAccountDtoOut> listAccount(PortalListAccountDtoIn listAccountDtoIn, PagingDtoIn pagingDtoIn) {
		return agentListCustomerRepository.list(pagingDtoIn.getPage(), pagingDtoIn.getMaxSize(),
				listAccountDtoIn.getUuidAgent(), listAccountDtoIn.getKeySearch());
	}

	@Override
	public void registerPlan(PortalRegisterPlanDtoIn registerPlanDtoIn) {
		if (Common.PLAN_TYPE.PRO != registerPlanDtoIn.getPlanType()) {
			throw new BadRequestException("wrong plan type");
		}

		if (Common.PLAN_PRO.DEFAULT_USER + registerPlanDtoIn.getAddonUser() > Common.PLAN_PRO.MAX_USER) {
			throw new BadRequestException("exceed max user");
		}

		Account account = getAccountRepository.get(registerPlanDtoIn.getUuidAccount());

		policyReviewerService.reviewPortalRegisterPlan(account, Common.PLAN_PRO.DEFAULT_USER,
				registerPlanDtoIn.getAddonDevice(), registerPlanDtoIn.getAddonOtt(), registerPlanDtoIn.getAddonUser());

		// call procedure manual register pro plan
		manualRegisterPlanRepository.register(
				registerPlanDtoIn.getPlanType(), account.getUsername(), Common.PLAN_PRO.DEFAULT_USER,
				registerPlanDtoIn.getAddonDevice(), registerPlanDtoIn.getAddonOtt(), registerPlanDtoIn.getAddonUser(),
				Common.PLAN_PRO.DEFAULT_DURATION
		);
	}

	@Override
	public void buyMorePlan(PortalBuyMorePlanDtoIn buyMorePlanDtoIn) {
		Account account = getAccountRepository.get(buyMorePlanDtoIn.getUuidAccount());
		AccountPlan accountPlan = getAccountPlanRepository.get(account.getUsername());
		Plan plans = getPlanRepository.get(accountPlan.getUuidPlan());

		if (Common.PLAN_TYPE.PRO != plans.getType()) {
			throw new BadRequestException("wrong plan type");
		}

		if (accountPlan.getAddonDevice() + buyMorePlanDtoIn.getAddonDevice() > Common.PLAN_PRO.MAX_DEVICE) {
			throw new BadRequestException("exceed max device");
		}

		if (accountPlan.getAddonOtt() + buyMorePlanDtoIn.getAddonOtt() > Common.PLAN_PRO.MAX_ADDON_OTT) {
			throw new BadRequestException("exceed max addon ott");
		}

		if (accountPlan.getAddonUser() + accountPlan.getLimitUser() + buyMorePlanDtoIn.getAddonUser() > Common.PLAN_PRO.MAX_USER) {
			throw new BadRequestException("exceed max user");
		}

		manualBuyMorePlanRepository.buyMorePlan(account.getUsername(), buyMorePlanDtoIn.getAddonUser(),
				buyMorePlanDtoIn.getAddonDevice(), buyMorePlanDtoIn.getAddonOtt());
	}

	@Override
	public void renewPlan(PortalPlanRenewDtoIn renewPlan) {
		Account account = getAccountRepository.get(renewPlan.getUuidAccount());
		AccountPlan accountPlan = getAccountPlanRepository.get(account.getUsername());
		Plan plans = getPlanRepository.get(accountPlan.getUuidPlan());

		if (Common.PLAN_TYPE.PRO != plans.getType()) {
			throw new BadRequestException("wrong plan type");
		}

		if (!(renewPlan.getDuration().equals(3) || renewPlan.getDuration().equals(6) || renewPlan.getDuration().equals(12))) {
			throw new BadRequestException("duration is not correct");
		}
		manualRenewPlanRepository.renewPlan(account.getUsername(), renewPlan.getDuration());
	}

	@Override
	public PortalAccountProfileDtoOut getAccountProfile(PortalAccountInfoDtoIn accountInfoDtoIn) {
		Account account = getAccountRepository.get(accountInfoDtoIn.getUuidAccount());
		AccountPlan accountPlan = getAccountPlanRepository.get(account.getUsername());
		Plan plans = getPlanRepository.get(accountPlan.getUuidPlan());
		Company company = getCompanyByAccountRepository.getCompany(accountInfoDtoIn.getUuidAccount());

		PortalAccountProfileMapper portalAccountProfileMapper = new PortalAccountProfileImpl();
		PortalPlanInfoDtoOut planInfoDtoOut = portalAccountProfileMapper.map(plans, accountPlan);
		PortalAccountInfoDtoOut accountInfoDtoOut = portalAccountProfileMapper.map(account, company);

		return new PortalAccountProfileDtoOut(accountInfoDtoOut, planInfoDtoOut);
	}

	@Override
	public PagingDTO<PortalHisPlanDtoOut> listPlanHistory(PortalHisPlanDtoIn hisPlanDtoIn, PagingDtoIn pagingDtoIn) {
		return portalHisPlanRepository.listHisPlan(hisPlanDtoIn, pagingDtoIn);
	}

}
