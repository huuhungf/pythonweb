package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.ParamScheduleAccountDtoIn;
import vn.vnpt.api.dto.in.ScheduleAccountDtoIn;
import vn.vnpt.api.dto.in.ScheduleDtoIn;
import vn.vnpt.api.dto.out.ScheduleAccountDtoOut;
import vn.vnpt.api.dto.out.ScheduleDtoOut;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.List;

public interface ScheduleService {

	void createSchedule(String userName, ScheduleDtoIn scheduleDtoIn);

	List<ScheduleDtoOut> listSchedules(String username, String role);

	ScheduleDtoOut getSchedule(String uuidSchedule);

	void deleteSchedule(String userName, String uuidSchedule);

	void updateSchedule(String userName, String uuidSchedule, ScheduleDtoIn scheduleDtoIn);

	void assignScheduleAccount(String userName, ScheduleAccountDtoIn scheduleAccountDtoIn);

	PagingDTO<ScheduleAccountDtoOut> listScheduleAccount(String username, ParamScheduleAccountDtoIn uuidGroup, PagingDtoIn pageDtoIn);
}
