package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.*;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.group.GetGroupByCompanyRepository;
import vn.vnpt.api.repository.group.GetGroupByDeviceRepository;
import vn.vnpt.api.repository.group.ListGroupByAccountRepository;
import vn.vnpt.api.repository.schedule.ScheduleRepository;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;
import vn.vnpt.context.DataContextHelper;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	private ScheduleRepository scheduleRepository;
	@Autowired
	private GetAccountByUsernameRepository getAccountByUsernameRepository;
	@Autowired
	private GetCompanyByAccountRepository getCompanyByAccountRepository;
	@Autowired
	private GetGroupByCompanyRepository getGroupByCompanyRepository;
	@Autowired
	private CheckPermissionService checkPermissionService;
	@Autowired
	private GetDeviceRepository getDeviceRepository;
	@Autowired
	private ListGroupByAccountRepository listGroupByAccountRepository;
	@Autowired
	private GetGroupByDeviceRepository getGroupByDeviceRepository;
	@Autowired
	private DataContextHelper dataContextHelper;

	@Override
	public void createSchedule(String userName, ScheduleDtoIn scheduleDtoIn) {

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.WEEK) {
			if (scheduleDtoIn.getWeekSchedule() == null || scheduleDtoIn.getWeekSchedule().isEmpty()) {
				throw new BadRequestException("Lịch làm việc tuần không có dữ liệu!");
			}
		}

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.MONTH) {
			if (scheduleDtoIn.getMonthSchedule() == null || scheduleDtoIn.getMonthSchedule().isEmpty()) {
				throw new BadRequestException("Lịch làm việc tháng không có dữ liệu!");
			}
		}

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.YEAR) {
			if (scheduleDtoIn.getYearSchedule() == null || scheduleDtoIn.getYearSchedule().isEmpty()) {
				throw new BadRequestException("Lịch làm việc năm không có dữ liệu!");
			}
		}

		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(userName);
		if (superAccount == null) {
			throw new NotFoundException(String.format("Không tìm thấy tài khoản %s", userName));
		}
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());

		Schedule schedule = scheduleRepository.createSchedule(company.getUuidCompany(), scheduleDtoIn);
		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.WEEK) {
			List<WeekScheduleDtoIn> wSchedules = scheduleDtoIn.getWeekSchedule()
					.stream().distinct().collect(Collectors.toList());
			for (WeekScheduleDtoIn wSchedule : wSchedules) {
				scheduleRepository.createWSchedule(schedule.getUuidSchedule(), wSchedule.getDayId(), wSchedule.getUuidShift());
			}
		}

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.MONTH) {
			List<MonthScheduleDtoIn> mSchedules = scheduleDtoIn.getMonthSchedule()
					.stream().distinct().collect(Collectors.toList());
			for (MonthScheduleDtoIn mSchedule : mSchedules) {
				scheduleRepository.createMSchedule(schedule.getUuidSchedule(), mSchedule.getDayId(), mSchedule.getUuidShift());
			}
		}

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.YEAR) {
			List<YearScheduleDtoIn> ySchedules = scheduleDtoIn.getYearSchedule()
					.stream().distinct().collect(Collectors.toList());
			for (YearScheduleDtoIn ySchedule : ySchedules) {
				scheduleRepository.createYSchedule(schedule.getUuidSchedule(), ySchedule.getDayId(),
						ySchedule.getMonthId(), ySchedule.getUuidShift());
			}
		}
	}

	@Override
	public List<ScheduleDtoOut> listSchedules(String username, String role) {
		String uuidCompany = "";
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
			Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
			uuidCompany = company.getUuidCompany();
		} else if (ConstantString.ROLE.OWNER.equals(role)) {
			Account owner = getAccountByUsernameRepository.getAccountByUserName(username);
			Device device = getDeviceRepository.getDevice(owner.getUuidDevice());
			uuidCompany = device.getUuidCompany();
		} else if (ConstantString.ROLE.USER.equals(role)) {
			Account account = getAccountByUsernameRepository.getAccountByUserName(username);
			Group groupCompany = listGroupByAccountRepository.list(account.getUuidAccount())
					.stream().filter(g -> g.getUuidCompany() != null)
					.findFirst()
					.get();

			uuidCompany = groupCompany.getUuidCompany();
		} else {
			throw new AccessDeniedException("access denied");
		}
		return scheduleRepository.listSchedules(uuidCompany);
	}

	@Override
	public ScheduleDtoOut getSchedule(String uuidSchedule) {
		Schedule schedule = scheduleRepository.getSchedule(uuidSchedule);

		ScheduleDtoOut scheduleDtoOut = new ScheduleDtoOut();
		scheduleDtoOut.setUuidSchedule(uuidSchedule);
		scheduleDtoOut.setScheduleName(schedule.getScheduleName());
		scheduleDtoOut.setCycleMode(schedule.getCycleMode());

		if (schedule.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.WEEK) {
			List<WScheduleDtoOut> wSchedules = scheduleRepository.listWSchedule(uuidSchedule);
			scheduleDtoOut.setWeekSchedule(wSchedules);
		}

		if (schedule.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.MONTH) {
			List<MScheduleDtoOut> mSchedules = scheduleRepository.listMSchedule(uuidSchedule);
			scheduleDtoOut.setMonthSchedule(mSchedules);
		}

		if (schedule.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.YEAR) {
			List<YScheduleDtoOut> ySchedules = scheduleRepository.listYSchedule(uuidSchedule);
			scheduleDtoOut.setYearSchedule(ySchedules);
		}

		return scheduleDtoOut;
	}

	@Override
	public void deleteSchedule(String userName, String uuidSchedule) {
		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(userName);
		if (superAccount == null) {
			throw new NotFoundException(String.format("Không tìm thấy tài khoản %s", userName));
		}
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		Schedule schedule = scheduleRepository.getSchedule(uuidSchedule);
		if (!company.getUuidCompany().equalsIgnoreCase(schedule.getUuidCompany())) {
			throw new NotFoundException("Không tìm thấy lịch trình");
		}
		scheduleRepository.deleteSchedule(uuidSchedule);
	}

	@Override
	public void updateSchedule(String userName, String uuidSchedule, ScheduleDtoIn scheduleDtoIn) {

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.WEEK) {
			if (scheduleDtoIn.getWeekSchedule() == null || scheduleDtoIn.getWeekSchedule().isEmpty()) {
				throw new BadRequestException("Lịch làm việc tuần không có dữ liệu!");
			}
		}

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.MONTH) {
			if (scheduleDtoIn.getMonthSchedule() == null || scheduleDtoIn.getMonthSchedule().isEmpty()) {
				throw new BadRequestException("Lịch làm việc tháng không có dữ liệu!");
			}
		}

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.YEAR) {
			if (scheduleDtoIn.getYearSchedule() == null || scheduleDtoIn.getYearSchedule().isEmpty()) {
				throw new BadRequestException("Lịch làm việc năm không có dữ liệu!");
			}
		}

		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(userName);
		if (superAccount == null) {
			throw new NotFoundException(String.format("Không tìm thấy tài khoản %s", userName));
		}
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());

		Schedule schedule = scheduleRepository.getSchedule(uuidSchedule);
		if (!company.getUuidCompany().equalsIgnoreCase(schedule.getUuidCompany())) {
			throw new NotFoundException("Không tìm thấy lịch trình");
		}

		scheduleRepository.updateSchedule(uuidSchedule,
				scheduleDtoIn.getScheduleName(), scheduleDtoIn.getCycleMode());

		// Xoa du lieu o bang tuan, thang, nam
		if (schedule.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.WEEK) {
			scheduleRepository.deleteWSchedule(uuidSchedule);
		} else if (schedule.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.MONTH) {
			scheduleRepository.deleteMSchedule(uuidSchedule);
		} else if (schedule.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.YEAR) {
			scheduleRepository.deleteYSchedule(uuidSchedule);
		}

		// Luu du lieu cap nhat vao bang tuan, thang, nam tuong ung
		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.WEEK) {
			List<WeekScheduleDtoIn> wSchedules = scheduleDtoIn.getWeekSchedule()
					.stream().distinct().collect(Collectors.toList());
			for (WeekScheduleDtoIn wSchedule : wSchedules) {
				scheduleRepository.createWSchedule(schedule.getUuidSchedule(), wSchedule.getDayId(), wSchedule.getUuidShift());
			}
		}

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.MONTH) {
			List<MonthScheduleDtoIn> mSchedules = scheduleDtoIn.getMonthSchedule()
					.stream().distinct().collect(Collectors.toList());
			for (MonthScheduleDtoIn mSchedule : mSchedules) {
				scheduleRepository.createMSchedule(schedule.getUuidSchedule(), mSchedule.getDayId(), mSchedule.getUuidShift());
			}
		}

		if (scheduleDtoIn.getCycleMode() == Common.SCHEDULE_CYCLE_MODE.YEAR) {
			List<YearScheduleDtoIn> ySchedules = scheduleDtoIn.getYearSchedule()
					.stream().distinct().collect(Collectors.toList());
			for (YearScheduleDtoIn ySchedule : ySchedules) {
				scheduleRepository.createYSchedule(schedule.getUuidSchedule(), ySchedule.getDayId(),
						ySchedule.getMonthId(), ySchedule.getUuidShift());
			}
		}
	}

	@Override
	public void assignScheduleAccount(String userName, ScheduleAccountDtoIn scheduleAccountDtoIn) {
		checkPermissionService.assignScheduleAccount(scheduleAccountDtoIn.getUuidGroup());

		if (!Common.isNullOrEmpty(scheduleAccountDtoIn.getUuidSchedule())) {
			Company company = dataContextHelper.getCompany();
			Schedule schedule = scheduleRepository.getSchedule(scheduleAccountDtoIn.getUuidSchedule());
			if (!schedule.getUuidCompany().equalsIgnoreCase(company.getUuidCompany())) {
				throw new BadRequestException("Không tìm thấy lịch trình");
			}
		}

		scheduleRepository.assignScheduleAccount(scheduleAccountDtoIn.getUuidSchedule(), scheduleAccountDtoIn.getUuidAccounts(), scheduleAccountDtoIn.getUuidGroup());
	}

	@Override
	public PagingDTO<ScheduleAccountDtoOut> listScheduleAccount(String username, ParamScheduleAccountDtoIn params, PagingDtoIn pageDtoIn) {
		String uuidGroup = params.getUuidGroup();
		String role = dataContextHelper.getRole();
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Company company = dataContextHelper.getCompany();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Group group = getGroupByCompanyRepository.get(company.getUuidCompany());
				uuidGroup = group.getUuidGroup();
			}
		} else if (ConstantString.ROLE.OWNER.equals(role)) {
			Device device = dataContextHelper.getDevice();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Group group = getGroupByDeviceRepository.get(device.getUuidDevice());
				uuidGroup = group.getUuidGroup();
			}
		}
		return scheduleRepository.listScheduleAccount(pageDtoIn, uuidGroup, params.getKeySearch());
	}

}
