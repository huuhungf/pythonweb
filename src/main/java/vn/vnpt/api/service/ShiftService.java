package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.ListShiftDtoIn;
import vn.vnpt.api.dto.in.ShiftAccountDtoIn;
import vn.vnpt.api.dto.in.ShiftDtoIn;
import vn.vnpt.api.dto.in.ShiftTmpDtoIn;
import vn.vnpt.api.dto.out.ListShiftAccountDtoOut;
import vn.vnpt.api.dto.out.ShiftDetailDtoOut;
import vn.vnpt.api.dto.out.ShiftDtoOut;

import java.util.List;

public interface ShiftService {
	List<ShiftDtoOut> getListShift(String username, ListShiftDtoIn listShiftDtoIn, String role);

	void createShift(String username, ShiftDtoIn shiftDtoIn);

	void update(String username, String uuidShift, ShiftDtoIn shiftDtoIn);

	void delete(String username, String uuidShift);

	ShiftDetailDtoOut getShift(String username, String uuidShift);

	void assignShiftTmp(ShiftTmpDtoIn shiftTmpDtoIn, String username);

	ListShiftAccountDtoOut listShiftsAccount(ShiftAccountDtoIn shiftAccountDtoIn);

}
