package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.ListShiftDtoIn;
import vn.vnpt.api.dto.in.ShiftAccountDtoIn;
import vn.vnpt.api.dto.in.ShiftDtoIn;
import vn.vnpt.api.dto.in.ShiftTmpDtoIn;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.group.ListGroupByAccountRepository;
import vn.vnpt.api.repository.schedule.ScheduleRepository;
import vn.vnpt.api.repository.shift.ShiftRepository;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.api.repository.user.GetAccountRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ShiftServiceImpl implements ShiftService {

	@Autowired
	private GetAccountByUsernameRepository getAccountByUsernameRepository;
	@Autowired
	private GetCompanyByAccountRepository getCompanyByAccountRepository;
	@Autowired
	private ShiftRepository shiftRepository;
	@Autowired
	private GetDeviceRepository getDeviceRepository;
	@Autowired
	private ListGroupByAccountRepository listGroupByAccountRepository;
	@Autowired
	private CheckPermissionService checkPermissionService;
	@Autowired
	private ScheduleRepository scheduleRepository;
	@Autowired
	private GetAccountRepository getAccountRepository;

	@Override
	public List<ShiftDtoOut> getListShift(String username, ListShiftDtoIn listShiftDtoIn, String role) {
		if (!Common.isNullOrEmpty(listShiftDtoIn.getUuidGroup())) {
			List<Shift> listShift = shiftRepository.listShiftByGroup(listShiftDtoIn.getUuidGroup());
			return listShift.stream().map(ShiftDtoOut::from).collect(Collectors.toList());
		}

		String uuidCompany = "";
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
			Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
			uuidCompany = company.getUuidCompany();
		} else if (ConstantString.ROLE.OWNER.equals(role)) {
			Account owner = getAccountByUsernameRepository.getAccountByUserName(username);
			Device device = getDeviceRepository.getDevice(owner.getUuidDevice());
			uuidCompany = device.getUuidCompany();
		} else if (ConstantString.ROLE.USER.equals(role)) {
			Account account = getAccountByUsernameRepository.getAccountByUserName(username);
			Group groupCompany = listGroupByAccountRepository.list(account.getUuidAccount())
					.stream().filter(g -> g.getUuidCompany() != null)
					.findFirst()
					.get();

			uuidCompany = groupCompany.getUuidCompany();
		} else {
			throw new AccessDeniedException("access denied");
		}

		List<Shift> shifts = shiftRepository.list(uuidCompany);
		return shifts.stream().map(ShiftDtoOut::from).collect(Collectors.toList());
	}

	@Override
	public void createShift(String username, ShiftDtoIn shiftDtoIn) {
		validateShiftDtoIn(shiftDtoIn);
		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
		if (superAccount == null) {
			throw new NotFoundException(String.format("account not found %s", username));
		}
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		shiftRepository.createShift(company.getUuidCompany(), shiftDtoIn);
	}

	@Override
	public void update(String username, String uuidShift, ShiftDtoIn shiftDtoIn) {
		validateShiftDtoIn(shiftDtoIn);
		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
		if (superAccount == null) {
			throw new NotFoundException(String.format("account not found %s", username));
		}
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		Shift shift = shiftRepository.get(uuidShift);
		if (!shift.getUuidCompany().equalsIgnoreCase(company.getUuidCompany())) {
			throw new AccessDeniedException("access denied");
		}
		shiftRepository.updateShift(uuidShift, shiftDtoIn);
	}

	@Override
	public void delete(String username, String uuidShift) {
		Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
		if (superAccount == null) {
			throw new NotFoundException(String.format("account not found %s", username));
		}
		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		Shift shift = shiftRepository.get(uuidShift);
		if (!shift.getUuidCompany().equalsIgnoreCase(company.getUuidCompany())) {
			throw new AccessDeniedException("access denied");
		}
		shiftRepository.deleteShift(uuidShift);

	}

	@Override
	public ShiftDetailDtoOut getShift(String username, String uuidShift) {
		Shift shift = shiftRepository.get(uuidShift);
		return ShiftDetailDtoOut.from(shift);
	}

	@Override
	public void assignShiftTmp(ShiftTmpDtoIn shiftTmpDtoIn, String username) {
		Account account = getAccountRepository.get(shiftTmpDtoIn.getUuidAccount());
		checkPermissionService.checkPermissionOnAccount(shiftTmpDtoIn.getUuidAccount());
		shiftRepository.assignShiftTmp(account.getUuidAccount(), shiftTmpDtoIn.getUuidShifts(),
				shiftTmpDtoIn.getTmpDate());
	}

	@Override
	public ListShiftAccountDtoOut listShiftsAccount(ShiftAccountDtoIn shiftAccountDtoIn) {
		ListShiftAccountDtoOut listShift = new ListShiftAccountDtoOut();

		List<ShiftAccountDtoOut> tmpShiftsDtoOut = new ArrayList<>();
		List<Shift> tmpShifts = shiftRepository.getShiftTmp(shiftAccountDtoIn.getUuidAccount(), shiftAccountDtoIn.getDate());
		for (Shift index : tmpShifts) {
			ShiftAccountDtoOut tmpShift = ShiftAccountDtoOut.builder()
					.uuidShift(index.getUuidShift())
					.shiftCode(index.getShiftCode())
					.onDuty(index.getOnDuty())
					.offDuty(index.getOffDuty()).build();
			tmpShiftsDtoOut.add(tmpShift);
		}
		listShift.setTmpShifts(tmpShiftsDtoOut);

		AccountSchedule accountSchedule = scheduleRepository.getAccountSchedule(shiftAccountDtoIn.getUuidAccount());
		List<ShiftAccountDtoOut> permanentShiftsDtoOut = new ArrayList<>();

		Schedule schedule = scheduleRepository.getSchedule(accountSchedule.getUuidSchedule());

		if (Common.SCHEDULE_CYCLE_MODE.WEEK == schedule.getCycleMode()) {
			Integer dayOfWeek = Common.getDayOfWeek(shiftAccountDtoIn.getDate(), ConstantString.DDMMYYYY);
			List<WScheduleDtoOut> wSchedules = scheduleRepository.listWSchedule(schedule.getUuidSchedule());
			for (WScheduleDtoOut w : wSchedules) {
				if (Objects.equals(w.getDayId(), dayOfWeek)) {
					Shift shift = shiftRepository.get(w.getUuidShift());
					ShiftAccountDtoOut permanentShift = ShiftAccountDtoOut.builder()
							.uuidShift(w.getUuidShift())
							.shiftCode(w.getShiftCode())
							.onDuty(shift.getOnDuty())
							.offDuty(shift.getOffDuty()).build();
					permanentShiftsDtoOut.add(permanentShift);
				}
			}
		}

		if (Common.SCHEDULE_CYCLE_MODE.MONTH == schedule.getCycleMode()) {
			Integer dayOfMonth = Common.getDayOfMonth(shiftAccountDtoIn.getDate(), ConstantString.DDMMYYYY);
			List<MScheduleDtoOut> mSchedules = scheduleRepository.listMSchedule(schedule.getUuidSchedule());
			for (MScheduleDtoOut m : mSchedules) {
				if (dayOfMonth.equals(m.getDayId())) {
					Shift shift = shiftRepository.get(m.getUuidShift());
					ShiftAccountDtoOut permanentShift = ShiftAccountDtoOut.builder()
							.uuidShift(m.getUuidShift())
							.shiftCode(m.getShiftCode())
							.onDuty(shift.getOnDuty())
							.offDuty(shift.getOffDuty()).build();
					permanentShiftsDtoOut.add(permanentShift);
				}
			}
		}

		if (Common.SCHEDULE_CYCLE_MODE.YEAR == schedule.getCycleMode()) {
			Integer month = Common.getMonth(shiftAccountDtoIn.getDate(), ConstantString.DDMMYYYY);
			Integer dayOfMonth = Common.getDayOfMonth(shiftAccountDtoIn.getDate(), ConstantString.DDMMYYYY);
			List<YScheduleDtoOut> ySchedules = scheduleRepository.listYSchedule(schedule.getUuidSchedule());
			for (YScheduleDtoOut y : ySchedules) {
				if (dayOfMonth.equals(y.getDayId()) && month.equals(y.getMonthId())) {
					Shift shift = shiftRepository.get(y.getUuidShift());
					ShiftAccountDtoOut permanentShift = ShiftAccountDtoOut.builder()
							.uuidShift(y.getUuidShift())
							.shiftCode(y.getShiftCode())
							.onDuty(shift.getOnDuty())
							.offDuty(shift.getOffDuty()).build();
					permanentShiftsDtoOut.add(permanentShift);
				}
			}
		}
		listShift.setPermanentShifts(permanentShiftsDtoOut);
		return listShift;
	}

	private void validateShiftDtoIn(ShiftDtoIn shiftDtoIn) {

		Integer onDuty = Common.getTimeInSec(shiftDtoIn.getOnDuty());
		Integer offDuty = Common.getTimeInSec(shiftDtoIn.getOffDuty()) + shiftDtoIn.getDayCount() * ConstantString.SECONDS_IN_A_DAY;

		if (shiftDtoIn.getOnLunch() != null && shiftDtoIn.getOffLunch() != null) {
			Integer onLunch = Common.getTimeInSec(shiftDtoIn.getOnLunch());
			Integer offLunch = Common.getTimeInSec(shiftDtoIn.getOffLunch());
			if (onLunch > offLunch) {
				throw new BadRequestException("TGBĐ ăn trưa < TGKT ăn trưa");
			}
		}

		// Điểm danh giờ vào
		if (onDuty >= offDuty) {
			throw new BadRequestException("TGBĐ làm việc < TGKT làm việc");
		}

		if (shiftDtoIn.getWorkingDay() * 100 < Math.ceil(shiftDtoIn.getWorkingDay() * 100)) {
			throw new BadRequestException("Số ngày công tối đa 2 số sau dấu phẩy");
		}
	}

}
