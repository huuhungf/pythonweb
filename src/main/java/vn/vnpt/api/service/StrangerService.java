package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.DeleteAllStrangerDtoIn;
import vn.vnpt.api.dto.in.DeleteListStrangerDtoIn;
import vn.vnpt.api.dto.in.WebListStrangerDtoIn;
import vn.vnpt.api.dto.out.StrangerDtoOut;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

public interface StrangerService {
	PagingDTO<StrangerDtoOut> list(String username, String role, WebListStrangerDtoIn webListStrangerDtoIn,
								   PagingDtoIn pagingDtoIn);
	void delete(String uuidStranger);

	void deleteList(DeleteListStrangerDtoIn deleteListStrangerDtoIn);

	void deleteAll(String username, String role, DeleteAllStrangerDtoIn deleteAllStrangerDtoIn);
}
