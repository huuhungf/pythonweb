package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.DeleteAllStrangerDtoIn;
import vn.vnpt.api.dto.in.DeleteListStrangerDtoIn;
import vn.vnpt.api.dto.in.WebListStrangerDtoIn;
import vn.vnpt.api.dto.out.StrangerDtoOut;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.model.Stranger;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.stranger.*;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
public class StrangerServiceBean implements StrangerService {

	private final GetAccountByUsernameRepository getAccountByUsernameRepository;
	private final GetCompanyRepository getCompanyRepository;
	private final GetDeviceRepository getDeviceRepository;
	private final WebAdminListStrangerRepository webAdminListStrangerRepository;
	private final WebSuperAdminListStrangerRepository webSuperAdminListStrangerRepository;
	private final DeleteStrangerRepository deleteStrangerRepository;
	private final DeleteListStrangerRepository deleteListStrangerRepository;
	private final DeleteStrangerByDeviceRepository deleteStrangerByDeviceRepository;
	private final UploadService uploadService;
	private final GetStrangerRepository getStrangerRepository;
	private final ListStrangerRepository listStrangerRepository;
	private final ListStrangerByDeviceRepository listStrangerByDeviceRepository;

	@Autowired
	public StrangerServiceBean(GetAccountByUsernameRepository getAccountByUsernameRepository,
							   GetCompanyRepository getCompanyRepository, GetDeviceRepository getDeviceRepository,
							   WebAdminListStrangerRepository webAdminListStrangerRepository,
							   WebSuperAdminListStrangerRepository webSuperAdminListStrangerRepository,
							   DeleteStrangerRepository deleteStrangerRepository,
							   DeleteListStrangerRepository deleteListStrangerRepository,
							   DeleteStrangerByDeviceRepository deleteStrangerByDeviceRepository,
							   UploadService uploadService,
							   GetStrangerRepository getStrangerRepository,
							   ListStrangerRepository listStrangerRepository,
							   ListStrangerByDeviceRepository listStrangerByDeviceRepository) {
		this.getAccountByUsernameRepository = getAccountByUsernameRepository;
		this.getCompanyRepository = getCompanyRepository;
		this.getDeviceRepository = getDeviceRepository;
		this.webAdminListStrangerRepository = webAdminListStrangerRepository;
		this.webSuperAdminListStrangerRepository = webSuperAdminListStrangerRepository;
		this.deleteStrangerRepository = deleteStrangerRepository;
		this.deleteListStrangerRepository = deleteListStrangerRepository;
		this.deleteStrangerByDeviceRepository = deleteStrangerByDeviceRepository;
		this.uploadService = uploadService;
		this.getStrangerRepository = getStrangerRepository;
		this.listStrangerRepository = listStrangerRepository;
		this.listStrangerByDeviceRepository = listStrangerByDeviceRepository;
	}

	@Override
	public PagingDTO<StrangerDtoOut> list(String username, String role, WebListStrangerDtoIn webListStrangerDtoIn, PagingDtoIn pagingDtoIn) {

		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
			if (superAccount == null) {
				throw new NotFoundException(String.format("account admin not found %s", username));
			}

			if (webListStrangerDtoIn.getUuidDevice() != null && !webListStrangerDtoIn.getUuidDevice().isEmpty()) {
				Device device = getDeviceRepository.getDevice(webListStrangerDtoIn.getUuidDevice());
				Company company = getCompanyRepository.getCompany(device.getUuidCompany());
				if (!superAccount.getUuidAccount().equals(company.getOwnedAccount())) {
					throw new AccessDeniedException("access denied");
				}
			}

			return webSuperAdminListStrangerRepository.list(username, webListStrangerDtoIn, pagingDtoIn);
		} else {
			if (webListStrangerDtoIn.getUuidDevice() == null || webListStrangerDtoIn.getUuidDevice().isEmpty()) {
				throw new BadRequestException("uuidDevice must not null or empty");
			}
			Device device = getDeviceRepository.getDevice(webListStrangerDtoIn.getUuidDevice());

			Account admin = getAccountByUsernameRepository.getAccountByUserName(username);
			if (admin == null) {
				throw new NotFoundException(String.format("account admin not found %s", username));
			}
			if (!admin.getUuidDevice().equals(device.getUuidDevice())) {
				throw new AccessDeniedException("access denied");
			}

			return webAdminListStrangerRepository.list(webListStrangerDtoIn, pagingDtoIn);
		}
	}

	@Override
	public void delete(String uuidStranger) {
		Stranger stranger = getStrangerRepository.get(uuidStranger);
		deleteStrangerRepository.delete(uuidStranger);
		uploadService.deleteFile(uploadService.extractHashFile(stranger.getImageUrl()));
	}

	@Override
	public void deleteList(DeleteListStrangerDtoIn deleteListStrangerDtoIn) {
		List<Stranger> strangers = listStrangerRepository.list(deleteListStrangerDtoIn.getUuidStrangers());
		deleteListStrangerRepository.delete(deleteListStrangerDtoIn.getUuidStrangers());

		// delete image file of stranger
		deleteStrangerImages(strangers);
	}

	private void deleteStrangerImages(List<Stranger> strangers) {
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		executorService.execute(() -> {
			List<String> hashFiles = strangers.stream()
					.map(stranger -> uploadService.extractHashFile(stranger.getImageUrl()))
					.collect(Collectors.toList());
			uploadService.deleteFiles(hashFiles);
		});
		executorService.shutdown();
	}

	@Override
	public void deleteAll(String username, String role, DeleteAllStrangerDtoIn deleteAllStrangerDtoIn) {
		Device device = getDeviceRepository.getDevice(deleteAllStrangerDtoIn.getUuidDevice());
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Account superAccount = getAccountByUsernameRepository.getAccountByUserName(username);
			if (superAccount == null) {
				throw new NotFoundException(String.format("account not found %s", username));
			}
			Company company = getCompanyRepository.getCompany(device.getUuidCompany());
			if (!company.getOwnedAccount().equals(superAccount.getUuidAccount())) {
				throw new AccessDeniedException("access denied");
			}
		} else {
			Account admin = getAccountByUsernameRepository.getAccountByUserName(username);
			if (admin == null) {
				throw new NotFoundException(String.format("account not found %s", username));
			}
			if (!admin.getUuidDevice().equals(device.getUuidDevice())) {
				throw new AccessDeniedException("access denied");
			}
		}

		List<Stranger> strangers = listStrangerByDeviceRepository.list(deleteAllStrangerDtoIn.getUuidDevice(),
				deleteAllStrangerDtoIn.getSerialNumber());
		deleteStrangerByDeviceRepository.delete(device.getUuidDevice(), deleteAllStrangerDtoIn.getSerialNumber());

		deleteStrangerImages(strangers);
	}
}
