package vn.vnpt.api.service;

import java.util.List;

public interface UploadService {
	String uploadImage(byte[] contents, String title, String bucket, String subFolder);

	String uploadAudio(byte[] contents, String title, String bucket, String subFolder, String contentType);

	String extractHashFile(String fileLink);

	void deleteFile(String hashFile);

	void deleteFiles(List<String> hashFiles);

	String copyFile(String bucket, String objectName, String destBucket);

	String copyFile(String hashFile, String destBucket);

	String extractHashFileFromUploadLink(String uploadUrl, String targetBucket);

	String getUrl(String hashFile);

	String presignedPutObject(String bucket, String objectName);

	String genImageName(String subFolder);
}
