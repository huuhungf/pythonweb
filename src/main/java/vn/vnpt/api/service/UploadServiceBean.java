package vn.vnpt.api.service;

import io.minio.MinioClient;
import io.minio.Result;
import io.minio.errors.*;
import io.minio.messages.DeleteError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xmlpull.v1.XmlPullParserException;
import vn.vnpt.common.Common;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.*;

@Service
public class UploadServiceBean implements UploadService {
	private final Logger logger = LoggerFactory.getLogger(UploadServiceBean.class);

	@Value("${storage.type}")
	private String STORAGE_TYPE;

	@Value("${storage.type-audio}")
	private String STORAGE_TYPE_AUDIO;

	@Value("${storage.public-url}")
	private String STORAGE_PUBLIC_URL;

	private final MinioClient minioClient;

	@Autowired
	public UploadServiceBean(@Qualifier("getMinioClient") MinioClient minioClient) {
		this.minioClient = minioClient;
	}

	public String uploadImage(byte[] contents, String title, String bucket, String subFolder) {
		return uploadFile(contents, title, bucket, subFolder, "image/jpg", STORAGE_TYPE);
	}

	public String uploadAudio(byte[] contents, String title, String bucket, String subFolder, String contentType) {
		return uploadFile(contents, title, bucket, subFolder, contentType, STORAGE_TYPE_AUDIO);
	}

	public void deleteFile(String hashFile) {
		try {
			String bucket = hashFile.split("/")[0];
			String objectName = hashFile.substring(bucket.length() + 1);
			logger.info("MINIO check exist bucket folder " + bucket);
			boolean isExist = this.minioClient.bucketExists(bucket);
			if (!isExist) {
				return;
			}

			logger.info(String.format("MINIO --START delete : %s", hashFile));
			this.minioClient.removeObject(bucket, objectName);
			logger.info(String.format("MINIO --FINISH delete: %s", hashFile));
		} catch (Exception ex) {
			logger.error("delete file: " + hashFile, ex);
			throw new RuntimeException("Could not delete file: " + hashFile);
		}
	}

	public void deleteFiles(List<String> hashFiles) {
		if (hashFiles.isEmpty()) {
			return;
		}

		Map<String, List<String>> bucketMap = new HashMap<>();

		for (String hashFile : hashFiles) {
			String bucket = hashFile.split("/")[0];
			String objectName = hashFile.substring(bucket.length() + 1);

			List<String> objectNames = bucketMap.getOrDefault(bucket, new ArrayList<>());
			objectNames.add(objectName);

			bucketMap.put(bucket, objectNames);
		}

		logger.info(String.format("MINIO --START delete : %s", Arrays.toString(hashFiles.toArray())));
		for (Map.Entry<String, List<String>> entry : bucketMap.entrySet()) {
			try {
				String bucket = entry.getKey();
				logger.info("MINIO check exist bucket folder " + bucket);
				boolean isExist = this.minioClient.bucketExists(bucket);
				if (!isExist) {
					continue;
				}

				for (Result<DeleteError> errorResult : minioClient.removeObjects(bucket, entry.getValue())) {
					DeleteError error = errorResult.get();
					logger.error("could not delete file: " + error.objectName() + ". Error: " + error.message());
					throw new RuntimeException("Could not delete file: " + error.objectName());
				}
			} catch (Exception ex) {
				logger.error("delete files: " + Arrays.toString(hashFiles.toArray()), ex);
				throw new RuntimeException("Could not delete files: " + Arrays.toString(hashFiles.toArray()));
			}
		}
		logger.info(String.format("MINIO --FINISH delete: %s", Arrays.toString(hashFiles.toArray())));
	}

	@Override
	public String copyFile(String hashFile, String destBucket) {
		String bucket = hashFile.split("/")[0];
		String objectName = hashFile.substring(bucket.length() + 1);
		return copyFile(bucket, objectName, destBucket);
	}

	@Override
	public String extractHashFileFromUploadLink(String uploadUrl, String targetBucket) {
		URL url;
		try {
			url = new URL(uploadUrl);
		} catch (MalformedURLException e) {
			throw new BadRequestException(String.format("upload url wrong format %s", uploadUrl));
		}
		String hashFile = url.getPath().replaceFirst("/", "");
		String bucket = hashFile.split("/")[0];
		String objectName = hashFile.substring(bucket.length() + 1);

		if (!targetBucket.equals(bucket)) {
			throw new BadRequestException(String.format("wrong url type %s", uploadUrl));
		}

		try {
			this.minioClient.statObject(bucket, objectName);
			return hashFile;
		} catch (InvalidBucketNameException | ErrorResponseException e) {
			e.printStackTrace();
			throw new BadRequestException(String.format("not found file for url %s", uploadUrl));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not stat object %s", hashFile));
		}
	}

	@Override
	public String getUrl(String hashFile) {
		String bucket = hashFile.split("/")[0];
		String objectName = hashFile.substring(bucket.length() + 1);
		return getUrlFile(bucket, objectName);
	}

	@Override
	public String presignedPutObject(String bucket, String objectName) {
		try {
			return this.minioClient.presignedPutObject(bucket, objectName, (int) Duration.ofHours(2).getSeconds());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not presigned put object url: %s, %s", bucket, objectName));
		}
	}

	@Override
	public String genImageName(String subFolder) {
		return subFolder + "/" + STORAGE_TYPE + "_" + Common.GenerateUUID();
	}

	@Override
	public String copyFile(String bucket, String objectName, String destBucket) {
		try {
			logger.info("MINIO check exist bucket folder " + bucket);
			boolean isExist = this.minioClient.bucketExists(bucket);
			if (!isExist) {
				throw new RuntimeException(String.format("bucket not exist: %s", bucket));
			}

			isExist = this.minioClient.bucketExists(destBucket);
			if (!isExist) {
				throw new RuntimeException(String.format("bucket not exist: %s", destBucket));
			}

			if (bucket.equals(destBucket)) {
				return getUrlFile(bucket, objectName);
			}

			this.minioClient.copyObject(bucket, objectName, destBucket);

			return getUrlFile(destBucket, objectName);
		} catch (Exception e) {
			logger.error("move file: " + bucket + "/" + objectName, e);
			throw new RuntimeException("Could not move file: " + bucket + "/" + objectName);
		}
	}

	public String getUrlFile(String folder, String pathFile) {
		String urlFile = "";
		try {
			String url = minioClient.getObjectUrl(folder, pathFile);
			StringBuilder urlFileBuilder = new StringBuilder(STORAGE_PUBLIC_URL);
			String[] arrUrl = url.split("/");
			if (arrUrl.length > 2) {
				for (int i = 3; i < arrUrl.length; i++) {
					urlFileBuilder.append("/");
					urlFileBuilder.append(arrUrl[i]);
				}
			}
			urlFile = urlFileBuilder.toString();
		} catch (InvalidKeyException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException
				| NoResponseException | ErrorResponseException | InternalException | IOException | XmlPullParserException e) {
			e.printStackTrace();
			throw new NotFoundException(e.getMessage());
		}
		return urlFile;
	}

	public String extractHashFile(String fileLink) {
		return fileLink.replace(STORAGE_PUBLIC_URL + "/", "");
	}

	private String uploadFile(byte[] contents, String title, String bucket, String subFolder, String contentType, String storageType) {
		String hash = "";
		ByteArrayInputStream bais = null;
		try {
			logger.info("uploadFile check exist bucket folder " + bucket);
			boolean isExist = this.minioClient.bucketExists(bucket);
			if (!isExist) {
				this.minioClient.makeBucket(bucket);
			}

			bais = new ByteArrayInputStream(contents);
			hash = subFolder + "/" + storageType + "_" + Common.GenerateUUID();

			String hashFile = bucket + "/" + hash;
			logger.info(String.format("uploadFile minio --START hashFile: %s", hashFile));
			this.minioClient.putObject(bucket, hash, bais, contentType);
			logger.info(String.format("uploadFile minio --FINISH hashFile: %s", hashFile));

		} catch (Exception ex) {
			logger.error("uploadFile title: " + title + ", exception: ", ex);
			System.out.println(ex.getMessage());
			throw new RuntimeException("Could not upload file title: " + title);
		} finally {
			if (bais != null) {
				try {
					bais.close();
				} catch (IOException ex) {
					logger.error("uploadFile title: " + title + ", close exception: ", ex);
					System.out.println(ex.getMessage());
				}
			}
		}
		return getUrlFile(bucket, hash);
	}
}
