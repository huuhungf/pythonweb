package vn.vnpt.api.service;

import vn.vnpt.api.dto.in.WorkingDayDetailADayDtoIn;
import vn.vnpt.api.dto.in.WorkingDayDetailDtoIn;
import vn.vnpt.api.dto.in.WorkingDayReportDtoIn;
import vn.vnpt.api.dto.in.WorkingDayReportSummaryDtoIn;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;

public interface WorkingDayService {

	ListDtoOut<WorkingDayReportDtoOut> workingDayReport(WorkingDayReportDtoIn workingDayReport);

	PagingDTO<WorkingDayReportSummaryDtoOut> workingDayReportSummary(WorkingDayReportSummaryDtoIn wDReportSummaryDtoIn,
																	 PagingDtoIn pagingDtoIn);

	WorkingDayDetailDtoOut workingDayDetail(WorkingDayDetailDtoIn workingDayDetailDtoIn, String uuidAccount);

	WorkingDayDetailADayDtoOut workingDetailADay(WorkingDayDetailADayDtoIn wddDtoOut,  String uuidAccount);
}
