package vn.vnpt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.WorkingDayDetailADayDtoIn;
import vn.vnpt.api.dto.in.WorkingDayDetailDtoIn;
import vn.vnpt.api.dto.in.WorkingDayReportDtoIn;
import vn.vnpt.api.dto.in.WorkingDayReportSummaryDtoIn;
import vn.vnpt.api.dto.out.*;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.checkin.CheckinRepository;
import vn.vnpt.api.repository.checkin.GetHisCheckinDetailRepository;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.group.GetGroupByCompanyRepository;
import vn.vnpt.api.repository.group.GetGroupByDeviceRepository;
import vn.vnpt.api.repository.group.ListAdminGroupRepository;
import vn.vnpt.api.repository.group.ListGroupByAccountRepository;
import vn.vnpt.api.repository.schedule.ScheduleRepository;
import vn.vnpt.api.repository.shift.ShiftRepository;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.api.repository.user.GetAccountRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.success.model.PagingDTO;
import vn.vnpt.common.success.model.PagingDtoIn;
import vn.vnpt.context.DataContextHelper;
import vn.vnpt.shift.WorkingDayReport;
import vn.vnpt.shift.WorkingDayReporter;
import vn.vnpt.shift.WorkingShiftStats;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

@Service
public class WorkingDayServiceImpl implements WorkingDayService {

	@Autowired
	private GetAccountRepository getAccountRepository;
	@Autowired
	private WorkingDayReporter workingDayReporter;
	@Autowired
	private CheckinRepository checkinRepository;
	@Autowired
	private GetAccountByUsernameRepository getAccountByUsernameRepository;
	@Autowired
	private GetCompanyByAccountRepository getCompanyByAccountRepository;
	@Autowired
	private GetGroupByCompanyRepository getGroupByCompanyRepository;
	@Autowired
	private ScheduleRepository scheduleRepository;
	@Autowired
	private ShiftRepository shiftRepository;
	@Autowired
	private GetDeviceRepository getDeviceRepository;
	@Autowired
	private GetGroupByDeviceRepository getGroupByDeviceRepository;
	@Autowired
	private ListAdminGroupRepository listAdminGroupRepository;
	@Autowired
	private ListGroupByAccountRepository listGroupByAccountRepository;
	@Autowired
	private GetHisCheckinDetailRepository getHisCheckinDetailRepository;
	@Autowired
	private DataContextHelper dataContextHelper;

	@Override
	public ListDtoOut<WorkingDayReportDtoOut> workingDayReport(WorkingDayReportDtoIn workingDayReportDtoIn) {
		List<String> uuidAccounts = Arrays.asList(workingDayReportDtoIn.getUuidAccounts().split(","));

		Date startDate;
		Date endDate;
		try {
			startDate = Common.addDate(Common.convertStringToDate(workingDayReportDtoIn.getStartDate(),
					ConstantString.DDMMYYYY), -1);
			endDate = Common.addDate(Common.convertStringToDate(workingDayReportDtoIn.getEndDate(),
					ConstantString.DDMMYYYY), 1);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new BadRequestException("invalid startDate, endDate");
		}

		String uuidCompany = getUuidCompany();

		final Map<String, Schedule> scheduleMap = new ConcurrentHashMap<>();
		Map<String, Shift> shiftMap = new HashMap<>();
		cacheSchedule(scheduleMap, uuidCompany, shiftMap);

		ForkJoinPool fjp = new ForkJoinPool(parallelism(uuidAccounts.size()));
		List<WorkingDayReportDtoOut> data;
		try {
			data = fjp.submit(() ->
					uuidAccounts.parallelStream().map(uuidAccount -> {
						List<WorkingDayReportDtoOut> res = new ArrayList<>();

						Account account = getAccountRepository.get(uuidAccount);
						AccountSchedule accountSchedule;
						try {
							accountSchedule = scheduleRepository.getAccountSchedule(account.getUuidAccount());
						} catch (NotFoundException e) {
							WorkingDayReportDtoOut wdr = new WorkingDayReportDtoOut();
							wdr.setUuidAccount(uuidAccount);
							wdr.setFullName(account.getFullName());
							wdr.setUserCode(account.getUserCode());
							wdr.setDate("");
							wdr.setDayId(0);
							wdr.setUuidShift("");
							wdr.setShiftCode("");
							wdr.setCheckinIn("");
							wdr.setCheckinOut("");
							wdr.setWorkingTime(0L);
							wdr.setWorkingDay(0f);
							wdr.setLateTime(0L);
							wdr.setEarlyTime(0L);
							res.add(wdr);

							WorkingDayReportDtoOut sumDtoOut = new WorkingDayReportDtoOut();
							sumDtoOut.setRowType(ConstantString.ONE);
							sumDtoOut.setUuidAccount(uuidAccount);
							sumDtoOut.setWorkingDay(0.0f);
							sumDtoOut.setWorkingTime(0L);
							sumDtoOut.setLateTime(0L);
							sumDtoOut.setEarlyTime(0L);
							res.add(sumDtoOut);
							return res;
						}

						Map<String, List<Shift>> tmpShifts = listShiftTmpAccount(uuidAccount, Common.convertDateToString(startDate,
								ConstantString.DDMMYYYY), Common.convertDateToString(endDate, ConstantString.DDMMYYYY), shiftMap);

						List<HisCheckin> hisCheckins = checkinRepository.listCheckinAccounts(uuidAccount,
								Common.convertDateToString(startDate, ConstantString.DDMMYYYY),
								Common.convertDateToString(endDate, ConstantString.DDMMYYYY));

						List<WorkingDayReport> workList = workingDayReporter.report(account.getUuidAccount(), workingDayReportDtoIn.getStartDate(),
								workingDayReportDtoIn.getEndDate(), scheduleMap, hisCheckins, accountSchedule.getUuidSchedule(), tmpShifts);

						WorkingDayReportDtoOut sumDtoOut = new WorkingDayReportDtoOut();
						sumDtoOut.setRowType(ConstantString.ONE);
						sumDtoOut.setUuidAccount(uuidAccount);
						sumDtoOut.setWorkingDay(0.0f);
						sumDtoOut.setWorkingTime(0L);
						sumDtoOut.setLateTime(0L);
						sumDtoOut.setEarlyTime(0L);

						for (WorkingDayReport wdr : workList) {
							for (Map.Entry<Shift, WorkingShiftStats> e : wdr.getWsStats().entrySet()) {
								Shift shift = e.getKey();
								WorkingShiftStats stats = e.getValue();
								WorkingDayReportDtoOut wdrDtoOut = new WorkingDayReportDtoOut();
								wdrDtoOut.setUuidAccount(account.getUuidAccount());
								wdrDtoOut.setFullName(account.getFullName());
								wdrDtoOut.setUserCode(account.getUserCode());
								wdrDtoOut.setDate(wdr.getDate());
								wdrDtoOut.setDayId(wdr.getDayId());
								wdrDtoOut.setUuidShift(shift.getUuidShift());
								wdrDtoOut.setShiftCode(shift.getShiftCode());
								wdrDtoOut.setCheckinIn(stats.getTimeCheckin());
								wdrDtoOut.setCheckinOut(stats.getTimeCheckout());
								wdrDtoOut.setWorkingTime(stats.getWorkingTime());
								wdrDtoOut.setWorkingDay(stats.getWorkingDay());
								wdrDtoOut.setLateTime(stats.getLateTime());
								wdrDtoOut.setEarlyTime(stats.getEarlyTime());

								sumDtoOut.setWorkingDay(sumDtoOut.getWorkingDay() + stats.getWorkingDay());
								sumDtoOut.setWorkingTime(sumDtoOut.getWorkingTime() + stats.getWorkingTime());
								sumDtoOut.setLateTime(sumDtoOut.getLateTime() + stats.getLateTime());
								sumDtoOut.setEarlyTime(sumDtoOut.getEarlyTime() + stats.getEarlyTime());

								res.add(wdrDtoOut);
							}
						}
						res.add(sumDtoOut);

						return res;
					}).flatMap(List::stream).collect(Collectors.toList())
			).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			if (e.getCause() instanceof RuntimeException) {
				throw (RuntimeException) e.getCause();
			}
			throw new RuntimeException("could not workingDayReport");
		} finally {
			fjp.shutdown();
		}

		return new ListDtoOut<>(data);
	}

	@Override
	public PagingDTO<WorkingDayReportSummaryDtoOut> workingDayReportSummary(WorkingDayReportSummaryDtoIn wDReportSummaryDtoIn,
	                                                                        PagingDtoIn pagingDtoIn) {

		String uuidCompany = getUuidCompany();
		String uuidGroup = getUuidGroup(wDReportSummaryDtoIn.getUuidGroup());

		PagingDTO<ScheduleAccountDtoOut> accountGroup = scheduleRepository.listScheduleAccount(pagingDtoIn, uuidGroup,
				wDReportSummaryDtoIn.getKeySearch());

		if (accountGroup.getData().isEmpty()) {
			PagingDTO<WorkingDayReportSummaryDtoOut> pagingDTO = new PagingDTO<>();
			pagingDTO.setPage(pagingDtoIn.getPage());
			pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
			pagingDTO.setTotalPages(accountGroup.getTotalPages());
			pagingDTO.setTotalElement(accountGroup.getTotalElement());
			pagingDTO.setData(new ArrayList<>());
			return pagingDTO;
		}

		Date startDate;
		Date endDate;
		try {
			startDate = Common.addDate(Common.convertStringToDate(wDReportSummaryDtoIn.getStartDate(),
					ConstantString.DDMMYYYY), -1);
			endDate = Common.addDate(Common.convertStringToDate(wDReportSummaryDtoIn.getEndDate(),
					ConstantString.DDMMYYYY), 1);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new BadRequestException("invalid startDate, endDate");
		}

		Map<String, Schedule> scheduleMap = new ConcurrentHashMap<>();
		Map<String, Shift> shiftMap = new HashMap<>();
		cacheSchedule(scheduleMap, uuidCompany, shiftMap);

		ForkJoinPool fjp = new ForkJoinPool(parallelism(accountGroup.getData().size()));
		List<WorkingDayReportSummaryDtoOut> listWDRSummaryDtoOut;
		try {
			listWDRSummaryDtoOut = fjp.submit(() -> accountGroup.getData().parallelStream()
					.map(account -> {
						if (account.getUuidSchedule() == null) {
							WorkingDayReportSummaryDtoOut wDRSumOut = new WorkingDayReportSummaryDtoOut();
							wDRSumOut.setUuidAccount(account.getUuidAccount());
							wDRSumOut.setFullName(account.getFullName());
							wDRSumOut.setUserCode(account.getUserCode());
							wDRSumOut.setWorkingTime(0L);
							wDRSumOut.setWorkingDay(0f);
							wDRSumOut.setShiftStats(new ArrayList<>());
							return wDRSumOut;
						}
						Map<String, List<Shift>> tmpShifts = listShiftTmpAccount(account.getUuidAccount(), Common.convertDateToString(startDate,
								ConstantString.DDMMYYYY), Common.convertDateToString(endDate, ConstantString.DDMMYYYY), shiftMap);

						List<HisCheckin> listHisCheckin = checkinRepository.listCheckinAccounts(account.getUuidAccount(),
								Common.convertDateToString(startDate, ConstantString.DDMMYYYY),
								Common.convertDateToString(endDate, ConstantString.DDMMYYYY));

						List<WorkingDayReport> wdrs = workingDayReporter.report(account.getUuidAccount(), wDReportSummaryDtoIn.getStartDate(),
								wDReportSummaryDtoIn.getEndDate(), scheduleMap, listHisCheckin, account.getUuidSchedule(), tmpShifts);

						long sumWorkingTime = 0L;
						float sumWorkingDay = 0;
						Map<Shift, ShiftStatsDtoOut> shiftStatsMap = new HashMap<>();
						for (WorkingDayReport wdr : wdrs) {
							for (Map.Entry<Shift, WorkingShiftStats> e : wdr.getWsStats().entrySet()) {
								Shift shift = e.getKey();
								WorkingShiftStats stats = e.getValue();

								ShiftStatsDtoOut statsDto = shiftStatsMap.get(shift);
								if (statsDto == null) {
									statsDto = new ShiftStatsDtoOut();
									statsDto.setUuidShift(shift.getUuidShift());
									statsDto.setShiftCode(shift.getShiftCode());
									statsDto.setWorkingTime(stats.getWorkingTime());
									statsDto.setWorkingDay(stats.getWorkingDay());
								} else {
									statsDto.setWorkingTime(statsDto.getWorkingTime() + stats.getWorkingTime());
									statsDto.setWorkingDay(statsDto.getWorkingDay() + stats.getWorkingDay());
								}
								shiftStatsMap.put(shift, statsDto);
								sumWorkingTime += stats.getWorkingTime();
								sumWorkingDay += stats.getWorkingDay();
							}
						}

						WorkingDayReportSummaryDtoOut wDSRSummaryDtoOut = new WorkingDayReportSummaryDtoOut();
						wDSRSummaryDtoOut.setUuidAccount(account.getUuidAccount());
						wDSRSummaryDtoOut.setFullName(account.getFullName());
						wDSRSummaryDtoOut.setUserCode(account.getUserCode());
						wDSRSummaryDtoOut.setWorkingTime(sumWorkingTime);
						wDSRSummaryDtoOut.setWorkingDay(sumWorkingDay);
						wDSRSummaryDtoOut.setShiftStats(new ArrayList<>(shiftStatsMap.values()));
						wDSRSummaryDtoOut.setUuidSchedule(account.getUuidSchedule());
						wDSRSummaryDtoOut.setScheduleName(account.getScheduleName());
						return wDSRSummaryDtoOut;
					}).collect(Collectors.toList())).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			if (e.getCause() instanceof RuntimeException) {
				throw (RuntimeException) e.getCause();
			}
			throw new RuntimeException("could not calculate workingDayReportSummary");
		} finally {
			fjp.shutdown();
		}

		PagingDTO<WorkingDayReportSummaryDtoOut> pagingDTO = new PagingDTO<>();
		pagingDTO.setPage(pagingDtoIn.getPage());
		pagingDTO.setMaxSize(pagingDtoIn.getMaxSize());
		pagingDTO.setTotalPages(accountGroup.getTotalPages());
		pagingDTO.setTotalElement(accountGroup.getTotalElement());
		pagingDTO.setData(listWDRSummaryDtoOut);
		return pagingDTO;
	}

	private void cacheSchedule(Map<String, Schedule> scheduleMap, String uuidCompany, Map<String, Shift> shiftMap) {

		List<ScheduleDtoOut> schedules = scheduleRepository.listSchedules(uuidCompany);
		schedules.forEach(s -> {
			Schedule schedule = new Schedule();
			schedule.setUuidSchedule(s.getUuidSchedule());
			schedule.setScheduleName(s.getScheduleName());
			schedule.setCycleMode(s.getCycleMode());
			schedule.setUuidCompany(uuidCompany);

			if (Common.SCHEDULE_CYCLE_MODE.WEEK == schedule.getCycleMode()) {
				List<WScheduleDtoOut> wSchedules = scheduleRepository.listWSchedule(schedule.getUuidSchedule());
				schedule.setWeekSchedules(wSchedules.stream().map(w -> {
					Shift shift = shiftMap.get(w.getUuidShift());
					if (shift == null) {
						shift = shiftRepository.get(w.getUuidShift());
						shiftMap.put(w.getUuidShift(), shift);
					}
					WSchedule wSchedule = new WSchedule();
					wSchedule.setDayId(w.getDayId());
					wSchedule.setUuidSchedule(schedule.getUuidSchedule());
					wSchedule.setUuidShift(w.getUuidShift());
					wSchedule.setShift(shift);
					return wSchedule;
				}).collect(Collectors.toList()));
			}

			if (Common.SCHEDULE_CYCLE_MODE.MONTH == schedule.getCycleMode()) {
				List<MScheduleDtoOut> mSchedules = scheduleRepository.listMSchedule(schedule.getUuidSchedule());
				schedule.setMonthSchedules(mSchedules.stream().map(m -> {
					Shift shift = shiftMap.get(m.getUuidShift());
					if (shift == null) {
						shift = shiftRepository.get(m.getUuidShift());
						shiftMap.put(m.getUuidShift(), shift);
					}
					MSchedule mSchedule = new MSchedule();
					mSchedule.setDayId(m.getDayId());
					mSchedule.setUuidSchedule(schedule.getUuidSchedule());
					mSchedule.setUuidShift(m.getUuidShift());
					mSchedule.setShift(shift);
					return mSchedule;
				}).collect(Collectors.toList()));
			}

			if (Common.SCHEDULE_CYCLE_MODE.YEAR == schedule.getCycleMode()) {
				List<YScheduleDtoOut> ySchedules = scheduleRepository.listYSchedule(schedule.getUuidSchedule());
				schedule.setYearSchedules(ySchedules.stream().map(y -> {
					Shift shift = shiftMap.get(y.getUuidShift());
					if (shift == null) {
						shift = shiftRepository.get(y.getUuidShift());
						shiftMap.put(y.getUuidShift(), shift);
					}
					YSchedule ySchedule = new YSchedule();
					ySchedule.setMonthId(y.getMonthId());
					ySchedule.setDayId(y.getDayId());
					ySchedule.setUuidSchedule(schedule.getUuidSchedule());
					ySchedule.setUuidShift(y.getUuidShift());
					ySchedule.setShift(shift);
					return ySchedule;
				}).collect(Collectors.toList()));
			}

			scheduleMap.put(schedule.getUuidSchedule(), schedule);
		});
	}

	private Map<String, List<Shift>> listShiftTmpAccount(String uuidAccount, String startDate, String endDate, Map<String, Shift> shiftMap) {
		List<AccountTempShift> accountTempShifts = shiftRepository.listAccountTempShift(uuidAccount, startDate, endDate);
		Map<String, List<Shift>> shifts = new HashMap<>();
		for (AccountTempShift tmpShift : accountTempShifts) {
			Shift shift = shiftMap.get(tmpShift.getUuidShift());
			if (shift == null) { // Cache shift
				shift = shiftRepository.get(tmpShift.getUuidShift());
				shiftMap.put(tmpShift.getUuidShift(), shift);
			}
			List<Shift> tmpShifts = shifts.getOrDefault(tmpShift.getTempShiftDate(), new ArrayList<>());
			tmpShifts.add(shift);
			shifts.put(tmpShift.getTempShiftDate(), tmpShifts);
		}
		return shifts;
	}

	@Override
	public WorkingDayDetailDtoOut workingDayDetail(WorkingDayDetailDtoIn workingDayDetailDtoIn, String uuidAccount) {

		Date startDate;
		Date endDate;
		try {
			startDate = Common.addDate(Common.convertStringToDate(workingDayDetailDtoIn.getStartDate(),
					ConstantString.DDMMYYYY), -1);
			endDate = Common.addDate(Common.convertStringToDate(workingDayDetailDtoIn.getEndDate(),
					ConstantString.DDMMYYYY), 1);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new BadRequestException("invalid startDate, endDate");
		}

		String uuidCompany = getUuidCompany();

		final Map<String, Schedule> scheduleMap = new HashMap<>();
		Map<String, Shift> shiftMap = new HashMap<>();
		cacheSchedule(scheduleMap, uuidCompany, shiftMap);
		Map<String, List<Shift>> tmpShifts = listShiftTmpAccount(uuidAccount, workingDayDetailDtoIn.getStartDate(),
				workingDayDetailDtoIn.getEndDate(), shiftMap);

		List<ListWorkingDayDetailDtoOut> res = new ArrayList<>();

		Account account = getAccountRepository.get(uuidAccount);
		AccountSchedule accountSchedule = new AccountSchedule();
		try {
			accountSchedule = scheduleRepository.getAccountSchedule(account.getUuidAccount());
		} catch (NotFoundException e) {
			return WorkingDayDetailDtoOut.builder()
					.uuidAccount(uuidAccount)
					.userCode(account.getUserCode())
					.fullName(account.getFullName())
					.data(res)
					.build();
		}

		List<HisCheckin> hisCheckins = checkinRepository.listCheckinAccounts(uuidAccount,
				Common.convertDateToString(startDate, ConstantString.DDMMYYYY),
				Common.convertDateToString(endDate, ConstantString.DDMMYYYY));

		List<WorkingDayReport> workList = workingDayReporter.report(account.getUuidAccount(), workingDayDetailDtoIn.getStartDate(),
				workingDayDetailDtoIn.getEndDate(), scheduleMap, hisCheckins, accountSchedule.getUuidSchedule(), tmpShifts);

		for (WorkingDayReport wdr : workList) {
			ListWorkingDayDetailDtoOut wdrDtoOut = new ListWorkingDayDetailDtoOut();
			wdrDtoOut.setDate(wdr.getDate());
			wdrDtoOut.setDayId(wdr.getDayId());

			if (!wdr.getShifts().isEmpty()) {
				if (!wdr.isCheckIn()) {
					wdrDtoOut.setWorkingStatus(Common.WORKING_DAY_STATUS.NO_CHECKIN);
				} else {
					wdrDtoOut.setWorkingStatus(wdr.isWorkingFullTime() ?
							Common.WORKING_DAY_STATUS.WORKING_FULL_TIME : Common.WORKING_DAY_STATUS.NOT_WORKING_FULL_TIME);
				}
			}

			res.add(wdrDtoOut);
		}


		return WorkingDayDetailDtoOut.builder()
				.uuidAccount(uuidAccount)
				.userCode(account.getUserCode())
				.fullName(account.getFullName())
				.data(res)
				.build();
	}

	@Override
	public WorkingDayDetailADayDtoOut workingDetailADay(WorkingDayDetailADayDtoIn wddDtoIn, String uuidAccount) {

		Date dateCheckin;
		try {
			dateCheckin = Common.convertStringToDate(wddDtoIn.getDate(), ConstantString.DDMMYYYY);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not parse date: %s", wddDtoIn.getDate()));
		}
		String startDate = Common.convertDateToString(Common.addDate(dateCheckin, -1), ConstantString.DDMMYYYY);
		String endDate = Common.convertDateToString(Common.addDate(dateCheckin, 1), ConstantString.DDMMYYYY);

		String uuidCompany = getUuidCompany();

		final Map<String, Schedule> scheduleMap = new HashMap<>();
		Map<String, Shift> shiftMap = new HashMap<>(); // Map cache shift
		cacheSchedule(scheduleMap, uuidCompany, shiftMap);
		Map<String, List<Shift>> tmpShifts = listShiftTmpAccount(uuidAccount, wddDtoIn.getDate(), wddDtoIn.getDate(), shiftMap);

		Account account = getAccountRepository.get(uuidAccount);
		AccountSchedule accountSchedule = new AccountSchedule();
		try {
			accountSchedule = scheduleRepository.getAccountSchedule(account.getUuidAccount());
		} catch (NotFoundException e) {
			return WorkingDayDetailADayDtoOut.builder()
					.userCode(account.getUserCode())
					.fullName(account.getFullName())
					.workingStatus(0)
					.workingStats(new ArrayList<>())
					.hisCheckins(new ArrayList<>())
					.build();
		}

		List<HisCheckin> listHisCheckins = checkinRepository.listCheckinAccounts(uuidAccount, startDate, endDate);

		List<WorkingDayReport> workList = workingDayReporter.report(account.getUuidAccount(), wddDtoIn.getDate(),
				wddDtoIn.getDate(), scheduleMap, listHisCheckins, accountSchedule.getUuidSchedule(), tmpShifts);

		int workingStatus = Common.WORKING_DAY_STATUS.NO_SHIFT;
		List<WorkingStatAccountDtoOut> workingStats = new ArrayList<>();
		for (WorkingDayReport wdr : workList) {
			if (!wdr.getShifts().isEmpty()) {
				if (!wdr.isCheckIn()) {
					workingStatus = Common.WORKING_DAY_STATUS.NO_CHECKIN;
				} else {
					workingStatus = wdr.isWorkingFullTime() ?
							Common.WORKING_DAY_STATUS.WORKING_FULL_TIME : Common.WORKING_DAY_STATUS.NOT_WORKING_FULL_TIME;
				}
			}
			for (Map.Entry<Shift, WorkingShiftStats> e : wdr.getWsStats().entrySet()) {
				Shift shift = e.getKey();
				WorkingShiftStats stats = e.getValue();
				WorkingStatAccountDtoOut wdrDtoOut = WorkingStatAccountDtoOut.builder()
						.uuidShift(shift.getUuidShift())
						.shiftCode(shift.getShiftCode())
						.checkIn(stats.getTimeCheckin())
						.checkOut(stats.getTimeCheckout())
						.workingTime(stats.getWorkingTime().intValue())
						.workingDay(stats.getWorkingDay())
						.lateTime(stats.getLateTime().intValue())
						.earlyTime(stats.getEarlyTime().intValue())
						.build();
				workingStats.add(wdrDtoOut);
			}
		}

		List<HisCheckinAccountDtoOut> hisCheckins = getHisCheckinDetailRepository.listCheckinAccount(account.getUuidAccount(),
				startDate, endDate, Common.HIS_CHECKIN_CHANNEL_ID.MANUAL);

		return WorkingDayDetailADayDtoOut.builder()
				.userCode(account.getUserCode())
				.fullName(account.getFullName())
				.workingStatus(workingStatus)
				.workingStats(workingStats)
				.hisCheckins(hisCheckins)
				.build();
	}

	private int parallelism(int size) {
		if (size <= ConstantString.ZERO) {
			return ConstantString.ONE;
		}
		return Math.min(size, ConstantString.MAX_PARALLEL);
	}

	private String getUuidCompany() {
		String role = dataContextHelper.getRole();
		String uuidCompany = "";
		if (ConstantString.ROLE.SUPERADMIN.equals(role)
				|| ConstantString.ROLE.OWNER.equals(role)
				|| ConstantString.ROLE.USER.equals(role)
		) {
			Company company = dataContextHelper.getCompany();
			uuidCompany = company.getUuidCompany();
		} else {
			throw new AccessDeniedException("access denied");
		}

		return uuidCompany;
	}

	private String getUuidGroup(String uuidGroupInput) {
		String uuidGroup = uuidGroupInput;
		String role = dataContextHelper.getRole();
		if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			Company company = dataContextHelper.getCompany();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Group group = getGroupByCompanyRepository.get(company.getUuidCompany());
				uuidGroup = group.getUuidGroup();
			}
		} else if (ConstantString.ROLE.OWNER.equals(role)) {
			Device device = dataContextHelper.getDevice();
			if (Common.isNullOrEmpty(uuidGroup) || ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Group group = getGroupByDeviceRepository.get(device.getUuidDevice());
				uuidGroup = group.getUuidGroup();
			}
		} else if (ConstantString.ROLE.USER.equals(role)) {
			if (ConstantString.GROUP_ALL.equals(uuidGroup)) {
				Account account = dataContextHelper.getContextAccount();
				List<AdminGroup> adminGroups = listAdminGroupRepository.list(account.getUuidAccount());
				uuidGroup = adminGroups.stream().map(AdminGroup::getUuidGroup).collect(Collectors.joining(","));
			}
		} else {
			throw new AccessDeniedException("access denied");
		}

		return uuidGroup;
	}
}
