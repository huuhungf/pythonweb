package vn.vnpt.api.validators;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.api.repository.user.GetAccountRepository;
import vn.vnpt.common.exception.NotFoundException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator of unique username
 */
public class AccountExistValidator implements ConstraintValidator<AccountExist, String> {

    @Autowired
	private GetAccountRepository accountRepository;

    @Override
    public void initialize(AccountExist constraintAnnotation) {

    }

    @Override
    public boolean isValid(String uuidAccount, ConstraintValidatorContext context) {
        // If the repository is null then return null
        if(uuidAccount == null || accountRepository == null){
            return true;
        }
        // Check if the uuidDevice is exist
        try {
            return accountRepository.get(uuidAccount) != null;
        } catch (NotFoundException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
