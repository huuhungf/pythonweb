package vn.vnpt.api.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateValidator implements ConstraintValidator<ValidateDate, String> {
	private String pattern;

	@Override
	public void initialize(ValidateDate validateDate) {
		pattern = validateDate.pattern();
	}

	@Override
	public boolean isValid(String date, ConstraintValidatorContext context) {
		if (date == null) {
			return true;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		sdf.setLenient(false);
		try {
			return date.equals(sdf.format(sdf.parse(date)));
		} catch (ParseException e) {
			return false;
		}
	}
}
