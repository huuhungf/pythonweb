package vn.vnpt.api.validators;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.api.repository.device.CheckDeviceCodeExistRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator of unique username
 */
public class DeviceExistValidator implements ConstraintValidator<DeviceExist, String> {

	@Autowired
	private CheckDeviceCodeExistRepository checkDeviceCodeExistRepository;

	@Override
	public void initialize(DeviceExist constraintAnnotation) {

	}

	@Override
	public boolean isValid(String deviceCode, ConstraintValidatorContext context) {
		if (deviceCode == null) {
			return true;
		}
		// Check if the device code is exist
		try {
			return checkDeviceCodeExistRepository.isExist(deviceCode);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
