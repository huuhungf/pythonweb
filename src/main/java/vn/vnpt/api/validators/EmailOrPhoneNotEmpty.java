package vn.vnpt.api.validators;

import vn.vnpt.common.errorcode.ErrorCode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ TYPE, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = EmailOrPhoneNotEmptyValidator.class)
public @interface EmailOrPhoneNotEmpty {
	String message() default ErrorCode.IDG_00002010;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
