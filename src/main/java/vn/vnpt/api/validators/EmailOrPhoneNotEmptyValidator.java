package vn.vnpt.api.validators;

import vn.vnpt.common.Common;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

public class EmailOrPhoneNotEmptyValidator implements ConstraintValidator<EmailOrPhoneNotEmpty, Object> {

	@Override
	public void initialize(EmailOrPhoneNotEmpty constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		try {
			Field fieldPhone = value.getClass().getDeclaredField("phoneNumber");
			fieldPhone.setAccessible(true);
			String phoneNumber = (String) fieldPhone.get(value);

			Field fieldMail = value.getClass().getDeclaredField("email");
			fieldMail.setAccessible(true);
			String email = (String) fieldMail.get(value);

			return !(Common.isNullOrEmpty(phoneNumber) && Common.isNullOrEmpty(email));
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Unsupported class: " + value.getClass());
		}
	}
}
