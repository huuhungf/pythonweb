package vn.vnpt.api.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ TYPE, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = ImageExtensionValidator.class)
@Documented
public @interface ImageExtension {

	String message() default "IDG-00000030";;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}