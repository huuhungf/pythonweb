package vn.vnpt.api.validators;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ImageExtensionValidator implements ConstraintValidator<ImageExtension, MultipartFile> {
	private final Set<String> imageContentTypes = new HashSet<>(Arrays.asList("image/png", "image/jpeg", "image/gif"));

	@Override
	public boolean isValid(MultipartFile value, ConstraintValidatorContext context) {
		return imageContentTypes.contains(value.getContentType());
	}
}
