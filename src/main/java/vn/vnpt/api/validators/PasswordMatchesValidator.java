package vn.vnpt.api.validators;

import vn.vnpt.api.dto.in.ChangePasswordDtoIn;
import vn.vnpt.api.dto.in.ConfirmForgotPasswordDtoIn;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

	@Override
	public void initialize(PasswordMatches constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if (value instanceof ChangePasswordDtoIn) {
			final ChangePasswordDtoIn changePasswordDtoIn = (ChangePasswordDtoIn) value;
			return changePasswordDtoIn.getPassword() != null && changePasswordDtoIn.getPassword().equals(changePasswordDtoIn.getPasswordConfirm());
		}

		if (value instanceof ConfirmForgotPasswordDtoIn) {
			final ConfirmForgotPasswordDtoIn confirmForgotPasswordDtoIn = (ConfirmForgotPasswordDtoIn) value;
			return confirmForgotPasswordDtoIn.getPassword() != null && confirmForgotPasswordDtoIn.getPassword().equals(confirmForgotPasswordDtoIn.getPasswordConfirm());
		}

		throw new IllegalArgumentException("Unsupported class: " + value.getClass());

	}
}
