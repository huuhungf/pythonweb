package vn.vnpt.api.validators;

import vn.vnpt.common.errorcode.ErrorCode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(value = {ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueDeviceCodeValidator.class)
@Documented
public @interface UniqueDeviceCode {
	String message() default ErrorCode.IDG_00002003;

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
}
