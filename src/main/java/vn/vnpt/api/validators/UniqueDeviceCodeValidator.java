package vn.vnpt.api.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.repository.device.GetDeviceByDeviceCodeRepository;
import vn.vnpt.common.exception.NotFoundException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueDeviceCodeValidator implements ConstraintValidator<UniqueDeviceCode, String> {

	private final GetDeviceByDeviceCodeRepository deviceByDeviceCodeRepository;

	@Autowired
	public UniqueDeviceCodeValidator(GetDeviceByDeviceCodeRepository deviceByDeviceCodeRepository) {
		this.deviceByDeviceCodeRepository = deviceByDeviceCodeRepository;
	}

	@Override
	public boolean isValid(String deviceCode, ConstraintValidatorContext context) {
		if (deviceCode == null) {
			return true;
		}
		try {
			Device device = deviceByDeviceCodeRepository.getDevice(deviceCode.toUpperCase());
			return device == null;
		} catch (NotFoundException e) {
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
