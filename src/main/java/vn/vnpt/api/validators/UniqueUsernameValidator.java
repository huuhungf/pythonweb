package vn.vnpt.api.validators;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.api.repository.user.CheckUsernameExistRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator of unique username
 */
public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, Object> {

	@Autowired
	private CheckUsernameExistRepository checkUsernameExistRepository;

	@Override
	public void initialize(UniqueUsername constraintAnnotation) {

	}

	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		if (object == null) {
			return true;
		}

		if (object instanceof String) {
			String username = ((String) object).toUpperCase();
			try {
				return !checkUsernameExistRepository.isExist(username);
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}

		throw new IllegalArgumentException("Unsupported class: " + object.getClass());
	}
}
