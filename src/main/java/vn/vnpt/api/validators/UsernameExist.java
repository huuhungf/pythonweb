package vn.vnpt.api.validators;

import vn.vnpt.common.errorcode.ErrorCode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = UsernameExistValidator.class)
@Documented
public @interface UsernameExist {
	String message() default ErrorCode.IDG_00002005;

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
}
