package vn.vnpt.api.validators;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;
import vn.vnpt.common.exception.NotFoundException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator of unique username
 */
public class UsernameExistValidator implements ConstraintValidator<UsernameExist, String> {

    @Autowired
	private GetAccountByUsernameRepository accountByUsernameRepository;

    @Override
    public void initialize(UsernameExist constraintAnnotation) {

    }

    @Override
    public boolean isValid(String username, ConstraintValidatorContext context) {
        // If the repository is null then return null
        if(username == null || accountByUsernameRepository == null) {
            return true;
        }
        // Check if the uuidDevice is exist
        try {
            return accountByUsernameRepository.getAccountByUserName(username.toUpperCase()) != null;
        } catch (NotFoundException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
