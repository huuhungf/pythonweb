package vn.vnpt.api.validators;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.common.exception.NotFoundException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator of unique username
 */
public class UuidDeviceExistValidator implements ConstraintValidator<UuidDeviceExist, String> {

    @Autowired
	private GetDeviceRepository getDeviceRepository;

    @Override
    public void initialize(UuidDeviceExist constraintAnnotation) {

    }

    @Override
    public boolean isValid(String uuidDevice, ConstraintValidatorContext context) {
        // If the repository is null then return null
        if(uuidDevice == null || getDeviceRepository == null){
            return true;
        }
        // Check if the uuidDevice is exist
        try {
            return getDeviceRepository.getDevice(uuidDevice) != null;
        } catch (NotFoundException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
