package vn.vnpt.api.validators;

import vn.vnpt.common.constant.ConstantString;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {DateValidator.class})
public @interface ValidateDate {
	String message();

	String pattern() default ConstantString.DDMMYYYY;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
