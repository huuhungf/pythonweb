package vn.vnpt.authentication;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.vnpt.common.success.model.ApiResult;

import java.util.HashMap;
import java.util.Map;

@Service
public class AdminLoginService {
	private final RestTemplate restTemplate;

	@Value("${oauth2.admin.adminLoginEndpointUri}")
	private String adminLoginUrl;

	@Value("${oauth2.admin.clientId}")
	private String adminClientId;

	public AdminLoginService(
			@Qualifier(value = "loadBalancedRestTemplate") RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public OAuth2AccessToken login(String username) {

		Map<String, String> body = new HashMap<>();
		body.put("username", username);
		body.put("clientId", adminClientId);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Map<String, String>> request = new HttpEntity<>(body, headers);

		ParameterizedTypeReference<ApiResult<OAuth2AccessToken>> typeReference = new ParameterizedTypeReference<ApiResult<OAuth2AccessToken>>() {
		};

		ApiResult<OAuth2AccessToken> result = restTemplate.exchange(adminLoginUrl, HttpMethod.POST, request, typeReference).getBody();

		return result.getObject();
	}
}
