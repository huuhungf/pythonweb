package vn.vnpt.authentication;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import vn.vnpt.common.exception.BadRequestException;

@Service
public class AppLoginService {
	private final RestTemplate restTemplate;

	@Value("${oauth2.app.appLoginEndpointUri}")
	private String appLoginUrl;

	@Value("${oauth2.app.clientId}")
	private String appClientId;

	@Value("${oauth2.app.secret}")
	private String appSecret;

	public AppLoginService(
			@Qualifier(value = "loadBalancedRestTemplate") RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public OAuth2AccessToken login(String username, String password) {

		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("username", username);
		body.add("password", password);
		body.add("grant_type", "password");
		body.add("client_id", appClientId);
		body.add("client_secret", appSecret);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);

		ParameterizedTypeReference<OAuth2AccessToken> typeReference = new ParameterizedTypeReference<OAuth2AccessToken>() {
		};

		OAuth2AccessToken accessToken = null;
		try {
			accessToken = restTemplate.exchange(appLoginUrl, HttpMethod.POST, request, typeReference).getBody();
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpClientErrorException clientErrorException = (HttpClientErrorException) e;
				if (clientErrorException.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
					throw new BadRequestException("Bad credentials");
				}
			}

			throw e;
		}

		return accessToken;
	}
}
