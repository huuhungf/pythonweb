/*******************************************************************************
 * Copyright (c) 2017 ANHTCN.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package vn.vnpt.authentication;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import vn.vnpt.common.Common;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author trananh
 */
public class MyEncryptPassword implements PasswordEncoder {

	/* (non-Javadoc)
	 * @see org.springframework.security.crypto.password.PasswordEncoder#encode(java.lang.CharSequence)
	 */
	@Override
	public String encode(CharSequence rawPassword) {

		// TODO Auto-generated method stub
		return Common.encryptPassword(rawPassword.toString());
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.crypto.password.PasswordEncoder#matches(java.lang.CharSequence, java.lang.String)
	 */
	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return matchesBcrypt(rawPassword, encodedPassword)
				|| matchesSha1(rawPassword, encodedPassword);
	}

	private boolean matchesSha1(CharSequence rawPassword, String encodedPassword) {
		try {
			byte[] encoded = MessageDigest.getInstance("SHA-1").digest(rawPassword.toString().getBytes());
			byte[] hash = DatatypeConverter.parseHexBinary(encodedPassword);
			return Arrays.equals(hash, encoded);
		} catch (NoSuchAlgorithmException | IllegalArgumentException e) {
			return false;
		}
	}

	private boolean matchesBcrypt(CharSequence rawPassword, String encodedPassword) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.matches(rawPassword, encodedPassword);
	}

}
