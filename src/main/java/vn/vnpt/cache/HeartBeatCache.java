package vn.vnpt.cache;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class HeartBeatCache implements Serializable {
	private Date time;
	private Integer battery;
	private Float lat;
	private Float lon;
	private String wifi;
	private String appVersion;
}
