package vn.vnpt.cache.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableConfigurationProperties(value = {CustomCacheProperties.class})
public class CacheConfig {
	private final CustomCacheProperties customCacheProperties;

	@Autowired
	public CacheConfig(CustomCacheProperties customCacheProperties) {
		this.customCacheProperties = customCacheProperties;
	}

	@Bean
	public RedisCacheManager redisCacheManager(RedisConnectionFactory redisConnectionFactory) {

		Map<String, RedisCacheConfiguration> configMap = new HashMap<>();

		for (String cacheName : customCacheProperties.getCacheNames()) {
			RedisCacheConfiguration configuration = RedisCacheConfiguration
					.defaultCacheConfig()
					.disableCachingNullValues()
					.entryTtl(Duration.ofMinutes(2));

			CacheProperties.Redis redis = customCacheProperties.getRedis().get(cacheName);
			if (redis != null) {
				configuration = configuration.entryTtl(redis.getTimeToLive());
			}

			configMap.put(cacheName, configuration);
		}

		RedisCacheConfiguration defaultConfig = RedisCacheConfiguration
				.defaultCacheConfig()
				.disableCachingNullValues()
				.entryTtl(Duration.ofMinutes(2));

		RedisCacheManager cacheManager = RedisCacheManager
				.builder(redisConnectionFactory)
				.initialCacheNames(configMap.keySet())
				.withInitialCacheConfigurations(configMap)
				.cacheDefaults(defaultConfig)
				.build();
		return cacheManager;
	}
}
