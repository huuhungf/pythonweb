package vn.vnpt.cache.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "cache")
@Data
public class CustomCacheProperties {
	private List<String> cacheNames = new ArrayList<>();
	private Map<String, CacheProperties.Redis> redis;
}
