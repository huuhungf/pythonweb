package vn.vnpt.common;

import io.sentry.Sentry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.async.DeferredResult;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.*;
import vn.vnpt.common.exception.model.ObjectReturn;
import vn.vnpt.common.response.ResponseEntites;
import vn.vnpt.common.success.model.ApiResult;
import vn.vnpt.context.DataContextHelper;

import java.util.concurrent.ForkJoinPool;

public abstract class AbstractResponseController {

	private final ResponseEntites<Object> responseEntites;

	@Autowired
	protected DataContextHelper dataContextHelper;

	protected AbstractResponseController(ResponseEntites<Object> responseEntites) {
		this.responseEntites = responseEntites;
	}

	public DeferredResult<ResponseEntity<?>> responseEntityDeferredResult(CallbackFunction<?> callbackFunction) {
		DeferredResult<ResponseEntity<?>> deferredResult = new DeferredResult<>(ConstantString.TIMEOUT_NONBLOCK);
		deferredResult.onTimeout(() -> deferredResult.setErrorResult(
				responseEntites.createErrorResponse(HttpStatus.REQUEST_TIMEOUT, ErrorCode.IDG_00000408)));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		ForkJoinPool forkJoinPool = new ForkJoinPool();
		forkJoinPool.submit(() -> {
			try {
				// Set authentication for new thread because of ThreadLocal
				SecurityContextHolder.getContext().setAuthentication(authentication);
				dataContextHelper.init(authentication);
				deferredResult.setResult(responseEntites.createSuccessResponse(HttpStatus.OK,
						new ApiResult<>(ErrorCode.IDG_00000000, callbackFunction.execute())));
			} catch (NotFoundException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.NOT_FOUND,
						ex.getErrorCode(), ex.getMessage().trim()));
				Sentry.captureException(ex);
			} catch (BadRequestException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.BAD_REQUEST,
						ex.getErrorCode(), ex.getMessage().trim()));
				Sentry.captureException(ex);
			} catch (IncorrectPasswordException | PasswordInvalidException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.BAD_REQUEST,
						ErrorCode.IDG_00000032, ex.getMessage().trim()));
				Sentry.captureException(ex);
			} catch (AccountChannelInactiveException | ExceedMaxUserOttException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.BAD_REQUEST,
						ErrorCode.IDG_00000400, ex.getMessage().trim()));
				Sentry.captureException(ex);
			} catch (AccessDeniedException | PrivilegeUpdateUserException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.FORBIDDEN,
						ErrorCode.IDG_00000403, ex.getMessage().trim()));
				Sentry.captureException(ex);
			} catch (Exception e) {
				e.printStackTrace();
				deferredResult.setErrorResult(responseEntites.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						ErrorCode.IDG_00000500, Common.subString(e.getMessage())));
				Sentry.captureException(e);
			} finally {
				dataContextHelper.clear();
			}
		});

		forkJoinPool.shutdown();
		return deferredResult;
	}

	public DeferredResult<ResponseEntity<?>> responseEntityDeferredLogResult(CallbackFunction<?> callbackFunction) {
		DeferredResult<ResponseEntity<?>> deferredResult = new DeferredResult<>(ConstantString.TIMEOUT_NONBLOCK);
		deferredResult.onTimeout(() -> deferredResult.setErrorResult(
				responseEntites.createErrorResponse(HttpStatus.REQUEST_TIMEOUT, ErrorCode.IDG_00000408)));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		ForkJoinPool forkJoinPool = new ForkJoinPool();
		forkJoinPool.submit(() -> {
			try {
				SecurityContextHolder.getContext().setAuthentication(authentication);
				dataContextHelper.init(authentication);
				ObjectReturn sendResult = (ObjectReturn) callbackFunction.execute();
				deferredResult.setResult(
						responseEntites.createSuccessResponse(sendResult.getStatus(), sendResult.getMessage()));
			} catch (NotFoundException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.NOT_FOUND,
						ex.getErrorCode(), ex.getMessage().trim()));
			} catch (IncorrectPasswordException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.BAD_REQUEST,
						ErrorCode.IDG_00000032, ex.getMessage().trim()));
			} catch (Exception ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						ErrorCode.IDG_00000500, Common.subString(ex.toString().trim())));
			} finally {
				dataContextHelper.clear();
			}
		});

		forkJoinPool.shutdown();
		return deferredResult;
	}

	public DeferredResult<ResponseEntity<?>> responseEntityDeferredResultCommon(CallbackFunction<?> callbackFunction) {
		DeferredResult<ResponseEntity<?>> deferredResult = new DeferredResult<>(ConstantString.TIMEOUT_NONBLOCK);
		deferredResult.onTimeout(() -> deferredResult.setErrorResult(
				responseEntites.createErrorResponse(HttpStatus.REQUEST_TIMEOUT, ErrorCode.IDG_00000408)));
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		ForkJoinPool forkJoinPool = new ForkJoinPool();
		forkJoinPool.submit(() -> {
			try {
				SecurityContextHolder.getContext().setAuthentication(authentication);
				dataContextHelper.init(authentication);
				deferredResult.setResult(responseEntites.createSuccessResponse(HttpStatus.OK,
						new ApiResult<>(ErrorCode.IDG_00000000, callbackFunction.execute())));
			} catch (NotFoundException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.NOT_FOUND,
						ex.getErrorCode(), ex.getMessage().trim()));
			} catch (IncorrectPasswordException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.BAD_REQUEST,
						ErrorCode.IDG_00000032, ex.getMessage().trim()));
			} catch (RuntimeException ex) {
				deferredResult.setResult(responseEntites.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						ErrorCode.IDG_00000500, Common.subString(ex.toString().trim())));
			} finally {
				dataContextHelper.clear();
			}
		});

		forkJoinPool.shutdown();
		return deferredResult;
	}
}
