/*******************************************************************************
 * Copyright (c) 2017 ANHTCN.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package vn.vnpt.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.uuid.Generators;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.nimbusds.jose.util.StandardCharset;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import vn.vnpt.api.model.Policy;
import vn.vnpt.common.constant.ConstantString;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author trananh
 */
public class Common {

	public static final class MINIO_CLIENT {

		public static final long CONNECT_TIMEOUT_MS = 60000;
		public static final long WRITE_TIMEOUT_MS = 60000;
		public static final long READ_TIMEOUT_MS = 60000;
	}

	public static final class STORAGE_MODE {

		public static final String MINIO = "minio";
		public static final String IPFS = "ipfs";
		public static final String FILE_SYSTEM = "file_system";
	}

	public static final class FILE_TITLE {

		public static final String IDG = "idg";
		public static final String IDGV2 = "idg-";
		public static final String DIGDOC = "digdoc";
	}

	public static final class NOTIFICATION_TYPE {
		public static final String CHECKIN_BY_FACE = "1";
		public static final String CHECKIN_BY_QR = "2";
		public static final String TABLET_SYNC_ACCOUNT = "3";
		public static final String TABLET_SYNC_CONFIG = "4";
	}

	public static final class DEVICE_LICENSE_STATUS {
		public static final int INITIAL = 0;
		public static final int ACTIVE = 1;
		public static final int DELETED = 2;
		public static final int LOCK = 3;
		public static final int DISCONNECTED = 4;
		public static final int PENDING = 5;
	}

	public static final class DEVICE_TOKEN_STATUS {
		public static final int ACTIVE = 1;
		public static final int INACTIVE = 2;
		public static final int LOCK = 3;
	}

	public static final class ACCOUNT_CHANNEL_STATUS {
		public static final int ACTIVE = 1;
		public static final int INACTIVE = 2;
	}

	public static final class COMPANY_ALWAY_ACTIVE {
		public static final int DISABLE = 0;
		public static final int ENABLE = 1;
	}

	public static final class COMPANY_NOTIFICATION {
		public static final int DISABLE = 0;
		public static final int ENABLE = 1;
	}

	public static final class ACCOUNT_SYNC_STATUS {
		public static final int CREATE_UPDATE = 1;
		public static final int DELETE = 3;
	}

	public static final class DEVICE_LICENSE_FACE_MASK {
		public static final int ENABLE = 1;
		public static final int DISABLE = 0;
	}

	public static final class DEVICE_LICENSE_FACE_LIVENESS {
		public static final int ENABLE = 1;
		public static final int DISABLE = 0;
	}

	public static final class DEVICE_LICENSE_VIRTUAL_ASSISTANT {
		public static final int ENABLE = 1;
		public static final int DISABLE = 0;
	}

	public static final class CHANNEL_CATEGORY {
		public static final String VIBER = "1";
		public static final String TELEGRAM = "2";
		public static final String FACEBOOK = "3";
		public static final String ZALO = "4";
		public static final String WEBHOOK = "5";
	}

	public static final class PLAN_TYPE {
		public static final int TRIAL = 1;
		public static final int PRO = 2;
		public static final int ENTERPRISE = 3;
	}

	public static final class PLAN_PRO {
		public static final int DEFAULT_USER = 50;
		public static final int MAX_USER = 300;
		public static final int DEFAULT_DURATION = 3;
		public static final int FREE_DEVICE = 5;
		public static final int MAX_DEVICE = 200;
		public static final int MAX_ADDON_OTT = 1000;
	}

	public static final class SCHEDULE_CYCLE_MODE {
		public static final int WEEK = 0;
		public static final int MONTH = 1;
		public static final int YEAR = 2;
	}

	public static final class IMAGE_TYPE {
		public static final int UPLOAD = 1;
		public static final int TABLET = 2;
	}

	public static final class ROUND_TYPE {
		public static final int UP = 1;
		public static final int DOWN = 2;
	}

	public static final class ACCOUNT_PLAN_STATUS {
		public static final int ACTIVE = 1;
		public static final int INACTIVE = 2;
		public static final int EXPIRE = 3;
	}

	public static final class WORKING_DAY_STATUS {
		public static final int NO_SHIFT = 1;
		public static final int NO_CHECKIN = 2;
		public static final int WORKING_FULL_TIME = 3;
		public static final int NOT_WORKING_FULL_TIME = 4;
	}

	public static final class HIS_CHECKIN_CHANNEL_ID {
		public static final int ALL = 0;
		public static final int MANUAL = 3;
	}


	public static String GenerateUUID() {
		UUID uuid2 = Generators.timeBasedGenerator().generate();
		return uuid2.toString();
	}

	public static String encryptPassword(String passwordToHash) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(passwordToHash);
//		try {
//			byte[] encoded = MessageDigest.getInstance("SHA-1").digest(passwordToHash.getBytes());
//			StringBuilder sb = new StringBuilder();
//			for (int i = 0; i < encoded.length; i++) {
//				sb.append(Integer.toString((encoded[i] & 0xff) + 0x100, 16).substring(1));
//			}
//
//			return sb.toString();
//		} catch (NoSuchAlgorithmException e) {
//			return "";
//		}
	}

	public static String subString(String str) {
		if (isNullOrEmpty(str))
			return "";

		if (str.length() > 100) {
			return str.substring(ConstantsString.ZERO, ConstantsString.MAX_SIZE);
		}
		return str;

	}

	public static Timestamp convertStringToTimestamp(String str_date) {
		try {
			DateFormat formatter;
			formatter = new SimpleDateFormat("MM/dd/yyyy");
			// you can change format of date
			Date date = formatter.parse(str_date);
			java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

			return timeStampDate;
		} catch (ParseException e) {
			System.out.println("Exception :" + e);
			return null;
		}
	}

//	public static String imageConvertMethod(String url) throws Exception {
//		ByteArrayOutputStream output = new ByteArrayOutputStream();
//
//		try (InputStream input = new URL(url).openStream()) {
//			byte[] buffer = new byte[512];
//			for (int length = 0; (length = input.read(buffer)) > 0;) {
//				output.write(buffer, 0, length);
//			}
//		}
//
//		byte[] byte_array = output.toByteArray();
//
//		byte[] img64 = com.sun.jersey.core.util.Base64.encode(byte_array);
//		String imageString = new String(img64);
//
//		return imageString;
//	}

	public static final Date getCurrentDate() {
		return DateUtils.truncate(new Date(), java.util.Calendar.DAY_OF_MONTH);
	}

	public static Date getCurrentTime() {
		return new Date();
	}

	public static long getCurrentTimeAddFive() {
		return System.currentTimeMillis() + 5 * 60 * 1000;
	}

	public static long getCurrentTimeLong() {
		return System.currentTimeMillis();
	}

	public static String convertDateToString(Date date, String pattern) {

		if (date == null) {
			return "";
		} else {
			SimpleDateFormat plainDateFormat = new SimpleDateFormat(pattern);
			return plainDateFormat.format(date);
		}
	}

	public static Date convertStringToDate(String date, String pattern) throws ParseException {

		if (date == null || pattern == null) {
			return null;
		} else {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			return simpleDateFormat.parse(date);
		}

	}

	public static BufferedImage decodeToImage(String imageString) throws IOException {
		BufferedImage image = null;
		// byte[] imageByte;
		// BASE64Decoder decoder = new BASE64Decoder();
		Base64.Decoder base64Decoder = Base64.getDecoder();
		byte[] imageByte = base64Decoder.decode(imageString.getBytes("UTF-8"));

		// imageByte = decoder.decodeBuffer(imageString);
		ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
		image = ImageIO.read(bis);
		bis.close();
		return image;
	}

	public static String getImageType(String base64) {
		String[] header = base64.split("[;]");
		if (header == null || header.length == 0)
			return null;
		return header[0].split("[/]")[1];
	}

	public static String removeBase64Header(String base64) {
		if (base64 == null)
			return null;
		return base64.trim().replaceFirst("data[:]image[/]([a-z])+;base64,", "");
	}

	public static String createFileImage(String base64, String fileName) throws IOException {
		// convert base64 string to image
		try {
			String imageString = removeBase64Header(base64);
			String imageType = getImageType(base64);
			BufferedImage image = decodeToImage(imageString);

			ImageIO.write(image, imageType,
					new File("src/main/resources/static/image_product/" + fileName + "." + imageType));
			return "image_product/" + fileName + "." + imageType;
		} catch (Exception e) {
			return null;
		}

	}

	@SuppressWarnings("finally")
	public static Timestamp convertStringToTimestamp1(String something) throws ParseException {
		Timestamp timestamp = null;
		Date parsedDate;
		SimpleDateFormat dateFormat = null;
		if (something == null || something.equals("")) {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			timestamp = new java.sql.Timestamp(dateFormat.parse("1990-01-01 00:00:00").getTime());
		} else {
			if (something.contains(".")) {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			} else if (something.contains(",")) {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SSS");
			} else {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
		}
		try {
			parsedDate = dateFormat.parse(something);
			timestamp = new java.sql.Timestamp(parsedDate.getTime());

		} catch (ParseException e) {
			timestamp = new java.sql.Timestamp(dateFormat.parse("1990-01-01 00:00:00").getTime());
			System.out.println(e.toString());
		} finally {
			return timestamp;
		}

	}

	public static String getAlphaNumeric(int len) {
		char[] ch = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
				'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
				'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
				'z'};

		char[] c = new char[len];
		Random random = new Random();
		for (int i = 0; i < len; i++) {
			c[i] = ch[random.nextInt(ch.length)];
		}

		return new String(c);
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}

	/**
	 * Check if String with spaces is Empty or Null
	 *
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str) {
		if (str != null && !str.trim().isEmpty())
			return false;
		return true;
	}

	public static String getCurrentDay() {
		String pattern = "yyyyMMdd";
		DateFormat df = new SimpleDateFormat(pattern);
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date today = new Date();

		return df.format(today);
	}

	public static <T> T castObject(Object obj, T defaultIfNull) {
		if (obj == null) {
			return defaultIfNull;
		}

		@SuppressWarnings("unchecked")
		T res = (T) obj;
		return res;
	}

	public static String replaceDotInUserName(String userName) {
		if (userName == null) {
			return null;
		}
		String result = userName.replace('.', '&');
		return result;
	}

	public static BufferedImage generateQRCodeImage(String barcodeText) throws Exception {
		QRCodeWriter barcodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.QR_CODE, 200, 200);

		return MatrixToImageWriter.toBufferedImage(bitMatrix);
	}

	public static String generateLicenseKey() {
		String sample = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder res = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 8; i++) {
			res.append(sample.charAt(random.nextInt(sample.length())));
		}

		return res.toString();
	}

	public static String generatePassword() {
		String sample = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder res = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 6; i++) {
			res.append(sample.charAt(random.nextInt(sample.length())));
		}

		return res.toString();
	}

	public static boolean isMatchPattern(String input, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(input);
		return matcher.matches();
	}

	public static <T> T getDefaultIfNull(T obj, T defaultIfNull) {
		if (obj == null) {
			return defaultIfNull;
		}
		return obj;
	}

	public static String getText2Speech(String gender, String fullName) {
		if (gender == null) {
			return ConstantString.TEXT2SPEECH_GREET + " " + fullName;
		}

		if (EnumGender.MALE.name().equalsIgnoreCase(gender)) {
			return ConstantString.TEXT2SPEECH_GREET + " anh " + fullName;
		} else if (EnumGender.FEMALE.name().equalsIgnoreCase(gender)) {
			return ConstantString.TEXT2SPEECH_GREET + " chị " + fullName;
		}

		return ConstantString.TEXT2SPEECH_GREET + " " + fullName;
	}

	public static Integer remainDays(Date current, Date expire) {
		Date truncCurrent = DateUtils.truncate(current, Calendar.DAY_OF_MONTH);
		Date truncExpire = DateUtils.truncate(expire, Calendar.DAY_OF_MONTH);

		if (truncExpire.before(truncCurrent)) {
			return 0;
		}

		return (int) ((truncExpire.getTime() - truncCurrent.getTime()) / 1000 / 86400) + 1;
	}

	public static String base64(byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		return new String(Base64.getEncoder().encode(bytes), StandardCharset.UTF_8);
	}

	public static String getUsername(String companyCode, String userCode) {
		return (companyCode + "_" + userCode).toUpperCase();
	}

	public static String lowerCase(String s) {
		if (s == null) {
			return null;
		}
		return s.toLowerCase();
	}

	public static String upperCase(String s) {
		if (s == null) {
			return null;
		}
		return s.toUpperCase();
	}

	public static <T> T parsePolicyConfig(ObjectMapper objectMapper, Policy policy, Class<T> tClass) {
		T config;
		try {
			config = objectMapper.readValue(policy.getConfiguration(), tClass);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not parse json %s", policy.getConfiguration()));
		}

		return config;
	}

	public static Integer getDayOfWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	public static Integer getDayOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	public static Integer getMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}

	public static Integer getDayOfWeek(String date, String pattern) {
		try {
			return getDayOfWeek(convertStringToDate(date, pattern));
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not get day of week: %s, %s", date, pattern));
		}
	}

	public static Integer getDayOfMonth(String date, String pattern) {
		try {
			return getDayOfMonth(convertStringToDate(date, pattern));
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not get day of month: %s, %s", date, pattern));
		}
	}

	public static Integer getMonth(String date, String pattern) {
		try {
			return getMonth(convertStringToDate(date, pattern));
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not get month: %s, %s", date, pattern));
		}
	}

	public static Date addDate(Date date, Integer numDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, numDate);
		return calendar.getTime();
	}

	public static Integer getTimeInSec(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);

		return hour * 3600 + min * 60 + sec;
	}

	public static Integer getTimeInSec(String date, String pattern) {
		try {
			return getTimeInSec(Common.convertStringToDate(date, pattern));
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not get time in sec: %s, %s", date, pattern));
		}
	}

	public static Integer getTimeInSec(String hhmm) {
		String[] arr = hhmm.split(":");
		if (arr.length < 2) {
			throw new RuntimeException(String.format("could not get time in sec: %s", hhmm));
		}

		int hour = 0;
		int min = 0;

		try {
			hour = Integer.parseInt(arr[0]);
			min = Integer.parseInt(arr[1]);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not get time in sec: %s", hhmm));
		}

		return hour * 3600 + min * 60;
	}

	public static String convertSecToHhMmSs(Integer sec) {
		int hour = sec / 3600;
		sec = sec % 3600;
		int min = sec / 60;
		sec = sec % 60;

		StringBuilder sb = new StringBuilder();
		if (hour < 10) {
			sb.append("0");
		}
		sb.append(hour).append(":");
		if (min < 10) {
			sb.append("0");
		}
		sb.append(min).append(":");
		if (sec < 10) {
			sb.append("0");
		}
		sb.append(sec);

		return sb.toString();
	}

	public static String convertSecToHhMm(Integer sec) {
		int hour = sec / 3600;
		sec = sec % 3600;
		int min = sec / 60;

		StringBuilder sb = new StringBuilder();
		if (hour < 10) {
			sb.append("0");
		}
		sb.append(hour).append(":");
		if (min < 10) {
			sb.append("0");
		}
		sb.append(min);

		return sb.toString();
	}

	public static Long timeToTsInSec(String date, String pattern) {
		try {
			Date d = convertStringToDate(date, pattern);
			return d.getTime() / 1000;
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not timeToTimeStampInSec: %s", date));
		}
	}

	public static Long timeToTsInSec(Date date) {
		return date.getTime() / 1000;
	}
}
