package vn.vnpt.common;

public class ConstantsString {

	public static final long TIMEOUT_NONBLOCK = 300000l;

	public static final int ZERO = 0;

	public static final int MAX_SIZE = 100;

	public static String AUTHORIZATION = "Authorization";

	public static String AUTHORIZATION_LOWER = "authorization";

	public static String BEARER = "Bearer";

	public static String EMAIL_CATEGORY_CREATE_ACCOUNT = "11";
	public static String EMAIL_CATEGORY_FORGOT_PASSWORD = "12";
	public static String EMAIL_CATEGORY_REGISTER_ACCOUNT = "13";
	public static String EMAIL_CATEGORY_CREATE_ACCOUNT_FOR_SUPER_ADMIN = "14";
	public static String EMAIL_CATEGORY_REGISTER_ACCOUNT_EXTERNAL = "15";
	public static String EMAIL_CATEGORY_CONFIRM_REGISTER_ACCOUNT_AGENT = "16";
	public static String EMAIL_CATEGORY_CHANGE_USERNAME = "17";
	public static String EMAIL_CHANNEL_CODE = "iattend";
	public static String EMAIL_STATUS_REGISTERED = "<b>(Đã đăng ký)<b>";
	public static String EMAIL_STATUS_UNREGISTERED = "(Chưa đăng ký)";

	public static class CHANNEL_NAME {
		public static final String TELEGRAM = "telegram";
		public static final String ZALO = "zalo";
		public static final String VIBER = "viber";
		public static final String FACEBOOK = "facebook";
	}
}
