package vn.vnpt.common;

import org.springframework.stereotype.Component;
import vn.vnpt.common.success.model.PagingDTO;

import java.lang.reflect.Field;

@Component
public class ConvertToObject {

	private static final int MAXSIZE = 10;

	private static final String NULL_DATA = "";

	private static final String SORT_ASC = "ASC";

	private static final String SORT_DESC = "DESC";

	private static final int PAGE_DEFAULT = 1;

	public static PagingDTO<?> convertPagingAndSort(int page, int maxSize, String sort, String propertiesSort,
			Class<?> className) {

		PagingDTO<?> pagingDTO = new PagingDTO<Object>();
		pagingDTO.setPage(page);

		if (page == 0) {
			pagingDTO.setPage(PAGE_DEFAULT);
		} else {
			pagingDTO.setPage(page);
		}

		if (propertiesSort == null || NULL_DATA.equals(propertiesSort)) {

			pagingDTO.setPropertiesSort(getFieldNames(className));
		} else {
			pagingDTO.setPropertiesSort(propertiesSort);
		}

		if (sort == null || NULL_DATA.equals(sort) || SORT_ASC.equals(sort)) {
			pagingDTO.setSort(SORT_ASC);
		} else {
			pagingDTO.setSort(SORT_DESC);
		}

		if (maxSize == 0) {
			pagingDTO.setMaxSize(MAXSIZE);
		} else {
			pagingDTO.setMaxSize(maxSize);
		}

		return pagingDTO;
	}

	private static String getFieldNames(Class<?> className) {
		Class<? extends Object> c = className;
		Field[] fields = c.getDeclaredFields();
		for (Field field : fields) {
			String name = field.getName();
			return name;
		}
		return "";
	}

}
