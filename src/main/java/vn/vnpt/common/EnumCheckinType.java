package vn.vnpt.common;

public enum  EnumCheckinType {
	FACE(1),
	QR(2);

	private final int value;

	EnumCheckinType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
