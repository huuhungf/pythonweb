package vn.vnpt.common;

public enum EnumUploadType {
	STRANGER,
	ROLL_CALL,
	LOG_CHECKIN
}
