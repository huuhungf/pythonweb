package vn.vnpt.common;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class RsaUtil {

	private static final KeyPairGenerator keyPairGenerator = getKeyPairGenerator();
	private static final SecureRandom random = new SecureRandom();

	public static KeyPair getKeyPair() {
		return keyPairGenerator.genKeyPair();
	}

	public static String encrypt(String content, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		byte[] contentBytes = content.getBytes();
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] cipherBytes = cipher.doFinal(contentBytes);
		String encoded = Base64.getEncoder().encodeToString(cipherBytes);
		return encoded;
	}

	public static String decrypt(String cipherContent, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
		byte[] cipherBytes = Base64.getDecoder().decode(cipherContent);
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decryptBytes = cipher.doFinal(cipherBytes);
		String decoded = new String(decryptBytes);
		return decoded;
	}

	public static String encodeKey(Key key) {
		byte[] bytes = key.getEncoded();
		return Base64.getEncoder().encodeToString(bytes);
	}

	public static PublicKey decodePublicKey(String keyStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] keyBytes = Base64.getDecoder().decode(keyStr);
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyFactory.generatePublic(spec);
		return publicKey;
	}

	public static PrivateKey decodePrivateKey(String keyStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] keyBytes = Base64.getDecoder().decode(keyStr);
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = keyFactory.generatePrivate(spec);
		return privateKey;
	}

	private static KeyPairGenerator getKeyPairGenerator() {
		try {
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(1024, random);
			return keyPairGenerator;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("could not init key pair generator");
		}
	}
}
