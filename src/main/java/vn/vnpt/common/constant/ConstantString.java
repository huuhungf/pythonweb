package vn.vnpt.common.constant;

public class ConstantString {

	public static final long TIMEOUT_NONBLOCK = 300000L;

	public static String TOKEN_ID = "token-id";

	public static String TOKEN_KEY = "token-key";

	public static String MAC_ADDRESS = "mac-address";

	public static String AUTHORIZATION = "authorization";

	public static final int SECONDS_IN_A_DAY = 86400;
	public static final int SECONDS_IN_A_MIN = 60;

	public static final int ZERO = 0;

	public static final int ONE = 1;

	public static final int MAX_PAGE_SIZE = 100;

	public static final int MAX_PARALLEL = 100;

	public static final String HEADER_LOG_TRANSACTION_ID = "Transaction Id";

	public static final String HEADER_LOG_TOKEN_ID = "Token Id";

	public static final String HEADER_LOG_PATH = "Path";

	public static final String HEADER_LOG_REQUEST_BODY = "Request Body";

	public static final String HEADER_LOG_RESPONSE_BODY = "Response Body";

	public static final String HEADER_LOG_STATUS_CODE = "Status Code";

	public static final String HEADER_LOG_TIME = "Log Time";

	public static final String EXCEPTION_AMOUNT_OF_PROJECT = "Amount of projects exceeds 5";

	public static final String EXCEPTION_UNIQUE_NAME_PROJECT = "Can not create duplicate project name";

	public static final String EXCEPTION_ADMIN_NOT_LEAVE_GROUP = "The administrator cannot leave the group";

	public static final String EXCEPTION_USER_ALREADY_IN_PROJECT = "User already in project !";

	public static final String ERROR_FILE_NOT_EXIST = "Folder not exist";

	public static final String EXCEPTION_INVALID_DATE = "Invalid date input";

	public static final String DATE_BEGIN_DEPLOY = "01/07/2019";

	public static final String EXCEPTION_PREVIOUS_YEAR_BEFORE_DEPLOY_TIME = "Hệ thống chưa triển khai vì vậy chưa có số liệu bạn yêu cầu";

	public static final String EXCEPTION_INVALID_OR_EXPIRED_ACCESS_TOKEN = "invalid or expired access token";

	public static final String SCHEDULE_DAILY_REPORT = "0 0 2 * * ?";

	public static final String USER_NAME = "user_name";

	public static final String DDMMYYYYHHMMSS = "dd/MM/yyyy HH:mm:ss";

	public static final String DDMMYYYY = "dd/MM/yyyy";

	public static final String DDMMYYYYHH = "dd/MM/yyyy HH";
	public static final String DDMMYYYYHHMM = "dd/MM/yyyy HH:mm";

	public static final String HHMM = "HH:mm";

	public static final String HHMMSS = "HH:mm:ss";


	public static final String VNPT_CHECKIN = "VNPT CHECKIN";

	public static final String REDIS_KEY_EVENT_EXPIRED_CHANNEL = "__keyevent@0__:expired";

	public static final String TEXT2SPEECH_GREET = "xin chào";

	public static final String SUB_FOLDER_SUPER_ADMIN = "super-admin";

	public static final String TIME_ZONE_GMT7 = "GMT+7";

	public static final String GROUP_ALL = "ALL";

	public static final String RESOLVER_SYSTEM = "(Hệ thống)";
	public static final String RESOLVER_ADMIN = "(Quản trị viên)";
	public static final String RESOLVER_DELETED = "(Tài khoản đã xóa)";

	public static final class LIST_HIS_CHECKIN_TYPE {
		public static final String ALL = "ALL";
		public static final String SINGLE = "SINGLE";
	}

	public static final class ROLE {
		public static final String OWNER = "OWNER";
		public static final String ADMIN = "ADMIN";
		public static final String SUPERADMIN = "SUPERADMIN";
		public static final String EXPORTER = "EXPORTER";
		public static final String USER = "USER";

	}

	public static final class STATUS_COMPLAINT {
		public static final int PENDING = 1;
		public static final int RESOLVED = 2;
		public static final int REJECT = 3;

	}

	public static final class STATUS_DB {
		public static final String SUCCESS = "SUCCESS";
		public static final String NOT_FOUND = "NOT_FOUND";
		public static final String ACCESS_DENIED = "ACCESS_DENIED";
		public static final String ROLE_NOT_EXIST = "ROLE_NOT_EXIST";
		public static final String DEVICE_NOT_EXIST = "DEVICE_NOT_EXIST";
		public static final String USER_NOT_EXIST = "USER_NOT_EXIST";
		public static final String DEVICE_LICENSE_NOT_EXIST = "DEVICE_LICENSE_NOT_EXIST";
		public static final String ADMIN_NOT_OWN_DEVICE = "ADMIN_USER_DO_NOT_HAVE_PRIVILEGE_TO_CREATE_USER";
		public static final String ACCOUNT_NOT_EXIST = "ACCOUNT_NOT_EXIST";
		public static final String SUPERADMIN_NOT_EXIST = "SUPERADMIN_NOT_EXIST";
		public static final String SUPERADMIN_NOT_HAVE_PRIVILEGE = "SUPERADMIN_NOT_HAVE_PRIVILEGE";
		public static final String PASSWORD_INVALID = "PASSWORD_INVALID";
		public static final String DEVICE_LICENSE_EXIST = "DEVICE_LICENSE_EXIST";
		public static final String DEVICE_TOKEN_NOT_EXIST = "TOKEN_NOT_EXIST";
		public static final String COMPANY_NOT_EXIST = "COMPANY_NOT_EXIST";
		public static final String CHANNEL_NOT_EXIST = "CHANNEL_NOT_EXIST";
		public static final String CONFIRMATION_TOKEN_NOT_EXIST = "CONFIRMATION_TOKEN_NOT_EXIST";
		public static final String ACCOUNT_CHANNEL_INACTIVE = "ACCOUNT_CHANNEL_INACTIVE";
		public static final String EXCEED_MAX_USER_OTT = "EXCEED_MAX_USER_OTT";
		public static final String UNREGISTERED_ACCOUNT_OTT = "UNREGISTERED_ACCOUNT_OTT";
		public static final String STRANGER_NOT_EXIST = "STRANGER_NOT_EXIST";
		public static final String USERNAME_ALREADY_EXIST = "USERNAME_ALREADY_EXIST";
		public static final String SUPERADMIN_EMAIL_ALREADY_EXIST = "SUPERADMIN_EMAIL_ALREADY_EXIST";
		public static final String NOT_ALLOWED = "NOT_ALLOWED";
		public static final String DEVICE_CODE_ALREADY_EXIST = "DEVICE_CODE_ALREADY_EXIST";
		public static final String GROUP_ALREADY_ASSIGNED = "GROUP_ALREADY_ASSINGED";
		public static final String CHILD_GROUP_ALREADY_ASSIGNED = "CHILD_GROUP_ALREADY_ASSINGED";
		public static final String C_RESULT_PRIVILEGE_UPDATE_USER = "ADMIN_USER_DO_NOT_HAVE_PRIVILEGE_TO_UPDATE_USER";
		public static final String C_RESULT_SYSTEM_ERROR = "SYSTEM_ERROR";
		public static final String COMPANY_CODE_ALREADY_EXIST = "COMPANY_CODE_ALREADY_EXIST";
		public static final String COMPANY_CODE_NOT_EXIST = "COMPANY_CODE_NOT_EXIST";
		public static final String PLAN_NOT_EXIST = "PLAN_NOT_EXIST";
	}

	public static final class FIRE_BASE {
		public static final String INSTANCE_NAME_APP = "[APP]";
		public static final String INSTANCE_NAME_ANDROID_TABLET = "[ANDROID_TABLET]";
		public static final String INSTANCE_NAME_IOS_TABLET = "[IOS_TABLET]";
	}

	public static final class OTT_SYNTAX {
		public static final String REGISTER = "dk";
		public static final String UNREGISTER = "huy";
	}

	public static final class OTT_MESSAGE {
		public static final String SYNTAX_INCORRECT = "Cú pháp không hợp lệ";
		public static final String REGISTER_SYNTAX_INCORRECT = "Cú pháp đăng ký không hợp lệ";
		public static final String UNREGISTER_SYNTAX_INCORRECT = "Cú pháp hủy đăng ký không hợp lệ";
		public static final String USERNAME_NOT_EXIST = "Tên đăng nhập không tồn tại: %s";
		public static final String PASSWORD_INCORRECT = "Mật khẩu không đúng";
		public static final String REGISTER_SUCCESS = "Đăng ký thành công";
		public static final String UNREGISTER_SUCCESS = "Hủy đăng ký thành công";
		public static final String ACCOUNT_CHANNEL_INACTIVE = "Kênh đăng ký chưa được kích hoạt";
		public static final String EXCEED_MAX_USER_OTT = "Không thể đăng ký vượt quá số lượng user cho phép";
		public static final String UNREGISTERED_ACCOUNT_OTT = "Bạn chưa đăng ký. Vui lòng đăng ký trước";

		public static final String FULL_NAME = "Họ và tên: ";
		public static final String ID = "Tên đăng nhập: ";
		public static final String ATTEND_ON = "Có mặt lúc: ";
		public static final String FB_TITLE = "Ảnh checkin";
		public static final String POWERED_BY_VNPT = "Powered by VNPT";

		public static final String COMPLAINT_TITLE = "Thông báo khiếu nại";
		public static final String COMPLAINT_CODE = "Mã khiếu nại: ";
		public static final String COMPLAINT_STATUS = "Trạng thái: ";
		public static final String COMPLAINT_RESOLVED = "Phê duyệt";
		public static final String COMPLAINT_REJECT = "Từ chối";
		public static final String COMPLAINT_RESOLVER = "Người phê duyệt: ";
		public static final String TRANS_ID = "Trans Id: ";
	}

	public static final class OTT_CARD_DATA_TYPE {
		public static final String TEXT = "text";
		public static final String IMAGE = "image";
	}

	public static final class OTT_CHANNEL {
		public static final String FB = "facebook";
	}

	public static final class SYNC_MODE {
		public static final String CLONE = "clone";
		public static final String PULL = "pull";
	}

	public static final class POLICYDEF {
		public static final String ENABLE_FACE_LIVENESS = "4";
		public static final String ENABLE_FACE_MASK = "5";
		public static final String ENABLE_FACE_PROB = "6";
		public static final String ENABLE_IMPORT_EXCEL = "8";
		public static final String ENABLE_LIST_HIS_CHECKIN = "9";
		public static final String LIMIT_USER = "11";
		public static final String LIMIT_DEVICE = "12";
		public static final String LIMIT_OTT = "13";
		public static final String ENABLE_VIRTUAL_ASSISTANT = "14";
	}

	public static final class ATTR_UNIT {
		public static final String USER = "USER";
		public static final String DEVICE = "DEVICE";
		public static final String OTT = "OTT";
	}

	public static final class OS {
		public static final String ANDROID = "android";
		public static final String IOS = "ios";
	}

	public static final class LIMIT_POLICY_TYPE {
		public static final String USER = "USER";
		public static final String DEVICE = "DEVICE";
		public static final String OTT = "OTT";
	}
}
