package vn.vnpt.common.errorcode;

public class ErrorCode {

	public static String IDG_00000000 = "IDG-00000000";
	public static String IDG_00000100 = "IDG-00000100";
	public static String IDG_00000101 = "IDG-00000101";
	public static String IDG_00000200 = "IDG-00000200";
	public static String IDG_00000201 = "IDG-00000201";
	public static String IDG_00000202 = "IDG-00000202";
	public static String IDG_00000203 = "IDG-00000203";
	public static String IDG_00000204 = "IDG-00000204";

	public static String IDG_00000205 = "IDG-00000205";
	public static String IDG_00000206 = "IDG-00000206";
	public static String IDG_00000300 = "IDG-00000300";
	public static String IDG_00000301 = "IDG-00000301";
	public static String IDG_00000302 = "IDG-00000302";
	public static String IDG_00000303 = "IDG-00000303";
	public static String IDG_00000304 = "IDG-00000304";
	public static String IDG_00000305 = "IDG-00000305";

	public static String IDG_00000307 = "IDG-00000307";
	public static String IDG_00000400 = "IDG-00000400";
	public static String IDG_00000401 = "IDG-00000401";
	public static String IDG_00000402 = "IDG-00000402";
	public static String IDG_00000403 = "IDG-00000403";
	public static String IDG_00000404 = "IDG-00000404";
	public static String IDG_00000405 = "IDG-00000405";
	public static String IDG_00000406 = "IDG-00000406";

	public static String IDG_00000407 = "IDG-00000407";
	public static String IDG_00000408 = "IDG-00000408";
	public static String IDG_00000409 = "IDG-00000409";
	public static String IDG_00000410 = "IDG-00000410";
	public static String IDG_00000411 = "IDG-00000411";
	public static String IDG_00000412 = "IDG-00000412";
	public static String IDG_00000413 = "IDG-00000413";
	public static String IDG_00000414 = "IDG-00000414";

	public static String IDG_00000415 = "IDG-00000415";
	public static String IDG_00000416 = "IDG-00000416";
	public static String IDG_00000417 = "IDG-00000417";
	public static String IDG_00000500 = "IDG-00000500";
	public static String IDG_00000501 = "IDG-00000501";
	public static String IDG_00000502 = "IDG-00000502";
	public static String IDG_00000503 = "IDG-00000503";
	public static String IDG_00000504 = "IDG-00000505";

	public static final String IDG_00000032 = "IDG-00000032";

	// sai dinh dang so dien thoai
	public static final String IDG_00000008 = "IDG-00000008";

	// Loi tablet checkin
	// Thiet bi khong ton tai
	public static final String IDG_00002001 = "IDG-00002001";
	// Tai khoan khong ton tai
	public static final String IDG_00002002 = "IDG-00002002";
	// Ma thiet bi da ton tai
	public static final String IDG_00002003 = "IDG-00002003";
	// uuidDevice da ton tai
	public static final String IDG_00002004 = "IDG-00002004";
	// username khong ton tai
	public static final String IDG_00002005 = "IDG-00002005";
	// user code khong dung dinh dang
	public static final String IDG_00002006 = "IDG-00002006";
	// device code khong dung dinh dang
	public static final String IDG_00002007 = "IDG-00002007";
	// phien lam viec tren thiet bi da het han, can login lai
	public static final String IDG_00002008 = "IDG-00002008";

	// thiet bi đã bị khóa
	public static final String IDG_00002009 = "IDG-00002009";

	// email không đúng định dạng
	public static final String IDG_00002010 = "IDG-00002010";

	// thiet bi chua active khong the login, can active tren web quan tri
	public static final String IDG_00002011 = "IDG-00002011";

	// Email đã đăng ký trong hệ thống
	public static final String IDG_00002012 = "IDG-00002012";

	// Mã pin không đúng định dạng
	public static final String IDG_00002013 = "IDG-00002013";

	// Số lượng user vượt quá ngưỡng của gói đăng ký
	public static final String IDG_00002014 = "IDG-00002014";

	// Số lượng thiết bị tablet vượt quá ngưỡng của gói đăng ký
	public static final String IDG_00002015 = "IDG-00002015";

	// Số lượng kênh ott vượt quá ngưỡng của gói đăng ký
	public static final String IDG_00002016 = "IDG-00002016";

	// Gói cước không hỗ trợ chống che mặt
	public static final String IDG_00002017 = "IDG-00002017";

	// Gói cước không hỗ trợ chống giả mạo
	public static final String IDG_00002018 = "IDG-00002018";

	// Gói cước không hỗ trợ điều chỉnh ngưỡng AI
	public static final String IDG_00002019 = "IDG-00002019";

	// Gói cước không hỗ trợ thêm mới nhanh danh sách người dùng dạng .xlsx
	public static final String IDG_00002020 = "IDG-00002020";

	// Gói cước không cho phép người dùng xem lịch sử check in cá nhân
	public static final String IDG_00002021 = "IDG-00002021";

	// Số lượng người dùng lúc đăng ký mới gói cước nhỏ hơn số lượng người dùng đang có
	public static final String IDG_00002023 = "IDG-00002023";

	// Số lượng thiết bị lúc đăng ký mới gói cước nhỏ hơn số lượng thiết bị đang có
	public static final String IDG_00002024 = "IDG-00002024";

	// Số lượng kênh OTT lúc đăng ký mới gói cước nhỏ hơn số lượng OTT đang có
	public static final String IDG_00002025 = "IDG-00002025";

	// username da ton tai trong he thong
	public static final String IDG_00000002= "IDG-00000002";
	//Loi xu ly rang buoc DB
	public static String IDG_00000600 = "IDG-00000600";
//	public static String IDG_00000040 = "IDG-00000040";
//
//	public static String IDG_00000041 = "IDG-00000041";
//
//	public static String IDG_00000050 = "IDG-00000050";
//
//	public static String IDG_00000051 = "IDG-00000051";
//
//	public static String IDG_00000052 = "IDG-00000052";
//
//	public static String IDG_00000053 = "IDG-00000053";
//
//	public static String IDG_00000054 = "IDG-00000054";
//
//	public static String IDG_00000055 = "IDG-00000055";
//
//	public static String IDG_00000056 = "IDG-00000056";
//
//	public static String IDG_00000057 = "IDG-00000057";
//
//	public static String IDG_00000058 = "IDG-00000059";
//
//	public static String IDG_00000060 = "IDG-00000060";
//
//	public static String IDG_00000061 = "IDG-00000061";
//
//	public static String IDG_00000062 = "IDG-00000062";
//
//	public static String IDG_00000063 = "IDG-00000063";
//
//	public static String IDG_00000064 = "IDG-00000064";
//
//	public static String IDG_00000090 = "IDG-00000090";
}
