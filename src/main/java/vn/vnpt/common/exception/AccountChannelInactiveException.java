package vn.vnpt.common.exception;

public class AccountChannelInactiveException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2407102742288585067L;

	public AccountChannelInactiveException(String message) {
		super(message);
	}

}
