package vn.vnpt.common.exception;

import vn.vnpt.common.errorcode.ErrorCode;

public class BadRequestException extends RuntimeException {

	private String errorCode = ErrorCode.IDG_00000400;

	public BadRequestException(String message) {
		super(message);
	}

	public BadRequestException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}
}
