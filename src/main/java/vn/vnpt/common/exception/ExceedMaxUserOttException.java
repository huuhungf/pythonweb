package vn.vnpt.common.exception;

public class ExceedMaxUserOttException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2407102742288585067L;

	public ExceedMaxUserOttException(String message) {
		super(message);
	}

}
