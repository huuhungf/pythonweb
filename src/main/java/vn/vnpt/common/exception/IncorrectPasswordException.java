package vn.vnpt.common.exception;

public class IncorrectPasswordException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2407102742288585067L;

	public IncorrectPasswordException(String message) {
		super(message);
	}

}
