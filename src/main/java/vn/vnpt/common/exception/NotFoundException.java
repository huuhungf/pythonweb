package vn.vnpt.common.exception;

import vn.vnpt.common.errorcode.ErrorCode;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2407102742288585067L;

	private String errorCode = ErrorCode.IDG_00000404;

	public NotFoundException(String message) {
		super(message);
	}

	public NotFoundException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}
}
