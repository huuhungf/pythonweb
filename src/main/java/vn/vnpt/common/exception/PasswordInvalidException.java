package vn.vnpt.common.exception;

public class PasswordInvalidException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2407102742288585067L;

	public PasswordInvalidException(String message) {
		super(message);
	}

}
