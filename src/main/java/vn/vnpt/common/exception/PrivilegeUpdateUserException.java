package vn.vnpt.common.exception;

public class PrivilegeUpdateUserException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 2407102742288585067L;

    public PrivilegeUpdateUserException(String message){
        super(message);
    }
}
