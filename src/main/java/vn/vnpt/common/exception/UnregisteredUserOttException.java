package vn.vnpt.common.exception;

public class UnregisteredUserOttException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2407102742288585067L;

	public UnregisteredUserOttException(String message) {
		super(message);
	}

}
