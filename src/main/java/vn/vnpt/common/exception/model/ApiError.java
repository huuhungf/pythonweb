package vn.vnpt.common.exception.model;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Data
public class ApiError {
	private List<messageObject> messageObjects;
	private List<MessageField> messageFields;
	private String statusCode;
	private String message;
	private HttpStatus status;
	private String error;

	public ApiError() {
		super();
	}

	public ApiError(List<MessageField> messageFields, List<messageObject> messageObjects, HttpStatus status) {
		super();
		this.messageObjects = messageObjects;
		this.messageFields = messageFields;
		this.message = "";
		this.status = status;
		this.statusCode = status.toString();
		this.error = "";
	}

	public ApiError(HttpStatus status, String message, List<MessageField> messageFields,
			List<messageObject> messageObjects) {
		super();
		this.messageObjects = messageObjects;
		this.messageFields = messageFields;
		this.message = message;
		this.statusCode = status.toString();
		this.status = status;
		this.error = "";
	}

	public ApiError(HttpStatus status, String message) {
		super();
		this.messageObjects = new ArrayList<>();
		this.messageFields = new ArrayList<>();
		this.message = message;
		this.statusCode = status.toString();
		this.status = status;
		this.error = "";
	}

	public ApiError(HttpStatus status, String message, String error) {
		super();
		this.messageObjects = new ArrayList<>();
		this.messageFields = new ArrayList<>();
		this.message = message;
		this.statusCode = status.toString();
		this.status = status;
		this.error = error;
	}
}
