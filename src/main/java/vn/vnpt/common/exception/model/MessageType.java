package vn.vnpt.common.exception.model;

public enum MessageType {
  SUCCESS, INFO, WARNING, ERROR
}
