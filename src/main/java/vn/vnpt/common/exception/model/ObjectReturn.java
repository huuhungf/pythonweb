package vn.vnpt.common.exception.model;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ObjectReturn {
	
	private HttpStatus status;
	
	private String message;
}
