package vn.vnpt.common.exception.model;

import lombok.Data;

@Data
public class messageObject {
	
	private String objectName;
	private String message;

	/**
	 * @param fieldName
	 * @param messages
	 */
	public messageObject(String objectName, String message) {
		super();
		this.objectName = objectName;
		this.message = message;
	}


}
