package vn.vnpt.common.pattern;

/**
 * @author trananh
 *
 */
public class Patterns {

	// parten dd/mm/yyyy
	public static final String IDG_DATE_DD_MM_YYYY = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)"
			+ "(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)"
			+ "0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|"
			+ "(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)"
			+ "(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";

	public static final String IDG_YYYY = "[\\d]{4}";

	public static final String IDG_HOUR_MINUTES = "^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])?$";

	public static final String IDG_SEX = "^[0-2]$";

	public static final String IDG_CAN_NANG = "\\d+(\\.\\d{1,2})?";

	public static final String IDG_CON_THU = "^\\d{1,2}$";

	public static final String IDG_LAN_SINH = "^\\d{1,2}$";

	public static final String IDG_QUYENSO = "^\\d{1,10}$";

	public static final String IDG_SO = "^\\d{1,6}$";

	public static final String IDG_CMT = "^\\d{9,11}$";

	public static final String IDG_EMAIL = ".+@.+\\.[a-zA-Z]+";

	public static final String IDG_PASSWORD = "^[^\\s]{6,}$";

	public static final String IDG_VN_PHONE_NUMBER = "^(03[2-9]|05[25689]|07[06789]|08[1-9]|09[0-9])[0-9]{7,8}$";

	public static final String OTT_REGISTER_TEXT = "^(D|d)(K|k)\\s+(\\S+)\\s+(\\S+)$";
	public static final String OTT_UNREGISTER_TEXT = "^(H|h)(U|u)(Y|y)\\s+(\\S+)$";

	public static final String IDG_USER_CODE = "^[a-zA-Z0-9]+$";

	public static final String IDG_DEVICE_CODE = "^[a-zA-Z0-9]+$";

	public static final String IDG_PIN_CODE = "^[0-9]{6}$";
}