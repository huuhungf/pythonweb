package vn.vnpt.common.success.model;

import lombok.Data;

import java.util.List;

@Data
public class PagingDTO<T> {

	private int page;

	private int maxSize;

	private long totalElement;

	private long totalPages;

	private String sort;

	private String propertiesSort;

	private List<T> data;

}
