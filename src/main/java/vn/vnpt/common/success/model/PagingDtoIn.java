package vn.vnpt.common.success.model;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Data
public class PagingDtoIn {
	
	private int page = 1;

	@NotNull(message = "IDG-00000001")
	@Range(min = 1, max = 100, message = "IDG-00000004")
    private Integer maxSize = 100;

    private String sort;

    private String propertiesSort;
}
