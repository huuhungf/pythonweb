package vn.vnpt.context;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.repository.company.GetCompanyByAccountRepository;
import vn.vnpt.api.repository.device.GetDeviceRepository;
import vn.vnpt.api.repository.user.GetAccountByUsernameRepository;

public abstract class AbstractDataContextLoader implements DataContextLoader {

	@Autowired
	protected GetAccountByUsernameRepository getAccountByUsernameRepository;
	@Autowired
	protected GetDeviceRepository getDeviceRepository;
	@Autowired
	protected GetCompanyByAccountRepository getCompanyByAccountRepository;

	protected DataContext dataContext() {
		return DataContextHolder.getDataContext();
	}

	protected String getRole() {
		return dataContext().getRole();
	}

	protected String getUsername() {
		return dataContext().getUsername();
	}

	protected IllegalStateException notSupport(String role) {
		return new IllegalStateException(String.format("not support role: %s", role));
	}

	@Override
	public Account getContextAccount() {
		if (dataContext().getContextAccount() != null) {
			return dataContext().getContextAccount();
		}

		Account contextAccount = getAccountByUsernameRepository.getAccountByUserName(getUsername());
		dataContext().setContextAccount(contextAccount);
		return contextAccount;
	}

	@Override
	public Device getDevice() {
		if (dataContext().getDevice() != null) {
			return dataContext().getDevice();
		}

		Account owner = getOwnerAccount();

		Device device = getDeviceRepository.getDevice(owner.getUuidDevice());
		dataContext().setDevice(device);
		return device;
	}

	@Override
	public Company getCompany() {
		if (dataContext().getCompany() != null) {
			return dataContext().getCompany();
		}

		Account superAccount = getSuperAccount();

		if (dataContext().getCompany() != null) {
			return dataContext().getCompany();
		}

		Company company = getCompanyByAccountRepository.getCompany(superAccount.getUuidAccount());
		dataContext().setCompany(company);
		return company;
	}
}
