package vn.vnpt.context;

import lombok.Data;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;

import java.util.HashMap;
import java.util.Map;

@Data
public class DataContext {
	private String role;
	private String username;
	private Account contextAccount;

	private Account superAccount;
	private Account ownerAccount;
	private Account userAccount;

	private Company company;
	private Device device;

	private Map<String, Object> otherData = new HashMap<>();

	private DataContextLoader dataContextLoader;
}
