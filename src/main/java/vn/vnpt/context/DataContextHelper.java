package vn.vnpt.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;
import vn.vnpt.common.constant.ConstantString;

@Component
public class DataContextHelper {

	private static final Logger logger = LogManager.getLogger(DataContextHelper.class);

	@Autowired
	@Qualifier(value = "userDataContextLoader")
	private DataContextLoader userDataContextLoader;
	@Autowired
	@Qualifier(value = "ownerDataContextLoader")
	private DataContextLoader ownerDataContextLoader;
	@Autowired
	@Qualifier(value = "superDataContextLoader")
	private DataContextLoader superDataContextLoader;
	@Autowired
	@Qualifier(value = "notSupportDataContextLoader")
	private DataContextLoader notSupportDataContextLoader;

	public void init(Authentication authentication) {
		String username = (String) authentication.getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		logger.info(String.format("username: %s, role: %s", username, role));

		DataContext dataContext = new DataContext();
		dataContext.setUsername(username);
		dataContext.setRole(role);

		if (ConstantString.ROLE.USER.equals(role)) {
			dataContext.setDataContextLoader(userDataContextLoader);
		} else if (ConstantString.ROLE.OWNER.equals(role)) {
			dataContext.setDataContextLoader(ownerDataContextLoader);
		} else if (ConstantString.ROLE.SUPERADMIN.equals(role)) {
			dataContext.setDataContextLoader(superDataContextLoader);
		} else {
			dataContext.setDataContextLoader(notSupportDataContextLoader);
		}

		DataContextHolder.setDataContext(dataContext);
	}

	public Account getContextAccount() {
		return dataContextLoader().getContextAccount();
	}

	public Account getUserAccount() {
		return dataContextLoader().getUserAccount();
	}

	public Account getOwnerAccount() {
		return dataContextLoader().getOwnerAccount();
	}

	public Account getSuperAccount() {
		return dataContextLoader().getSuperAccount();
	}

	public Device getDevice() {
		return dataContextLoader().getDevice();
	}

	public Company getCompany() {
		return dataContextLoader().getCompany();
	}

	public String getRole() {
		return dataContext().getRole();
	}

	public String getUsername() {
		return dataContext().getUsername();
	}

	public Object get(String key) {
		return dataContext().getOtherData().get(key);
	}

	public void put(String key, Object value) {
		dataContext().getOtherData().put(key, value);
	}

	public void clear() {
		DataContextHolder.clear();
	}

	private DataContext dataContext() {
		return DataContextHolder.getDataContext();
	}

	private DataContextLoader dataContextLoader() {
		return dataContext().getDataContextLoader();
	}
}
