package vn.vnpt.context;

import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;

public interface DataContextLoader {
	Account getContextAccount();

	Account getUserAccount();

	Account getOwnerAccount();

	Account getSuperAccount();

	Device getDevice();

	Company getCompany();
}
