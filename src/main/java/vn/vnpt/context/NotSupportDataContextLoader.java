package vn.vnpt.context;

import org.springframework.stereotype.Component;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;

@Component("notSupportDataContextLoader")
public class NotSupportDataContextLoader extends AbstractDataContextLoader {
	@Override
	public Account getContextAccount() {
		throw notSupport(getRole());
	}

	@Override
	public Account getUserAccount() {
		throw notSupport(getRole());
	}

	@Override
	public Account getOwnerAccount() {
		throw notSupport(getRole());
	}

	@Override
	public Account getSuperAccount() {
		throw notSupport(getRole());
	}

	@Override
	public Device getDevice() {
		throw notSupport(getRole());
	}

	@Override
	public Company getCompany() {
		throw notSupport(getRole());
	}
}
