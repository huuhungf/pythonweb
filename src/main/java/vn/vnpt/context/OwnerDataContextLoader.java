package vn.vnpt.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Device;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.user.GetAccountRepository;

@Component("ownerDataContextLoader")
public class OwnerDataContextLoader extends AbstractDataContextLoader {

	@Autowired
	private GetCompanyRepository getCompanyRepository;
	@Autowired
	private GetAccountRepository getAccountRepository;

	@Override
	public Account getUserAccount() {
		throw notSupport(getRole());
	}

	@Override
	public Account getOwnerAccount() {
		if (dataContext().getOwnerAccount() != null) {
			return dataContext().getOwnerAccount();
		}

		Account contextAccount = getContextAccount();
		dataContext().setOwnerAccount(contextAccount);
		return contextAccount;
	}

	@Override
	public Account getSuperAccount() {
		if (dataContext().getSuperAccount() != null) {
			return dataContext().getSuperAccount();
		}

		Device device = getDevice();
		Company company = getCompanyRepository.getCompany(device.getUuidCompany());
		dataContext().setCompany(company);
		Account superAccount = getAccountRepository.get(company.getOwnedAccount());
		dataContext().setSuperAccount(superAccount);
		return superAccount;
	}
}
