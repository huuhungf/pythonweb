package vn.vnpt.context;

import org.springframework.stereotype.Component;
import vn.vnpt.api.model.Account;

@Component("superDataContextLoader")
public class SuperDataContextLoader extends AbstractDataContextLoader {

	@Override
	public Account getUserAccount() {
		throw notSupport(getRole());
	}

	@Override
	public Account getOwnerAccount() {
		throw notSupport(getRole());
	}

	@Override
	public Account getSuperAccount() {
		if (dataContext().getSuperAccount() != null) {
			return dataContext().getSuperAccount();
		}

		Account contextAccount = getContextAccount();
		dataContext().setSuperAccount(contextAccount);
		return contextAccount;
	}
}
