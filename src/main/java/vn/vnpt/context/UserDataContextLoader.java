package vn.vnpt.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.Company;
import vn.vnpt.api.model.Group;
import vn.vnpt.api.repository.company.GetCompanyRepository;
import vn.vnpt.api.repository.group.ListGroupByAccountRepository;
import vn.vnpt.api.repository.user.GetAccountRepository;

@Component("userDataContextLoader")
public class UserDataContextLoader extends AbstractDataContextLoader {

	@Autowired
	private GetCompanyRepository getCompanyRepository;
	@Autowired
	private GetAccountRepository getAccountRepository;
	@Autowired
	private ListGroupByAccountRepository listGroupByAccountRepository;

	@Override
	public Account getUserAccount() {
		if (dataContext().getUserAccount() != null) {
			return dataContext().getUserAccount();
		}

		Account contextAccount = getContextAccount();
		dataContext().setUserAccount(contextAccount);
		return contextAccount;
	}

	@Override
	public Account getOwnerAccount() {
		throw notSupport(getRole());
	}

	@Override
	public Account getSuperAccount() {
		if (dataContext().getSuperAccount() != null) {
			return dataContext().getSuperAccount();
		}

		Account user = getUserAccount();
		Group groupCompany = listGroupByAccountRepository.list(user.getUuidAccount())
				.stream().filter(g -> g.getUuidCompany() != null)
				.findFirst()
				.get();

		Company company = getCompanyRepository.getCompany(groupCompany.getUuidCompany());
		dataContext().setCompany(company);
		Account superAccount = getAccountRepository.get(company.getOwnedAccount());
		dataContext().setSuperAccount(superAccount);
		return superAccount;
	}
}
