package vn.vnpt.embedding;

public interface EmbeddingService {
	String getEmbedding(String hashFile);
}
