package vn.vnpt.embedding;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.success.model.ApiResult;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class EmbeddingServiceBean implements EmbeddingService {

	@Value("${embedding.url}")
	private String embeddingUrl;

	private final RestTemplate restTemplate;

	private final ObjectMapper objectMapper;

	@Autowired
	public EmbeddingServiceBean(@Qualifier("vanillaRestTemplate") RestTemplate restTemplate, ObjectMapper objectMapper) {
		this.restTemplate = restTemplate;
		this.objectMapper = objectMapper;
	}

	@Override
	public String getEmbedding(String hashFile) {

		System.out.println("-------START REQUEST EMBEDDING IMG---------: " + hashFile);

		int retry = 0;
		BadRequestException ex = null;
		String embedding = null;
		while (retry < 5 && embedding == null) {
			ex = null;
			retry++;

			Map<String, String> body = new HashMap<>();
			body.put("img", hashFile);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			ParameterizedTypeReference<ApiResult<Map<String, String>>> typeReference = new ParameterizedTypeReference<ApiResult<Map<String, String>>>() {
			};

			ApiResult<Map<String, String>> apiResult = null;
			try {
				apiResult = restTemplate.exchange(embeddingUrl, HttpMethod.POST,
						new HttpEntity<>(body, headers), typeReference).getBody();
				if (apiResult != null) {
					embedding = apiResult.getObject().get("embedding");
				}
			} catch (RestClientException e) {
				if (e instanceof HttpClientErrorException) {
					HttpClientErrorException clientErrorException = (HttpClientErrorException) e;
					if (clientErrorException.getStatusCode().is4xxClientError()) {
						String res = clientErrorException.getResponseBodyAsString();
						TypeReference<Map<String, Object>> typeRef = new TypeReference<Map<String, Object>>() {
						};
						try {
							Map<String, Object> map = objectMapper.readValue(res, typeRef);
							@SuppressWarnings("unchecked")
							List<String> errors = (List<String>) map.get("errors");
							ex = new BadRequestException(String.join(", ", errors));
							// sleep and retry
							try {
								TimeUnit.MILLISECONDS.sleep(200);
							} catch (InterruptedException ie) {
								ie.printStackTrace();
							}
						} catch (IOException ioException) {
							ioException.printStackTrace();
							throw new RuntimeException("could not parse response embedding");
						}
					} else {
						e.printStackTrace();
						throw new RuntimeException("could not get embedding");
					}
				} else {
					e.printStackTrace();
					throw e;
				}
			}
		}

		System.out.println(String.format("-------DONE REQUEST EMBEDDING IMG--------- retry = %s, success = %s : ", retry, embedding != null) + hashFile);

		if (ex != null) {
			throw ex;
		}

		return embedding;
	}
}
