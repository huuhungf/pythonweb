package vn.vnpt.firebase;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

@Service
public class FirebaseService {

	private final Logger logger = LoggerFactory.getLogger(FirebaseService.class);

	public void sendMessage(PushNotificationRequest request)
			throws InterruptedException, ExecutionException {
		Message message = buildMessage(request);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(message);
		String response = sendAndGetResponse(request.getInstanceName(), message);
		logger.info("Sent message with data. Token: " + request.getToken() + ", " + response + " msg " + jsonOutput);
	}

	private String sendAndGetResponse(String instanceName, Message message) throws InterruptedException, ExecutionException {
		return FirebaseMessaging.getInstance(FirebaseApp.getInstance(instanceName)).sendAsync(message).get();
	}

	private AndroidConfig getAndroidConfig() {
		return AndroidConfig.builder()
				.setTtl(Duration.ofMinutes(2).toMillis())
				.setPriority(AndroidConfig.Priority.HIGH)
				.setNotification(AndroidNotification.builder().setSound(NotificationParameter.SOUND.getValue())
						.setColor(NotificationParameter.COLOR.getValue()).build()).build();
	}

	private ApnsConfig getApnsConfig() {
		return ApnsConfig.builder()
				.setAps(Aps.builder().setAlert("Checkin thành công")
						.setSound("default")
						.setCategory("CustomSamplePush")
						.setMutableContent(true)
						.build()).build();
	}

	private Message buildMessage(PushNotificationRequest request) {
		Message.Builder builder = Message.builder();
		if (request.getData() != null) {
			builder = builder.putAllData(request.getData());
		}
		if (request.getTitle() != null) {
			builder = builder.setNotification(new Notification(request.getTitle(), request.getMessage()));
		}
		if (request.isWithConfig()) {
			builder = builder.setApnsConfig(getApnsConfig());
		}
		if (request.getToken() != null) {
			builder = builder.setToken(request.getToken());
		}
		if (request.getTopic() != null) {
			builder = builder.setTopic(request.getTopic());
		}
		return builder.build();
	}
}
