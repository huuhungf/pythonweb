package vn.vnpt.firebase;

import lombok.Data;

import java.util.Map;

@Data
public class PushNotificationRequest {

	private String title;
	private String message;
	private String topic;
	private String token;
	private boolean withConfig;

	private Map<String, String> data;
	private String instanceName;
}