package vn.vnpt.firebase.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import vn.vnpt.common.constant.ConstantString;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Configuration
public class FirebaseConfig {

    @Value("${firebase.app-config-file}")
    private String appFirebaseConfigPath;

    @Value("${firebase.android-tablet-config-file}")
    private String androidTabletFirebaseConfigPath;

    @Value("${firebase.ios-tablet-config-file}")
    private String iosTabletFirebaseConfigPath;

    Logger logger = LoggerFactory.getLogger(FirebaseConfig.class);

    @PostConstruct
    public void initialize() {
        try {
            List<FireBaseOpt> fireBaseOpts = Arrays.asList(
                    new FireBaseOpt(ConstantString.FIRE_BASE.INSTANCE_NAME_APP, appFirebaseConfigPath),
                    new FireBaseOpt(ConstantString.FIRE_BASE.INSTANCE_NAME_ANDROID_TABLET, androidTabletFirebaseConfigPath),
                    new FireBaseOpt(ConstantString.FIRE_BASE.INSTANCE_NAME_IOS_TABLET, iosTabletFirebaseConfigPath)
            );

            if (FirebaseApp.getApps().isEmpty()) {
                for (FireBaseOpt fireBaseOpt: fireBaseOpts) {
                    FirebaseOptions options = new FirebaseOptions.Builder()
                            .setCredentials(GoogleCredentials.fromStream(
                                    new ClassPathResource(fireBaseOpt.configFilePath).getInputStream())).build();
                    FirebaseApp.initializeApp(options, fireBaseOpt.instanceName);
                }
                logger.info("Firebase application has been initialized");
            }
        } catch (IOException e) {
            logger.error("could not init firebase: ", e);
        }
    }

    private static class FireBaseOpt {
        private final String instanceName;
        private final String configFilePath;

        public FireBaseOpt(String instanceName, String configFilePath) {
            this.instanceName = instanceName;
            this.configFilePath = configFilePath;
        }
    }
}