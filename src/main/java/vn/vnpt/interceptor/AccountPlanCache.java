package vn.vnpt.interceptor;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AccountPlanCache implements Serializable {
	private String uuidAccountPlan;
	private String uuidAccount;
	private String uuidPlan;
	private Integer status;
	private Date activeDate;
	private Date expireDate;
}
