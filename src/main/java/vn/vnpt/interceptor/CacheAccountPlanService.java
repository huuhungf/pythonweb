package vn.vnpt.interceptor;

public interface CacheAccountPlanService {
	AccountPlanCache getAccountPlan(String uuidAccount);
	void deleteAccountPlan(String uuidAccount);
}
