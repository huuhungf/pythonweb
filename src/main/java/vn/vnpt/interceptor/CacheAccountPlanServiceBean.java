package vn.vnpt.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import vn.vnpt.api.model.AccountPlan;
import vn.vnpt.api.repository.plan.GetAccountPlanByAccountRepository;

@Service
public class CacheAccountPlanServiceBean implements CacheAccountPlanService {

	private final GetAccountPlanByAccountRepository getAccountPlanByAccountRepository;

	@Autowired
	public CacheAccountPlanServiceBean(GetAccountPlanByAccountRepository getAccountPlanByAccountRepository) {
		this.getAccountPlanByAccountRepository = getAccountPlanByAccountRepository;
	}

	@Cacheable(cacheNames = "AccountPlan", key = "#uuidAccount")
	@Override
	public AccountPlanCache getAccountPlan(String uuidAccount) {
		AccountPlan accountPlan = getAccountPlanByAccountRepository.get(uuidAccount);

		AccountPlanCache apCache = new AccountPlanCache();
		apCache.setUuidAccountPlan(accountPlan.getUuidAccountPlan());
		apCache.setUuidAccount(accountPlan.getUuidAccount());
		apCache.setUuidPlan(accountPlan.getUuidPlan());
		apCache.setStatus(accountPlan.getStatus());
		apCache.setActiveDate(accountPlan.getActiveDate());
		apCache.setExpireDate(accountPlan.getExpireDate());

		return apCache;
	}

	@CacheEvict(cacheNames = "AccountPlan", key = "#uuidAccount")
	@Override
	public void deleteAccountPlan(String uuidAccount) {
	}
}
