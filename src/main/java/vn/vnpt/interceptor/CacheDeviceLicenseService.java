package vn.vnpt.interceptor;

public interface CacheDeviceLicenseService {
	DeviceLicenseCache getDeviceLicense(String adminUsername, String serialNumber);
	void deleteDeviceLicense(String adminUsername, String serialNumber);
}
