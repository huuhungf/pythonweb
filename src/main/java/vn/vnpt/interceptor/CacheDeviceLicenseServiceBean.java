package vn.vnpt.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import vn.vnpt.api.model.DeviceLicense;
import vn.vnpt.api.repository.device.GetDeviceLicenseByAdminRepository;

@Service
public class CacheDeviceLicenseServiceBean implements CacheDeviceLicenseService {
	private final GetDeviceLicenseByAdminRepository getDeviceLicenseByAdminRepository;

	@Autowired
	public CacheDeviceLicenseServiceBean(GetDeviceLicenseByAdminRepository getDeviceLicenseByAdminRepository) {
		this.getDeviceLicenseByAdminRepository = getDeviceLicenseByAdminRepository;
	}

	@Cacheable(cacheNames = "DeviceLicense", key = "#adminUsername + '/' + #serialNumber")
	@Override
	public DeviceLicenseCache getDeviceLicense(String adminUsername, String serialNumber) {
		DeviceLicense dl = getDeviceLicenseByAdminRepository.get(adminUsername, serialNumber);
		DeviceLicenseCache dlc = new DeviceLicenseCache();
		dlc.setUuidDevice(dl.getUuidDevice());
		dlc.setSerialNumber(dl.getSerialNumber());
		dlc.setStatus(dl.getStatus());
		return dlc;
	}

	@CacheEvict(cacheNames = "DeviceLicense", key = "#adminUsername + '/' + #serialNumber")
	public void deleteDeviceLicense(String adminUsername, String serialNumber) {
	}
}
