package vn.vnpt.interceptor;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeviceLicenseCache implements Serializable {
	private String uuidDevice;
	private String serialNumber;
	private Integer status;
}
