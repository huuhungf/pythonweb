package vn.vnpt.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import vn.vnpt.api.repository.tokenchannel.GetTokenChannelRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.NotFoundException;
import vn.vnpt.common.exception.model.ApiError;

import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@Component
public class PortalCheckingInterceptor implements HandlerInterceptor {
	private static final Logger logger = LogManager.getLogger(PortalCheckingInterceptor.class);

	private final ObjectMapper objectMapper;
	private final GetTokenChannelRepository getTokenChannelRepository;

	@Autowired
	public PortalCheckingInterceptor(
			ObjectMapper objectMapper,
			GetTokenChannelRepository getTokenChannelRepository) {
		this.objectMapper = objectMapper;
		this.getTokenChannelRepository = getTokenChannelRepository;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String requestURI = request.getRequestURI();
		List<String> versions = Collections.singletonList("/v1/");
		for (String version : versions) {
			if (requestURI.startsWith(version)) {
				requestURI = requestURI.replaceFirst(version, "/");
				break;
			}
		}

		// unchecked in case not is agent
		if (!requestURI.startsWith("/portal")) {
			return true;
		}

		// in case is external
		if (!request.getDispatcherType().equals(DispatcherType.REQUEST)) {
			return true;
		}

		String uuidTokenChannel = request.getHeader("Service-Key");

		if (Common.isNullOrEmpty(uuidTokenChannel)) {
			ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ErrorCode.IDG_00000400, "Service-Key must not empty");
			String res = objectMapper.writeValueAsString(apiError);

			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
			response.getWriter().println(res);

			return false;
		}

		// check token channel from db
		try {
			getTokenChannelRepository.get(uuidTokenChannel);
		} catch (NotFoundException e) {
			logger.info(String.format("---Service-Key not exist---: %s", uuidTokenChannel));
			ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ErrorCode.IDG_00000400, "Service-Key not exist");
			String res = objectMapper.writeValueAsString(apiError);

			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
			response.getWriter().println(res);
			return false;
		}

		return true;
	}
}
