package vn.vnpt.interceptor.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import vn.vnpt.interceptor.PortalCheckingInterceptor;
import vn.vnpt.interceptor.TokenChannelCheckingInterceptor;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	private final TokenChannelCheckingInterceptor tokenChannelCheckingInterceptor;
	private final PortalCheckingInterceptor portalCheckingInterceptor;

	@Autowired
	public WebConfig(TokenChannelCheckingInterceptor tokenChannelCheckingInterceptor,
					 PortalCheckingInterceptor portalCheckingInterceptor) {
		this.tokenChannelCheckingInterceptor = tokenChannelCheckingInterceptor;
		this.portalCheckingInterceptor = portalCheckingInterceptor;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(tokenChannelCheckingInterceptor);
		registry.addInterceptor(portalCheckingInterceptor);
	}
}
