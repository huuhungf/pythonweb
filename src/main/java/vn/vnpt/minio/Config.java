package vn.vnpt.minio;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vn.vnpt.common.Common;

@Configuration
public class Config {
	@Value("${minio.service.url}")
	private String MINIO_SERVICE_URL;

	@Value("${minio.service.access-key}")
	private String MINIO_ACCESS_KEY;

	@Value("${minio.service.secret-key}")
	private String MINIO_SECRET_KEY;

	@Bean
	@Qualifier("getMinioClient")
	public MinioClient getMinioClient() throws Exception {
		MinioClient minioClient = new MinioClient(MINIO_SERVICE_URL, MINIO_ACCESS_KEY, MINIO_SECRET_KEY);
		minioClient.setTimeout(Common.MINIO_CLIENT.CONNECT_TIMEOUT_MS,
				Common.MINIO_CLIENT.WRITE_TIMEOUT_MS,
				Common.MINIO_CLIENT.READ_TIMEOUT_MS);
		return minioClient;
	}
}


