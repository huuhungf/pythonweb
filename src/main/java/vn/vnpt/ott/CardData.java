package vn.vnpt.ott;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CardData {
	private String title = "";
	private String subtitle = "";
	private String url = "";
	private String text;
	private List<Object> buttons = new ArrayList<>();
	private String type;

	private CardData() {
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private CardData cardData = new CardData();

		private Builder() {
		}

		public Builder type(String type) {
			cardData.type = type;
			return this;
		}

		public Builder title(String title) {
			cardData.title = title;
			return this;
		}

		public Builder title(TitleBuilder titleBuilder) {
			cardData.title = titleBuilder.build();
			return this;
		}

		public Builder subTitle(String subTitle) {
			cardData.subtitle = subTitle;
			return this;
		}

		public Builder url(String url) {
			cardData.url = url;
			return this;
		}

		public Builder text(String text) {
			cardData.text = text;
			return this;
		}

		public CardData build() {
			return cardData;
		}
	}
}
