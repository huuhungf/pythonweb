package vn.vnpt.ott;

import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;

public class ComplaintTitleBuilder implements TitleBuilder {

	private final String nameComplainer;
	private final String nameResolver;
	private final Integer status;
	private final String imgUrl;
	private final String transId;

	public ComplaintTitleBuilder(String nameComplainer, String nameResolver, Integer status, String imgUrl, String transId) {
		this.nameComplainer = nameComplainer;
		this.nameResolver = nameResolver;
		this.status = status;
		this.imgUrl = imgUrl;
		this.transId = transId;
	}

	@Override
	public String build() {
		StringBuilder sb = new StringBuilder();
		sb.append(ConstantString.OTT_MESSAGE.COMPLAINT_TITLE).append("\n");
		if (!Common.isNullOrEmpty(transId)) {
			sb.append(ConstantString.OTT_MESSAGE.TRANS_ID).append(transId).append("\n");
		}
		sb.append(ConstantString.OTT_MESSAGE.FULL_NAME).append(nameComplainer).append("\n");
		sb.append(ConstantString.OTT_MESSAGE.COMPLAINT_STATUS);
		switch (status) {
			case ConstantString.STATUS_COMPLAINT.RESOLVED:
				sb.append(ConstantString.OTT_MESSAGE.COMPLAINT_RESOLVED).append("\n");
				break;
			case ConstantString.STATUS_COMPLAINT.REJECT:
				sb.append(ConstantString.OTT_MESSAGE.COMPLAINT_REJECT).append("\n");
				break;
			default:
		}
		sb.append(ConstantString.OTT_MESSAGE.COMPLAINT_RESOLVER).append(nameResolver).append("\n");
		sb.append(ConstantString.OTT_MESSAGE.POWERED_BY_VNPT).append("\n");
		sb.append(imgUrl);

		return sb.toString();
	}
}
