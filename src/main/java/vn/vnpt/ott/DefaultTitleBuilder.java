package vn.vnpt.ott;

import vn.vnpt.common.constant.ConstantString;

public class DefaultTitleBuilder implements TitleBuilder {
	private final String name;
	private final String id;
	private final String dateCheckin;
	private final AccessType accessType;

	public DefaultTitleBuilder(String name, String id, String dateCheckin, AccessType accessType) {
		this.name = name;
		this.id = id;
		this.dateCheckin = dateCheckin;
		this.accessType = accessType;
	}

	@Override
	public String build() {
		StringBuilder sb = new StringBuilder();
		sb.append(ConstantString.OTT_MESSAGE.FULL_NAME).append(name).append("\n")
				.append(ConstantString.OTT_MESSAGE.ID).append(id).append("\n");

		switch (accessType) {
			case ATTEND:
				sb.append(ConstantString.OTT_MESSAGE.ATTEND_ON);
				break;
			case ACCESS_QR:
				sb.append(ConstantString.OTT_MESSAGE.ATTEND_ON);
				break;
			case ACCESS_FACE:
				sb.append(ConstantString.OTT_MESSAGE.ATTEND_ON);
				break;
			default:
		}

		sb.append(dateCheckin).append("\n");
		sb.append(ConstantString.OTT_MESSAGE.POWERED_BY_VNPT);

		return sb.toString();
	}
}
