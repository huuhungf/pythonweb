package vn.vnpt.ott;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class OttNotifyDtoIn {
	private String botId;

	private String userId;

	private String channel;

	@JsonProperty(value = "card_data")
	private List<CardData> cardData;
}
