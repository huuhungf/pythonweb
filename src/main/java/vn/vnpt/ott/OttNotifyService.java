package vn.vnpt.ott;

public interface OttNotifyService {
	void sendNotify(OttNotifyDtoIn ottNotifyDtoIn);
}
