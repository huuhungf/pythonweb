package vn.vnpt.ott;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OttNotifyServiceBean implements OttNotifyService {

	@Value("${ott.url}")
	private String notifyUrl;

	private final RestTemplate restTemplate;

	@Autowired
	public OttNotifyServiceBean(@Qualifier("vanillaRestTemplate") RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public void sendNotify(OttNotifyDtoIn ottNotifyDtoIn) {
		System.out.println("---START SEND OTT NOTIFY---: "+ ottNotifyDtoIn);
		restTemplate.exchange(notifyUrl, HttpMethod.POST, new HttpEntity<>(ottNotifyDtoIn), String.class);
		System.out.println("---SUCCESS SEND OTT NOTIFY---: "+ ottNotifyDtoIn);
	}
}
