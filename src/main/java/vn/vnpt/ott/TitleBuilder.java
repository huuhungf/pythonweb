package vn.vnpt.ott;

public interface TitleBuilder {
	String build();
}
