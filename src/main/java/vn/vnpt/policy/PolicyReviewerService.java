package vn.vnpt.policy;

import vn.vnpt.api.dto.in.AccountChannelDtoIn;
import vn.vnpt.api.dto.in.UpdateDeviceLicenseDtoIn;
import vn.vnpt.api.dto.in.WebCreateListAccountDtoIn;
import vn.vnpt.api.model.Account;
import vn.vnpt.api.model.DeviceLicense;

import java.util.List;
import java.util.Map;

public interface PolicyReviewerService {
	void reviewCreateAccount(Account superAccount, Integer numAddition, List<AccountChannelDtoIn> accountChannels);

	void reviewCreateDeviceLicense(Account superAccount, Integer numAddition);

	void reviewListHisCheckin(Account superAccount);

	void reviewCreateListAccount(Account superAccount, WebCreateListAccountDtoIn webCreateListAccountDtoIn);

	void reviewUpdateDeviceLicense(Account superAccount, DeviceLicense deviceLicense, UpdateDeviceLicenseDtoIn updateDeviceLicenseDtoIn);

	void reviewUpdateAccount(Account superAccount, Account account, List<AccountChannelDtoIn> accountChannels);

	void reviewActiveDeviceLicense(Account superAccount, DeviceLicense deviceLicense);

	void reviewExternalCreateAccount(Account superAccount, Integer numAddition);

	void reviewExternalRegisterOrExtendAccountPlan(Account superAccount, Integer numAccount, Integer numDevice, Integer numOtt);

	void reviewPortalRegisterPlan(Account superAccount, Integer numUser, Integer addonDevice, Integer addonOtt, Integer addonUser);
}
