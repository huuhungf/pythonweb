package vn.vnpt.policy;

import org.springframework.stereotype.Service;
import vn.vnpt.api.dto.in.AccountChannelDtoIn;
import vn.vnpt.api.dto.in.UpdateDeviceLicenseDtoIn;
import vn.vnpt.api.dto.in.WebCreateListAccountDtoIn;
import vn.vnpt.api.model.*;
import vn.vnpt.api.repository.plan.*;
import vn.vnpt.api.repository.policy.ListPolicyRepository;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.policy.maker.PolicyMaker;
import vn.vnpt.policy.maker.PolicyMakerImpl;
import vn.vnpt.policy.reviewer.ChainPolicyReviewer;
import vn.vnpt.policy.reviewer.EnablePolicyReviewer;
import vn.vnpt.policy.reviewer.LimitPolicyReviewer;
import vn.vnpt.policy.stats.LimitStatsImpl;

import java.util.List;

@Service
public class PolicyReviewerServiceBean implements PolicyReviewerService {

	private final ListPolicyRepository listPolicyRepository;
	private final CountCurrentAccountRepository countCurrentAccountRepository;
	private final GetAccountPlanRepository getAccountPlanRepository;
	private final CountCurrentDeviceLicenseRepository countCurrentDeviceLicenseRepository;
	private final CountActiveOttRepository countActiveOttRepository;
	private final CountActiveOttByUserRepository countActiveOttByUserRepository;
	private final GetPlanRepository getPlanRepository;

	public PolicyReviewerServiceBean(ListPolicyRepository listPolicyRepository,
									 CountCurrentAccountRepository countCurrentAccountRepository,
									 GetAccountPlanRepository getAccountPlanRepository,
									 CountCurrentDeviceLicenseRepository countCurrentDeviceLicenseRepository,
									 CountActiveOttRepository countActiveOttRepository,
									 CountActiveOttByUserRepository countActiveOttByUserRepository,
									 GetPlanRepository getPlanRepository) {
		this.listPolicyRepository = listPolicyRepository;
		this.countCurrentAccountRepository = countCurrentAccountRepository;
		this.getAccountPlanRepository = getAccountPlanRepository;
		this.countCurrentDeviceLicenseRepository = countCurrentDeviceLicenseRepository;
		this.countActiveOttRepository = countActiveOttRepository;
		this.countActiveOttByUserRepository = countActiveOttByUserRepository;
		this.getPlanRepository = getPlanRepository;
	}

	@Override
	public void reviewCreateAccount(Account superAccount, Integer numAddition, List<AccountChannelDtoIn> accountChannels) {
		// list polices
		AccountPlan accountPlan = getAccountPlanRepository.get(superAccount.getUsername());
		if (Common.ACCOUNT_PLAN_STATUS.ACTIVE != accountPlan.getStatus()) {
			throw new BadRequestException("Gói cước không hoạt động", ErrorCode.IDG_00000400);
		}
		LimitStatsImpl userLimitStatsImpl = new LimitStatsImpl();
		userLimitStatsImpl.setNumAddition(numAddition);
		userLimitStatsImpl.setTotal(countAccount(superAccount));

		LimitPolicyReviewer limitPolicyReviewer = new LimitPolicyReviewer(accountPlan.getLimitUser() + accountPlan.getAddonUser(),
				userLimitStatsImpl,
				ConstantString.LIMIT_POLICY_TYPE.USER);

		ChainPolicyReviewer chainPolicyReviewer = new ChainPolicyReviewer();
		chainPolicyReviewer.getReviewers().add(limitPolicyReviewer);

		int numOttAddition = accountChannels.stream()
				.filter(ac -> ac.getStatus() == Common.ACCOUNT_CHANNEL_STATUS.ACTIVE
						&& !Common.CHANNEL_CATEGORY.WEBHOOK.equals(ac.getUuidChannelCategory()))
				.mapToInt(AccountChannelDtoIn::getUserOTT)
				.sum();

		if (numOttAddition > 0) {
			LimitStatsImpl ottLimitStatsImpl = new LimitStatsImpl();
			ottLimitStatsImpl.setNumAddition(numOttAddition);
			ottLimitStatsImpl.setTotal(countOtt(superAccount));

			LimitPolicyReviewer limitNumOttPolicyReviewer = new LimitPolicyReviewer(
					accountPlan.getLimitUser() + accountPlan.getAddonUser() + accountPlan.getAddonOtt(),
					ottLimitStatsImpl, ConstantString.LIMIT_POLICY_TYPE.OTT);
			chainPolicyReviewer.getReviewers().add(limitNumOttPolicyReviewer);
		}

		chainPolicyReviewer.review();
	}

	@Override
	public void reviewCreateDeviceLicense(Account superAccount, Integer numAddition) {

		AccountPlan accountPlan = getAccountPlanRepository.get(superAccount.getUsername());
		if (Common.ACCOUNT_PLAN_STATUS.ACTIVE != accountPlan.getStatus()) {
			throw new BadRequestException("Gói cước không hoạt động", ErrorCode.IDG_00000400);
		}

		LimitStatsImpl limitStatsImpl = new LimitStatsImpl();
		limitStatsImpl.setNumAddition(numAddition);
		limitStatsImpl.setTotal(countDeviceLicense(superAccount));

		LimitPolicyReviewer limitPolicyReviewer = new LimitPolicyReviewer(
				accountPlan.getAddonDevice() + accountPlan.getFreeDevice(),
				limitStatsImpl,
				ConstantString.LIMIT_POLICY_TYPE.DEVICE);

		ChainPolicyReviewer chainPolicyReviewer = new ChainPolicyReviewer();
		chainPolicyReviewer.getReviewers().add(limitPolicyReviewer);
		chainPolicyReviewer.review();
	}

	@Override
	public void reviewListHisCheckin(Account superAccount) {
		// list polices
		List<Policy> policies = listPolicy(superAccount);

		PolicyMaker viewCheckinPolicyMaker = new PolicyMakerImpl(ConstantString.POLICYDEF.ENABLE_LIST_HIS_CHECKIN, policies);

		EnablePolicyReviewer enablePolicyReviewer = new EnablePolicyReviewer(viewCheckinPolicyMaker);

		ChainPolicyReviewer chainPolicyReviewer = new ChainPolicyReviewer();
		chainPolicyReviewer.getReviewers().add(enablePolicyReviewer);
		chainPolicyReviewer.review();
	}

	@Override
	public void reviewCreateListAccount(Account superAccount, WebCreateListAccountDtoIn webCreateListAccountDtoIn) {
		AccountPlan accountPlan = getAccountPlanRepository.get(superAccount.getUsername());
		if (Common.ACCOUNT_PLAN_STATUS.ACTIVE != accountPlan.getStatus()) {
			throw new BadRequestException("Gói cước không hoạt động", ErrorCode.IDG_00000400);
		}
		List<Policy> policies = listPolicyRepository.list(accountPlan.getUuidAccountPlan());

		PolicyMaker importExcelPolicyMaker = new PolicyMakerImpl(ConstantString.POLICYDEF.ENABLE_IMPORT_EXCEL, policies);
		EnablePolicyReviewer enablePolicyReviewer = new EnablePolicyReviewer(importExcelPolicyMaker);

		LimitStatsImpl limitStats = new LimitStatsImpl();
		limitStats.setNumAddition(webCreateListAccountDtoIn.getAccounts().size());
		limitStats.setTotal(countAccount(superAccount));

		LimitPolicyReviewer limitPolicyReviewer = new LimitPolicyReviewer(
				accountPlan.getLimitUser() + accountPlan.getAddonUser(),
				limitStats,
				ConstantString.LIMIT_POLICY_TYPE.USER
		);

		ChainPolicyReviewer chainPolicyReviewer = new ChainPolicyReviewer();
		chainPolicyReviewer.getReviewers().add(enablePolicyReviewer);
		chainPolicyReviewer.getReviewers().add(limitPolicyReviewer);

		int numOttAddition = webCreateListAccountDtoIn.getAccounts().stream()
				.flatMap(accountDtoIn -> accountDtoIn.getAccountChannels().stream())
				.filter(ac -> ac.getStatus() == Common.ACCOUNT_CHANNEL_STATUS.ACTIVE
						&& !Common.CHANNEL_CATEGORY.WEBHOOK.equals(ac.getUuidChannelCategory()))
				.mapToInt(AccountChannelDtoIn::getUserOTT)
				.sum();

		if (numOttAddition > 0) {
			LimitStatsImpl ottLimitStats = new LimitStatsImpl();
			ottLimitStats.setNumAddition(webCreateListAccountDtoIn.getAccounts().size());
			ottLimitStats.setTotal(countAccount(superAccount));

			LimitPolicyReviewer limitNumOttPolicyReviewer = new LimitPolicyReviewer(
					accountPlan.getLimitUser() + accountPlan.getAddonUser() + accountPlan.getAddonOtt(),
					ottLimitStats,
					ConstantString.LIMIT_POLICY_TYPE.OTT
			);

			chainPolicyReviewer.getReviewers().add(limitNumOttPolicyReviewer);
		}

		chainPolicyReviewer.review();
	}

	@Override
	public void reviewUpdateDeviceLicense(Account superAccount, DeviceLicense deviceLicense, UpdateDeviceLicenseDtoIn updateDeviceLicenseDtoIn) {
		List<Policy> policies = listPolicy(superAccount);

		ChainPolicyReviewer chainPolicyReviewer = new ChainPolicyReviewer();

		if (updateDeviceLicenseDtoIn.getFaceMask() == Common.DEVICE_LICENSE_FACE_MASK.ENABLE) {
			PolicyMaker maskPolicyMaker = new PolicyMakerImpl(ConstantString.POLICYDEF.ENABLE_FACE_MASK, policies);

			EnablePolicyReviewer maskPolicyReviewer = new EnablePolicyReviewer(maskPolicyMaker);

			chainPolicyReviewer.getReviewers().add(maskPolicyReviewer);
		}

		if (updateDeviceLicenseDtoIn.getFaceLiveness() == Common.DEVICE_LICENSE_FACE_LIVENESS.ENABLE) {
			PolicyMaker livenessPolicyMaker = new PolicyMakerImpl(ConstantString.POLICYDEF.ENABLE_FACE_LIVENESS, policies);

			EnablePolicyReviewer livenessPolicyReviewer = new EnablePolicyReviewer(livenessPolicyMaker);

			chainPolicyReviewer.getReviewers().add(livenessPolicyReviewer);
		}

		if (!updateDeviceLicenseDtoIn.getFaceProb().equals(deviceLicense.getFaceProb())) {
			PolicyMaker probPolicyMaker = new PolicyMakerImpl(ConstantString.POLICYDEF.ENABLE_FACE_PROB, policies);

			EnablePolicyReviewer probPolicyReviewer = new EnablePolicyReviewer(probPolicyMaker);

			chainPolicyReviewer.getReviewers().add(probPolicyReviewer);
		}

		if (updateDeviceLicenseDtoIn.getVirtualAssistant() == Common.DEVICE_LICENSE_VIRTUAL_ASSISTANT.ENABLE) {
			PolicyMaker probPolicyMaker = new PolicyMakerImpl(ConstantString.POLICYDEF.ENABLE_VIRTUAL_ASSISTANT, policies);

			EnablePolicyReviewer probPolicyReviewer = new EnablePolicyReviewer(probPolicyMaker);

			chainPolicyReviewer.getReviewers().add(probPolicyReviewer);
		}

		chainPolicyReviewer.review();
	}

	@Override
	public void reviewUpdateAccount(Account superAccount, Account account, List<AccountChannelDtoIn> accountChannels) {
		AccountPlan accountPlan = getAccountPlanRepository.get(superAccount.getUsername());

		ChainPolicyReviewer chainPolicyReviewer = new ChainPolicyReviewer();

		int numOtt = accountChannels.stream()
				.filter(ac -> ac.getStatus() == Common.ACCOUNT_CHANNEL_STATUS.ACTIVE
						&& !Common.CHANNEL_CATEGORY.WEBHOOK.equals(ac.getUuidChannelCategory()))
				.mapToInt(AccountChannelDtoIn::getUserOTT)
				.sum();
		int activeOtt = countOttByUser(account);

		int numAdditionOtt = numOtt - activeOtt;

		if (numAdditionOtt > 0) {
			LimitStatsImpl ottLimitStatsImpl = new LimitStatsImpl();
			ottLimitStatsImpl.setNumAddition(numAdditionOtt);
			ottLimitStatsImpl.setTotal(countOtt(superAccount));

			LimitPolicyReviewer limitPolicyReviewer = new LimitPolicyReviewer(
					accountPlan.getLimitUser() + accountPlan.getAddonUser() + accountPlan.getAddonOtt(),
					ottLimitStatsImpl,
					ConstantString.LIMIT_POLICY_TYPE.OTT
			);
			chainPolicyReviewer.getReviewers().add(limitPolicyReviewer);
		}

		chainPolicyReviewer.review();
	}

	@Override
	public void reviewActiveDeviceLicense(Account superAccount, DeviceLicense deviceLicense) {

		ChainPolicyReviewer chainPolicyReviewer = new ChainPolicyReviewer();

		if (Common.DEVICE_LICENSE_STATUS.INITIAL == deviceLicense.getStatus()) {
			AccountPlan accountPlan = getAccountPlanRepository.get(superAccount.getUsername());

			LimitStatsImpl limitStatsImpl = new LimitStatsImpl();
			limitStatsImpl.setNumAddition(1);
			limitStatsImpl.setTotal(countDeviceLicense(superAccount));

			LimitPolicyReviewer limitPolicyReviewer = new LimitPolicyReviewer(
					accountPlan.getAddonDevice() + accountPlan.getFreeDevice(),
					limitStatsImpl,
					ConstantString.LIMIT_POLICY_TYPE.DEVICE);

			chainPolicyReviewer.getReviewers().add(limitPolicyReviewer);
		}

		chainPolicyReviewer.review();
	}

	@Override
	public void reviewExternalCreateAccount(Account superAccount, Integer numAddition) {
		AccountPlan accountPlan = getAccountPlanRepository.get(superAccount.getUsername());

		LimitStatsImpl userLimitStatsImpl = new LimitStatsImpl();
		userLimitStatsImpl.setNumAddition(numAddition);
		userLimitStatsImpl.setTotal(countAccount(superAccount));

		LimitPolicyReviewer limitPolicyReviewer = new LimitPolicyReviewer(accountPlan.getLimitUser() + accountPlan.getAddonUser(),
				userLimitStatsImpl,
				ConstantString.LIMIT_POLICY_TYPE.USER
		);

		ChainPolicyReviewer chainPolicyReviewer = new ChainPolicyReviewer();
		chainPolicyReviewer.getReviewers().add(limitPolicyReviewer);

		chainPolicyReviewer.review();
	}

	@Override
	public void reviewExternalRegisterOrExtendAccountPlan(Account superAccount, Integer numAccount, Integer numDevice, Integer numOtt) {
		Integer currentAccount = countAccount(superAccount);
		Integer currentDevice = countDeviceLicense(superAccount);
		Integer currentOtt = countOtt(superAccount);

		if (numAccount < currentAccount || numDevice < currentDevice || numOtt + numAccount < currentOtt) {
			throw new BadRequestException("vui lòng chọn gói cước lớn hơn");
		}
	}

	public void reviewPortalRegisterPlan(Account superAccount, Integer numUser, Integer addonDevice, Integer addonOtt, Integer addonUser) {
		// verify account can register pro plan
		AccountPlan accountPlan = getAccountPlanRepository.get(superAccount.getUsername());
		Plan plan = getPlanRepository.get(accountPlan.getUuidPlan());
		if (Common.PLAN_TYPE.ENTERPRISE == plan.getType()) {
			throw new BadRequestException("not allowed");
		}

		// check so luong user, ott, device
		Integer currentUser = countAccount(superAccount);
		Integer currentOtt = countOtt(superAccount);
		Integer currentDevice = countDeviceLicense(superAccount);

		if (currentUser > numUser + addonUser) {
			throw new BadRequestException("numUser must larger than num current user", ErrorCode.IDG_00002023);
		}

		if (currentDevice > addonDevice + Common.PLAN_PRO.FREE_DEVICE) {
			throw new BadRequestException("numDevice must larger than num current device", ErrorCode.IDG_00002024);
		}

		if (currentOtt > numUser + addonUser + addonOtt) {
			throw new BadRequestException("numOtt must larger than num current ott", ErrorCode.IDG_00002025);
		}

	}

	private List<Policy> listPolicy(Account superAccount) {
		AccountPlan accountPlan = getAccountPlanRepository.get(superAccount.getUsername());

		List<Policy> policies = listPolicyRepository.list(accountPlan.getUuidAccountPlan());
		return policies;
	}

	private Integer countAccount(Account superAccount) {
		return countCurrentAccountRepository.count(superAccount.getUuidAccount());
	}

	private Integer countDeviceLicense(Account superAccount) {
		return countCurrentDeviceLicenseRepository.count(superAccount.getUuidAccount());
	}

	private Integer countOtt(Account superAccount) {
		return countActiveOttRepository.count(superAccount.getUuidAccount());
	}

	private Integer countOttByUser(Account account) {
		return countActiveOttByUserRepository.count(account.getUuidAccount());
	}
}
