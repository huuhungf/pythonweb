package vn.vnpt.policy.config;

import lombok.Data;

@Data
public class EnableConfig {
	private Integer enable;
}
