package vn.vnpt.policy.config;

import lombok.Data;

@Data
public class LimitDeviceConfig {
	private Integer quota;
	private Integer free;
}
