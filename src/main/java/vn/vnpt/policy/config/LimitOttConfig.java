package vn.vnpt.policy.config;

import lombok.Data;

@Data
public class LimitOttConfig {
	private Integer quota;
}
