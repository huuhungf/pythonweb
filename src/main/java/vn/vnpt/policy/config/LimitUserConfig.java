package vn.vnpt.policy.config;

import lombok.Data;

@Data
public class LimitUserConfig {
	private Integer quota;
}
