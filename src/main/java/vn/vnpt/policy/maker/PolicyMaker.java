package vn.vnpt.policy.maker;

import vn.vnpt.api.model.Policy;

public interface PolicyMaker {
	Policy make();
}
