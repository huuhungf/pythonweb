package vn.vnpt.policy.maker;

import lombok.Data;
import vn.vnpt.api.model.Policy;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class PolicyMakerImpl implements PolicyMaker {
	private String type;
	private List<Policy> policies;

	public PolicyMakerImpl(String type, List<Policy> policies) {
		this.type = type;
		this.policies = policies;
	}

	@Override
	public Policy make() {
		List<Policy> limitPolices = policies.stream()
				.filter(p -> p.getUuidPolicyDef()
						.equals(type))
				.collect(Collectors.toList());
		if (limitPolices.isEmpty()) {
			return null;
		}
		return limitPolices.get(0);
	}
}
