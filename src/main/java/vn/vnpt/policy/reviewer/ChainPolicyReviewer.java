package vn.vnpt.policy.reviewer;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChainPolicyReviewer implements PolicyReviewer {
	private List<PolicyReviewer> reviewers = new ArrayList<>();

	@Override
	public void review() {
		for (PolicyReviewer reviewer : reviewers) {
			reviewer.review();
		}
	}
}
