package vn.vnpt.policy.reviewer;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import vn.vnpt.api.model.Policy;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.policy.config.EnableConfig;
import vn.vnpt.policy.maker.PolicyMaker;

@Data
public class EnablePolicyReviewer implements PolicyReviewer {
	private final ObjectMapper objectMapper = new ObjectMapper();

	private PolicyMaker policyMaker;

	public EnablePolicyReviewer(PolicyMaker policyMaker) {
		this.policyMaker = policyMaker;
	}

	@Override
	public void review() {
		Policy policy = policyMaker.make();
		if (policy == null) {
			return;
		}

		EnableConfig enableConfig = Common.parsePolicyConfig(objectMapper, policy, EnableConfig.class);

		if (enableConfig.getEnable() != ConstantString.ONE) {
			String errorCode = ErrorCode.IDG_00000400;
			switch (policy.getUuidPolicyDef()) {
				case ConstantString.POLICYDEF.ENABLE_FACE_MASK:
					errorCode = ErrorCode.IDG_00002017;
					break;
				case ConstantString.POLICYDEF.ENABLE_FACE_LIVENESS:
					errorCode = ErrorCode.IDG_00002018;
					break;
				case ConstantString.POLICYDEF.ENABLE_FACE_PROB:
					errorCode = ErrorCode.IDG_00002019;
					break;
				case ConstantString.POLICYDEF.ENABLE_IMPORT_EXCEL:
					errorCode = ErrorCode.IDG_00002020;
					break;
				case ConstantString.POLICYDEF.ENABLE_LIST_HIS_CHECKIN:
					errorCode = ErrorCode.IDG_00002021;
					break;
				default:
					errorCode = ErrorCode.IDG_00000400;
			}
			throw new BadRequestException("feature is not enabled", errorCode);
		}
	}
}
