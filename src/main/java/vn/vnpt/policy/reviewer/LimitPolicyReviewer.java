package vn.vnpt.policy.reviewer;

import lombok.Data;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.errorcode.ErrorCode;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.policy.stats.LimitStats;

@Data
public class LimitPolicyReviewer implements PolicyReviewer {

	private LimitStats limitStats;
	private Integer quota;
	private String type;

	public LimitPolicyReviewer(Integer quota, LimitStats limitStats, String type) {
		this.quota = quota;
		this.limitStats = limitStats;
		this.type = type;
	}

	@Override
	public void review() {
		if (quota < limitStats.total() + limitStats.additionNumber()) {

			switch (type) {
				case ConstantString.LIMIT_POLICY_TYPE.USER:
					throw new BadRequestException(String.format("exceed quota user %s", quota),
							ErrorCode.IDG_00002014);
				case ConstantString.LIMIT_POLICY_TYPE.DEVICE:
					throw new BadRequestException(String.format("exceed quota device license %s",
							quota), ErrorCode.IDG_00002015);
				case ConstantString.LIMIT_POLICY_TYPE.OTT:
					throw new BadRequestException(String.format("exceed quota ott %s", quota),
							ErrorCode.IDG_00002016);
				default:
					throw new RuntimeException(String.format("not support type: %s", type));
			}
		}
	}
}
