package vn.vnpt.policy.stats;

public interface LimitStats {
	Integer total();
	Integer additionNumber();
}
