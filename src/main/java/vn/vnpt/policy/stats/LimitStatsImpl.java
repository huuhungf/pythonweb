package vn.vnpt.policy.stats;

import lombok.Data;

@Data
public class LimitStatsImpl implements LimitStats{
	private Integer numAddition;
	private Integer total;

	@Override
	public Integer total() {
		return total;
	}

	@Override
	public Integer additionNumber() {
		return numAddition;
	}
}
