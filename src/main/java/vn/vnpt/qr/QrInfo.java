package vn.vnpt.qr;

import lombok.Data;

import java.io.Serializable;

/**
 * QrInfo is cached in redis
 */
@Data
public class QrInfo implements Serializable {
	private String token;
	private String uuidAccount;
	private String username;
	private String fullName;
	private String userCode;
	private String imageUrl;
	private String deviceCode;
	private String adminUsername;
}
