package vn.vnpt.qr;

public interface QrService {
	QrInfo getQrInfo(String token);
	void saveQrInfo(QrInfo qrInfo);
	void deleteQrInfo(String token);
}
