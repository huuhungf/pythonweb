package vn.vnpt.qr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import vn.vnpt.qr.config.QrProperties;

import java.util.concurrent.TimeUnit;

@Service
public class QrServiceBean implements QrService {

	private final RedisTemplate<Object, Object> redisTemplate;
	private final QrProperties qrProperties;

	@Autowired
	public QrServiceBean(RedisTemplate<Object, Object> redisTemplate, QrProperties qrProperties) {
		this.redisTemplate = redisTemplate;
		this.qrProperties = qrProperties;
	}

	public void saveQrInfo(QrInfo qrInfo) {
		redisTemplate.opsForValue().set(getRedisKey(qrInfo.getToken()), qrInfo, qrProperties.getTtl().getSeconds(), TimeUnit.SECONDS);
	}

	@Override
	public void deleteQrInfo(String token) {
		redisTemplate.delete(getRedisKey(token));
	}

	public QrInfo getQrInfo(String token) {
		return (QrInfo) redisTemplate.opsForValue().get(getRedisKey(token));
	}

	private String getRedisKey(String token) {
		return qrProperties.getCacheName() + "::" + token;
	}
}
