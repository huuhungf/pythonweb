package vn.vnpt.qr.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@ConfigurationProperties(prefix = "qr")
@Data
public class QrProperties {
	private String cacheName;
	private Duration ttl = Duration.ofMinutes(5);
	private String imageUrl;
}
