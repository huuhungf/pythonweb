package vn.vnpt.shift;

import lombok.Data;
import vn.vnpt.api.model.HisCheckin;
import vn.vnpt.api.model.Schedule;
import vn.vnpt.api.model.Shift;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Data
public class WorkingDayReport {
	private String uuidAccount;
	private String date;
	private Integer dayId;
	private Schedule schedule;
	private List<HisCheckin> hisCheckins;
	private List<Shift> shifts;
	private boolean isCheckIn; // true: đã checkin || false: chưa checkin
	private boolean isWorkingFullTime = true; // true: làm việc đủ thời gian, false: không đủ thời gian
	private Map<Shift, WorkingShiftStats> wsStats = new HashMap<>();

	public void calculateStats() {
		for (Shift shift : shifts) {
			calculateForShift(shift);
		}
	}

	private void calculateForShift(Shift shift) {
		// Tinh gio duoc phep diem danh vao theo giay
		long onDuty = Common.timeToTsInSec(date, ConstantString.DDMMYYYY) + Common.getTimeInSec(shift.getOnDuty());
		long onTimeIn = onDuty - shift.getOnTimeIn() * ConstantString.SECONDS_IN_A_MIN;
		long cutIn = onDuty + shift.getCutIn() * ConstantString.SECONDS_IN_A_MIN;

		long offDuty = Common.timeToTsInSec(date, ConstantString.DDMMYYYY) + Common.getTimeInSec(shift.getOffDuty())
				+ shift.getDayCount() * ConstantString.SECONDS_IN_A_DAY;
		long onTimeOut = offDuty - shift.getOnTimeOut() * ConstantString.SECONDS_IN_A_MIN;
		long cutOut = offDuty + shift.getCutOut() * ConstantString.SECONDS_IN_A_MIN;

		WorkingShiftStats stats = wsStats.getOrDefault(shift, new WorkingShiftStats());
		// Trường hợp có shift nhưng lịch sử checkin rỗng => Có ca làm việc nhưng ko có dữ liệu chấm công (2)
		isCheckIn = hisCheckins
				.parallelStream()
				.anyMatch(x -> Objects.equals(Common.convertDateToString(x.getDateCheckin(), ConstantString.DDMMYYYY), date));

		for (HisCheckin hc : hisCheckins) {
			long checkinTs = Common.timeToTsInSec(hc.getDateCheckin());
			// Neu thoi gian diem danh nam giua khoang diem danh thi ghi nhan
			// Chi ghi nhan lan som nhat
			if (onTimeIn <= checkinTs
					&& checkinTs <= cutIn) {
				if (!stats.getIsCheckin()) {
					stats.setIsCheckin(true);
					stats.setCheckinTs(checkinTs);
					stats.setTimeCheckin(Common.convertDateToString(hc.getDateCheckin(), ConstantString.HHMMSS));
				}
			}

			if (onTimeOut <= checkinTs
					&& checkinTs <= cutOut) {
				stats.setIsCheckOut(true);
				stats.setCheckoutTs(checkinTs);
				stats.setTimeCheckout(Common.convertDateToString(hc.getDateCheckin(), ConstantString.HHMMSS));
			}
		}

		// Neu co ca diem danh vao va diem danh ra thi ghi nhan cong
		Long lateTime = 0L;
		Long earlyTime = 0L;
		if (stats.getIsCheckin()) {
			lateTime = calLateTime(stats.getCheckinTs(), onDuty, shift);
		}
		if (stats.getIsCheckOut()) {
			earlyTime = calEarlyTime(stats.getCheckoutTs(), offDuty, shift);
		}
		if (stats.getIsCheckin() && stats.getIsCheckOut()) {
			Long workingTime = Long.valueOf(shift.getWorkingTime());
			workingTime = workingTime - lateTime - earlyTime;
			if (workingTime < 0) {
				workingTime = 0L;
			}

			stats.setWorkingDay(shift.getWorkingDay());
			stats.setWorkingTime(workingTime);
		}
		stats.setLateTime(lateTime);
		stats.setEarlyTime(earlyTime);
		if (Long.valueOf(shift.getWorkingTime()) > stats.getWorkingTime()) {
			isWorkingFullTime = false;
		}

		wsStats.put(shift, stats);
	}

	private long calLateTime(long checkinTs, long onDuty, Shift shift) {
		// Neu khong ap dung tinh di muon
		if (shift.getIsLate() == ConstantString.ZERO) {
			return 0;
		}

		// Neu co ap dung tinh di muon
		int lateGrace = shift.getLateGrace() * ConstantString.SECONDS_IN_A_MIN;
		long lateIn = onDuty + lateGrace;

		long lateTime = checkinTs - lateIn;
		// Khong di muon
		if (lateTime <= 0) {
			return 0;
		}

		// Co di muon
		// Co tinh thoi gian cho phep di muon vao thoi gian di muon
		if (shift.getIsLateGrace() == ConstantString.ZERO) {
			lateTime += lateGrace;
		}
		lateTime = roundTime(lateTime, shift.getRoundTypeLate(), shift.getRoundStepLate());
		return lateTime;
	}

	private long calEarlyTime(long checkoutTs, long offDuty, Shift shift) {
		// Neu khong ap dung tinh ve som
		if (shift.getIsEarly() == ConstantString.ZERO) {
			return 0;
		}

		// Neu co ap dung tinh ve som
		int earlyGrace = shift.getEarlyGrace() * ConstantString.SECONDS_IN_A_MIN;
		long earlyOut = offDuty - earlyGrace;

		long earlyTime = earlyOut - checkoutTs;
		// Khong ve som
		if (earlyTime <= 0) {
			return 0;
		}

		// Co ve som
		// Co tinh thoi gian cho phep ve som vao thoi gian ve som
		if (shift.getIsEarlyGrace() == ConstantString.ZERO) {
			earlyTime += earlyGrace;
		}
		earlyTime = roundTime(earlyTime, shift.getRoundTypeEarly(), shift.getRoundStepEarly());
		return earlyTime;
	}

	/**
	 * @param sec:       time in seconds
	 * @param roundType: 1: lam tron len, 2: lam tron xuong
	 * @param roundStep: muc lam tron theo phut
	 * @return so phut duoc lam tron
	 */
	private long roundTime(long sec, int roundType, int roundStep) {
		if (roundStep == 0) {
			roundStep = 1;
		}
		long res = sec / (roundStep * ConstantString.SECONDS_IN_A_MIN);
		long remain = sec % (roundStep * ConstantString.SECONDS_IN_A_MIN);
		if (remain > 0 && roundType == Common.ROUND_TYPE.UP) {
			res += roundStep;
		}
		return res;
	}
}
