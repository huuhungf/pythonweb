package vn.vnpt.shift;

import org.springframework.stereotype.Service;
import vn.vnpt.api.model.*;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WorkingDayReporter {

	public List<WorkingDayReport> report(String uuidAccount, String fromDate, String toDate,
										 Map<String, Schedule> scheduleMap, List<HisCheckin> checkinList,
										 String uuidSchedule, Map<String, List<Shift>> tmpShifts) {

		Schedule schedule = scheduleMap.get(uuidSchedule);
		Date fromD;
		Date toD;
		try {
			fromD = Common.convertStringToDate(fromDate, ConstantString.DDMMYYYY);
			toD = Common.convertStringToDate(toDate, ConstantString.DDMMYYYY);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("could not parse date: %s, %s", fromDate, toDate));
		}
		Date date = fromD;
		List<String> dateList = new ArrayList<>();

		while (!date.after(toD)) {
			dateList.add(Common.convertDateToString(date, ConstantString.DDMMYYYY));
			date = Common.addDate(date, 1);
		}

		List<WorkingDayReport> reports = dateList.parallelStream().map(dateStr -> {
			WorkingDayReport wdr = report(uuidAccount, checkinList,
					dateStr, schedule, tmpShifts);
			return wdr;
		}).collect(Collectors.toList());
		return reports;
	}

	private WorkingDayReport report(String uuidAccount, List<HisCheckin> hisCheckins, String date, Schedule schedule, Map<String, List<Shift>> tmpShifts) {
		Integer dayOfWeek = Common.getDayOfWeek(date, ConstantString.DDMMYYYY);

		Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		List<Shift> shifts = tmpShifts.getOrDefault(date, new ArrayList<>());
		if (shifts.isEmpty()) {
			// Get shifts with this date
			if (Common.SCHEDULE_CYCLE_MODE.WEEK == schedule.getCycleMode()) {
				List<WSchedule> wss = schedule.getWeekSchedules().stream()
						.filter(ws -> dayOfWeek.equals(ws.getDayId()))
						.collect(Collectors.toList());
				shifts = wss.stream().map(WSchedule::getShift).collect(Collectors.toList());
			} else if (Common.SCHEDULE_CYCLE_MODE.MONTH == schedule.getCycleMode()) {
				Integer dayOfMonth = Common.getDayOfMonth(date, ConstantString.DDMMYYYY);
				List<MSchedule> mss = schedule.getMonthSchedules().stream()
						.filter(ms -> dayOfMonth.equals(ms.getDayId()))
						.collect(Collectors.toList());
				shifts = mss.stream().map(MSchedule::getShift).collect(Collectors.toList());
			} else if (Common.SCHEDULE_CYCLE_MODE.YEAR == schedule.getCycleMode()) {
				Integer month = Common.getMonth(date, ConstantString.DDMMYYYY);
				Integer dayOfMonth = Common.getDayOfMonth(date, ConstantString.DDMMYYYY);
				List<YSchedule> yss = schedule.getYearSchedules().stream()
						.filter(ys -> month.equals(ys.getMonthId()) && dayOfMonth.equals(ys.getDayId()))
						.collect(Collectors.toList());
				shifts = yss.stream().map(YSchedule::getShift).collect(Collectors.toList());
			}
		}

		List<HisCheckin> checkinsInDate = hisCheckins.stream().filter(
				hc -> date.equals(Common.convertDateToString(hc.getDateCheckin(), ConstantString.DDMMYYYY))
						|| date.equals(Common.convertDateToString(Common.addDate(hc.getDateCheckin(), 1), ConstantString.DDMMYYYY))
						|| date.equals(Common.convertDateToString(Common.addDate(hc.getDateCheckin(), -1), ConstantString.DDMMYYYY))
		).collect(Collectors.toList());

		WorkingDayReport wdr = new WorkingDayReport();
		wdr.setUuidAccount(uuidAccount);
		wdr.setDate(date);
		wdr.setDayId(dayOfWeek);
		wdr.setSchedule(schedule);
		wdr.setHisCheckins(checkinsInDate);
		wdr.setShifts(shifts);
		wdr.calculateStats();

		return wdr;
	}
}
