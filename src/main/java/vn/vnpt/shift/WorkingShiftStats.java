package vn.vnpt.shift;

import lombok.Data;

@Data
public class WorkingShiftStats {
	private Long checkinTs;
	private String timeCheckin;
	private Long checkoutTs;
	private String timeCheckout;
	private Float workingDay = 0.0f;
	private Long workingTime = 0L;
	private Boolean isCheckin = false;
	private Boolean isCheckOut = false;
	private Long lateTime = 0L;
	private Long earlyTime = 0L;
}
