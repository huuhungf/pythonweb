package vn.vnpt.text2speech;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/*
{
	"text": "Xin Chào Đồng Thị Như Quỳnh",
	"text_split": true,
	"model": "news"
}
 */
@Data
public class AddTextDtoIn {
	private String text;

	@JsonProperty("text_split")
	private Boolean textSplit = false;

	private String model = "news";

	private String region = "female_north";
}
