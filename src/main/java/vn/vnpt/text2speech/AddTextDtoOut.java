package vn.vnpt.text2speech;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/*
{
    "code": "success",
    "text_id": "0afa2691a4f1675d0cbc69af74f9ca8c",
    "version": "1.0.0"
}
 */
@Data
public class AddTextDtoOut {
	private String code;

	@JsonProperty("text_id")
	private String textId;

	private String version;
}
