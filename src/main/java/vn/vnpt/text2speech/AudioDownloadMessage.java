package vn.vnpt.text2speech;

import lombok.Data;
import vn.vnpt.api.model.DeviceLicense;

import java.util.List;

@Data
public class AudioDownloadMessage {
	private String updater;
	private String uuidAccount;
	private String fullName;
	private String text;

	private String bucketName;
	private String subFolder;

	private String oldAudioUrl;

	private List<DeviceLicense> deviceLicenses;
}
