package vn.vnpt.text2speech;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/*
{
	"text_id": "0afa2691a4f1675d0cbc69af74f9ca8c"
}
 */
@Data
public class CheckStatusDtoIn {
	@JsonProperty("text_id")
	private String textId;
}
