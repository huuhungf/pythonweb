package vn.vnpt.text2speech;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/*
{
    "code": "success",
    "playlist": [
        {
            "audio_link": "https://storage-cic.vnpt.vn/text-speech/20201204/18372f7b5ca340fd92fb896d6745fe5c.wav",
            "idx": "1",
            "text": "Xin Chào Đồng Thị Như Quỳnh",
            "text_len": 27,
            "total": "1"
        }
    ],
    "text_id": "0afa2691a4f1675d0cbc69af74f9ca8c",
    "version": "1.0.0"
}
 */
@Data
public class CheckStatusDtoOut {
	private String code;

	@JsonProperty("text_id")
	private String textId;

	private String version;
}
