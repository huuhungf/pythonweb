package vn.vnpt.text2speech;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
/*
{
	"text_id": "e6a3f86547acac5c40d6bfe2b2a88003",
	"type": "wav"
}
 */
@Data
public class DownloadDtoIn {
	@JsonProperty("text_id")
	private String textId;

	private String type = "wav";
}
