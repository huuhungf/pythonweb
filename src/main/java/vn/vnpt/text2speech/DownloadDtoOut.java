package vn.vnpt.text2speech;

import lombok.Data;

@Data
public class DownloadDtoOut {
	String contentType;

	private byte[] content;
}
