package vn.vnpt.text2speech;

public interface Text2SpeechService {
	AddTextDtoOut addText(AddTextDtoIn addTextDtoIn);
	CheckStatusDtoOut checkStatus(CheckStatusDtoIn checkStatusDtoIn);
	DownloadDtoOut download(DownloadDtoIn downloadDtoIn);
	DownloadDtoOut addTextAndDownload(AddTextDtoIn addTextDtoIn);
}
