package vn.vnpt.text2speech;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import vn.vnpt.common.exception.BadRequestException;

import java.util.concurrent.TimeUnit;

@Service
public class Text2SpeechServiceBean implements Text2SpeechService {

	private static final Logger logger = LogManager.getLogger(Text2SpeechServiceBean.class);

	@Value(value = "${text2speech.url}")
	private String baseUrl;

	private final RestTemplate restTemplate;

	@Autowired
	public Text2SpeechServiceBean(@Qualifier(value = "vanillaRestTemplate") RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public AddTextDtoOut addText(AddTextDtoIn addTextDtoIn) {
		logger.info("--------TEXT2SPEECH START ADD TEXT------: " + addTextDtoIn);
		String url = UriComponentsBuilder.fromHttpUrl(baseUrl).toUriString();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<AddTextDtoIn> request = new HttpEntity<>(addTextDtoIn, headers);

		try {
			AddTextDtoOut addTextDtoOut = restTemplate.exchange(url, HttpMethod.POST, request, AddTextDtoOut.class).getBody();
			logger.info("--------TEXT2SPEECH SUCCESS ADD TEXT------: " + addTextDtoOut);
			return addTextDtoOut;
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpClientErrorException httpE = (HttpClientErrorException) e;
				if (httpE.getStatusCode().is4xxClientError()) {
					throw new BadRequestException("bad request");
				}
			}
			throw e;
		}
	}

	/*
		/check-status
	 */
	@Override
	public CheckStatusDtoOut checkStatus(CheckStatusDtoIn checkStatusDtoIn) {
		logger.info("--------TEXT2SPEECH START CHECK STATUS------: " + checkStatusDtoIn);
		String url = UriComponentsBuilder.fromHttpUrl(baseUrl).path("/check-status").toUriString();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<CheckStatusDtoIn> request = new HttpEntity<>(checkStatusDtoIn, headers);
		try {
			CheckStatusDtoOut checkStatusDtoOut = restTemplate.exchange(url, HttpMethod.POST, request, CheckStatusDtoOut.class).getBody();
			int retry = 0;
			while (checkStatusDtoOut != null && "pending".equalsIgnoreCase(checkStatusDtoOut.getCode()) && retry < 2) {
				retry++;
				try {
					TimeUnit.MILLISECONDS.sleep(200);
					checkStatusDtoOut = restTemplate.exchange(url, HttpMethod.POST, request, CheckStatusDtoOut.class).getBody();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			logger.info("--------TEXT2SPEECH SUCCESS CHECK STATUS------: " + checkStatusDtoOut);
			return checkStatusDtoOut;
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpClientErrorException httpE = (HttpClientErrorException) e;
				if (httpE.getStatusCode().is4xxClientError()) {
					throw new BadRequestException("bad request");
				}
			}
			throw e;
		}
	}

	/*
		/download
	 */
	@Override
	public DownloadDtoOut download(DownloadDtoIn downloadDtoIn) {
		logger.info("--------TEXT2SPEECH START DOWNLOAD------: " + downloadDtoIn);
		String url = UriComponentsBuilder.fromHttpUrl(baseUrl).path("/download").toUriString();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<DownloadDtoIn> request = new HttpEntity<>(downloadDtoIn, headers);
		try {
			ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.POST, request, byte[].class);
			DownloadDtoOut downloadDtoOut = new DownloadDtoOut();
			String contentType = "audio/wav";
			if (response.getHeaders().getContentType() != null) {
				contentType = response.getHeaders().getContentType().toString();
			}
			downloadDtoOut.setContentType(contentType);
			downloadDtoOut.setContent(response.getBody());
			logger.info("--------TEXT2SPEECH SUCCESS DOWNLOAD------: " + downloadDtoIn);
			return downloadDtoOut;
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpClientErrorException httpE = (HttpClientErrorException) e;
				if (httpE.getStatusCode().is4xxClientError()) {
					throw new BadRequestException("bad request");
				}
			}
			throw e;
		}
	}

	@Override
	public DownloadDtoOut addTextAndDownload(AddTextDtoIn addTextDtoIn) {
		try {
			AddTextDtoOut addTextDtoOut = addText(addTextDtoIn);

			if (!addTextDtoOut.getCode().equals("success")) {
				return null;
			}

			CheckStatusDtoIn checkStatusDtoIn = new CheckStatusDtoIn();
			checkStatusDtoIn.setTextId(addTextDtoOut.getTextId());
			CheckStatusDtoOut checkStatusDtoOut = checkStatus(checkStatusDtoIn);
			if (!checkStatusDtoOut.getCode().equals("success")) {
				return null;
			}

			DownloadDtoIn downloadDtoIn = new DownloadDtoIn();
			downloadDtoIn.setTextId(addTextDtoOut.getTextId());
			return download(downloadDtoIn);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
