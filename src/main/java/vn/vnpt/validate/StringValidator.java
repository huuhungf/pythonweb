package vn.vnpt.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class StringValidator implements ConstraintValidator<vn.vnpt.validate.ValidateString, String> {

	List<String> valueList = null;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return valueList.contains(value.toUpperCase());
	}

	@Override
	public void initialize(vn.vnpt.validate.ValidateString constraintAnnotation) {
		valueList = new ArrayList<String>();
		Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClazz();

		@SuppressWarnings("rawtypes")
		Enum[] enumValArr = enumClass.getEnumConstants();

		for (@SuppressWarnings("rawtypes")
				Enum enumVal : enumValArr) {
			valueList.add(enumVal.toString().toUpperCase());
		}

	}

}
