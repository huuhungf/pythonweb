package vn.vnpt.validate;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = StringValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@ReportAsSingleViolation
public @interface ValidateString {

	Class<? extends Enum<?>> enumClazz();

	String message() default "IDG-00000090";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
