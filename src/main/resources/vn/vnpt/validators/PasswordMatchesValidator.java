package vn.vnpt.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import vn.vnpt.api.dto.in.AccountDtoIn;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

	@Override
	public void initialize(PasswordMatches constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		final AccountDtoIn accountDtoIn = (AccountDtoIn) value;
		return accountDtoIn.getPassword().equals(accountDtoIn.getPassword());

	}
}
