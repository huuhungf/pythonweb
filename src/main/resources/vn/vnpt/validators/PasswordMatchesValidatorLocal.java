//package vn.vnpt.validators;
//
//import javax.validation.ConstraintValidator;
//import javax.validation.ConstraintValidatorContext;
//
//
//
//public class PasswordMatchesValidatorLocal implements ConstraintValidator<PasswordMatches, Object> {
//
//    @Override
//    public void initialize(final PasswordMatches constraintAnnotation) {
//        //
//    }
//
//    @Override
//    public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
//        final AccountPasswordDTO user = (AccountPasswordDTO) obj;
//        return user.getNewPassword().equals(user.getMatchingPassword());
//    }
//
//}