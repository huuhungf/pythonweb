//package onnroute.com.settings.validators;
//
//import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
//import static java.lang.annotation.ElementType.FIELD;
//import static java.lang.annotation.ElementType.TYPE;
//import static java.lang.annotation.RetentionPolicy.RUNTIME;
//
//import java.lang.annotation.Documented;
//import java.lang.annotation.Retention;
//import java.lang.annotation.Target;
//
//import javax.validation.Constraint;
//import javax.validation.Payload;
//
//import org.springframework.beans.factory.annotation.Value;
//
//@Target({ TYPE, FIELD, ANNOTATION_TYPE })
//@Retention(RUNTIME)
//@Constraint(validatedBy = PasswordSameDBvalidator.class)
//@Documented
//public @interface PasswordSameDB {
//
//	String message();
//
//    Class<?>[] groups() default {};
//
//    Class<? extends Payload>[] payload() default {};
//}
