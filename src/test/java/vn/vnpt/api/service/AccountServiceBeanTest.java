package vn.vnpt.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.vnpt.api.model.Group;
import vn.vnpt.api.repository.group.ListGroupByAccountRepository;
import vn.vnpt.api.repository.group.ListRemovedGroupByAccountRepository;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountServiceBeanTest {
	@Autowired
	private ListGroupByAccountRepository listGroupByAccountRepository;

	@Autowired
	private ListRemovedGroupByAccountRepository listRemovedGroupByAccountRepository;

	@Test
	public void test() {
		String uuidAccount = "b9e209fd-a3dd-c6f4-e053-6c1b9f0a8fee";
		List<Group> groups = listGroupByAccountRepository.list(uuidAccount);
		groups.addAll(listRemovedGroupByAccountRepository.list(uuidAccount));
		List<String> uuidDevices = groups.stream()
				.filter(group -> group.getUuidDevice() != null)
				.map(Group::getUuidDevice)
				.collect(Collectors.toList());
		System.out.println(uuidDevices);
	}
}