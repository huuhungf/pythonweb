package vn.vnpt.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import vn.vnpt.api.dto.in.MessageNotification;
import vn.vnpt.common.ConstantsString;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
public class KafkaServiceBeanTest {

	@Autowired
	private KafkaTemplate<String, MessageNotification> kafkaTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void sendKafka() throws InterruptedException {
		Map<String, String> messageProperties = new HashMap<>();
		messageProperties.put("username", "phuongha");
		messageProperties.put("password", "123456");

		String propertiesJson = null;
		try {
			propertiesJson = objectMapper.writeValueAsString(messageProperties);
		} catch (JsonProcessingException e) {
			throw new RuntimeException("could not json message properties: " + messageProperties.entrySet().stream()
					.map(entry -> entry.getKey() + ":" + entry.getValue())
					.collect(Collectors.joining(", ")));
		}

		MessageNotification messageNotification = new MessageNotification();
		messageNotification.setDestEmails(Collections.singletonList("hoangphuongdtvt288@gmail.com"));
		messageNotification.setChannelCode(ConstantsString.EMAIL_CHANNEL_CODE);
		messageNotification.setEmailCategory(ConstantsString.EMAIL_CATEGORY_CREATE_ACCOUNT);
		messageNotification.setMessageProperties(propertiesJson);

		ListenableFuture<SendResult<String, MessageNotification>> future =
				kafkaTemplate.send("topic_notification_dev", messageNotification);
		future.addCallback(new ListenableFutureCallback<SendResult<String, MessageNotification>>() {
			@Override
			public void onFailure(Throwable ex) {
				ex.printStackTrace();
			}

			@Override
			public void onSuccess(SendResult<String, MessageNotification> result) {
				System.out.println(String.format("send success to topic %s with message " +
						result.getProducerRecord().value(), "topic_notification_dev"));
			}
		});


		Thread.sleep(20000);
	}
}