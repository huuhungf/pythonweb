package vn.vnpt.api.service;

import io.minio.MinioClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UploadServiceBeanTest {

	@Autowired
	private UploadService uploadService;

	@Autowired
	private MinioClient minioClient;

	@Test
	public void extractHashFile() {
		System.out.println(uploadService.extractHashFile("https://storage-cic.vnpt.vn/tablet-checkin-dev/afe64241-e531-2d20-e053-d51c000a4575/20200923/IDG01_4c24cba0-fd8c-11ea-8162-39f711852a5d"));
	}

	@Test
	public void deleteFile() {
		uploadService.deleteFile("tablet-checkin-dev/afe64241-e531-2d20-e053-d51c000a4575/20200923/IDG01_4c24cba0-fd8c-11ea-8162-39f711852a5d");
	}

	@Test
	public void copyFile() {
		System.out.println(uploadService.copyFile("tablet-checkin-dev/b085ae7e-be34-2676-e053-d71c000aef61/20201202/FACE_5b4ed50b-3490-11eb-9d24-0fb7a5cb6b18", "tablet-checkin-dev"));
	}

	@Test
	public void presignedPutObject() {
		String url = uploadService.presignedPutObject("tablet-checkin-voice", "test/123");
		System.out.println(url);
	}

	@Test
	public void testExtractHashFile() {
		String hashFile = uploadService.extractHashFileFromUploadLink("https://storage-cic.vnpt.vn/tablet-checkin-dev/test/1234?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=tabletcheckin%2F20210114%2FIDC-NTL%2Fs3%2Faws4_request&X-Amz-Date=20210114T060236Z&X-Amz-Expires=7200&X-Amz-SignedHeaders=host&X-Amz-Signature=df1fca9214da0c8716b13c780fb5ea19f4bc61c04d9eb58c504213b24a631ecf", "");
		System.out.println(hashFile);
	}
}