package vn.vnpt.authentication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AdminLoginServiceTest {

	@Autowired
	private AdminLoginService adminLoginService;

	@Test
	public void login() {
		System.out.println(adminLoginService.login("vietpt.hue@vnpt.vn".toUpperCase()).getValue());
	}
}