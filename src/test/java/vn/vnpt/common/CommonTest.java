package vn.vnpt.common;

import org.junit.Test;
import vn.vnpt.api.dto.in.ShiftDtoIn;
import vn.vnpt.common.constant.ConstantString;
import vn.vnpt.common.exception.BadRequestException;
import vn.vnpt.common.pattern.Patterns;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.Assert.*;

public class CommonTest {

	@Test
	public void generateLicenseKey() {
		System.out.println(Common.generateLicenseKey());
	}

	@Test
	public void test() {
		String[] arr = "dk     abc      abc          ".split("\\s+");
		Arrays.stream(arr).forEach(System.out::println);
	}

	@Test
	public void isMatchPattern() {
		System.out.println(Common.isMatchPattern("dk     abc      abc          ", Patterns.OTT_REGISTER_TEXT));
	}

	@Test
	public void compressImage() throws IOException {
		File input = new File("/home/robert/Downloads/photo_2019-08-22_09-46-04.jpg");
		BufferedImage image = ImageIO.read(input);

		File compressedImageFile = new File("/home/robert/Downloads/compress_20201005_101206.jpg");
		OutputStream os = new FileOutputStream(compressedImageFile);

		Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
		ImageWriter writer = writers.next();

		ImageOutputStream ios = ImageIO.createImageOutputStream(os);
		writer.setOutput(ios);

		ImageWriteParam param = writer.getDefaultWriteParam();

		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionQuality(0.5f);  // Change the quality value you prefer
		writer.write(null, new IIOImage(image, null, null), param);

		os.close();
		ios.close();
		writer.dispose();
	}

	@Test
	public void encryptPassword() {
		System.out.println(Common.encryptPassword("Abc@123456"));
	}

	@Test
	public void testConvertTime() throws ParseException {
		System.out.println(Common.convertStringToDate("04:03", ConstantString.HHMM));
	}

	@Test
	public void validateShiftDtoIn() {
		ShiftDtoIn shiftDtoIn = new ShiftDtoIn();
		shiftDtoIn.setOnTimeIn("22:00");
		shiftDtoIn.setOnDuty("00:30"); // Thời gian bắt đầu làm việc
		shiftDtoIn.setCutIn("5:00");

		shiftDtoIn.setOnTimeOut("04:30");
		shiftDtoIn.setOffDuty("06:00"); // Thời gian kết thúc làm việc
		shiftDtoIn.setCutOut("07:00");

		shiftDtoIn.setDayCount(1);
		shiftDtoIn.setShiftCode("Ca 2");

		Integer onDuty = Common.getTimeInSec(shiftDtoIn.getOnDuty());
		Integer onTimeIn = Common.getTimeInSec(shiftDtoIn.getOnTimeIn());
		Integer cutIn = Common.getTimeInSec(shiftDtoIn.getCutIn()) + shiftDtoIn.getDayCount() * ConstantString.SECONDS_IN_A_DAY;

		Integer offDuty = Common.getTimeInSec(shiftDtoIn.getOffDuty()) + shiftDtoIn.getDayCount() * ConstantString.SECONDS_IN_A_DAY;
		Integer onTimeOut = Common.getTimeInSec(shiftDtoIn.getOnTimeOut()) + shiftDtoIn.getDayCount() * ConstantString.SECONDS_IN_A_DAY;
		Integer cutOut = Common.getTimeInSec(shiftDtoIn.getCutOut()) + shiftDtoIn.getDayCount() * ConstantString.SECONDS_IN_A_DAY;
		if (shiftDtoIn.getDayCount() != 0) {
			if (onDuty <= Common.getTimeInSec(shiftDtoIn.getCutIn())) {
				onDuty += shiftDtoIn.getDayCount() * ConstantString.SECONDS_IN_A_DAY;
			}
			if (offDuty <= Common.getTimeInSec(shiftDtoIn.getCutOut())) {
				offDuty += shiftDtoIn.getDayCount() * ConstantString.SECONDS_IN_A_DAY;
			}
		}
		if (shiftDtoIn.getOnLunch() != null && shiftDtoIn.getOffLunch() != null) {
			Integer onLunch = Common.getTimeInSec(shiftDtoIn.getOnLunch());
			Integer offLunch = Common.getTimeInSec(shiftDtoIn.getOffLunch());
			if (onLunch > offLunch) {
				throw new BadRequestException("TGBĐ ăn trưa < TGKT ăn trưa");
			}
		}

		// Điểm danh giờ vào
		if (onDuty > offDuty) {
			throw new BadRequestException("TGBĐ làm việc < TGKT làm việc");
		}
		if (!(onTimeIn <= onDuty && onDuty <= cutIn)) {
			throw new BadRequestException(String.format("TGBĐ điểm danh vào < TGBĐ làm việc < TGKT điểm danh vào: %s < %s < %s", shiftDtoIn.getOnTimeIn(), shiftDtoIn.getOnDuty(), shiftDtoIn.getCutIn()));
		}

		// Điểm danh giờ ra
		if (!(onTimeOut <= offDuty && offDuty <= cutOut)) {
			throw new BadRequestException(String.format("TGBĐKT điểm danh ra < TGBĐKT làm việc < TGKT điểm danh ra: %s < %s < %s", shiftDtoIn.getOnTimeOut(), shiftDtoIn.getOffDuty(), shiftDtoIn.getCutOut()));
		}

		if (cutIn > onTimeOut) {
			throw new BadRequestException("TGKT điểm danh vào phải < TGBĐ điểm danh ra");
		}

		System.out.println("Ok");
	}
}