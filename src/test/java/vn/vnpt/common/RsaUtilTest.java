package vn.vnpt.common;

import org.junit.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;

import static org.junit.Assert.*;

public class RsaUtilTest {

	@Test
	public void encrypt() throws InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException {
		KeyPair keyPair = RsaUtil.getKeyPair();
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();

		String privateKeyStr = RsaUtil.encodeKey(privateKey);
		assertEquals(privateKey, RsaUtil.decodePrivateKey(privateKeyStr));
		System.out.println("---PRIVATE KEY---: " + privateKeyStr);

		String publicKeyStr = RsaUtil.encodeKey(publicKey);
		assertEquals(publicKey, RsaUtil.decodePublicKey(publicKeyStr));
		System.out.println("---PUBLIC KEY---: " + publicKeyStr);

		String content = "Test Content 123456 @!$*~^{}";
		System.out.println("---CONTENT---: " + content);

		String encoded = RsaUtil.encrypt(content, privateKey);
		System.out.println("---ENCODED---: " + encoded);

		String decoded = RsaUtil.decrypt(encoded, publicKey);
		System.out.println("---DECODED---: " + decoded);

		assertEquals(content, decoded);
	}

	@Test
	public void decrypt() throws InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException {
		PublicKey publicKey = RsaUtil.decodePublicKey("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0GDEOjHXJkPPpAPIle1pROp4UdK1ViK/6VgCeyRC1vi0TwhK0xjm/pIjLIEVbxTr7Ys++FTPl23jxPzvVFsof+e4IukwQMDxGtcc1/cO9+9OmbauJV0UciPc62Ypqem0J4YvLPH+D6Efe/jMmNAyEA4C+hBQCfhvkXKXLxXDiYQIDAQAB");
		String decoded = RsaUtil.decrypt("FK1EK0hHpRitvx8ItWnf6OrG7cujlKCf2FIRGTJJ0xwCrWp/sOLdqZtLHIaUW52KKaMm99uwB3bhJsF7qDuWfcLhofEj2JFJl6uJgcbxSl3m83ZWMGbBU3nbYOgj3dVaAs0K7VDm4pq1DVev/s4UMSo8fNDo/1kJJUP8QtpaCBE=",
				publicKey);
		System.out.println(decoded);
	}
}