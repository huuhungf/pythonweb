package vn.vnpt.embedding;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EmbeddingServiceBeanTest {

	@Autowired
	private EmbeddingService embeddingService;

	@Test
	public void getEmbedding() {
//		System.out.println(embeddingService.getEmbedding("tablet-checkin-dev/afe64241-e531-2d20-e053-d51c000a4575/20201005/CHECKIN_2b622d3b-06ba-11eb-9974-03ce6d718062"));
		System.out.println(embeddingService.getEmbedding("tablet-checkin-dev/b085ae7e-be34-2676-e053-d71c000aef61/20201015/FACE_8fa699c5-0ebd-11eb-b507-4fc51d9f4a84"));
	}
}