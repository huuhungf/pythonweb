package vn.vnpt.firebase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.vnpt.common.Common;
import vn.vnpt.common.constant.ConstantString;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FirebaseServiceTest {

	@Autowired
	private FirebaseService firebaseService;

	@Test
	public void sendMessage() {
		Map<String, String> data = new HashMap<>();
		data.put("notificationType", Common.NOTIFICATION_TYPE.CHECKIN_BY_FACE);
		data.put("imageUrl", "https://storage-cic.vnpt.vn/tablet-checkin-event/20201023/FACE_424e28e6-14d5-11eb-adac-4ba1539f1c5f");
		data.put("fullName", "Quỳnh");
		data.put("checkinType", "1");
		data.put("dateCheckin", Common.convertDateToString(Common.getCurrentTime(), ConstantString.DDMMYYYYHHMMSS));
		data.put("userCode", "userCode1");

		PushNotificationRequest notificationRequest = new PushNotificationRequest();
		notificationRequest.setInstanceName(ConstantString.FIRE_BASE.INSTANCE_NAME_APP);
		notificationRequest.setWithConfig(true);
		notificationRequest.setData(data);
		// android
//		notificationRequest.setToken("fqDvONouRwy3zDbpaCKfTH:APA91bG-3FTJf0ofNWznGTfH871YZ8MHyM5iAG8wO4A_RpPeYyfxLYPP_XcWpOpcmyHCAUnskvQqXr5y9NzbXm3LXza5dDOs-4kh1ZTQYjDQq40UI-m5CKSJasgP8JSy79UflwPggIex");

		// ios
		notificationRequest.setToken("ctZJuxlHokhEpCGJKIMW1v:APA91bEg7zKoit7NG5ewCtZ5f54UmxGFLwehsdLQjhamY90G5ivGQJW2KRWCKA0GLK6ESVno5oWnokZ7FJle75E5MbFokgFvFVE0j5EXjOw9vg2wJAPUS1Z-Ln2lDq0aGuRgcF120qPC");
		notificationRequest.setTitle("");
		try {
			firebaseService.sendMessage(notificationRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void sendMessageTablet() {
		Map<String, String> data = new HashMap<>();
		data.put("notificationType", Common.NOTIFICATION_TYPE.TABLET_SYNC_ACCOUNT);

		PushNotificationRequest notificationRequest = new PushNotificationRequest();
		notificationRequest.setInstanceName(ConstantString.FIRE_BASE.INSTANCE_NAME_IOS_TABLET);

		// ipad
		notificationRequest.setToken("cDE7yb-_YUjWoDpqGR16Z8:APA91bF_IlGf3mAtymSUnel5ug52fLCDBU5YekQ-M7RDp61BOhePwIoa5cnpBygMLeAVv-6sCL6MIUEcwTmGejQyKB6LrU_jcgrDwfzmcjWyDVwjM6NK9qfZGiHp4Xod8ezB1_BFE4jE");
		try {
			firebaseService.sendMessage(notificationRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}