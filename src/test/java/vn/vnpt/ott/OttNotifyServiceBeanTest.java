package vn.vnpt.ott;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OttNotifyServiceBeanTest {

	@Autowired
	private OttNotifyService ottNotifyService;

	@Test
	public void sendNotifyTelegram() {
		OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
		ottNotifyDtoIn.setBotId("n00p22a5c1-0a03-11eb-8840-0f27d76fa0fc123vn");
		ottNotifyDtoIn.setUserId("320801528");
		ottNotifyDtoIn.setChannel("telegram");

		CardData cardData = CardData.builder()
				.type("image")
				.url("https://storage-cic.vnpt.vn/tablet-checkin-dev/qr_code.png")
				.title(new DefaultTitleBuilder("Hoàng Phương", "IC01_PHUONGHA", "21/10/2020 15:07:25", AccessType.ACCESS_QR))
				.build();

		ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));

		ottNotifyService.sendNotify(ottNotifyDtoIn);
	}

	@Test
	public void sendNotifyZalo() {
		OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
		ottNotifyDtoIn.setBotId("d00c22a5c1-0a03-11eb-8840-0f27d76fa0fc123");
		ottNotifyDtoIn.setUserId("8443511740359226167");
		ottNotifyDtoIn.setChannel("zalo");

		CardData cardData = CardData.builder()
				.type("image")
				.url("https://storage-cic.vnpt.vn/tablet-checkin-dev/qr_code.png")
				.title(new DefaultTitleBuilder("Phuong ha", "123456", "21/10/2020 15:07:25", AccessType.ACCESS_QR))
				.build();

		ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));

		ottNotifyService.sendNotify(ottNotifyDtoIn);
	}

	@Test
	public void sendNotifyViber() {
		OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
		ottNotifyDtoIn.setBotId("d00c22a5c1-0a03-11eb-8840-0f27d76fa0fc123");
		ottNotifyDtoIn.setUserId("ZGSLbhaETiPGhaAPQTTuAQ==");
		ottNotifyDtoIn.setChannel("viber");

		CardData cardData = CardData.builder()
				.type("image")
				.url("https://storage-cic.vnpt.vn/tablet-checkin-dev/qr_code.png")
				.title(new DefaultTitleBuilder("Hoàng Phương", "123456", "21/10/2020 15:07:25", AccessType.ACCESS_QR))
				.build();

		ottNotifyDtoIn.setCardData(Collections.singletonList(cardData));

		ottNotifyService.sendNotify(ottNotifyDtoIn);
	}

	@Test
	public void sendNotifyFaceBook() {
		OttNotifyDtoIn ottNotifyDtoIn = new OttNotifyDtoIn();
		ottNotifyDtoIn.setBotId("d00c22a5c1-0a03-11eb-8840-0f27d76fa0fc123");
		ottNotifyDtoIn.setUserId("4795384020501715");
		ottNotifyDtoIn.setChannel("facebook");

		TitleBuilder titleBuilder = new DefaultTitleBuilder("Phuong ha", "123456", "21/10/2020 15:07:25", AccessType.ACCESS_QR);
		CardData cardData = CardData.builder()
				.type("image")
				.url("https://storage-cic.vnpt.vn/tablet-checkin-dev/qr_code.png")
				.title("ABC")
				.build();

		CardData cardData1 = CardData.builder()
				.type("text")
				.text(titleBuilder.build())
				.build();

		ottNotifyDtoIn.setCardData(Arrays.asList(cardData, cardData1));

		ottNotifyService.sendNotify(ottNotifyDtoIn);
	}
}