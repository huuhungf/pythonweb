package vn.vnpt.text2speech;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class Text2SpeechServiceBeanTest {

	@Autowired
	private Text2SpeechService text2SpeechService;

	@Test
	public void download() {
		DownloadDtoIn downloadDtoIn = new DownloadDtoIn();
		downloadDtoIn.setTextId("e6a3f86547acac5c40d6bfe2b2a88003");
		System.out.println(text2SpeechService.download(downloadDtoIn));
	}

	@Test
	public void addTextAndDownload() {
		AddTextDtoIn addTextDtoIn = new AddTextDtoIn();
		addTextDtoIn.setText("Xin chào Nghiêm Đình Thanh Công");
		System.out.println(text2SpeechService.addTextAndDownload(addTextDtoIn));
	}
}